# Magasin de données

## Schema de la Base de Données et fonctionnement

### Data
Les données brutes sont stockées dans une seule table de la base de données nommée `data`.\
Chaque ligne dans cette table correspond à un système de culture.

La structure de la table `data` correspond donc à la structure des données désirées, comprenant en colonne l'ensemble des variables de requêtage sur l'application. 

### Metadata

Certaines colonnes sont des données internes privées, alors que d'autres correspondent aux variables de requêtage de l'application :
il est donc nécessaire d'avoir une seconde table nous permettant de décrire ces colonnes et exprimer ce qu'il est possible d'en faire :
- variable quantitative ;
- variable qualitative ;
- variable pouvant servir de filtre ;
Les variables qui n'entrent pas au moins l'une de ces catégories sont considérées comme internes à l'application.

La table `Metadata` correspond à ces données de description, sous le format suivant :

|   Colonne    |  Type   |  nullable  | description de la colonne                                                                                           |
|:------------:|:-------:|:----------:|---------------------------------------------------------------------------------------------------------------------|
| id           | uuid    |  not null  | un simple identifiant de ligne                                                                                      |
| columnname   | text    |  not null  | nom de la colonne dans la table `data` correspondant à la variable                                                  |
| displayname  | text    |  not null  | nom à afficher pour la variable dans l'application                                                                  |
| unity        | text    |            | unité de la variable pour affichage dans l'application (ex : `L/ha`)                                                |
| qualitative  | boolean |            | permet de définir si la variable est une données qualitative                                                        |
| quantitative | boolean |            | permet de définir si la variable est une données quantitative                                                       |
| filter       | boolean |            | permet de définir si la variable peut être utilisée en tant que filtre                                              |
| separator    | text    |            | Si la variable est à données multiples, définit le caractère de séparation (null si la variable n'est pas multiple) |
| hidden       | boolean |            | Permet de savoir si la variable nécessite une authentification pour être vue                                        |


C'est donc à partir de cette table `metadata` que l'on récupère la liste des variables pouvant être proposées à l'utilisateur sur l'application.
Elle permet également d'effectuer les recherches et croisements de données de façon convenable (en gérant les cas de multiples valeurs notamment).


## Construction du magasin de données

Le magasin de données en base de données est générée depuis le `document tableur` fourni par l'INRA.
Depuis les données fournies, on génère dans un onglet le tableur qui servira de fichier `CSV` d'import des données dans la table `data` de la base de données.

À titre d'exemple, le [fichier `DEPHYgraph_datastore_example.ods`](DEPHYgraph_datastore_example.ods) est disponible avec des données fictives.

### Génération du fichier CSV d'import

La dernière version en date du document remplit automatiquement l'onglet qui servira à produire le `CSV`.
Ce fichier `CSV` doit être généré avec les paramètres suivant :
- encodage en Unicode
- double point (':') pour séparateur de champs
- double quote (' " ') pour séparateur de texte
- "Enregistrer le contenu de la cellule comme affichée" cochée
- Ne pas mettre entre guillemet toutes les cellules de texte (celà permet de garder les valeurs absentes plutot qu'avoir des valeurs vides en base)


### Création des tables en base de données

Le dernier script en date pour la construction de la base de données se situe dans le sources du projet au chemin suivant : `src/main/resources/migration/20180709-structure-magasin.sql`.

Ce script crée la table `data` avec les colonnes correspondant aux dernières données définies pour le magasin de données, ainsi que la table `metadata`.
Cette dernière est également alimentée avec les méta-données telles que définies lors des derniers échanges avec l'INRA (définition des variables quantitatives, qualitatives, filtres et visibilité).

Ce script s'exécute comme n'importe quel script SQL dans la base de données postgreSQL:\
`
psql -U dephygraphUser -d dephygraph < /path/to/20180709-structure-magasin.sql
`

Avec dans cet exemple une base de données nommées `dephygraph` et un utilisateur `dephygraphUser`.

Il peut également être nécessaire de donner les droits adéquats à l'utilisateur sur la base de données :\
`
grant ALL ON ALL tables in schema public TO dephygraph;
`

### Import des données depuis le CSV

Une fois le fichier `CSV` généré, l'import en base de données se fait via une commande dans l'intéprêteur postgreSQL.\
Pour entrer dans l'interprèteur, il suffit d'entrer la commande `psql -U dephygraphUser -d dephygraph` (ici avec le nom de la base de données et l'utilisateur comme au point précédent).

Une fois dans l'invite de commande de `psql`, l'import se fait par la commande suivante :\
`
\copy data (realized, dephyNn, validation, c101_sector, c102_pz0, c103_campaign,
c104_generalTotalIFT, c202_regionPre2015, c201_administrativeRegion, c203_department,
latitude, longitude, c204_groundType, c205_groundTexture, c206_usefulReserve,
c207_irrigable, c208_managementType, c209_purpose, c210_liveStockUnit, c211_industrialFarming,
c212_farmingType, c213_species, c214_grapeVariety, c215_rootStock, c216_plantingDensity,
c217_plantingYear, c301_groundWorkType, c302_rotationType, c303_mechanicalWeeding,
c304_mechanicalWeedingInterventionFrequency, c305_categoryOfLevers, c401_totalIFT,
c402_biocontrolIFT, c403_herbicideIFT, c404_exceptHerbicideIFT, c405_insecticideIFT,
c406_fungicideIFT, c407_molluscicideIFT, c408_controllerIFT, c409_otherIFT,
c410_biologicalWaysSolution, c501_fertilizationExpenses, c502_seedExpenses,
c503_phytoExpenses, c504_mechanizationExpenses, c505_labourExpenses, c506_outLabourTotalExpenses,
c601_performance, c602_grossProceeds, c603_grossProfit, c604_semiNetMargin,
c701_workingTime, c702_fuelConsumption, c703_irrigationVolume,
c801_fertilizationUnityN, c802_fertilizationMineralUnityN, c803_fertilizationOrganicUnityN,
c804_fertilizationUnityP, c805_fertilizationMineralUnityP, c806_fertilizationOrganicUnityP,
c807_fertilizationUnityK, c808_fertilizationMineralUnityK, c809_fertilizationOrganicUnityK)
FROM '/tmp/Magasin_de_données_DEPHYgraph.csv' DELIMITER ':'  CSV HEADER ;
`

Si le schema de la base n'a pas été modifié par rapport au script de création, il est possible d'importer le `CSV` sans expliciter chaque colonne:\
`
\copy data (realized,dephyNb,validation,c101_sector,c102_pz0,c103_networkyears,c104_campaign,c105_generalTotalIFT,c202_regionPre2015,c201_administrativeRegion,c203_department,latitude,longitude,c204_groundType,c205_groundTexture,c206_usefulReserve,c207_irrigable,c208_managementType,c209_purpose,c210_liveStockUnit,c211_industrialFarming,c212_farmingType,c213_species,c214_grapeVariety,c215_rootStock,c216_plantingDensity,c217_plantingYear,c301_groundWorkType,c302_rotationType,c303_mechanicalWeeding,c304_mechanicalWeedingInterventionFrequency,c305_categoryOfLevers,c401_totalIFT,c402_totalIFTwithTS,c403_biocontrolIFT,c404_IFTTS,c405_herbicideIFT,c406_exceptHerbicideIFT,c407_insecticideIFT,c408_fungicideIFT,c411_otherIFT,c412_biologicalWaysSolution,c501_fertilizationExpenses,c502_seedExpenses,c503_phytoExpenses,c504_mechanizationExpenses,c505_labourExpenses,c506_outLabourTotalExpenses,c602_grossProceeds,c603_grossProfit,c604_semiNetMargin,c701_workingTime,c702_fuelConsumption,c703_irrigationVolume,c801_fertilizationUnityN,c802_fertilizationMineralUnityN,c803_fertilizationOrganicUnityN,c804_fertilizationUnityP,c805_fertilizationMineralUnityP,c806_fertilizationOrganicUnityP,c807_fertilizationUnityK,c808_fertilizationMineralUnityK,c809_fertilizationOrganicUnityK) FROM '/PATH_TO/Magasin_de_donnees_DEPHYgraph_20210609.csv' DELIMITER ':'  CSV HEADER ;
`