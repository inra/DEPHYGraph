import requests

BASE_URL='https://dephygraph-latest.demo.codelutin.com'
#BASE_URL='http://localhost:8084/dephy-graph'
MIN_DATA_REQUIRED=0

def get_quali_distribution(json):
    return requests.post(
        BASE_URL+'/api/v1/statistics/qualitativeDistribution/',
        json=json)

def get_quali_quali_distribution(json):
    return requests.post(
        BASE_URL+'/api/v1/statistics/qualitativeQualitativeDistribution/',
        json=json)

def get_quanti_quali_distribution(json):
    return requests.post(
        BASE_URL+'/api/v1/statistics/quantitativeQualitativeDistribution/',
        json=json)

def get_quanti_quanti_distribution(json):
    return requests.post(
        BASE_URL+'/api/v1/statistics/quantitativeQuantitativeDistribution/',
        json=json)

def get_quanti_distribution(json):
    return requests.post(
        BASE_URL+'/api/v1/statistics/quantitativeDistribution/',
        json=json)

# Exemple de requête pour Y = 'Campagne', filtrée sur le département et le type de conduite avec cohorte
response = get_quali_distribution({
    "columnName":"c104_campaign_dis",
    "filterParams":[
        { "columnName":"c107_departement","values":["17 - Charente-Maritime"] },
        { "columnName":"c110_managementType","values":["Agriculture biologique"] }
    ],
    "filterCohort":{ "years":[2018,2019,2020,2021],"networkAges":[] },
    "minDataRequired": MIN_DATA_REQUIRED
})
print(response)
print('Effectif total: ', response.json()['effectifTotal'])

# Exemple de requête pour Y = 'Campagne' et X = 'Type de travail au sol', filtrée sur le type de conduite avec cohorte
response = get_quali_quali_distribution({
    "xColumnName":"c201_groundWorkType",
    "yColumnName":"c104_campaign_dis",
    "filterParams":[
        { "columnName":"c110_managementType","values":["Agriculture biologique"] }
    ],
    "filterCohort":{ "years":[2018,2019,2020,2021],"networkAges":[] },
    "minDataRequired": MIN_DATA_REQUIRED
})
print('Effectif total: ', response.json()['effectifTotal'])

# Exemple de requête pour Y = 'IFT Total' et X = 'Type de travail au sol', filtrée sur le type de conduite avec cohorte
response = get_quanti_quali_distribution({
    "xColumnName":"c201_groundWorkType",
    "yColumnName":"c601_totalIFT",
    "filterParams":[
        { "columnName":"c110_managementType","values":["Agriculture biologique"] }
    ],
    "filterCohort":{ "years":[2018,2019,2020,2021],"networkAges":[] },
    "minDataRequired": MIN_DATA_REQUIRED
})
print('Effectif total: ', response.json()['effectifTotal'])

# Exemple de requête pur Y = 'IFT Total' et X = 'Consommation de carburant', filtrée sur le type de conduite avec cohorte
response = get_quanti_quanti_distribution({
    "xColumnName":"c304_fuelConsumption",
    "yColumnName":"c601_totalIFT",
    "filterParams":[
        { "columnName":"c110_managementType","values":["Agriculture biologique"] }
    ],
    "filterCohort":{ "years":[2018,2019,2020,2021],"networkAges":[] },
    "minDataRequired": MIN_DATA_REQUIRED
})
print('Effectif total: ', response.json()['effectifTotal'])

# Exemple de requête pour Y = 'IFT Total', filtrée sur le type de conduite avec cohorte
response = get_quanti_distribution({
    "columnName":"c601_totalIFT",
    "filterParams":[
        { "columnName":"c110_managementType","values":["Agriculture biologique"] }
    ],
    "filterCohort":{ "years":[2018,2019,2020,2021],"networkAges":[] }
})
print('Effectif total: ', response.json()['effectifTotal'])

