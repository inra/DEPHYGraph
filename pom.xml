<!--
  #%L
  DEPHY-Graph
  %%
  Copyright (C) 2018 Inra
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>org.nuiton</groupId>
    <artifactId>mop</artifactId>
    <version>0.17</version>
  </parent>

  <groupId>fr.inra</groupId>
  <artifactId>dephy-graph</artifactId>
  <version>2.4.0-SNAPSHOT</version>
  <packaging>war</packaging>

  <name>DEPHY-Graph</name>
  <url />
  <description>Interface graphique interactive permettant un accès généralisé aux références produites par le réseau DEPHY.</description>

  <inceptionYear>2018</inceptionYear>

  <distributionManagement>
    <repository>
      <id>dephygraph-release</id>
      <url>https://nexus.nuiton.org/nexus/content/repositories/dephygraph-release</url>
    </repository>
  </distributionManagement>

  <organization>
    <name>Inrae</name>
    <url>https://www.inrae.fr/</url>
  </organization>

  <scm>
    <connection>scm:git:git@gitlab.nuiton.org:inra/DEPHYGraph.git</connection>
    <developerConnection>scm:git:git@gitlab.nuiton.org:inra/DEPHYGraph.git</developerConnection>
    <url>https://gitlab.nuiton.org/inra/DEPHYGraph</url>
    <tag>HEAD</tag>
  </scm>

  <licenses>
    <license>
      <name>GNU Affero General Public License version 3</name>
      <url>http://www.gnu.org/licenses/agpl.txt</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <properties>
    <platform>gitlab</platform>
    <platformDomain>codelutin</platformDomain>
    <projectId>DEPHYGraph</projectId>
    <ciViewId>DEPHYGraph</ciViewId>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <timestamp>${maven.build.timestamp}</timestamp>
    <maven.build.timestamp.format>yyyyMMddHHmmss</maven.build.timestamp.format>
    <logbackVersion>1.5.16</logbackVersion>
    <h2Version>1.4.196</h2Version>
    <resteasy.version>6.2.11.Final</resteasy.version>
    <postgresqlVersion>42.7.5</postgresqlVersion>
    <jdbiVersion>3.47.0</jdbiVersion>
    <commonsLang3Version>3.17.0</commonsLang3Version>
    <commonsMath3Version>3.6.1</commonsMath3Version>
    <guavaVersion>33.4.0-jre</guavaVersion>
    <javaJwtVersion>4.5.0</javaJwtVersion>

    <javaVersion>21</javaVersion>
    <!-- cf https://maven.apache.org/plugins/maven-compiler-plugin/examples/set-compiler-source-and-target.html -->
    <maven.compiler.source>${javaVersion}</maven.compiler.source>
    <maven.compiler.target>${javaVersion}</maven.compiler.target>
    <!-- FIXME issues: Found Banned Dependency: javax.json.bind:javax.json.bind-api:jar:1.0 -->
    <enforcerPluginVersion>3.0.0-M1</enforcerPluginVersion>
    <enforcer.skip>true</enforcer.skip>
    <animalSnifferPluginVersion>1.16</animalSnifferPluginVersion>
  </properties>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>org.jdbi</groupId>
        <artifactId>jdbi3-bom</artifactId>
        <type>pom</type>
        <version>${jdbiVersion}</version>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <dependencies>
    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-config</artifactId>
      <version>3.5</version>
    </dependency>

    <dependency>
      <groupId>ch.qos.logback</groupId>
      <artifactId>logback-classic</artifactId>
      <version>${logbackVersion}</version>
    </dependency>

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
      <version>${commonsLang3Version}</version>
    </dependency>

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-math3</artifactId>
      <version>${commonsMath3Version}</version>
    </dependency>


    <dependency>
      <groupId>com.google.guava</groupId>
      <artifactId>guava</artifactId>
      <version>${guavaVersion}</version>
    </dependency>

    <dependency>
      <groupId>org.postgresql</groupId>
      <artifactId>postgresql</artifactId>
      <version>${postgresqlVersion}</version>
    </dependency>

    <dependency>
      <groupId>org.jdbi</groupId>
      <artifactId>jdbi3-core</artifactId>
    </dependency>

    <dependency>
      <groupId>org.jdbi</groupId>
      <artifactId>jdbi3-postgres</artifactId>
    </dependency>

    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-databind</artifactId>
      <version>2.18.2</version> <!-- in sync with  RESTEasy -->
    </dependency>

    <dependency>
      <groupId>jakarta.servlet</groupId>
      <artifactId>jakarta.servlet-api</artifactId>
      <version>5.0.0</version>
      <scope>provided</scope>
    </dependency>

    <!-- REASTEasy, for webservices -->
    <!-- needed for Servlet 3 -->
    <dependency>
      <groupId>org.jboss.resteasy</groupId>
      <artifactId>resteasy-servlet-initializer</artifactId>
      <version>${resteasy.version}</version>
    </dependency>
    <dependency>
      <groupId>org.jboss.resteasy</groupId>
      <artifactId>resteasy-client</artifactId>
      <version>${resteasy.version}</version>
    </dependency>
    <dependency>
      <groupId>org.jboss.resteasy</groupId>
      <artifactId>resteasy-core</artifactId>
      <version>${resteasy.version}</version>
    </dependency>
    <!-- needed for JSON response -->
    <dependency>
      <groupId>org.jboss.resteasy</groupId>
      <artifactId>resteasy-jackson2-provider</artifactId>
      <version>${resteasy.version}</version>
    </dependency>

    <!-- Connexion usage : JWT -->
    <dependency>
      <groupId>com.auth0</groupId>
      <artifactId>java-jwt</artifactId>
      <version>${javaJwtVersion}</version>
    </dependency>

    <!-- tests -->
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.13.2</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>com.opentable.components</groupId>
      <artifactId>otj-pg-embedded</artifactId>
      <version>0.13.4</version>
      <scope>test</scope>
    </dependency>

    <!-- API doc -->
    <dependency>
      <groupId>io.swagger.core.v3</groupId>
      <artifactId>swagger-jaxrs2</artifactId>
      <version>2.2.28</version>
      <exclusions>
        <exclusion>
          <groupId>com.fasterxml.jackson.jaxrs</groupId>
          <artifactId>jackson-jaxrs-json-provider</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>io.swagger.core.v3</groupId>
      <artifactId>swagger-annotations</artifactId>
      <version>2.1.13</version>
    </dependency>

  </dependencies>


  <build>

    <resources>
      <resource>
        <directory>src/main/resources</directory>
        <filtering>true</filtering>
      </resource>
    </resources>
    <testResources>
      <testResource>
        <directory>src/test/resources</directory>
      </testResource>
    </testResources>

    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-war-plugin</artifactId>
        <version>3.3.2</version>
        <configuration>
          <webResources>
            <resource>
              <directory>src/main/webapp</directory>
              <filtering>true</filtering>
              <excludes>
                <exclude>**/build/**/*.*</exclude>
                <exclude>**/font/**/*.*</exclude>
                <exclude>**/img/**/*.*</exclude>
                <exclude>**/help/**/*.*</exclude>
              </excludes>
            </resource>
          </webResources>
        </configuration>
      </plugin>

      <plugin>
        <artifactId>maven-resources-plugin</artifactId>
        <version>3.3.0</version>
        <executions>
          <execution>
            <id>copy-webclient</id>
            <phase>process-resources</phase>
            <goals>
              <goal>copy-resources</goal>
            </goals>
            <configuration>
              <outputDirectory>${project.build.directory}/${project.build.finalName}</outputDirectory>
              <resources>
                <resource>
                  <directory>${project.basedir}/src/main/webclient/dephygraph/dist</directory>
                  <filtering>false</filtering>
                </resource>
              </resources>
            </configuration>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-antrun-plugin</artifactId>
        <version>3.1.0</version>
        <executions>
          <execution>
            <id>rename-index</id>
            <phase>prepare-package</phase>
            <configuration>
              <target>
                <move file="${project.build.directory}/${project.build.finalName}/index.html" tofile="${project.build.directory}/${project.build.finalName}/generated-index.html" />
              </target>
            </configuration>
            <goals>
              <goal>run</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-enforcer-plugin</artifactId>
        <version>3.0.0</version>
      </plugin>

      <plugin>
        <groupId>org.eclipse.jetty</groupId>
        <artifactId>jetty-maven-plugin</artifactId>
        <version>11.0.24</version>
        <configuration>
          <httpConnector>
            <port>8084</port>
          </httpConnector>
          <webApp>
            <contextPath>/dephy-graph</contextPath>
          </webApp>
        </configuration>
      </plugin>

    </plugins>
  </build>
</project>

