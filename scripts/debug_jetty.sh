#!/bin/sh

export DEBUG_PORT=9000
export MAVEN_OPTS="-Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=$DEBUG_PORT,server=y,suspend=y"
mvn jetty:run
