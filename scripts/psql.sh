#!/bin/bash

VERSION=16
DB=dephygraph
docker exec -it postgres-${VERSION}-${DB} psql -U postgres ${DB}