#!/bin/bash

VERSION=16
DB_DIR=/home/postgresql-${VERSION}
DB=dephygraph
docker run \
  --name postgres-${VERSION}-${DB} \
  --rm \
  -v ${DB_DIR}-${DB}:/var/lib/postgresql/data \
  -e POSTGRES_DB=${DB} \
  -e POSTGRES_PASSWORD=whatever \
  -p 5432:5432 \
  -d postgres:${VERSION}