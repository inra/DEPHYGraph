---
-- #%L
-- DEPHY-Graph
-- %%
-- Copyright (C) 2018 - 2019 Inra
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Lesser Public License for more details.
-- 
-- You should have received a copy of the GNU General Lesser Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/lgpl-3.0.html>.
-- #L%
---
-- Database definition

------------------------------------
------------------------------------
-- Tables
------------------------------------
------------------------------------

-- Data structure
CREATE TABLE IF NOT EXISTS data(
    realized                                      TEXT,
    dephyNb                                       TEXT,
    validation                                    boolean,
    c101_sector                                   TEXT,
    c102_pz0                                      TEXT,
    c103_networkyears                             TEXT,
    c104_campaign                                 TEXT,
    c105_generalTotalIFT                          decimal,
    c202_regionPre2015                            TEXT,
    c201_administrativeRegion                     TEXT,
    c203_department                               TEXT,
    latitude                                      decimal,
    longitude                                     decimal,
    c204_groundType                               TEXT,
    c205_groundTexture                            TEXT,
    c206_usefulReserve                            TEXT,
    c207_irrigable                                TEXT,
    c208_managementType                           TEXT,
    c209_purpose                                  TEXT,
    c210_liveStockUnit                            TEXT,
    c211_industrialFarming                        TEXT,
    c212_farmingType                              TEXT,
    c213_species                                  TEXT,
    c214_grapeVariety                             TEXT,
    c215_rootStock                                TEXT,
    c216_plantingDensity                          TEXT,
    c217_plantingYear                             TEXT,
    c301_groundWorkType                           TEXT,
    c302_rotationType                             TEXT,
    c303_mechanicalWeeding                        TEXT,
    c304_mechanicalWeedingInterventionFrequency   decimal,
    c305_categoryOfLevers                         TEXT,
    c401_totalIFT                                 decimal,
    c402_totalIFTwithTS                           decimal,
    c403_biocontrolIFT                            decimal,
    c404_IFTTS                                    decimal,
    c405_herbicideIFT                             decimal,
    c406_exceptHerbicideIFT                       decimal,
    c407_insecticideIFT                           decimal,
    c408_fungicideIFT                             decimal,
    c409_molluscicideIFT                          decimal,
    c410_controllerIFT                            decimal,
    c411_otherIFT                                 decimal,
    c412_biologicalWaysSolution                   decimal,
    c501_fertilizationExpenses                    decimal,
    c502_seedExpenses                             decimal,
    c503_phytoExpenses                            decimal,
    c504_mechanizationExpenses                    decimal,
    c505_labourExpenses                           decimal,
    c506_outLabourTotalExpenses                   decimal,
    c601_performance                              decimal,
    c602_grossProceeds                            decimal,
    c603_grossProfit                              decimal,
    c604_semiNetMargin                            decimal,
    c701_workingTime                              decimal,
    c702_fuelConsumption                          decimal,
    c703_irrigationVolume                         decimal,
    c801_fertilizationUnityN                      decimal,
    c802_fertilizationMineralUnityN               decimal,
    c803_fertilizationOrganicUnityN               decimal,
    c804_fertilizationUnityP                      decimal,
    c805_fertilizationMineralUnityP               decimal,
    c806_fertilizationOrganicUnityP               decimal,
    c807_fertilizationUnityK                      decimal,
    c808_fertilizationMineralUnityK               decimal,
    c809_fertilizationOrganicUnityK               decimal
);

-- Metadata structure
CREATE TABLE IF NOT EXISTS metadata(
    id              UUID        PRIMARY KEY,
    columnName      TEXT        UNIQUE NOT NULL,
    displayName     TEXT        NOT NULL,
    unity           TEXT,
    qualitative     BOOLEAN,
    quantitative    BOOLEAN,
    filter          BOOLEAN,
    irFilter        BOOLEAN,
    separator       TEXT,
    category        TEXT,
    hidden          BOOLEAN
);

-- Miscellaneous table, used to contain other data, like last extraction date to make datastore.
CREATE TABLE IF NOT EXISTS miscellaneous(
    id      UUID        PRIMARY KEY,
    key     TEXT        UNIQUE NOT NULL,
    data    TEXT
);

-- AgrosystInfo structure : contains temporary session information, should be clean periodically (a cron is scheduled in application)
CREATE TABLE IF NOT EXISTS agrosystInfo(
    id              UUID        PRIMARY KEY,
    dephyNb         TEXT[],
    expirationDate  DATE        NOT NULL
);

-- TOTHINK, if dephygraph is the database user
-- GRANT SELECT, INSERT, UPDATE, DELETE ON metadata TO dephygraph;
-- GRANT SELECT, INSERT, UPDATE, DELETE ON data TO dephygraph;
-- GRANT SELECT, INSERT, UPDATE, DELETE ON miscellaneous TO dephygraph;
-- GRANT SELECT, INSERT, UPDATE, DELETE ON agrosystInfo TO dephygraph;


------------------------------------
------------------------------------
-- Meta structure : insert metadata
------------------------------------
------------------------------------

-- INSERT INTO metadata VALUES ( uuid                                             , columnNamne   ,                                   displayName         ,                                         unity     ,       qualitative, quantitative,  filter,   irFilter, separator, category,                      hidden);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'realized',                                       'Réalisé/Synthétisé',                                         null,             true,       false,          true,     true,     null,     ''                              , true);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'dephyNb',                                        'N° Dephy',                                                   null,             true,       false,          true,     false,    null,     ''                              , true);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'validation',                                     'Validation',                                                 null,             true,       false,          true,     true,     null,     ''                              , true);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c101_sector',                                    'Filière',                                                    null,             true,       false,          true,     false,    null,     ''                              , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c102_pz0',                                       'Ancienneté dans le réseau',                                  null,             true,       false,          true,     false,    null,     ''                              , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c103_networkyears',                              'Nombre d''années dans le réseau',                            null,             true,       false,          true,     false,    null,     ''                              , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c104_campaign',                                  'Campagne(s)',                                                null,             true,       false,          true,     false,    ',',      ''                              , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c105_generalTotalIFT',                           'IFT Total (hors biocontrôle, hors TS) - méthode pré2015',    null,             false,      true,           false,    false,    null,     ''                              , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c202_regionPre2015',                             'Région (pré 2015)',                                          null,             true,       false,          true,     false,    null,     '02-Contexte'                   , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c201_administrativeRegion',                      'Région Administrative',                                      null,             true,       false,          true,     false,    null,     '02-Contexte'                   , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c203_department',                                'Département',                                                null,             true,       false,          true,     false,    null,     '02-Contexte'                   , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'latitude',                                       'Latitude',                                                   null,             false,      true,           true,     false,    null,     '02-Contexte'                   , true);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'longitude',                                      'Longitude',                                                  null,             false,      true,           true,     false,    null,     '02-Contexte'                   , true);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c204_groundType',                                'Type de sol',                                                null,             true,       false,          true,     false,    null,     '02-Contexte'                   , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c205_groundTexture',                             'Texture de sol',                                             null,             true,       false,          true,     false,    null,     '02-Contexte'                   , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c206_usefulReserve',                             'Réserve utile',                                              null,             true,       false,          true,     false,    null,     '02-Contexte'                   , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c207_irrigable',                                 'Irrigable',                                                  null,             true,       false,          true,     false,    null,     '02-Contexte'                   , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c208_managementType',                            'Type de conduite',                                           null,             true,       false,          true,     false,    null,     '02-Contexte'                   , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c209_purpose',                                   'Destination',                                                null,             true,       false,          true,     false,    ',',      '02-Contexte'                   , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c210_liveStockUnit',                             'Atelier d''élevage',                                         null,             true,       false,          true,     false,    ',',      '02-Contexte'                   , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c211_industrialFarming',                         'Culture industrielle',                                       null,             true,       false,          true,     false,    null,     '02-Contexte'                   , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c212_farmingType',                               'Plein Champs/serre/hors sol',                                null,             true,       false,          true,     false,    null,     '02-Contexte'                   , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c213_species',                                   'Espèces (pérennes)',                                         null,             true,       false,          true,     false,    ',',      '02-Contexte'                   , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c214_grapeVariety',                              'Variété/Cépage',                                             null,             true,       false,          true,     false,    ',',      '02-Contexte'                   , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c215_rootStock',                                 'Porte-Greffe',                                               null,             true,       false,          true,     false,    ',',      '02-Contexte'                   , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c216_plantingDensity',                           'Densité de plantation',                                      'Plants/ha',      true,       false,          true,     false,    null,     '02-Contexte'                   , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c217_plantingYear',                              'Année de plantation',                                        null,             false,      true,           true,     false,    ',',      '02-Contexte'                   , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c301_groundWorkType',                            'Type de travail du sol',                                     null,             true,       false,          true,     false,    null,     '03-Strategie Agronomique'      , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c302_rotationType',                              'Type de rotation',                                           null,             true,       false,          true,     false,    null,     '03-Strategie Agronomique'      , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c303_mechanicalWeeding',                         'Désherbage mécanique',                                       null,             true,       false,          true,     false,    null,     '03-Strategie Agronomique'      , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c304_mechanicalWeedingInterventionFrequency',    'Fréquence d''intervention de désherbage mécanique',          'Nb/an',          false,      true,           false,    false,    null,     '03-Strategie Agronomique'      , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c305_categoryOfLevers',                          'Catégorie de leviers',                                       null,             true,       false,          true,     false,    ',',      '03-Strategie Agronomique'      , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c401_totalIFT',                                  'IFT Total (hors biocontrôle, hors TS) - méthode pré2015',    null,             false,      true,           false,    false,    null,     '04-Performances - IFT'         , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c402_totalIFTwithTS',                             'IFT Total (hors biocontrôle, avec TS) - méthode pré2015',   null,             false,      true,           false,    false,    null,     '04-Performances - IFT'         , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c403_biocontrolIFT',                             'IFT Biocontrole - méthode pré2015',                          null,             false,      true,           false,    false,    null,     '04-Performances - IFT'         , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c404_IFTTS',                                     'IFT TS (traitement de semences) - méthode pré2015',          null,             false,      true,           false,    false,    null,     '04-Performances - IFT'         , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c405_herbicideIFT',                              'IFT Herbicides - méthode pré2015',                           null,             false,      true,           false,    false,    null,     '04-Performances - IFT'         , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c406_exceptHerbicideIFT',                        'IFT Hors herbicides - méthode pré2015',                      null,             false,      true,           false,    false,    null,     '04-Performances - IFT'         , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c407_insecticideIFT',                            'IFT Insecticides - méthode pré2015',                         null,             false,      true,           false,    false,    null,     '04-Performances - IFT'         , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c408_fungicideIFT',                              'IFT Fongicides - méthode pré2015',                           null,             false,      true,           false,    false,    null,     '04-Performances - IFT'         , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c409_molluscicideIFT',                           'IFT Molluscicides - méthode pré2015',                        null,             false,      true,           false,    false,    null,     '04-Performances - IFT'         , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c410_controllerIFT',                             'IFT Régulateurs - méthode pré2015',                          null,             false,      true,           false,    false,    null,     '04-Performances - IFT'         , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c411_otherIFT',                                  'IFT Autres - méthode pré2015',                               null,             false,      true,           false,    false,    null,     '04-Performances - IFT'         , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c412_biologicalWaysSolution',                    'Recours aux moyens biologiques',                             null,             false,      true,           false,    false,    null,     '04-Performances - IFT'         , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c501_fertilizationExpenses',                     'Charges Fertilisation',                                      'Euros/ha/an',    false,      true,           false,    false,    null,     '05-Performances - Charges'     , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c502_seedExpenses',                              'Charges semences',                                           'Euros/ha/an',    false,      true,           false,    false,    null,     '05-Performances - Charges'     , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c503_phytoExpenses',                             'Charges phytos',                                             'Euros/ha/an',    false,      true,           false,    false,    null,     '05-Performances - Charges'     , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c504_mechanizationExpenses',                     'Charges mécanisation',                                       'Euros/ha/an',    false,      true,           false,    false,    null,     '05-Performances - Charges'     , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c505_labourExpenses',                            'Charges Main d''oeuvre',                                     'Euros/ha/an',    false,      true,           false,    false,    null,     '05-Performances - Charges'     , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c506_outLabourTotalExpenses',                    'Charges totales (hors Main d''oeuvre)',                      'Euros/ha/an',    false,      true,           false,    false,    null,     '05-Performances - Charges'     , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c601_performance',                               'Rendement',                                                  't/ha/an',        false,      true,           false,    false,    null,     '06-Performances - Marges'      , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c602_grossProceeds',                             'Produit brut',                                               'Euros/ha/an',    false,      true,           false,    false,    null,     '06-Performances - Marges'      , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c603_grossProfit',                               'Marge brute',                                                'Euros/ha/an',    false,      true,           false,    false,    null,     '06-Performances - Marges'      , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c604_semiNetMargin',                             'Marge semi-nette',                                           'Euros/ha/an',    false,      true,           false,    false,    null,     '06-Performances - Marges'      , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c701_workingTime',                               'Temps de travail',                                           'h/ha/an',        false,      true,           false,    false,    null,     '07-Performances - Consommation', false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c702_fuelConsumption',                           'Consommation de carbutrant',                                 'L/ha/an',        false,      true,           false,    false,    null,     '07-Performances - Consommation', false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c703_irrigationVolume',                          'Volume d''irrigation',                                       'mm/an',          false,      true,           false,    false,    null,     '07-Performances - Consommation', false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c801_fertilizationUnityN',                       'Unités N',                                                   'Unités',         false,      true,           false,    false,    null,     '08-Fertilisation'              , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c802_fertilizationMineralUnityN',                'Unités N minéral',                                           'Unités',         false,      true,           false,    false,    null,     '08-Fertilisation'              , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c803_fertilizationOrganicUnityN',                'Unités N organique',                                         'Unités',         false,      true,           false,    false,    null,     '08-Fertilisation'              , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c804_fertilizationUnityP',                       'Unités P',                                                   'Unités',         false,      true,           false,    false,    null,     '08-Fertilisation'              , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c805_fertilizationMineralUnityP',                'Unités P minéral',                                           'Unités',         false,      true,           false,    false,    null,     '08-Fertilisation'              , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c806_fertilizationOrganicUnityP',                'Unités P organique',                                         'Unités',         false,      true,           false,    false,    null,     '08-Fertilisation'              , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c807_fertilizationUnityK',                       'Unités K',                                                   'Unités',         false,      true,           false,    false,    null,     '08-Fertilisation'              , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c808_fertilizationMineralUnityK',                'Unités K minéral',                                           'Unités',         false,      true,           false,    false,    null,     '08-Fertilisation'              , false);
INSERT INTO metadata VALUES ( md5(random()::text || clock_timestamp()::text)::uuid, 'c809_fertilizationOrganicUnityK',                'Unités K organique',                                         'Unités',         false,      true,           false,    false,    null,     '08-Fertilisation'              , false);


-- Add last Agrosyst Extraction Date of the Data
INSERT INTO miscellaneous(id, key, data) VALUES (md5(random()::text || clock_timestamp()::text)::uuid, 'lastAgrosystExtractionDate', 'Mars 2018') ON CONFLICT (key) DO UPDATE SET data = EXCLUDED.data;

