---
-- #%L
-- DEPHY-Graph
-- %%
-- Copyright (C) 2018 - 2019 Inra
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Lesser Public License for more details.
-- 
-- You should have received a copy of the GNU General Lesser Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/lgpl-3.0.html>.
-- #L%
---

-- INSERT INTO data VALUES ('realized', 'dephyNb', validation, 'sector',      'pz0',      'campaign',         'region', 'latitude', 'longitude', 'groundType', 'groundTexture', totalIFT,           rootstock);
INSERT INTO data VALUES ('Synthétisé', 'GCF31915', true,  'Grandes cultures', 'PZ0',      '2014, 2015, 2016', null, null, null, 'Tourbe',                      'Limon-argileux', 1.45308960974216,  null);
INSERT INTO data VALUES ('Synthétisé', 'PYF10215', true,  'Grandes cultures', 'PZ0',      '2011',             null, null, null, 'Tourbe',                      'Argileux',       8.55783081054688,  null);
INSERT INTO data VALUES ('Synthétisé', 'PYF10202', true,  'Grandes cultures', 'PZ0',      '2009, 2010, 2011', null, null, null, 'Tourbe',                      'Limoneux',       2.64801011118107,  null);
INSERT INTO data VALUES ('Synthétisé', 'PYF10467', true,  'Grandes cultures', 'Post-PZ0', '2013, 2014',       null, null, null, 'Limon battant sain',          'Limon-argileux', null,              null);
INSERT INTO data VALUES ('Synthétisé', 'PYF27458', true,  'Grandes cultures', 'Post-PZ0', '2014',             null, null, null, 'Limon argileux/craie',        'Argileux',       0.272543721366674, null);
INSERT INTO data VALUES ('Réalisé',    'FAK27458', true,  'Grandes cultures', null,       '2014',             null, null, null, 'Limon argileux/craie',        'Limoneux',       5.72884214948863,  null);

INSERT INTO data VALUES ('Synthétisé', 'GCF11163', false, 'Grandes cultures', 'PZ0',      '2010',             null, null, null, 'Champagne et aubue profonde', 'Limon-argileux', 3.09143781638704,  null);
INSERT INTO data VALUES ('Synthétisé', 'GCF24632', true,  'Grandes cultures', 'PZ0',      '2010, 2011, 2012', null, null, null, 'Limon argileux/craie',        'Argileux',       6.69947671890259,  null);
INSERT INTO data VALUES ('Synthétisé', 'PYF10866', false, 'Grandes cultures', 'Post-PZ0', '2016',             null, null, null, 'Limon argileux/craie',        'Limoneux',       null,              null);
INSERT INTO data VALUES ('Synthétisé', 'PYF10281', true,  'Grandes cultures', 'PZ0',      '2009, 2010, 2011', null, null, null, 'Champagne et aubue profonde', 'Limon-argileux', 5.24928286112845,  null);
INSERT INTO data VALUES ('Synthétisé', 'GCF39532', false, 'Grandes cultures', 'Post-PZ0', '2013, 2014, 2016', null, null, null, 'Marais non calcaire',         'Argileux',       null,              null);
INSERT INTO data VALUES ('Synthétisé', 'PYF10374', true,  'Grandes cultures', 'PZ0',      '2011',             null, null, null, 'Limon argileux/craie',        'Limoneux',       5.67272910103202,  null);
INSERT INTO data VALUES ('Synthétisé', 'PYF10234', false, 'Grandes cultures', 'PZ0',      '2009, 2010, 2011', null, null, null, 'Champagne et aubue profonde', 'Limon-argileux', 2.59181213378906,  null);
INSERT INTO data VALUES ('Synthétisé', 'PYF10297', true,  'Grandes cultures', 'PZ0',      '2009, 2010, 2011', null, null, null, 'Limon battant sain',          'Argileux',       0.9528256226331,   null);
INSERT INTO data VALUES ('Synthétisé', 'PYF10574', true,  'Grandes cultures', 'Post-PZ0', '2015',             null, null, null, 'tourbe',                      'Limoneux',       null,              null);
INSERT INTO data VALUES ('Synthétisé', 'GCF26982', false, 'Grandes cultures', 'Post-PZ0', '2011',             null, null, null, 'Limon battant sain',          'Limon-argileux', null,              null);
INSERT INTO data VALUES ('Synthétisé', 'PYF10953', true,  'Grandes cultures', 'PZ0',      '2009, 2010, 2011', null, null, null, 'tourbe',                      'Argileux',       1.15542670711875,  null);
