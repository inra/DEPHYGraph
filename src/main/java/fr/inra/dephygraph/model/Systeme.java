package fr.inra.dephygraph.model;

import java.util.Objects;

public class Systeme {
    protected String dephyNb;
    protected String campaign;
    protected String region;
    protected String ancienneRegion;
    protected String departement;
    protected String arrondissement;
    protected String bassinViticole;

    public String getDephyNb() {
        return dephyNb;
    }

    public void setDephyNb(String dephyNb) {
        this.dephyNb = dephyNb;
    }

    public String getCampaign() {
        return campaign;
    }

    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getAncienneRegion() {
        return ancienneRegion;
    }

    public void setAncienneRegion(String ancienneRegion) {
        this.ancienneRegion = ancienneRegion;
    }

    public String getDepartement() {
        return departement;
    }

    public void setDepartement(String departement) {
        this.departement = departement;
    }

    public String getArrondissement() {
        return arrondissement;
    }

    public void setArrondissement(String arrondissement) {
        this.arrondissement = arrondissement;
    }

    public String getBassinViticole() {
        return bassinViticole;
    }

    public void setBassinViticole(String bassinViticole) {
        this.bassinViticole = bassinViticole;
    }

    public String getZoneGeo(TypeDecoupageGeo typeDecoupageGeo) {
        return switch(typeDecoupageGeo) {
            case REGION -> getRegion();
            case DEPARTEMENT -> getDepartement();
            case ARRONDISSEMENT -> getArrondissement();
            case BASSIN_VITICOLE -> getBassinViticole();
            case ANCIENNE_REGION -> getAncienneRegion();
        };
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Systeme systeme = (Systeme) o;
        return Objects.equals(dephyNb, systeme.dephyNb)
                && Objects.equals(campaign, systeme.campaign)
                && Objects.equals(region, systeme.region)
                && Objects.equals(ancienneRegion, systeme.ancienneRegion)
                && Objects.equals(departement, systeme.departement)
                && Objects.equals(arrondissement, systeme.arrondissement)
                && Objects.equals(bassinViticole, systeme.bassinViticole);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                dephyNb, campaign, region, ancienneRegion, departement, arrondissement, bassinViticole);
    }
}
