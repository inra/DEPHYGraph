package fr.inra.dephygraph.model.param;

import fr.inra.dephygraph.model.FilterCohort;

import java.util.List;
import java.util.Optional;

public abstract class AbstractDistributionParam {
    protected List<FilterParam> filterParams;
    protected FilterCohort filterCohort;
    protected Integer minDataRequired;
    protected Integer minDataRequiredForCohort;
    protected boolean authenticated = false;

    public List<FilterParam> getFilterParams() {
        return Optional.ofNullable(filterParams).orElse(List.of());
    }

    public void setFilterParams(List<FilterParam> filterParams) {
        this.filterParams = filterParams;
    }

    public FilterCohort getFilterCohort() {
        return filterCohort;
    }

    public void setFilterCohort(FilterCohort filterCohort) {
        this.filterCohort = filterCohort;
    }

    public Integer getMinDataRequired() {
        return minDataRequired;
    }

    public void setMinDataRequired(Integer minDataRequired) {
        this.minDataRequired = minDataRequired;
    }

    public Integer getMinDataRequiredForCohort() {
        return minDataRequiredForCohort;
    }

    public void setMinDataRequiredForCohort(Integer minDataRequiredForCohort) {
        this.minDataRequiredForCohort = minDataRequiredForCohort;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }
}
