package fr.inra.dephygraph.model.param;

public class DistributionWithOneDimensionParam extends AbstractDistributionParam {
    protected String columnName;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }
}
