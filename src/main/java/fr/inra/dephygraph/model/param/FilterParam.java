package fr.inra.dephygraph.model.param;

import java.util.List;

public class FilterParam {
    protected String columnName;
    protected List<String> values;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }
}
