package fr.inra.dephygraph.model.param;

public class DistributionWithTwoDimensionsParam extends AbstractDistributionParam {
    protected String xColumnName;
    protected String yColumnName;

    public String getxColumnName() {
        return xColumnName;
    }

    public void setxColumnName(String xColumnName) {
        this.xColumnName = xColumnName;
    }

    public String getyColumnName() {
        return yColumnName;
    }

    public void setyColumnName(String yColumnName) {
        this.yColumnName = yColumnName;
    }
}
