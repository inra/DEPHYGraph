package fr.inra.dephygraph.model;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Collections;
import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class StatisticData extends Data {

    protected double average;
    protected double median;
    protected double min;
    protected double max;
    protected double firstQuartile;
    protected double thirdQuartile;
    protected double firstDecile;
    protected double ninthDecile;
    protected Double[] values;
    protected List<SystemeWithYXValues> systemes;

    public static StatisticData getEmptyData() {
        StatisticData data = new StatisticData();
        data.setValuename("");
        data.setCoordinates(Collections.emptyList());
        data.setAverage(0.0d);
        data.setMedian(0.0d);
        data.setMin(0.0d);
        data.setMax(0.0d);
        data.setFirstQuartile(0.0d);
        data.setThirdQuartile(0.0d);
        data.setFirstDecile(0.0d);
        data.setNinthDecile(0.0d);
        data.setValues(new Double[]{});
        data.setSystemes(Collections.emptyList());
        return data;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public double getMedian() {
        return median;
    }

    public void setMedian(double median) {
        this.median = median;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public double getFirstQuartile() {
        return firstQuartile;
    }

    public void setFirstQuartile(double firstQuartile) {
        this.firstQuartile = firstQuartile;
    }

    //XXX trick because Spgeed does not support attribute camelCase in setter...
    public void setFirstquartile(double firstQuartile) {
        this.firstQuartile = firstQuartile;
    }

    public double getThirdQuartile() {
        return thirdQuartile;
    }

    public void setThirdQuartile(double thirdQuartile) {
        this.thirdQuartile = thirdQuartile;
    }

    //XXX trick because Spgeed does not support attribute camelCase in setter...
    public void setThirdquartile(double thirdQuartile) {
        this.thirdQuartile = thirdQuartile;
    }

    public double getFirstDecile() {
        return firstDecile;
    }

    public void setFirstDecile(double firstDecile) {
        this.firstDecile = firstDecile;
    }

    //XXX trick because Spgeed does not support attribute camelCase in setter...
    public void setFirstdecile(double firstDecile) {
        this.firstDecile = firstDecile;
    }

    public double getNinthDecile() {
        return ninthDecile;
    }

    public void setNinthDecile(double ninthDecile) {
        this.ninthDecile = ninthDecile;
    }

    //XXX trick because Spgeed does not support attribute camelCase in setter...
    public void setNinthdecile(double ninthDecile) {
        this.ninthDecile = ninthDecile;
    }

    public Double[] getValues() {
        // NB : Pour le #11363, on pourrait faire en sorte que le tableau de values null soit remplacé à la volée par un tableau vide tel que :
        // if (values == null) { return new Double[0]; }
        return values;
    }

    public void setValues(Double[] values) {
        this.values = values;
    }

    public List<SystemeWithYXValues> getSystemes() {
        return systemes;
    }

    public void setSystemes(List<SystemeWithYXValues> systemes) {
        this.systemes = systemes;
    }
}
