package fr.inra.dephygraph.model;

public class QuantiResult extends ResultWithCoordinatesAndSectors {
    protected LightStatisticData repartition;

    public LightStatisticData getRepartition() {
        return repartition;
    }

    public void setRepartition(LightStatisticData repartition) {
        this.repartition = repartition;
    }
}
