package fr.inra.dephygraph.model;

public class SystemeWithYXValues extends Systeme {
    protected Double yVal;
    protected Double xVal;

    public double getyVal() {
        return yVal;
    }

    public void setyVal(double yVal) {
        this.yVal = yVal;
    }

    public Double getxVal() {
        return xVal;
    }

    public void setxVal(Double xVal) {
        this.xVal = xVal;
    }
}
