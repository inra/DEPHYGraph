package fr.inra.dephygraph.model;

public enum SortType {
    ASCENDING,
    DESCENDING;
}
