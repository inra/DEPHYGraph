package fr.inra.dephygraph.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public enum Sector {
    ARBORICULTURE("Arboriculture"),
    VITICULTURE("Viticulture"),
    CULTURES_TROPICALES("Cultures tropicales"),
    MARAICHAGE("Maraîchage"),
    GRANDES_CULTURES_POLYCULTURE_ELEVAGE("Grandes cultures Polyculture-élevage"),
    HORTICULTURE("Horticulture");

    public static final String METADATA_COLUMN_NAME = MetadataConstants.SECTOR_COLUMN_NAME;

    public static final String ALL_MARKER = "ALL";

    public static final String LIST_SEPARATOR = ",";

    public static final Set<Sector> ALL_SECTORS = new HashSet<>();

    private static final Map<String, Sector> SECTOR_BY_LABEL = new HashMap<>();
    static {
        for (Sector s : Sector.values()) {
            ALL_SECTORS.add(s);
            SECTOR_BY_LABEL.put(s.getLabel(), s);
        }
    }

    public static Sector fromLabel(String label) {
        return SECTOR_BY_LABEL.get(label);
    }

    Sector(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    private final String label;
}
