package fr.inra.dephygraph.model;

public enum Pz0 {
    SYSTEME_DANS_RESEAU_DEPHY("Systèmes évoluant au sein du réseau DEPHY"),
    SYSTEME_AVANT_ENTREE_DANS_RESEAU_DEPHY("Systèmes avant l'entrée dans le réseau DEPHY");

    public static final String METADATA_COLUMN_NAME = MetadataConstants.PZ0_COLUMN_NAME;

    Pz0(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    private final String label;
}
