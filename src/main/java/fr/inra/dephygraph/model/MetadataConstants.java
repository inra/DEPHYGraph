package fr.inra.dephygraph.model;

public final class MetadataConstants {
    public static final String SECTOR_COLUMN_NAME = "c101_sector";
    public static final String PZ0_COLUMN_NAME = "c102_pz0";
    public static final String REGION_COLUMN_NAME = "c105_administrativeregion";
    public static final String REGION_PRE2015_COLUMN_NAME = "c106_regionpre2015";
    public static final String DEPARTEMENT_COLUMN_NAME = "c107_departement";
    public static final String ARRONDISSEMENT_COLUMN_NAME = "arrondissement";
    public static final String BASSIN_VITICOLE_COLUMN_NAME = "c108_winebasin";

    public static final String EVOLUTION_IFT_SIMPLIFIED_CATEGORY_NAME = "Evolution de l'IFT";
}
