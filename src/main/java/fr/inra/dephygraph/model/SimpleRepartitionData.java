package fr.inra.dephygraph.model;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class SimpleRepartitionData extends Data {

    protected int valueOccurrence;

    protected List<SystemeWithYXValues> systemes;

    public int getValueOccurrence() {
        return valueOccurrence;
    }

    public void setValueOccurrence(int valueOccurrence) {
        this.valueOccurrence = valueOccurrence;
    }

    //XXX trick because Spgeed does not support attribute camelCase in setter...
    public void setValueoccurrence(int valueOccurrence) {
        this.valueOccurrence = valueOccurrence;
    }

    public List<SystemeWithYXValues> getSystemes() {
        return systemes;
    }

    public void setSystemes(List<SystemeWithYXValues> systemes) {
        this.systemes = systemes;
    }
}
