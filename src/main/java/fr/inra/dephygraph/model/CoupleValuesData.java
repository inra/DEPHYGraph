package fr.inra.dephygraph.model;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class CoupleValuesData extends Data {

    protected Double abscissa;
    protected Double ordinate;
    protected int weight;

    protected List<SystemeWithYXValues> systemes;

    public Double getAbscissa() {
        return abscissa;
    }

    public void setAbscissa(Double abscissa) {
        this.abscissa = abscissa;
    }

    public Double getOrdinate() {
        return ordinate;
    }

    public void setOrdinate(Double ordinate) {
        this.ordinate = ordinate;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public List<SystemeWithYXValues> getSystemes() {
        return systemes;
    }

    public void setSystemes(List<SystemeWithYXValues> systemes) {
        this.systemes = systemes;
    }
}
