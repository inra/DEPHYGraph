package fr.inra.dephygraph.model;

import java.util.List;
import java.util.Map;

public class DistributionEffectif {
    protected Map<String, Long> effectifParRegion;
    protected Map<String, Long> effectifParAncienneRegion;
    protected Map<String, Long> effectifParDepartement;
    protected Map<String, Long> effectifParArrondissement;
    protected Map<String, Long> effectifParBassinViticole;
    protected List<Intervalle<Long>> intervallesEffectifRegion;
    protected List<Intervalle<Long>> intervallesEffectifAncienneRegion;
    protected List<Intervalle<Long>> intervallesEffectifDepartement;
    protected List<Intervalle<Long>> intervallesEffectifArrondissement;
    protected List<Intervalle<Long>> intervallesEffectifBassinViticole;

    public void setEffectifParDecoupage(TypeDecoupageGeo typeDecoupageGeo, Map<String, Long> values) {
        switch(typeDecoupageGeo) {
            case REGION -> effectifParRegion = values;
            case ANCIENNE_REGION -> effectifParAncienneRegion = values;
            case DEPARTEMENT -> effectifParDepartement = values;
            case ARRONDISSEMENT -> effectifParArrondissement = values;
            case BASSIN_VITICOLE -> effectifParBassinViticole = values;
        }
    }

    public Map<String, Long> getEffectifParDecoupage(TypeDecoupageGeo typeDecoupageGeo) {
        return switch(typeDecoupageGeo) {
            case REGION -> effectifParRegion;
            case ANCIENNE_REGION -> effectifParAncienneRegion;
            case DEPARTEMENT -> effectifParDepartement;
            case ARRONDISSEMENT -> effectifParArrondissement;
            case BASSIN_VITICOLE -> effectifParBassinViticole;
        };
    }

    public void setIntervallesEffectif(TypeDecoupageGeo typeDecoupageGeo, List<Intervalle<Long>> values) {
        switch(typeDecoupageGeo) {
            case REGION -> intervallesEffectifRegion = values;
            case ANCIENNE_REGION -> intervallesEffectifAncienneRegion = values;
            case DEPARTEMENT -> intervallesEffectifDepartement = values;
            case ARRONDISSEMENT -> intervallesEffectifArrondissement = values;
            case BASSIN_VITICOLE -> intervallesEffectifBassinViticole = values;
        }
    }

    public Map<String, Long> getEffectifParRegion() {
        return effectifParRegion;
    }

    public Map<String, Long> getEffectifParAncienneRegion() {
        return effectifParAncienneRegion;
    }

    public Map<String, Long> getEffectifParDepartement() {
        return effectifParDepartement;
    }

    public Map<String, Long> getEffectifParArrondissement() {
        return effectifParArrondissement;
    }

    public Map<String, Long> getEffectifParBassinViticole() {
        return effectifParBassinViticole;
    }

    public List<Intervalle<Long>> getIntervallesEffectifRegion() {
        return intervallesEffectifRegion;
    }

    public List<Intervalle<Long>> getIntervallesEffectifAncienneRegion() {
        return intervallesEffectifAncienneRegion;
    }

    public List<Intervalle<Long>> getIntervallesEffectifDepartement() {
        return intervallesEffectifDepartement;
    }

    public List<Intervalle<Long>> getIntervallesEffectifArrondissement() {
        return intervallesEffectifArrondissement;
    }

    public List<Intervalle<Long>> getIntervallesEffectifBassinViticole() {
        return intervallesEffectifBassinViticole;
    }

}
