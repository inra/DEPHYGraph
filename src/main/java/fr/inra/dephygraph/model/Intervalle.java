package fr.inra.dephygraph.model;

public class Intervalle<T> {
    protected T low;
    protected T high;

    public static <T> Intervalle<T> from(T low, T high)  {
        return new Intervalle<>(low, high);
    }

    public Intervalle(T low, T high) {
        this.low = low;
        this.high = high;
    }

    public T getLow() {
        return low;
    }

    public void setLow(T low) {
        this.low = low;
    }

    public T getHigh() {
        return high;
    }

    public void setHigh(T high) {
        this.high = high;
    }

    @Override
    public String toString() {
        return "[" + low + "," + high + "]";
    }
}
