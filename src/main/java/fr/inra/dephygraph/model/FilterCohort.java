package fr.inra.dephygraph.model;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public class FilterCohort {
    protected Set<Integer> years;
    protected Set<String> networkAges;

    public static FilterCohort ofYears(Integer... years) {
        FilterCohort cohort = new FilterCohort();
        cohort.setYears(Set.of(years));
        return cohort;
    }

    public static FilterCohort ofNetworkAges(String... networkAges) {
        FilterCohort cohort = new FilterCohort();
        cohort.setNetworkAges(Set.of(networkAges));
        return cohort;
    }

    public Set<Integer> getYears() {
        return years;
    }

    public void setYearsFromStrings(Collection<String> values) {
        years = values.stream()
                .map(y -> Integer.parseInt(y, 10))
                .collect(Collectors.toSet());
    }

    public void setYears(Set<Integer> years) {
        this.years = years;
    }

    public Set<String> getNetworkAges() {
        return networkAges;
    }

    public void setNetworkAges(Set<String> networkAges) {
        this.networkAges = networkAges;
    }
}
