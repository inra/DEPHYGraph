package fr.inra.dephygraph.model;

public enum TypeDecoupageGeo {
    REGION(MetadataConstants.REGION_COLUMN_NAME, "Région"),
    ANCIENNE_REGION(MetadataConstants.REGION_PRE2015_COLUMN_NAME, "Ancienne région"),
    DEPARTEMENT(MetadataConstants.DEPARTEMENT_COLUMN_NAME, "Département"),
    ARRONDISSEMENT(MetadataConstants.ARRONDISSEMENT_COLUMN_NAME, "Arrondissement"),
    BASSIN_VITICOLE(MetadataConstants.BASSIN_VITICOLE_COLUMN_NAME, "Bassin viticole")
    ;

    TypeDecoupageGeo(String columnName, String label) {
        this.columnName = columnName;
        this.label = label;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getLabel() {
        return label;
    }

    private final String columnName;
    private final String label;

}
