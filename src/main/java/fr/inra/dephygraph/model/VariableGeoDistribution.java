package fr.inra.dephygraph.model;

import java.util.List;
import java.util.Map;

public class VariableGeoDistribution {
    protected String unit;
    protected SortType sortType = SortType.ASCENDING;
    protected Map<String, Double> moyenneParRegion;
    protected Map<String, Double> moyenneParAncienneRegion;
    protected Map<String, Double> moyenneParDepartement;
    protected Map<String, Double> moyenneParArrondissement;
    protected Map<String, Double> moyenneParBassinViticole;
    protected List<Intervalle<Double>> intervallesRegion;
    protected List<Intervalle<Double>> intervallesAncienneRegion;
    protected List<Intervalle<Double>> intervallesDepartement;
    protected List<Intervalle<Double>> intervallesArrondissement;
    protected List<Intervalle<Double>> intervallesBassinViticole;

    public void setMoyenneParDecoupage(TypeDecoupageGeo typeDecoupageGeo, Map<String, Double> values) {
        switch(typeDecoupageGeo) {
            case REGION -> moyenneParRegion = values;
            case ANCIENNE_REGION -> moyenneParAncienneRegion = values;
            case DEPARTEMENT -> moyenneParDepartement = values;
            case ARRONDISSEMENT -> moyenneParArrondissement = values;
            case BASSIN_VITICOLE -> moyenneParBassinViticole = values;
            default -> {}
        }
    }

    public void setIntervalles(TypeDecoupageGeo typeDecoupageGeo, List<Intervalle<Double>> values) {
        switch(typeDecoupageGeo) {
            case REGION -> intervallesRegion = values;
            case ANCIENNE_REGION -> intervallesAncienneRegion = values;
            case DEPARTEMENT -> intervallesDepartement = values;
            case ARRONDISSEMENT -> intervallesArrondissement = values;
            case BASSIN_VITICOLE -> intervallesBassinViticole = values;
            default -> {}
        }
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public SortType getSortType() {
        return sortType;
    }

    public void setSortType(SortType sortType) {
        this.sortType = sortType;
    }

    public Map<String, Double> getMoyenneParRegion() {
        return moyenneParRegion;
    }

    public Map<String, Double> getMoyenneParAncienneRegion() {
        return moyenneParAncienneRegion;
    }

    public Map<String, Double> getMoyenneParDepartement() {
        return moyenneParDepartement;
    }

    public Map<String, Double> getMoyenneParArrondissement() {
        return moyenneParArrondissement;
    }

    public Map<String, Double> getMoyenneParBassinViticole() {
        return moyenneParBassinViticole;
    }

    public List<Intervalle<Double>> getIntervallesRegion() {
        return intervallesRegion;
    }

    public List<Intervalle<Double>> getIntervallesAncienneRegion() {
        return intervallesAncienneRegion;
    }

    public List<Intervalle<Double>> getIntervallesDepartement() {
        return intervallesDepartement;
    }

    public List<Intervalle<Double>> getIntervallesArrondissement() {
        return intervallesArrondissement;
    }

    public List<Intervalle<Double>> getIntervallesBassinViticole() {
        return intervallesBassinViticole;
    }
}
