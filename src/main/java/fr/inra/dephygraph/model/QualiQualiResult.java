package fr.inra.dephygraph.model;

import java.util.List;

public class QualiQualiResult extends ResultWithCoordinatesAndSectors {
    protected List<DoubleRepartitionData> repartition;

    public List<DoubleRepartitionData> getRepartition() {
        return repartition;
    }

    public void setRepartition(List<DoubleRepartitionData> repartition) {
        this.repartition = repartition;
    }
}
