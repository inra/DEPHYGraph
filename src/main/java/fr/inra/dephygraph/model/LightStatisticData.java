package fr.inra.dephygraph.model;

public class LightStatisticData {
    protected String valueName;
    protected double average;
    protected double median;
    protected double min;
    protected double max;
    protected double firstQuartile;
    protected double thirdQuartile;
    protected double firstDecile;
    protected double ninthDecile;
    protected Double[] values;
    protected long systemsCount;

    public static LightStatisticData from(StatisticData data) {
        LightStatisticData lightData = new LightStatisticData();
        lightData.valueName = data.getValueName();
        lightData.average = data.getAverage();
        lightData.median = data.getMedian();
        lightData.min = data.getMin();
        lightData.max = data.getMax();
        lightData.firstQuartile = data.getFirstQuartile();
        lightData.thirdQuartile = data.getThirdQuartile();
        lightData.firstDecile = data.getFirstDecile();
        lightData.ninthDecile = data.getNinthDecile();
        lightData.values = data.getValues();
        lightData.systemsCount = data.getSystemes().size();
        return lightData;
    }

    public String getValueName() {
        return valueName;
    }

    public void setValueName(String valueName) {
        this.valueName = valueName;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public double getMedian() {
        return median;
    }

    public void setMedian(double median) {
        this.median = median;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public double getFirstQuartile() {
        return firstQuartile;
    }

    public void setFirstQuartile(double firstQuartile) {
        this.firstQuartile = firstQuartile;
    }

    public double getThirdQuartile() {
        return thirdQuartile;
    }

    public void setThirdQuartile(double thirdQuartile) {
        this.thirdQuartile = thirdQuartile;
    }

    public double getFirstDecile() {
        return firstDecile;
    }

    public void setFirstDecile(double firstDecile) {
        this.firstDecile = firstDecile;
    }

    public double getNinthDecile() {
        return ninthDecile;
    }

    public void setNinthDecile(double ninthDecile) {
        this.ninthDecile = ninthDecile;
    }

    public Double[] getValues() {
        return values;
    }

    public void setValues(Double[] values) {
        this.values = values;
    }

    public long getSystemsCount() {
        return systemsCount;
    }

    public void setSystemsCount(long systemsCount) {
        this.systemsCount = systemsCount;
    }
}
