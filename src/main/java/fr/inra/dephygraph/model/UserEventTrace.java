package fr.inra.dephygraph.model;

import java.util.Map;

public class UserEventTrace {
    protected String topic;
    protected Map<String, Object> screen;
    protected Map<String, Object> data;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public Map<String, Object> getScreen() {
        return screen;
    }

    public void setScreen(Map<String, Object> screen) {
        this.screen = screen;
    }
}
