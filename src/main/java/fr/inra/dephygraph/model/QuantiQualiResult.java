package fr.inra.dephygraph.model;

import java.util.List;

public class QuantiQualiResult extends ResultWithCoordinatesAndSectors {
    protected List<LightStatisticData> repartition;

    public List<LightStatisticData> getRepartition() {
        return repartition;
    }

    public void setRepartition(List<LightStatisticData> repartition) {
        this.repartition = repartition;
    }
}
