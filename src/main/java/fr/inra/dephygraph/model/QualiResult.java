package fr.inra.dephygraph.model;

import java.util.List;

public class QualiResult extends ResultWithCoordinatesAndSectors {
    protected List<SimpleRepartitionData> repartition;

    public List<SimpleRepartitionData> getRepartition() {
        return repartition;
    }

    public void setRepartition(List<SimpleRepartitionData> repartition) {
        this.repartition = repartition;
    }
}
