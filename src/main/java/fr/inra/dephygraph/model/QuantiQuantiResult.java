package fr.inra.dephygraph.model;

import java.util.List;

public class QuantiQuantiResult extends ResultWithCoordinatesAndSectors {
    protected List<CoupleValuesData> repartition;

    public List<CoupleValuesData> getRepartition() {
        return repartition;
    }

    public void setRepartition(List<CoupleValuesData> repartition) {
        this.repartition = repartition;
    }
}
