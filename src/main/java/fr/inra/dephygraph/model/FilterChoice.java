package fr.inra.dephygraph.model;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class FilterChoice extends Metadata {

    protected List<String> values;

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public void addValue(String value) {
        if (this.values == null) {
            this.values = new ArrayList<>();
        }
        this.values.add(value);
    }

    public void copyMetadata(Metadata fromMetadata) {
        this.columnname = fromMetadata.getColumnname();
        this.displayname = fromMetadata.getDisplayname();
        this.unity = fromMetadata.getUnity();
        this.quantitative = fromMetadata.isQuantitative();
        this.qualitative = fromMetadata.isQualitative();
        this.filter = fromMetadata.isFilter();
        this.separator = fromMetadata.getSeparator();
        this.hidden = fromMetadata.isHidden();
        this.availabilitybysector = fromMetadata.getAvailabilitybysector();
    }

    public static FilterChoice fromMetadata(Metadata metadata) {
        FilterChoice filterChoice = new FilterChoice();
        filterChoice.copyMetadata(metadata);
        return filterChoice;
    }
}
