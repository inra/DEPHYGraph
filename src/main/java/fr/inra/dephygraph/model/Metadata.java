package fr.inra.dephygraph.model;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class Metadata {

    protected UUID id;

    // Name of the column in Data table
    protected String columnname;
    // Name to display in UI
    protected String displayname;
    // Unity of the column, should be null
    protected String unity;
    // Define if data is quantitative
    protected boolean quantitative;
    // Define if data is qualitative
    protected boolean qualitative;
    // Define if data could be used as filter
    protected boolean filter;
    // Define if data could be used as filter for logged account
    protected boolean irfilter;
    // Define a separator is column could contain several data. If null, unique data
    protected String separator;
    // Define the category of the metadata : usefull to sort them
    protected String category;
    // Define is the data should be hidden on UI
    protected boolean hidden;
    // Define if the variable is displayed with a higher priority
    protected boolean recommendedquery;
    // Define if the filter is displayed with a higher priority
    protected boolean recommendedfilter;
    // Comma separated list of sectors
    protected String availabilitybysector;
    // Definition of the variable
    protected String vardef;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getColumnname() {
        return columnname;
    }

    public void setColumnname(String columnname) {
        this.columnname = columnname;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getUnity() {
        return unity;
    }

    public void setUnity(String unity) {
        this.unity = unity;
    }

    public boolean isQualitative() {
        return qualitative;
    }

    public void setQualitative(boolean qualitative) {
        this.qualitative = qualitative;
    }

    public boolean isQuantitative() {
        return quantitative;
    }

    public void setQuantitative(boolean quantitative) {
        this.quantitative = quantitative;
    }

    public boolean isFilter() {
        return filter;
    }

    public void setFilter(boolean filter) {
        this.filter = filter;
    }

    public boolean isIrfilter() {
        return irfilter;
    }

    public void setIrfilter(boolean irfilter) {
        this.irfilter = irfilter;
    }

    public String getSeparator() {
        return separator;
    }

    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public boolean isMultiple() {
        return StringUtils.isNotBlank(this.separator);
    }

    public boolean isRecommendedquery() {
        return recommendedquery;
    }

    public void setRecommendedquery(boolean recommendedquery) {
        this.recommendedquery = recommendedquery;
    }

    public String getAvailabilitybysector() {
        return availabilitybysector;
    }

    public void setAvailabilitybysector(String availabilitybysector) {
        this.availabilitybysector = availabilitybysector;
    }

    public boolean isRecommendedfilter() {
        return recommendedfilter;
    }

    public void setRecommendedfilter(boolean recommendedfilter) {
        this.recommendedfilter = recommendedfilter;
    }

    public String getVardef() {
        return vardef;
    }

    public void setVardef(String vardef) {
        this.vardef = vardef;
    }
}
