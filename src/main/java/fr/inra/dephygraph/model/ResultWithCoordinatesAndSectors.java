package fr.inra.dephygraph.model;

import java.util.Set;
import java.util.stream.Collectors;

public abstract class ResultWithCoordinatesAndSectors {
    protected Set<Sector> sectors;
    protected String sectorsAvailabilityDescription;
    protected Set<Coordinates> coordinates;
    protected DistributionEffectif distributionEffectif = new DistributionEffectif();
    protected VariableGeoDistribution variableYGeoDistribution = new VariableGeoDistribution();
    protected VariableGeoDistribution variableXGeoDistribution = new VariableGeoDistribution();
    protected long effectifTotal;
    protected String lastAgrosystExtractionDate;

    public Set<Sector> getSectors() {
        return sectors;
    }

    public void setSectors(Set<Sector> sectors) {
        this.sectors = sectors;
        if (sectors.size() == Sector.ALL_SECTORS.size()) {
            this.sectorsAvailabilityDescription = "Ces données sont disponibles pour toutes les filières";
        } else if (sectors.size() == 1) {
            String label = sectors.stream().findFirst().get().getLabel();
            this.sectorsAvailabilityDescription =  "Ces données sont disponibles pour la filière " + label;
        } else if (sectors.size() > 1 ){
            String labels = sectors.stream()
                    .map(Sector::getLabel)
                    .collect(Collectors.joining(", "));
            this.sectorsAvailabilityDescription = "Ces données sont disponibles pour les filières " + labels;
        }
    }

    public String getSectorsAvailabilityDescription() {
        return sectorsAvailabilityDescription;
    }

    public Set<Coordinates> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Set<Coordinates> coordinates) {
        this.coordinates = coordinates;
    }

    public DistributionEffectif getDistributionEffectif() {
        return distributionEffectif;
    }

    public void setDistributionEffectif(DistributionEffectif distributionEffectif) {
        this.distributionEffectif = distributionEffectif;
    }

    public VariableGeoDistribution getVariableYGeoDistribution() {
        return variableYGeoDistribution;
    }

    public void setVariableYGeoDistribution(VariableGeoDistribution variableYGeoDistribution) {
        this.variableYGeoDistribution = variableYGeoDistribution;
    }

    public VariableGeoDistribution getVariableXGeoDistribution() {
        return variableXGeoDistribution;
    }

    public void setVariableXGeoDistribution(VariableGeoDistribution variableXGeoDistribution) {
        this.variableXGeoDistribution = variableXGeoDistribution;
    }

    public long getEffectifTotal() {
        return effectifTotal;
    }

    public void setEffectifTotal(long effectifTotal) {
        this.effectifTotal = effectifTotal;
    }

    public String getLastAgrosystExtractionDate() {
        return lastAgrosystExtractionDate;
    }

    public void setLastAgrosystExtractionDate(String lastAgrosystExtractionDate) {
        this.lastAgrosystExtractionDate = lastAgrosystExtractionDate;
    }
}
