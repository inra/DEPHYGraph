package fr.inra.dephygraph.exceptions;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class AuthorizationExceptionMapper implements ExceptionMapper<AuthorizationException> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationExceptionMapper.class);

    @Override
    public Response toResponse(AuthorizationException exception) {
        LOGGER.warn("Authorization failed", exception);
        return Response.status(Response.Status.UNAUTHORIZED).entity(exception).build();
    }
}
