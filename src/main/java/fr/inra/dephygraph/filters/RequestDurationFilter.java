package fr.inra.dephygraph.filters;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

@WebFilter(urlPatterns = {
        "/api/v1/statistics/*"
})
public class RequestDurationFilter implements Filter {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestDurationFilter.class);

    protected String getClientIp(HttpServletRequest request) {
        String clientIp = "";
        if (request != null) {
            clientIp = request.getHeader("X-FORWARDED-FOR");
            if (clientIp == null || "".equals(clientIp)) {
                clientIp = request.getRemoteAddr();
            }
        }
        return clientIp;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain filterChain) throws IOException, ServletException {
        Instant start = Instant.now();
        filterChain.doFilter(request, response);
        Instant finish = Instant.now();
        String pathInfo = ((HttpServletRequest) request).getPathInfo();
        String clientIp = getClientIp((HttpServletRequest)request);
        LOGGER.info("{} - {} ms ({})", pathInfo, Duration.between(start, finish).toMillis(), clientIp);
    }
}
