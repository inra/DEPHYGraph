package fr.inra.dephygraph.providers;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.cache.Cache;
import fr.inra.dephygraph.model.FilterChoice;
import fr.inra.dephygraph.model.Metadata;
import fr.inra.dephygraph.rest.AgrosystInfo;
import jakarta.ws.rs.ext.ParamConverter;
import jakarta.ws.rs.ext.ParamConverterProvider;
import jakarta.ws.rs.ext.Provider;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * @author ymartel (martel@codelutin.com)
 */
@Provider
public class DephyGraphParamConverterProvider implements ParamConverterProvider {

    protected final Cache<String, Metadata> metadataCache;

    public DephyGraphParamConverterProvider(Cache<String, Metadata> metadataCache) {
        this.metadataCache = metadataCache;
    }

    @Override
    public <T> ParamConverter<T> getConverter(Class<T> rawType, Type genericType, Annotation[] annotations) {

        // Specific converter for Metadata param.
        if (rawType.isAssignableFrom(Metadata.class)) {
            return (ParamConverter<T>) new MetadataParamConverter(metadataCache);
        }

        // Specific converter for FilterChoice param.
        if (rawType.isAssignableFrom(FilterChoice.class)) {
            return (ParamConverter<T>) new FilterChoiceParamConverter(metadataCache);
        }

        // Specific converter for AgrosystInfo param.
        if (rawType.isAssignableFrom(AgrosystInfo.class)) {
            return (ParamConverter<T>) new AgrosystInfoParamConverter();
        }

        return null;
    }

}
