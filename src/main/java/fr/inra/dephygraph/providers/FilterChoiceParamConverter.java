package fr.inra.dephygraph.providers;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.cache.Cache;
import fr.inra.dephygraph.model.FilterChoice;
import fr.inra.dephygraph.model.Metadata;
import jakarta.ws.rs.ProcessingException;
import jakarta.ws.rs.ext.ParamConverter;

import java.io.IOException;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class FilterChoiceParamConverter implements ParamConverter<FilterChoice> {

    protected final Cache<String, Metadata> metadataCache;
    protected final ObjectMapper mapper = new ObjectMapper();

    public FilterChoiceParamConverter(Cache<String, Metadata> metadataCache) {
        this.metadataCache = metadataCache;
    }

    @Override
    public FilterChoice fromString(String value) {
        try {
            FilterChoice filterChoice = mapper.readerFor(FilterChoice.class).readValue(value);
            Metadata metadata = metadataCache.getIfPresent(filterChoice.getColumnname());
            if (metadata != null) {
                filterChoice.copyMetadata(metadata);
            }
            return filterChoice;
        } catch (IOException e) {
            throw new ProcessingException(e);
        }
    }

    @Override
    public String toString(FilterChoice value) {
        try {
            return mapper.writer().writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new ProcessingException(e);
        }
    }
}
