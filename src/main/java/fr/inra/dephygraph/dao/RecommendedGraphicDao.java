package fr.inra.dephygraph.dao;

import fr.inra.dephygraph.model.RecommendedGraphic;
import org.jdbi.v3.core.Handle;

import java.util.List;

public class RecommendedGraphicDao {

    protected final Handle handle;

    public RecommendedGraphicDao(Handle handle) {
        this.handle = handle;
    }

    public List<RecommendedGraphic> getRecommendedGraphics() {
        String sql = "SELECT * FROM recommendedgraphic";
        List<RecommendedGraphic> result = handle.createQuery(sql)
                .mapToBean(RecommendedGraphic.class)
                .list();
        return result;
    }
}
