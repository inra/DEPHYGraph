package fr.inra.dephygraph.dao;

import fr.inra.dephygraph.rest.AgrosystInfo;
import org.jdbi.v3.core.Handle;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

public class AgrosystInfoDao {

    protected final Handle handle;

    public AgrosystInfoDao(Handle handle) {
        this.handle = handle;
    }

    public void saveInfo(AgrosystInfo agrosystInfo) throws Exception {
        String sql = "INSERT INTO agrosystInfo (id, dephyNb, expirationDate) VALUES (:id, :dephyNb, :expirationDate)";
        handle.createUpdate(sql)
                .bind("id", agrosystInfo.getId())
                .bindArray("dephyNb", String.class, agrosystInfo.getAllowedDephyNb())
                .bind("expirationDate", agrosystInfo.getExpirationDate())
                .execute();
    }

    public List<String> retrieveDephyNb(UUID id) throws Exception {
        String sql = "SELECT unnest(dephyNb) FROM agrosystInfo where id = :id AND expirationDate > :now";
        List<String> result = handle.createQuery(sql)
                .bind("id", id)
                .bind("now", OffsetDateTime.now())
                .mapTo(String.class)
                .list();
        return result;
    }

    public AgrosystInfo retrieveInfo(UUID id) throws Exception {
        String sql = "SELECT id, dephyNb as allowedDephyNb FROM agrosystInfo where id = :id AND expirationDate > :now";
        AgrosystInfo result = handle.createQuery(sql)
                .bind("id", id)
                .bind("now", OffsetDateTime.now())
                .mapToBean(AgrosystInfo.class)
                .findOne()
                .orElse(null);
        return result;
    }

    public int removeExpiredEntries() throws Exception {
        String sql = "DELETE FROM agrosystInfo where expirationDate < :now";
        int result = handle.createUpdate(sql)
                .bind("now", OffsetDateTime.now())
                .execute();
        return result;
    }
}
