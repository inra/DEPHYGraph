package fr.inra.dephygraph.dao;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2022 Inrae
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import fr.inra.dephygraph.model.Coordinates;
import fr.inra.dephygraph.model.CoupleValuesData;
import fr.inra.dephygraph.model.Data;
import fr.inra.dephygraph.model.DoubleRepartitionData;
import fr.inra.dephygraph.model.FilterChoice;
import fr.inra.dephygraph.model.FilterCohort;
import fr.inra.dephygraph.model.Metadata;
import fr.inra.dephygraph.model.Pz0;
import fr.inra.dephygraph.model.SimpleRepartitionData;
import fr.inra.dephygraph.model.SpecificValue;
import fr.inra.dephygraph.model.StatisticData;
import fr.inra.dephygraph.model.SystemeWithYXValues;
import fr.inra.dephygraph.model.TypeDecoupageGeo;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.argument.AbstractArgumentFactory;
import org.jdbi.v3.core.argument.Argument;
import org.jdbi.v3.core.config.ConfigRegistry;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.Query;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DatamartDao {

    protected final Handle handle;

    static class WithDataMapping {
        protected final TypeReference<List<Coordinates>> coordinatesListType = new TypeReference<>() {};
        protected final TypeReference<List<SystemeWithYXValues>> systemesListType = new TypeReference<>() {};
        protected final ObjectMapper mapper = new ObjectMapper();

        protected void initData(Data data, ResultSet rs) throws Exception {
            data.setValuename(rs.getString("valueName"));
            data.setCoordinates(getCoordinates(rs));
        }

        protected List<Coordinates> getCoordinates(ResultSet rs) throws Exception {
            if (rs.getObject("coordinates") != null) {
                String json = rs.getObject("coordinates").toString();
                List<Coordinates> coordinates = mapper.readValue(json, coordinatesListType);
                return coordinates;
            } else {
                return Collections.emptyList();
            }
        }

        public List<SystemeWithYXValues> getSystemes(ResultSet rs) throws Exception {
            if (rs.getObject("systems") != null) {
                String json = rs.getObject("systems").toString();
                List<SystemeWithYXValues> systemes = mapper.readValue(json, systemesListType);
                return systemes;
            } else {
                return Collections.emptyList();
            }
        }

        protected Double[] getValues(ResultSet rs) throws Exception {
            if (rs.getObject("values") != null) {
                String json = rs.getObject("values").toString();
                Double[] values = mapper.readValue(json, Double[].class);
                return values;
            } else {
                return new Double[0];
            }
        }

        protected List<String> getStringListFromArray(Array array) throws Exception {
            Object[] data = (Object[]) array.getArray();
            List<String> result = Arrays.stream(data).map(o -> {
                if (o != null) {
                    return o.toString();
                } else {
                    return "";
                }
            }).collect(Collectors.toList());
            return result;
        }
    }

    public static final class SimpleRepartitionDataMapper extends WithDataMapping implements RowMapper<SimpleRepartitionData> {
        @Override
        public SimpleRepartitionData map(ResultSet rs, StatementContext ctx) throws SQLException {
            try {
                SimpleRepartitionData data = new SimpleRepartitionData();
                initData(data, rs);
                data.setValueOccurrence(rs.getInt("valueOccurrence"));
                data.setSystemes(getSystemes(rs));
                return data;
            } catch (Exception e) {
                throw new SQLException(e);
            }
        }
    }

    public static final class StatisticDataMapper extends WithDataMapping implements RowMapper<StatisticData> {
        @Override
        public StatisticData map(ResultSet rs, StatementContext ctx) throws SQLException {
            try {
                StatisticData data = new StatisticData();
                initData(data, rs);
                data.setAverage(rs.getDouble("average"));
                data.setMin(rs.getDouble("min"));
                data.setMax(rs.getDouble("max"));
                data.setFirstQuartile(rs.getDouble("firstQuartile"));
                data.setThirdQuartile(rs.getDouble("thirdQuartile"));
                data.setFirstDecile(rs.getDouble("firstDecile"));
                data.setNinthDecile(rs.getDouble("ninthDecile"));
                data.setMedian(rs.getDouble("median"));
                data.setValues(getValues(rs));
                data.setSystemes(getSystemes(rs));
                return data;
            } catch (Exception e) {
                throw new SQLException(e);
            }
        }
    }

    public static final class DoubleRepartitionDataMapper extends WithDataMapping implements RowMapper<DoubleRepartitionData> {
        @Override
        public DoubleRepartitionData map(ResultSet rs, StatementContext ctx) throws SQLException {
            try {
                DoubleRepartitionData data = new DoubleRepartitionData();
                initData(data, rs);
                data.setValueOccurrence(rs.getInt("valueOccurrence"));
                if (rs.getObject("innerRepartition") != null) {
                    String json = rs.getObject("innerRepartition").toString();
                    SimpleRepartitionData[] innerRepartition = mapper.readValue(json, SimpleRepartitionData[].class);
                    Arrays.sort(innerRepartition, Comparator.comparing(Data::getValueName));
                    data.setInnerRepartition(innerRepartition);
                } else {
                    data.setInnerrepartition(new SimpleRepartitionData[0]);
                }
                data.setSystemes(getSystemes(rs));
                return data;
            } catch (Exception e) {
                throw new SQLException(e);
            }
        }
    }

    public static final class CoupleValuesDataMapper extends WithDataMapping implements RowMapper<CoupleValuesData> {
        @Override
        public CoupleValuesData map(ResultSet rs, StatementContext ctx) throws SQLException {
            try {
                CoupleValuesData data = new CoupleValuesData();
                initData(data, rs);
                data.setAbscissa(rs.getDouble("abscissa"));
                data.setOrdinate(rs.getDouble("ordinate"));
                data.setWeight(rs.getInt("weight"));
                data.setSystemes(getSystemes(rs));
                return data;
            } catch (Exception e) {
                throw new SQLException(e);
            }
        }
    }

    static final class SpecificValueMapper extends WithDataMapping implements RowMapper<SpecificValue> {
        private final boolean withXValue;
        private final boolean withYValue;

         SpecificValueMapper() {
             this(false);
         }

         SpecificValueMapper(boolean withYValue) {
             this.withXValue = true;
             this.withYValue = withYValue;
         }

         SpecificValueMapper(boolean withXValue, boolean withYValue) {
             this.withXValue = withXValue;
             this.withYValue = withYValue;
         }

        @Override
        public SpecificValue map(ResultSet rs, StatementContext ctx) throws SQLException {
             try {
                 SpecificValue value = new SpecificValue();
                 if (withXValue) {
                     value.setXvalue(getStringListFromArray(rs.getArray("xValue")));
                 }
                 if (withYValue) {
                     value.setYvalue(getStringListFromArray(rs.getArray("yValue")));
                 }
                 value.setSpecific(rs.getString("specific"));
                 Coordinates coordinates = mapper.readerFor(Coordinates.class).readValue(rs.getString("coordinates"));
                 value.setCoordinates(coordinates);
                 return value;
             } catch (Exception e) {
                 throw new SQLException(e);
             }
        }
    }

    static final class FilterChoiceValuesArgumentFactory extends AbstractArgumentFactory<FilterChoice> {
        FilterChoiceValuesArgumentFactory() {
            super(Types.ARRAY);
        }

        @Override
        protected Argument build(FilterChoice value, ConfigRegistry config) {
            return (position, statement, ctx) -> {
                Array sqlArray = statement.getConnection().createArrayOf("varchar", value.getValues().toArray());
                Objects.requireNonNull(sqlArray);
                ctx.addCleanable(sqlArray::free);
                statement.setArray(position, sqlArray);
            };
        }
    }

    public DatamartDao(Handle handle) {
        this.handle = handle;
        handle.registerArgument(new FilterChoiceValuesArgumentFactory());
    }

    public List<Metadata> getMetadata(boolean isQuantitative, boolean isQualitative, boolean isFilter) {
        String sql = "SELECT * FROM " + Metadata.class.getSimpleName() +
                " WHERE qualitative = :isQualitative " +
                " AND quantitative = :isQuantitative " +
                " AND filter = :isFilter " +
                " AND hidden = false ";
        List<Metadata> result = handle.createQuery(sql)
                .bind("isQualitative", isQualitative)
                .bind("isQuantitative", isQuantitative)
                .bind("isFilter", isFilter)
                .mapToBean(Metadata.class)
                .list();
        return result;
    }

    public List<Metadata> getAllMetadata() {
        String sql = "SELECT * FROM " + Metadata.class.getSimpleName() +
                " ORDER BY category ASC, displayname ASC ";
        List<Metadata> result = handle.createQuery(sql)
                .mapToBean(Metadata.class)
                .list();
        return result;
    }

    public List<Metadata> getAllVisibleMetadata() {
        String sql = "SELECT * FROM " + Metadata.class.getSimpleName() +
                " WHERE (qualitative = true OR quantitative = true) " +
                " AND hidden = false " +
                " ORDER BY category ASC, displayname ASC ";
        List<Metadata> result = handle.createQuery(sql)
                .mapToBean(Metadata.class)
                .list();
        return result;
    }

    public List<Metadata> getAllQualitativeMetadata() {
        String sql = "SELECT * FROM " + Metadata.class.getSimpleName() +
                " WHERE qualitative = true " +
                " AND hidden = false " +
                " ORDER by category ASC, displayname ASC";
        List<Metadata> result = handle.createQuery(sql)
                .mapToBean(Metadata.class)
                .list();
        return result;
    }

    public Metadata getMetadataByColumnName(String columnName) {
        String sql = "SELECT * FROM " + Metadata.class.getSimpleName() +
                " WHERE columnname = :columnName";
        Metadata result = handle.createQuery(sql)
                .bind("columnName", columnName)
                .mapToBean(Metadata.class)
                .findOne()
                .orElse(null);
        return result;
    }

    public List<String> getQualitativeMetadataValues(String columnName) {
        String sql = "SELECT DISTINCT(" + columnName + ") " +
                " FROM data d " +
                " WHERE " + columnName + "::text IS NOT NULL " +
                " AND " + columnName + "::text <> 'N/A' " +
                " AND EXISTS (" +
                "  SELECT 1 FROM metadata m WHERE m.columnname = :columnname " +
                "  AND m.qualitative = true " +
                " ) " +
                " ORDER BY " + columnName + " ASC ";
        List<String> values = handle.createQuery(sql)
                .bind("columnname", columnName)
                .mapTo(String.class)
                .list();
        return values;
    }

    public List<Metadata> getAllIrFilterMetadata() {
        String sql = "SELECT * FROM " + Metadata.class.getSimpleName() + " "
                + " WHERE irfilter = true "
                + " ORDER By displayname ASC";
        List<Metadata> list = handle.createQuery(sql)
                .mapToBean(Metadata.class)
                .list();
        return list;
    }

    public List<String> getFilterValues(Metadata metadata) {
        List<String> values;
        String columnname = metadata.getColumnname();
        if (StringUtils.isBlank(metadata.getSeparator())) {
            String sql = "SELECT distinct(" + columnname + "::text) FROM data WHERE " + columnname + " IS NOT NULL";
            values = handle.createQuery(sql)
                    .mapTo(String.class)
                    .list();
        } else {
            // Manage possible multiple values : create query like
            // select distinct(trim(values)) from ( select regexp_split_to_table(coalesce(COLUMN, ''), 'SEPARATOR') as values from data ) as valueTable;
            StringBuilder sqlBuilder = new StringBuilder("SELECT distinct(trim(values)) ");
            sqlBuilder.append(" FROM ( select regexp_split_to_table(")
                    .append(columnname)
                    .append(", '")
                    .append(metadata.getSeparator())
                    .append("') as values from data ")
                    .append(" WHERE " + columnname + " IS NOT NULL ")
                    .append(" ) as valueTable");
            String sql = sqlBuilder.toString();
            values = handle.createQuery(sql)
                    .mapTo(String.class)
                    .list();
        }

        return values;
    }

    public List<Metadata> getAllFilterMetadata() {
        String sql = "SELECT * FROM " + Metadata.class.getSimpleName() + " "
                + " WHERE filter = true "
                + " AND hidden = false "
                + " ORDER By displayname ASC";
        List<Metadata> result = handle.createQuery(sql)
                .mapToBean(Metadata.class)
                .list();
        return result;
    }

    protected boolean isPz0Variable(Metadata var) {
        return var.getColumnname().compareToIgnoreCase(Pz0.METADATA_COLUMN_NAME) == 0;
    }

    protected String getCohortYearsPredicateWithoutPz0(String extraPredicate) {
        String cohortYearsPredicate = " AND d1.dephynb IN ( " +
                " SELECT d2.dephynb FROM data d2 " +
                " WHERE d2.c104_campaign_dis::int in (<cohortYears>) AND " +
                extraPredicate +
                " GROUP BY d2.dephynb " +
                " HAVING COUNT(d2.dephynb) = :cohortYearsCount " +
                " ) " +
                " AND d1.c104_campaign_dis::int in (<cohortYears>) ";

        return cohortYearsPredicate;
    }

    protected String getCohortYearsPredicateWithPz0(String extraPredicate, Map<String, Object> parameters) {
        // Voir ticket #11704 pour l'explication du cas particulier de la présence dans le réseau avec les cohortes
        String cohortYearsPredicate = " AND d1.dephynb IN ( " +
                " SELECT d2.dephynb FROM data d2 " +
                " WHERE d2.c104_campaign_dis::int in (<cohortYears>) AND " +
                " d2.c102_pz0 = :dansReseauDephy AND " +
                extraPredicate +
                " AND EXISTS (" +
                "   SELECT 1 FROM data d3 WHERE d3.dephynb = d2.dephynb AND d3.c102_pz0 = :avantReseauDephy AND " + extraPredicate +
                " ) " +
                " GROUP BY d2.dephynb " +
                " HAVING COUNT(d2.dephynb) = :cohortYearsCount " +
                " ) " +
                " AND (" +
                " d1.c104_campaign_dis::int in (<cohortYears>) " +
                " OR d1.c102_pz0 = :avantReseauDephy " +
                " ) ";

        parameters.put("dansReseauDephy", Pz0.SYSTEME_DANS_RESEAU_DEPHY.getLabel());
        parameters.put("avantReseauDephy", Pz0.SYSTEME_AVANT_ENTREE_DANS_RESEAU_DEPHY.getLabel());

        return cohortYearsPredicate;
    }

    protected String getCohortNetworkAgesPredicateWithoutPz0(String extraPredicate) {
        String cohortNetworkAgesPredicate = " AND d1.dephynb IN ( " +
                " SELECT d2.dephynb FROM data d2 " +
                " WHERE d2.c103_networkyears in (<cohortNetworkAges>) AND " +
                extraPredicate +
                " GROUP BY d2.dephynb " +
                " HAVING COUNT(d2.dephynb) = :cohortNetworkAgesCount " +
                " ) " +
                " AND d1.c103_networkyears in (<cohortNetworkAges>) ";

        return cohortNetworkAgesPredicate;
    }

    protected String getCohortNetworkAgesPredicateWithPz0(String extraPredicate, Map<String, Object> parameters) {
        // Voir ticket #11704 pour l'explication du cas particulier de la présence dans le réseau avec les cohortes
        String cohortNetworkAgesPredicate = " AND d1.dephynb IN ( " +
                " SELECT d2.dephynb FROM data d2 " +
                " WHERE d2.c103_networkyears in (<cohortNetworkAges>) AND " +
                " d2.c102_pz0 = :dansReseauDephy AND " +
                extraPredicate +
                " AND EXISTS (" +
                "   SELECT 1 FROM data d3 WHERE d3.dephynb = d2.dephynb AND d3.c102_pz0 = :avantReseauDephy AND " + extraPredicate +
                " ) " +
                " GROUP BY d2.dephynb " +
                " HAVING COUNT(d2.dephynb) = :cohortNetworkAgesCount " +
                " ) " +
                " AND (" +
                " d1.c103_networkyears in (<cohortNetworkAges>) " +
                " OR d1.c102_pz0 = :avantReseauDephy " +
                " ) ";

        parameters.put("dansReseauDephy", Pz0.SYSTEME_DANS_RESEAU_DEPHY.getLabel());
        parameters.put("avantReseauDephy", Pz0.SYSTEME_AVANT_ENTREE_DANS_RESEAU_DEPHY.getLabel());

        return cohortNetworkAgesPredicate;
    }

    public List<SimpleRepartitionData> getQualiDistribution(Metadata yVar, List<FilterChoice> filterChoices,
                                                            FilterCohort filterCohort,
                                                            int minDataRequired, int minDataRequiredForCohort) {
        if (!yVar.isQualitative()) {
            return Collections.emptyList();
        }

        String yVarSeparator = Optional.ofNullable(yVar.getSeparator()).orElse("@");

        String yVarSelector = String.format("" +
                        "trim (regexp_split_to_table(TRIM (COALESCE(%s::text, 'N/A') ), '%s')) as yvar",
                yVar.getColumnname(), yVarSeparator);

        Map<String, Object> parameters = new HashMap<>();
        Pair<String, Map<String, Object>> filterAndClause = createFilterAndClause(filterChoices, parameters);

        Optional<Set<Integer>> maybeCohortYears = Optional.ofNullable(filterCohort)
                .map(FilterCohort::getYears)
                .filter(CollectionUtils::isNotEmpty);
        Optional<Set<String>> maybeCohortNetworkAges = Optional.ofNullable(filterCohort)
                .map(FilterCohort::getNetworkAges)
                .filter(CollectionUtils::isNotEmpty);

        String varExtraPredicateForCohortPredicate = yVar.getColumnname() + "::text <> 'N/A' " + filterAndClause.getLeft();

        String varCohortYearsPredicate;
        if (maybeCohortYears.isPresent()) {
            parameters.put("cohortYearsCount", maybeCohortYears.get().size());
            if (isPz0Variable(yVar)) {
                varCohortYearsPredicate = getCohortYearsPredicateWithPz0(varExtraPredicateForCohortPredicate, parameters);
            } else {
                varCohortYearsPredicate = getCohortYearsPredicateWithoutPz0(varExtraPredicateForCohortPredicate);
            }
        } else {
            varCohortYearsPredicate = "";
        }

        String varCohortNetworkAgesPredicate;
        if (maybeCohortNetworkAges.isPresent()) {
            parameters.put("cohortNetworkAgesCount", maybeCohortNetworkAges.get().size());
            if (isPz0Variable(yVar)) {
                varCohortNetworkAgesPredicate = getCohortNetworkAgesPredicateWithPz0(varExtraPredicateForCohortPredicate, parameters);
            } else {
                varCohortNetworkAgesPredicate = getCohortNetworkAgesPredicateWithoutPz0(varExtraPredicateForCohortPredicate);
            }
        } else {
            varCohortNetworkAgesPredicate = "";
        }

        String yDataViewQuery = " ydataview AS ( SELECT " +
                " d1.dephynb " +
                " , latitude " +
                " , longitude " +
                " , c104_campaign_dis as campaign " +
                " , " + TypeDecoupageGeo.REGION.getColumnName() + " as region " +
                " , " + TypeDecoupageGeo.ANCIENNE_REGION.getColumnName() + " as ancienneRegion " +
                " , " + TypeDecoupageGeo.DEPARTEMENT.getColumnName() + " as departement " +
                " , " + TypeDecoupageGeo.ARRONDISSEMENT.getColumnName() + " as arrondissement " +
                " , " + TypeDecoupageGeo.BASSIN_VITICOLE.getColumnName() + " as bassin " +
                " , " + yVarSelector +
                " FROM data d1 " +
                " WHERE 1 = 1 " + filterAndClause.getLeft() +
                varCohortYearsPredicate +
                varCohortNetworkAgesPredicate +
                " ) ";

        Optional<FilterChoice> maybeYvarFilter = findFilterChoiceForMetadata(filterChoices, yVar);

        String variableWhereClause = " WHERE yvar <> 'N/A' ";
        if (maybeYvarFilter.isPresent() && maybeYvarFilter.get().isMultiple()) {
            variableWhereClause += " AND yvar in (<filteredYVar>) ";
        } else {
            maybeYvarFilter = Optional.empty();
        }

        String innerRepartitionQuery = " SELECT " +
                " yvar, count(yvar) as countpery " +
                " , jsonb_agg(json_build_object('latitude', latitude, 'longitude', longitude)) as coordinates " +
                " , jsonb_agg(json_build_object(" +
                "    'dephyNb', ydataview.dephynb, " +
                "    'campaign', ydataview.campaign, " +
                "    'region', ydataview.region, " +
                "    'ancienneRegion', ydataview.ancienneRegion, " +
                "    'departement', ydataview.departement, " +
                "    'arrondissement', ydataview.arrondissement, " +
                "    'bassinViticole', ydataview.bassin, " +
                "    'yVal', NULL, " +
                "    'xVal', NULL )) as systems " +
                " FROM ydataview " +
                variableWhereClause +
                " GROUP BY yvar ";

        String minDataRequiredClause;
        if (isPz0Variable(yVar)) {
            minDataRequiredClause = "";
        } else {
            minDataRequiredClause = " WHERE countpery >= :minDataRequired ";
            parameters.put("minDataRequired", minDataRequired);
        }
        String q = "WITH " + yDataViewQuery;
        q += " SELECT " +
                " yvar as valuename " +
                " , countpery as valueoccurrence " +
                " , jsonb_distinct_text(coordinates) as coordinates " +
                " , systems " +
                " FROM ( " + innerRepartitionQuery + " ) t " +
                minDataRequiredClause +
                " ORDER BY yvar ";

        parameters.put("minDataRequired", minDataRequired);

        Query query = handle.createQuery(q).bindMap(parameters);
        maybeCohortYears.ifPresent(years -> query.bindList("cohortYears", years));
        maybeCohortNetworkAges.ifPresent(ages -> query.bindList("cohortNetworkAges", ages));
        maybeYvarFilter.ifPresent(filterChoice -> query.bindList("filteredYVar", filterChoice.getValues()));

        List<SimpleRepartitionData> result = query.map(new SimpleRepartitionDataMapper()).list();
        if (isPz0Variable(yVar)) {
            Map<String, Integer> resultMap = result.stream().collect(Collectors.toMap(Data::getValueName, d -> d.getSystemes().size()));
            if (resultMap.get(Pz0.SYSTEME_AVANT_ENTREE_DANS_RESEAU_DEPHY.getLabel()) < minDataRequiredForCohort
                    || resultMap.get(Pz0.SYSTEME_DANS_RESEAU_DEPHY.getLabel()) < minDataRequired) {
                result.clear();
            }
        }

        return result;
    }

    public StatisticData getQuantiDistribution(Metadata quantitative, List<FilterChoice> filterChoices,
                                               FilterCohort filterCohort, int minDataRequired) {
        if (!quantitative.isQuantitative()) {
            return null;
        }

        Map<String, Object> parameters = new HashMap<>();
        Pair<String, Map<String, Object>> filterAndClause = createFilterAndClause(filterChoices, parameters);

        Optional<Set<Integer>> maybeCohortYears = Optional.ofNullable(filterCohort)
                .map(FilterCohort::getYears)
                .filter(CollectionUtils::isNotEmpty);
        Optional<Set<String>> maybeCohortNetworkAges = Optional.ofNullable(filterCohort)
                .map(FilterCohort::getNetworkAges)
                .filter(CollectionUtils::isNotEmpty);

        String yVar = quantitative.getColumnname();

        String cohortYearsJoin = " JOIN ( " +
                " SELECT d2.dephynb, count(d2.dephynb) FROM data d2 " +
                " WHERE d2.c104_campaign_dis::int in (<cohortYears>) AND " + yVar + " <> numeric 'NaN' " +
                filterAndClause.getLeft() +
                " GROUP BY d2.dephynb " +
                " HAVING COUNT(d2.dephynb) = :cohortYearsCount " +
                " ) cy " +
                " ON cy.dephynb = d1.dephynb AND d1.c104_campaign_dis::int in (<cohortYears>) ";

        String cohortNetworkAgesJoin = " JOIN ( " +
                " SELECT d2.dephynb, count(d2.dephynb) FROM data d2 " +
                " WHERE d2.c103_networkyears in (<cohortNetworkAges>) AND " + yVar + " <> numeric 'NaN' " +
                filterAndClause.getLeft() +
                " GROUP BY d2.dephynb " +
                " HAVING COUNT(d2.dephynb) = :cohortNetworkAgesCount " +
                " ) cna " +
                " ON cna.dephynb = d1.dephynb AND d1.c103_networkyears in (<cohortNetworkAges>) ";

        String innerQuery = " SELECT " +
                " $$" + quantitative.getColumnname() + "$$ as valueName " +
                " , " + String.format("ROUND(AVG(%s::numeric), 2) AS average ", yVar) +
                " , " + String.format("ROUND(MIN(%s::numeric), 2) AS min ", yVar) +
                " , " + String.format("ROUND(MAX(%s::numeric), 2) AS max ", yVar) +
                " , " + String.format("ROUND(PERCENTILE_DISC(0.25) WITHIN GROUP (ORDER BY %s::numeric), 2) AS firstQuartile", yVar) +
                " , " + String.format("ROUND(PERCENTILE_DISC(0.75) WITHIN GROUP (ORDER BY %s::numeric), 2) AS thirdQuartile", yVar) +
                " , " + String.format("ROUND(PERCENTILE_DISC(0.1) WITHIN GROUP (ORDER BY %s::numeric), 2) AS firstDecile", yVar) +
                " , " + String.format("ROUND(PERCENTILE_DISC(0.9) WITHIN GROUP (ORDER BY %s::numeric), 2) AS ninthDecile", yVar) +
                " , " + String.format("ROUND(PERCENTILE_DISC(0.5) WITHIN GROUP (ORDER BY %s::numeric), 2) AS median", yVar) +
                " , " + String.format("json_agg(%s) as values", yVar) +
                " , jsonb_agg(json_build_object('latitude', latitude, 'longitude', longitude)) as coordinates " +
                " , jsonb_agg(json_build_object(" +
                "     'dephyNb', d1.dephynb, " +
                "     'campaign', c104_campaign_dis, " +
                "     'region',  " + TypeDecoupageGeo.REGION.getColumnName() + ", " +
                "     'ancienneRegion', " + TypeDecoupageGeo.ANCIENNE_REGION.getColumnName() + ", " +
                "     'departement', " + TypeDecoupageGeo.DEPARTEMENT.getColumnName() + ", " +
                "     'arrondissement', " + TypeDecoupageGeo.ARRONDISSEMENT.getColumnName() + ", " +
                "     'bassinViticole', " + TypeDecoupageGeo.BASSIN_VITICOLE.getColumnName() + ", " +
                "     'yVal', ROUND(d1." + yVar + "::numeric, 2) " +
                " )) as systems " +
                " , COUNT(DISTINCT(d1.dephynb, c104_campaign_dis)) AS effectif " +
                " FROM data d1 " +
                maybeCohortYears.map(years -> cohortYearsJoin).orElse("") +
                maybeCohortNetworkAges.map((ages) -> cohortNetworkAgesJoin).orElse("") +
                " WHERE " + yVar + " <> numeric 'NaN' " + filterAndClause.getLeft();

        String q = "SELECT " +
                " valueName, average, min, max, firstQuartile, thirdQuartile, firstDecile, ninthDecile, median" +
                ", values, jsonb_distinct_text(coordinates) as coordinates, systems " +
                " FROM (" + innerQuery + ") t " +
                " WHERE effectif >= :minDataRequired ";

        maybeCohortYears.ifPresent(years -> parameters.put("cohortYearsCount", years.size()));
        maybeCohortNetworkAges.ifPresent(ages -> parameters.put("cohortNetworkAgesCount", ages.size()));

        parameters.put("minDataRequired", minDataRequired);

        Query query =  handle.createQuery(q).bindMap(parameters);
        maybeCohortYears.ifPresent(years -> query.bindList("cohortYears", years));
        maybeCohortNetworkAges.ifPresent(ages -> query.bindList("cohortNetworkAges", ages));

        StatisticData result = query.map(new StatisticDataMapper()).findOne().orElse(StatisticData.getEmptyData());

        return result;
    }

    public List<StatisticData> getQuantiQualiDistribution(Metadata yVar, Metadata xVar, List<FilterChoice> filterChoices,
                                                          FilterCohort filterCohort,
                                                          int minDataRequired, int minDataRequiredForCohort) {
        if (!yVar.isQuantitative() || !xVar.isQualitative()) {
            return Collections.emptyList();
        }

        String xVarSeparator = Optional.ofNullable(xVar.getSeparator()).orElse("@");
        String xVarSelector = String.format(
                "trim (regexp_split_to_table(TRIM (COALESCE(%s::text, 'N/A') ), '%s')) as xvar",
                xVar.getColumnname(), xVarSeparator);

        Map<String, Object> parameters = new HashMap<>();
        Pair<String, Map<String, Object>> filterAndClause = createFilterAndClause(filterChoices, parameters);

        Optional<Set<Integer>> maybeCohortYears = Optional.ofNullable(filterCohort)
                .map(FilterCohort::getYears)
                .filter(CollectionUtils::isNotEmpty);
        Optional<Set<String>> maybeCohortNetworkAges = Optional.ofNullable(filterCohort)
                .map(FilterCohort::getNetworkAges)
                .filter(CollectionUtils::isNotEmpty);

        Optional<FilterChoice> maybeXvarFilter = findFilterChoiceForMetadata(filterChoices, xVar);
        String variablesWhereClause = " WHERE xvar <> 'N/A' AND yvar::numeric <> numeric 'NaN' ";
        if (maybeXvarFilter.isPresent() && maybeXvarFilter.get().isMultiple()) {
            variablesWhereClause += " AND xvar in (<filteredXVar>) ";
        } else {
            maybeXvarFilter = Optional.empty();
        }

        String extraPredicateForCohortPredicate =
                xVar.getColumnname() + " <> 'N/A' AND " + yVar.getColumnname() + "::numeric <> numeric 'NaN' " +
                        filterAndClause.getLeft();
        String cohortYearsPredicate;
        if (maybeCohortYears.isPresent()) {
            parameters.put("cohortYearsCount", maybeCohortYears.get().size());
            if (isPz0Variable(xVar)) {
                cohortYearsPredicate = getCohortYearsPredicateWithPz0(extraPredicateForCohortPredicate, parameters);
            } else {
                cohortYearsPredicate = getCohortYearsPredicateWithoutPz0(extraPredicateForCohortPredicate);
            }
        } else {
            cohortYearsPredicate = "";
        }

        String cohortNetworkAgesPredicate;
        if (maybeCohortNetworkAges.isPresent()) {
            parameters.put("cohortNetworkAgesCount", maybeCohortNetworkAges.get().size());
            if (isPz0Variable(xVar)) {
                cohortNetworkAgesPredicate = getCohortNetworkAgesPredicateWithPz0(extraPredicateForCohortPredicate, parameters);
            } else {
                cohortNetworkAgesPredicate = getCohortNetworkAgesPredicateWithoutPz0(extraPredicateForCohortPredicate);
            }
        } else {
            cohortNetworkAgesPredicate = "";
        }

        String dataViewQuery = "dataview AS ( SELECT * FROM ( SELECT " +
                " d1.dephynb " +
                " , latitude " +
                " , longitude " +
                " , c104_campaign_dis as campaign " +
                " , " + TypeDecoupageGeo.REGION.getColumnName() + " as region " +
                " , " + TypeDecoupageGeo.ANCIENNE_REGION.getColumnName() + " as ancienneRegion " +
                " , " + TypeDecoupageGeo.DEPARTEMENT.getColumnName() + " as departement " +
                " , " + TypeDecoupageGeo.ARRONDISSEMENT.getColumnName() + " as arrondissement " +
                " , " + TypeDecoupageGeo.BASSIN_VITICOLE.getColumnName() + " as bassin " +
                " , " + xVarSelector +
                " , " + yVar.getColumnname() + " as yvar " +
                " FROM data d1 " +
                " WHERE 1 = 1 " + filterAndClause.getLeft() +
                cohortYearsPredicate +
                cohortNetworkAgesPredicate +
                " ) t " +
                variablesWhereClause +
                " ) ";

        String minDataRequiredClause;
        if (isPz0Variable(xVar)) {
            minDataRequiredClause = "";
        } else {
            minDataRequiredClause = " HAVING COUNT(yvar) >= :minDataRequired ";
            parameters.put("minDataRequired", minDataRequired);
        }
        String q = "WITH " + dataViewQuery;
        q += " SELECT " +
                " xvar as valuename " +
                " , ROUND(AVG(yvar::numeric), 2) AS average " +
                " , ROUND(MIN(yvar::numeric), 2) AS min " +
                " , ROUND(MAX(yvar::numeric), 2) AS max " +
                " , ROUND(PERCENTILE_DISC(0.25) WITHIN GROUP (ORDER BY yvar::numeric), 2) AS firstQuartile " +
                " , ROUND(PERCENTILE_DISC(0.75) WITHIN GROUP (ORDER BY yvar::numeric), 2) AS thirdQuartile " +
                " , ROUND(PERCENTILE_DISC(0.1) WITHIN GROUP (ORDER BY yvar::numeric), 2) AS firstDecile " +
                " , ROUND(PERCENTILE_DISC(0.9) WITHIN GROUP (ORDER BY yvar::numeric), 2) AS ninthDecile " +
                " , ROUND(PERCENTILE_DISC(0.5) WITHIN GROUP (ORDER BY yvar::numeric), 2) AS median " +
                " , json_agg(yvar) as values " +
                " , jsonb_agg(json_build_object('latitude', latitude, 'longitude', longitude)) as coordinates " +
                " , jsonb_agg(json_build_object(" +
                "     'dephyNb', dephynb, " +
                "     'campaign', campaign, " +
                "     'region',  region, " +
                "     'ancienneRegion', ancienneRegion, " +
                "     'departement', departement, " +
                "     'arrondissement', arrondissement, " +
                "     'bassinViticole', bassin, " +
                "     'yVal', ROUND(yvar::numeric, 2) " +
                " )) as systems " +
                " FROM dataview " +
                " GROUP BY xvar " +
                minDataRequiredClause;

        Query query = handle.createQuery(q).bindMap(parameters);
        maybeCohortYears.ifPresent(years -> query.bindList("cohortYears", years));
        maybeCohortNetworkAges.ifPresent(ages -> query.bindList("cohortNetworkAges", ages));
        maybeXvarFilter.ifPresent(filterChoice -> query.bindList("filteredXVar", filterChoice.getValues()));

        List<StatisticData> result = query.map(new StatisticDataMapper()).list();
        if (CollectionUtils.isNotEmpty(result) && isPz0Variable(xVar)) {
            Map<String, Integer> resultMap = result.stream().collect(Collectors.toMap(Data::getValueName, d -> d.getSystemes().size()));
            if (resultMap.containsKey(Pz0.SYSTEME_AVANT_ENTREE_DANS_RESEAU_DEPHY.getLabel())) {
                if (resultMap.get(Pz0.SYSTEME_AVANT_ENTREE_DANS_RESEAU_DEPHY.getLabel()) < minDataRequiredForCohort) {
                    result.clear();
                }
            }
            if (resultMap.containsKey(Pz0.SYSTEME_DANS_RESEAU_DEPHY.getLabel())) {
                if (resultMap.get(Pz0.SYSTEME_DANS_RESEAU_DEPHY.getLabel()) < minDataRequired) {
                    result.clear();
                }
            }
        }

        return result;
    }

    protected Optional<FilterChoice> findFilterChoiceForMetadata(List<FilterChoice> filterChoices, Metadata metadata) {
        Optional<FilterChoice> maybeFilter = filterChoices.stream()
                .filter(f -> Objects.equals(f.getColumnname(), metadata.getColumnname()))
                .findFirst();
        return maybeFilter;
    }

    public List<DoubleRepartitionData> getQualiQualiDistribution(Metadata yVar, Metadata xVar, List<FilterChoice> filterChoices,
                                                                 FilterCohort filterCohort,
                                                                 int minDataRequired, int minDataRequiredForCohort) {
        if (!yVar.isQualitative() || !xVar.isQualitative()) {
            return Collections.emptyList();
        }

        String xVarSeparator = Optional.ofNullable(xVar.getSeparator()).orElse("@");
        String yVarSeparator = Optional.ofNullable(yVar.getSeparator()).orElse("@");

        String xVarSelector = String.format(
                "trim (regexp_split_to_table(TRIM (COALESCE(%s::text, 'N/A') ), '%s')) as xvar",
                xVar.getColumnname(), xVarSeparator);
        String yVarSelector = String.format(
                "trim (regexp_split_to_table(TRIM (COALESCE(%s::text, 'N/A') ), '%s')) as yvar",
                yVar.getColumnname(), yVarSeparator);

        Map<String, Object> parameters = new HashMap<>();
        Pair<String, Map<String, Object>> filterAndClause = createFilterAndClause(filterChoices, parameters);

        Optional<Set<Integer>> maybeCohortYears = Optional.ofNullable(filterCohort)
                .map(FilterCohort::getYears)
                .filter(CollectionUtils::isNotEmpty);
        Optional<Set<String>> maybeCohortNetworkAges = Optional.ofNullable(filterCohort)
                .map(FilterCohort::getNetworkAges)
                .filter(CollectionUtils::isNotEmpty);

        boolean hasPz0Var = Stream.of(yVar, xVar).anyMatch(this::isPz0Variable);

        String xVarExtraPredicateForCohortPredicate = xVar.getColumnname() + "::text <> 'N/A' " + filterAndClause.getLeft();
        String yVarExtraPredicateForCohortPredicate = yVar.getColumnname() + "::text <> 'N/A' " + filterAndClause.getLeft();

        String xVarCohortYearsPredicate;
        String yVarCohortYearsPredicate;
        if (maybeCohortYears.isPresent()) {
            parameters.put("cohortYearsCount", maybeCohortYears.get().size());
            if (hasPz0Var) {
                xVarCohortYearsPredicate = getCohortYearsPredicateWithPz0(xVarExtraPredicateForCohortPredicate, parameters);
                yVarCohortYearsPredicate = getCohortYearsPredicateWithPz0(yVarExtraPredicateForCohortPredicate, parameters);
            } else {
                xVarCohortYearsPredicate = getCohortYearsPredicateWithoutPz0(xVarExtraPredicateForCohortPredicate);
                yVarCohortYearsPredicate = getCohortYearsPredicateWithoutPz0(yVarExtraPredicateForCohortPredicate);
            }
        } else {
            xVarCohortYearsPredicate = "";
            yVarCohortYearsPredicate = "";
        }

        String xVarCohortNetworkAgesPredicate;
        String yVarCohortNetworkAgesPredicate;
        if (maybeCohortNetworkAges.isPresent()) {
            parameters.put("cohortNetworkAgesCount", maybeCohortNetworkAges.get().size());
            if (hasPz0Var) {
                xVarCohortNetworkAgesPredicate = getCohortNetworkAgesPredicateWithPz0(xVarExtraPredicateForCohortPredicate, parameters);
                yVarCohortNetworkAgesPredicate = getCohortNetworkAgesPredicateWithPz0(yVarExtraPredicateForCohortPredicate, parameters);
            } else {
                xVarCohortNetworkAgesPredicate = getCohortNetworkAgesPredicateWithoutPz0(xVarExtraPredicateForCohortPredicate);
                yVarCohortNetworkAgesPredicate = getCohortNetworkAgesPredicateWithoutPz0(yVarExtraPredicateForCohortPredicate);
            }
        } else {
            xVarCohortNetworkAgesPredicate = "";
            yVarCohortNetworkAgesPredicate = "";
        }

        String xDataViewQuery = "xdataview AS ( SELECT " +
                " d1.dephynb " +
                " , latitude " +
                " , longitude " +
                " , c104_campaign_dis as campaign " +
                " , " + TypeDecoupageGeo.REGION.getColumnName() + " as region " +
                " , " + TypeDecoupageGeo.ANCIENNE_REGION.getColumnName() + " as ancienneRegion " +
                " , " + TypeDecoupageGeo.DEPARTEMENT.getColumnName() + " as departement " +
                " , " + TypeDecoupageGeo.ARRONDISSEMENT.getColumnName() + " as arrondissement " +
                " , " + TypeDecoupageGeo.BASSIN_VITICOLE.getColumnName() + " as bassin " +
                " , " + xVarSelector +
                " FROM data d1 " +
                " WHERE 1 = 1 " + filterAndClause.getLeft() +
                xVarCohortYearsPredicate +
                xVarCohortNetworkAgesPredicate +
                " ) ";
        String yDataViewQuery = "ydataview AS ( SELECT " +
                " d1.dephynb " +
                " , c104_campaign_dis as campaign " +
                " , " + yVarSelector +
                " FROM data d1 " +
                " WHERE 1 = 1 " + filterAndClause.getLeft() +
                yVarCohortYearsPredicate +
                yVarCohortNetworkAgesPredicate +
                " ) ";

        Optional<FilterChoice> maybeXvarFilter = findFilterChoiceForMetadata(filterChoices, xVar);
        Optional<FilterChoice> maybeYvarFilter = findFilterChoiceForMetadata(filterChoices, yVar);

        String variablesWhereClause = " WHERE xvar <> 'N/A' AND yvar <> 'N/A' ";
        if (maybeXvarFilter.isPresent() && maybeXvarFilter.get().isMultiple()) {
            variablesWhereClause += " AND xvar in (<filteredXVar>) ";
        } else {
            maybeXvarFilter = Optional.empty();
        }
        if (maybeYvarFilter.isPresent() && maybeYvarFilter.get().isMultiple()) {
            variablesWhereClause += " AND yvar in (<filteredYVar>) ";
        } else {
            maybeYvarFilter = Optional.empty();
        }

        String innerRepartitionQuery = " SELECT " +
                " yvar, xvar, count(xvar) as countpery " +
                " , jsonb_agg(json_build_object('latitude', latitude, 'longitude', longitude)) as coordinates " +
                " , jsonb_agg(json_build_object(" +
                "     'dephyNb', xdataview.dephynb, " +
                "     'campaign', xdataview.campaign," +
                "     'region',  xdataview.region, " +
                "     'ancienneRegion', xdataview.ancienneRegion, " +
                "     'departement', xdataview.departement, " +
                "     'arrondissement', xdataview.arrondissement, " +
                "     'bassinViticole', xdataview.bassin," +
                "     'yVal', NULL," +
                "     'xVal', NULL " +
                " )) as systems " +
                " FROM xdataview " +
                " JOIN ydataview " +
                " ON xdataview.campaign = ydataview.campaign and xdataview.dephynb = ydataview.dephynb " +
                variablesWhereClause +
                " GROUP BY xvar, yvar " +
                " ORDER by xvar asc ";

        String minDataRequiredClause;
        if (isPz0Variable(xVar) || isPz0Variable(yVar)) {
            // Pour ce cas précis, le seuil d'anonymisation est traité après la requête SQL
            // pour gérer le seuil spécifique avant entrée/dans réseau
            minDataRequiredClause = "";
        } else {
            minDataRequiredClause = " WHERE countpery >= :minDataRequired ";
            parameters.put("minDataRequired", minDataRequired);
        }
        String q = "WITH " + xDataViewQuery + ", " + yDataViewQuery;
        q += " SELECT " +
                " yvar as valueName " +
                " , sum(countpery) as valueOccurrence " +
                " , json_agg(json_build_object('valuename', xvar, 'valueoccurrence', countpery)) as innerRepartition " +
                " , jsonb_distinct_flatten_array(coordinates) as coordinates " +
                " , jsonb_distinct_flatten_array(systems) as systems " +
                " FROM ( " + innerRepartitionQuery + " ) t " +
                minDataRequiredClause +
                " GROUP BY yvar " +
                " ORDER BY yvar ASC ";

        Query query = handle.createQuery(q).bindMap(parameters);
        maybeCohortYears.ifPresent(years -> query.bindList("cohortYears", years));
        maybeCohortNetworkAges.ifPresent(ages -> query.bindList("cohortNetworkAges", ages));
        maybeXvarFilter.ifPresent(filterChoice -> query.bindList("filteredXVar", filterChoice.getValues()));
        maybeYvarFilter.ifPresent(filterChoice -> query.bindList("filteredYVar", filterChoice.getValues()));

        List<DoubleRepartitionData> result = query.map(new DoubleRepartitionDataMapper()).list();

        Predicate<SimpleRepartitionData> beforePz0ExclusionPredicate = (simpleRepartitionData ->
                simpleRepartitionData.getValueName().equals(Pz0.SYSTEME_AVANT_ENTREE_DANS_RESEAU_DEPHY.getLabel())
                && simpleRepartitionData.getValueOccurrence() < minDataRequiredForCohort);
        Predicate<SimpleRepartitionData> afterPz0ExclusionPredicate = (simpleRepartitionData ->
                simpleRepartitionData.getValueName().equals(Pz0.SYSTEME_DANS_RESEAU_DEPHY.getLabel())
                && simpleRepartitionData.getValueOccurrence() < minDataRequired);
        Predicate<SimpleRepartitionData> pz0ExclusionPredicate = beforePz0ExclusionPredicate.or(afterPz0ExclusionPredicate);

        if (isPz0Variable(xVar)) {
            result.removeIf(doubleRepartitionData ->
                    Arrays.stream(doubleRepartitionData.getInnerRepartition()).anyMatch(pz0ExclusionPredicate));
        }

        if (isPz0Variable(yVar) && result.stream().anyMatch(pz0ExclusionPredicate)) {
            result.clear();
        }

        return result;
    }

    public List<CoupleValuesData> getQuantiQuantiDistribution(Metadata yQuantitative, Metadata xQuantitative,
                                                              List<FilterChoice> filterChoices, FilterCohort filterCohort) {
        if (!xQuantitative.isQuantitative() || !yQuantitative.isQuantitative()) {
            return Collections.emptyList();
        }

        Map<String, Object> parameters = new HashMap<>();
        Pair<String, Map<String, Object>> filterAndClause = createFilterAndClause(filterChoices, parameters);

        Optional<Set<Integer>> maybeCohortYears = Optional.ofNullable(filterCohort)
                .map(FilterCohort::getYears)
                .filter(CollectionUtils::isNotEmpty);
        Optional<Set<String>> maybeCohortNetworkAges = Optional.ofNullable(filterCohort)
                .map(FilterCohort::getNetworkAges)
                .filter(CollectionUtils::isNotEmpty);

        String cohortYearsJoin = " JOIN ( " +
                " SELECT d2.dephynb, count(d2.dephynb) FROM data d2 " +
                " WHERE d2.c104_campaign_dis::int in (<cohortYears>) " +
                " AND " + yQuantitative.getColumnname() + " <> numeric 'NaN' " +
                " AND " + xQuantitative.getColumnname() + " <> numeric 'NaN' " +
                filterAndClause.getLeft() +
                " GROUP BY d2.dephynb " +
                " HAVING COUNT(d2.dephynb) = :cohortYearsCount " +
                " ) cy " +
                " ON cy.dephynb = d1.dephynb AND d1.c104_campaign_dis::int in (<cohortYears>) ";

        String cohortNetworkAgesJoin = " JOIN ( " +
                " SELECT d2.dephynb, count(d2.dephynb) FROM data d2 " +
                " WHERE d2.c103_networkyears in (<cohortNetworkAges>) " +
                " AND " + yQuantitative.getColumnname() + " <> numeric 'NaN' " +
                " AND " + xQuantitative.getColumnname() + " <> numeric 'NaN' " +
                filterAndClause.getLeft() +
                " GROUP BY d2.dephynb " +
                " HAVING COUNT(d2.dephynb) = :cohortNetworkAgesCount " +
                " ) cna " +
                " ON cna.dephynb = d1.dephynb AND d1.c103_networkyears in (<cohortNetworkAges>) ";

        String innerQuery = " SELECT " +
                yQuantitative.getColumnname() + " as yvar " +
                " , " + xQuantitative.getColumnname() + " as xvar " +
                " , json_build_object('latitude', latitude, 'longitude', longitude) as coordinates " +
                " , json_build_object(" +
                "     'dephyNb', d1.dephynb, " +
                "     'campaign', c104_campaign_dis, " +
                "     'region', " + TypeDecoupageGeo.REGION.getColumnName() + ", " +
                "     'ancienneRegion', " + TypeDecoupageGeo.ANCIENNE_REGION.getColumnName() + ", " +
                "     'departement', " + TypeDecoupageGeo.DEPARTEMENT.getColumnName() + ", " +
                "     'arrondissement', " + TypeDecoupageGeo.ARRONDISSEMENT.getColumnName() + ", " +
                "     'bassinViticole', " + TypeDecoupageGeo.BASSIN_VITICOLE.getColumnName() + ", " +
                "     'yVal', " + yQuantitative.getColumnname() + ", " +
                "     'xVal', " + xQuantitative.getColumnname() +
                " ) as systems " +
                " FROM data d1 " +
                maybeCohortYears.map(years -> cohortYearsJoin).orElse("") +
                maybeCohortNetworkAges.map(ages -> cohortNetworkAgesJoin).orElse("") +
                " WHERE " +
                yQuantitative.getColumnname() + " <> numeric 'NaN' AND " + xQuantitative.getColumnname() + " <> numeric 'NaN' " +
                filterAndClause.getLeft();

        String q = "SELECT $$" + xQuantitative.getColumnname() + " x " + yQuantitative.getColumnname()+ "$$ AS valueName " +
                " , xvar AS abscissa " +
                " , yvar AS ordinate " +
                " , COUNT(systems) as weight " +
                " , jsonb_agg(coordinates) as coordinates " +
                " , json_agg(systems) as systems " +
                " FROM ( " + innerQuery + " ) t " +
                " GROUP BY xvar, yvar " +
                " ORDER BY xvar::double precision, yvar::double precision";

        maybeCohortYears.ifPresent(years -> parameters.put("cohortYearsCount", years.size()));
        maybeCohortNetworkAges.ifPresent(ages -> parameters.put("cohortNetworkAgesCount", ages.size()));

        Query query =  handle.createQuery(q).bindMap(parameters);
        maybeCohortYears.ifPresent(years -> query.bindList("cohortYears", years));
        maybeCohortNetworkAges.ifPresent(ages -> query.bindList("cohortNetworkAges", ages));

        List<CoupleValuesData> result = query.map(new CoupleValuesDataMapper()).list();

        return result;
    }

    /**
     * Search values for a specific choice to represent it in a {@link Data}.
     * Could have one or two search metadata (qualitative, quantitative,
     * quali x quanti, quanti x quanti, quali x quali), so we have kind of request like
     * "Select specific, xData, yData from Data where [Filters] And specific IN specificValues"
     * Of course, need to manage if Metadata are multiple. For the moment, consider specific is not multiple.
     *
     * @param specific the specific Metadata we search (for the moment : dephyNb)
     * @param xData : a search Metadata
     * @param yData : an other search metadata
     * @param otherFilters : list of {@link FilterChoice} that ould reduce datas
     *
     * @return list of values for all specific data wanted
     *
     * @since 20180914
     */
    public List<SpecificValue> getSpecificValues(FilterChoice specific, Metadata xData, Metadata yData, List<FilterChoice> otherFilters) {

        List<FilterChoice> allFilters = Lists.newArrayList(otherFilters);
        allFilters.add(specific);

        String specificColumnName = specific.getColumnname();
        StringBuilder sqlBuilder = new StringBuilder("WITH dataview AS (");
        Pair<String, Map<String, Object>> withRequest = makeViewClauseWithTwoColumns(xData, yData, allFilters, Lists.newArrayList(specificColumnName, "latitude", "longitude"));
        sqlBuilder.append(withRequest.getLeft());
        Map<String, Object> parameters = withRequest.getRight();
        sqlBuilder.append(" ) ");

        // Just the query now !
        sqlBuilder.append(" SELECT d." + specificColumnName + " AS specific ");
        if (xData != null) {
            sqlBuilder.append(", ( SELECT array_agg( d2.col1::text) FROM dataview d2 WHERE d2.").append(specificColumnName).append(" = d.").append(specificColumnName).append(") AS xValue ");
        }
        if (yData != null) {
            sqlBuilder.append(", ( SELECT array_agg(d3.col2::text) FROM dataview d3 WHERE d3.").append(specificColumnName).append(" = d.").append(specificColumnName).append(") AS yValue ");
        }

        sqlBuilder.append(", json_build_object('latitude', d.latitude, 'longitude', d.longitude) AS coordinates");

        sqlBuilder.append(" FROM dataview d ");
        sqlBuilder.append(" WHERE d.col1 is NOT NULL and d.col2 is NOT NULL");
        sqlBuilder.append(" GROUP BY specific, d.latitude, d.longitude ");
        sqlBuilder.append(" ORDER BY specific ASC");

        List<SpecificValue> result = handle.createQuery(sqlBuilder.toString())
                .bindMap(parameters)
                .map(new SpecificValueMapper(xData != null, yData != null))
                .list();

        return result;
    }

    /**
     * Search values for a specific choice to represent it in a {@link Data}.
     * Could have one or two search metadata (qualitative, quantitative,
     * quali x quanti, quanti x quanti, quali x quali), so we have kind of request like
     * "Select specific, xData, yData from Data where [Filters] And specific IN specificValues"
     * Of course, need to manage if Metadata are multiple. For the moment, consider specific is not multiple.
     *
     * @param specific the specific Metadata we search (for the moment : dephyNb)
     * @param data : a search Metadata
     * @param otherFilters : list of {@link FilterChoice} that ould reduce datas
     *
     * @return list of values for all specific data wanted
     *
     * @since 20180914
     */
    public List<SpecificValue> getSpecificValues(FilterChoice specific, Metadata data, List<FilterChoice> otherFilters) {

        List<FilterChoice> allFilters = Lists.newArrayList(otherFilters);
        allFilters.add(specific);

        String specificColumnName = specific.getColumnname();
        if (data.isQualitative() && data.isMultiple()) {
            // Use a view to split all data multiple values as table
            StringBuilder sqlBuilder = new StringBuilder("WITH dataview AS (SELECT trim(regexp_split_to_table(")
                    .append("COALESCE(")
                    .append(data.getColumnname())
                    .append(", 'N/A')") // End of COALESCE to put empty value in case of null
                    .append(", '")
                    .append(data.getSeparator())
                    .append("')) as values ")
                    .append(" , ").append(specificColumnName).append(", latitude, longitude ")
                    .append(" from data  ");

            // Manage Filters
            Pair<String, Map<String, Object>> filterClause = createFilterAndClause(allFilters, null);
            sqlBuilder.append(" WHERE 1 = 1 ").append(filterClause.getLeft());
            Map<String, Object> parameters = filterClause.getRight();
            // End of view
            sqlBuilder.append(" ) ");

            sqlBuilder.append(" SELECT d.").append(specificColumnName).append(" AS specific ");
            sqlBuilder.append(" , ( SELECT array_agg(distinct COALESCE(d2.values, 'N/A')) FROM dataview d2 WHERE d2.").append(specificColumnName).append(" = d.").append(specificColumnName).append(") AS xValue ");
            sqlBuilder.append(", json_build_object('latitude', d.latitude, 'longitude', d.longitude) AS coordinates");

            sqlBuilder.append(" FROM dataview d ");

            sqlBuilder.append(" GROUP BY specific, d.latitude, d.longitude ");
            sqlBuilder.append(" ORDER BY specific ASC, xValue ASC ");

            List<SpecificValue> result = handle.createQuery(sqlBuilder.toString())
                    .bindMap(parameters)
                    .map(new SpecificValueMapper())
                    .list();
            return result;

        } else {
            StringBuilder sqlBuilder = new StringBuilder("SELECT d.").append(specificColumnName).append(" AS specific ");
            sqlBuilder.append(", ( SELECT array_agg(distinct d2.").append(data.getColumnname()).append(") FROM data d2 WHERE d2.").append(specificColumnName).append(" = d.").append(specificColumnName).append(") AS xValue ");
            sqlBuilder.append(", json_build_object('latitude', d.latitude, 'longitude', d.longitude) AS coordinates");
            sqlBuilder.append(" FROM data d ");
            sqlBuilder.append(" WHERE 1 = 1 ");
            sqlBuilder.append(" AND d.").append(data.getColumnname()).append(" is NOT NULL ");
            Pair<String, Map<String, Object>> filterClause = createFilterAndClause(allFilters, null);
            sqlBuilder.append(filterClause.getLeft() + " ");
            Map<String, Object> parameters = filterClause.getRight();

            sqlBuilder.append(" GROUP BY specific, d.latitude, d.longitude ");
            sqlBuilder.append(" ORDER BY specific ASC, xValue ASC ");

            List<SpecificValue> result = handle.createQuery(sqlBuilder.toString())
                    .bindMap(parameters)
                    .map(new SpecificValueMapper())
                    .list();
            return result;
        }
    }

    public List<String> getEffectiveFilterValues(Metadata metadata, List<String> purposedValues) {

        String sql;
        String columnname = metadata.getColumnname();

        if (StringUtils.isBlank(metadata.getSeparator())) {
            sql = "SELECT distinct(" + columnname + "::text) FROM data WHERE " + columnname + " IS NOT NULL ";

            if (purposedValues != null && !purposedValues.isEmpty()) {
                sql += " AND " + columnname + " = ANY (:purposedValues) ";
            }
        } else {
            // Manage possible multiple values : create query like
            // select distinct(trim(values)) from ( select regexp_split_to_table(coalesce(COLUMN, ''), 'SEPARATOR') as values from data ) as valueTable;
            StringBuilder sqlBuilder = new StringBuilder("SELECT distinct(trim(values)) ");
            sqlBuilder.append(" FROM ( select regexp_split_to_table(")
                    .append(columnname)
                    .append(", '")// end of COALESCE, used to avoid null value and nothing return in WITH clause !
                    .append(metadata.getSeparator())
                    .append("') as values from data ")
                    .append(" WHERE " + columnname + " IS NOT NULL ");

            if (purposedValues != null && !purposedValues.isEmpty()) {
                sqlBuilder.append(" AND " + columnname + " = ANY (:purposedValues) ");
            }

            sqlBuilder.append(" ) as valueTable");
            sql = sqlBuilder.toString();
        }
        Query query = handle.createQuery(sql);
        if (purposedValues != null && !purposedValues.isEmpty()) {
            query.bindArray("purposedValues", String.class, purposedValues);
        }
        List<String> result = query.mapTo(String.class).list();
        return result;
    }

    protected Pair<String, Map<String, Object>> createFilterAndClause(List<FilterChoice> filterChoices, Map<String, Object> parameters) {

        String clause = "";

        if (filterChoices != null && !filterChoices.isEmpty()) {
            StringBuilder clauseBuilder = new StringBuilder("");
            if (parameters == null) {
                parameters = new HashMap<>();
            }

            // Add each filter as "AND" statement in WHERE clause
            for (FilterChoice filterChoice : filterChoices) {
                String paramName = "param"+ System.nanoTime();
                if (filterChoice.isMultiple()) {
                    String separator = filterChoice.getSeparator();
                    clauseBuilder.append(" AND regexp_replace(").append(filterChoice.getColumnname()).append(", '\\s").append(separator).append("\\s', '").append(separator).append("') ~ :").append(paramName);
                    //clauseBuilder.append(" AND replace(LOWER(").append(filterChoice.getColumnname()).append("), ' ', '') ~ ${").append(paramName).append("}");

                    String likeValue = filterChoice.getValues().stream()
                            //.map(choiceValue -> choiceValue.toLowerCase())
                            .map(choiceValue -> choiceValue + "|" + separator + choiceValue + "|" + separator + choiceValue + separator + "|" + choiceValue + separator)
                            .collect(Collectors.joining("|"));
                    parameters.put(paramName, likeValue);  // ex : 'foo|,foo|,foo,|foo,' cause multiple can be in several form
                } else if (filterChoice.getValues().size() > 1) {
                    clauseBuilder.append(" AND ").append(filterChoice.getColumnname()).append("::text = ANY (:").append(paramName).append(")");
                    parameters.put(paramName, filterChoice);
                } else {
                    clauseBuilder.append(" AND ").append(filterChoice.getColumnname()).append("::text = :").append(paramName);
                    parameters.put(paramName, filterChoice.getValues().get(0));
                }
            }

            clause = clauseBuilder.toString();
        }
        return Pair.of(clause, parameters);
    }

    protected Pair<String, Map<String, Object>> makeViewClauseWithTwoColumns(Metadata col1, Metadata col2, List<FilterChoice> filterChoices, List<String> wantedColumns) {

        String flatWantedColumns = wantedColumns.stream().collect(Collectors.joining(", "));

        String viewSql;

        // Here, this is simple : select col1, col2 from data where filters.
        if (!col1.isMultiple() && ! col2.isMultiple()) {

            // Just need to manage null values as "N/A" for qualitative metadata ...
            String col1Selector = col1.getColumnname();
            String col2Selector = col2.getColumnname();
            if (col1.isQualitative()) {
                col1Selector = new StringBuilder("TRIM (COALESCE(")
                        .append(col1.getColumnname())
                        .append("::text, 'N/A') )").toString();
            }
            if (col2.isQualitative()) {
                col2Selector = new StringBuilder("TRIM (COALESCE(")
                        .append(col2.getColumnname())
                        .append("::text, 'N/A') )").toString();
            }

            viewSql = " SELECT " + col1Selector + "::text as col1, " + col2Selector + "::text AS col2, " + flatWantedColumns + " FROM data WHERE 1 = 1 ";
            if (col1.isQuantitative()) {
                viewSql += " AND " + col1Selector + "::numeric <> numeric 'NaN' ";
            }
            Pair<String, Map<String, Object>> filterClause = createFilterAndClause(filterChoices, null);
            viewSql += filterClause.getLeft() + " ";
            viewSql += " ORDER BY col1, col2 ASC ";
            return Pair.of(viewSql, filterClause.getRight());
        }

        // With multiple values, this is harder : need to split multiple values to manage them one by one ...
        // SELECT splitCol1 as col1, splitCol2 as col2 FROM
        //          ( SELECT regexp_split_to_table(col1) as splitCol1, splitData.splitCol2 as splitCol2 FROM
        //                  ( SELECT col1, regexp_split_to_table(col2) as splitCol2 FROM data WHERE otherFilters )
        //          )
        // WHERE col1Filters + col2Filters

        StringBuilder sqlBuilder = new StringBuilder("select flatCol1 as xData, flatCol2 as yData, ").append(flatWantedColumns);
        // First, need to extract filters on selected metadata, cause we need to take into account the multiple split values
        List<FilterChoice> col1Filters = null;
        List<FilterChoice> col2Filters = null;
        if (filterChoices != null && !filterChoices.isEmpty()) {
            col1Filters = filterChoices.stream().filter(filterChoice -> StringUtils.equalsIgnoreCase(col1.getColumnname(), filterChoice.getColumnname())).collect(Collectors.toList());
            col2Filters = filterChoices.stream().filter(filterChoice -> StringUtils.equalsIgnoreCase(col2.getColumnname(), filterChoice.getColumnname())).collect(Collectors.toList());
            filterChoices.removeAll(col1Filters);
            filterChoices.removeAll(col2Filters);
        }

        // Then, make correct selector for the two columns (if there are multiple, need to split them as temporary table
        String col1Selector = col1.getColumnname();
        if (col1.isQualitative()) {
            // USE COALESCE to avoid null value and nothing return in WITH clause !
            col1Selector = new StringBuilder( "TRIM (COALESCE(")
                    .append(col1.getColumnname())
                    .append("::text, 'N/A') )").toString();
        }
        if (col1.isMultiple()) {
            col1Selector = new StringBuilder("trim (regexp_split_to_table(")
                    .append(col1Selector)
                    .append(", '").append(col1.getSeparator()).append("'))").toString();
        }

        String col2Selector = col2.getColumnname();
        if (col2.isQualitative()) {
            // USE COALESCE to avoid null value and nothing return in WITH clause !
            col2Selector = new StringBuilder( "TRIM (COALESCE(")
                    .append(col2.getColumnname())
                    .append("::text, 'N/A') )").toString();
        }
        if (col2.isMultiple()) {
            col2Selector = new StringBuilder("trim (regexp_split_to_table(")
                    .append(col2Selector)
                    .append(", '").append(col2.getSeparator()).append("'))").toString();
        }

        // LEt's build !
        // select trim(regexp_split_to_table(COALESCE(valueTable.fieldToSplit, 'N/A'), ',')) as values,
        //        valueTable.fieldAsTable as quali from ( select campaign as fieldToSplit, trim (COALESCE(pz0, 'N/A')) as fieldAsTable from data) as valueTable ;
        Map<String, Object> parameters = null;
        viewSql = " SELECT flatCol1::text as col1, flatCol2::text as col2, " + flatWantedColumns + " FROM (";
        viewSql += " SELECT " + col1Selector + " as flatCol1, splitDataCol2.col2AsTable as flatCol2, " + flatWantedColumns + " FROM ";
        viewSql += " ( SELECT " + col1.getColumnname() + "::text, " + col2Selector + " AS col2AsTable, " + flatWantedColumns + " FROM data ";
        viewSql += " WHERE 1 = 1 ";
        if (col1.isQuantitative() && !col1.isMultiple()) {
            viewSql += " AND " + col1.getColumnname() + "::numeric <> numeric 'NaN' ";
        }
        if (filterChoices != null && !filterChoices.isEmpty()) {
            Pair<String, Map<String, Object>> filterClause = createFilterAndClause(filterChoices, null);
            viewSql += filterClause.getLeft() + " ";
            parameters = filterClause.getRight();
        }
        viewSql += " ) AS splitDataCol2 "; // end of select col1, splitCol2 from data
        viewSql += " ) AS flatData "; // end of select splitCol1, splitCol2 from select

        viewSql += " WHERE 1 = 1 ";
        // Add filters on col1
        if (col1Filters != null && !col1Filters.isEmpty()) {
            col1Filters.forEach(fc -> fc.setColumnname("flatCol1"));
            Pair<String, Map<String, Object>> filterClause = createFilterAndClause(col1Filters, parameters);
            viewSql += filterClause.getLeft() + " ";
            parameters = filterClause.getRight();
        }
        // Add filter on col2
        if (col2Filters != null && !col2Filters.isEmpty()) {
            col2Filters.forEach(fc -> fc.setColumnname("flatCol2"));
            Pair<String, Map<String, Object>> filterClause = createFilterAndClause(col2Filters, parameters);
            viewSql += filterClause.getLeft() + " ";
            parameters = filterClause.getRight();
        }

        viewSql += " ORDER BY col1, col2 ASC ";

        //viewSql += " GROUP BY dephyNb ";
        return Pair.of(viewSql, parameters);
    }

}
