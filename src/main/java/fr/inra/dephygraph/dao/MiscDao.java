package fr.inra.dephygraph.dao;

import org.jdbi.v3.core.Handle;

public class MiscDao {

    public final static String LAST_AGROSYST_EXTRACTION_DATE = "lastAgrosystExtractionDate";
    public final static String DATA_PRODUCTION_TIMESTAMP = "dataProductionTimestamp";

    protected final Handle handle;

    public MiscDao(Handle handle) {
        this.handle = handle;
    }

    protected String getMiscValue(String key) {
        String sql = "SELECT data FROM miscellaneous where key = :key";
        String result = handle.createQuery(sql)
                .bind("key", key)
                .mapTo(String.class)
                .findOne()
                .orElse(null);
        return result;
    }

    public String getLastAgrosystExtractionDate() {
        return getMiscValue(LAST_AGROSYST_EXTRACTION_DATE);
    }

    public String getDataProductionTimestamp() {
        return getMiscValue(DATA_PRODUCTION_TIMESTAMP);
    }
}
