package fr.inra.dephygraph;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.postgresql.ds.PGSimpleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.util.Map;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class DephyGraphDatasource {

    private static final Logger LOGGER = LoggerFactory.getLogger(DephyGraphDatasource.class);

    protected DataSource dataSource;

    protected Jdbi jdbi;

    public DephyGraphDatasource() {}

    public DephyGraphDatasource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbi = Jdbi.create(this.dataSource);
        executeInitScripts();
    }

    public DataSource getDataSource() {
        if (this.dataSource == null) {
            init();
        }
        return this.dataSource;
    }

    public Jdbi getJdbi() {
        if (this.jdbi == null) {
            init();
        }
        return this.jdbi;
    }

    protected void init() {
        DephyGraphConfig config = DephyGraphConfig.get();
        String url = config.getDataSourceUrl();
        String user = config.getDataSourceUser();
        String password = config.getDataSourcePassword();

        PGSimpleDataSource ds = new PGSimpleDataSource();
        ds.setUrl(url);
        ds.setUser(user);
        ds.setPassword(password);
        this.dataSource = ds;

        this.jdbi = Jdbi.create(ds);

        executeInitScripts();
    }

    protected void executeInitScripts() {
        try {
            String q = "SELECT version();";
            try (Handle h = jdbi.open()) {
                Map<String, Object> versionInfo = h.createQuery(q).mapToMap().one();
                LOGGER.info("DB server version: " + versionInfo.get("version"));
            }
        } catch (Exception e) {
            LOGGER.error("Failed to get DB server version", e);
            throw new RuntimeException(e.getMessage());
        }

        try {
            String q = "" +
                    " CREATE OR REPLACE FUNCTION jsonb_distinct_text(a jsonb) RETURNS jsonb AS $$ " +
                    " DECLARE res jsonb; " +
                    " BEGIN " +
                    "  SELECT jsonb_agg(t.value) FROM ( " +
                    "    SELECT DISTINCT value FROM jsonb_array_elements(a) " +
                    "  ) t INTO res; " +
                    "  RETURN res; " +
                    "END; " +
                    "$$ LANGUAGE plpgsql;";
            try (Handle h = jdbi.open()) {
                h.execute(q);
            }
        } catch (Exception e) {
            LOGGER.error("Failed to create function", e);
            throw new RuntimeException(e.getMessage());
        }

        try {
            String q = "" +
                    " DROP AGGREGATE IF EXISTS jsonb_distinct_flatten_array(jsonb); " +
                    " CREATE AGGREGATE jsonb_distinct_flatten_array(jsonb) " +
                    " ( " +
                    "    sfunc = jsonb_concat, " +
                    "    stype = jsonb, " +
                    "    FINALFUNC = jsonb_distinct_text, " +
                    "    initcond = '[]' " +
                    " ); ";
            try (Handle h = jdbi.open()) {
                h.execute(q);
            }
        } catch (Exception e) {
            LOGGER.error("Failed to create aggregate", e);
            throw new RuntimeException(e.getMessage());
        }
    }
}
