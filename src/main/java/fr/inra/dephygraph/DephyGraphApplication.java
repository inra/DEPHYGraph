package fr.inra.dephygraph;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import fr.inra.dephygraph.dao.DatamartDao;
import fr.inra.dephygraph.dao.MiscDao;
import fr.inra.dephygraph.exceptions.AuthorizationExceptionMapper;
import fr.inra.dephygraph.exceptions.IllegalArgumentExceptionMapper;
import fr.inra.dephygraph.exceptions.NoSuchMetadataExceptionMapper;
import fr.inra.dephygraph.filters.CORSResponseFilter;
import fr.inra.dephygraph.model.FilterValues;
import fr.inra.dephygraph.model.Metadata;
import fr.inra.dephygraph.providers.DephyGraphParamConverterProvider;
import fr.inra.dephygraph.rest.AgrosystService;
import fr.inra.dephygraph.rest.FiltersService;
import fr.inra.dephygraph.rest.InfoService;
import fr.inra.dephygraph.rest.MetadataService;
import fr.inra.dephygraph.rest.StatisticsService;
import fr.inra.dephygraph.rest.UserEventsLoggingService;
import io.swagger.v3.jaxrs2.integration.resources.AcceptHeaderOpenApiResource;
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;
import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;
import org.apache.commons.lang3.StringUtils;
import org.jdbi.v3.core.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author ymartel (martel@codelutin.com)
 */
@ApplicationPath("/api")
public class DephyGraphApplication extends Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(DephyGraphApplication.class);

    protected final DephyGraphDatasource ds = new DephyGraphDatasource();

    protected final HashSet<Object> singletons = new HashSet<>();

    protected final Cache<String, Metadata> metadataCache = CacheBuilder.newBuilder().build();
    protected final Cache<String, FilterValues> filterCache = CacheBuilder.newBuilder().build();
    protected final Cache<String, FilterValues> irFilterCache = CacheBuilder.newBuilder().build();

    protected String dataProductionTimestamp = null;

    public DephyGraphApplication() {
        fillCaches(ds);
        Executors.newScheduledThreadPool(1)
                .scheduleAtFixedRate(new CacheRefreshTask(), 10, 10, TimeUnit.SECONDS);

        singletons.add(new StatisticsService(ds, metadataCache));
        singletons.add(new MetadataService(ds));
        singletons.add(new FiltersService(ds, filterCache, irFilterCache));
        singletons.add(new AgrosystService(ds));
        singletons.add(new InfoService(ds));
        singletons.add(new UserEventsLoggingService());

        singletons.add(new CORSResponseFilter());

        singletons.add(new DephyGraphParamConverterProvider(metadataCache));

        LOGGER.info("Effectif minimum: {}", DephyGraphConfig.get().getMinDataRequired());

        String logConfigLocation = DephyGraphConfig.get().getLogConfigLocation();
        if (StringUtils.isNotBlank(logConfigLocation)) {
            File log4jConfigurationFile = new File(logConfigLocation);
            String log4jConfigurationFileAbsolutePath = log4jConfigurationFile.getAbsolutePath();

            if (log4jConfigurationFile.exists()) {
                LOGGER.info("Will use logging configuration {}", log4jConfigurationFileAbsolutePath);
                LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
                try {
                    JoranConfigurator configurator = new JoranConfigurator();
                    configurator.setContext(context);
                    context.reset();
                    configurator.doConfigure(log4jConfigurationFile);
                } catch (JoranException e) {
                    // StatusPrinter will handle this
                }
                StatusPrinter.printInCaseOfErrorsOrWarnings(context);
            } else {
                LOGGER.warn("No file found '{}'. Default logging configuration will be used.", log4jConfigurationFileAbsolutePath);
            }
        }


    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<>();
        classes.add(AcceptHeaderOpenApiResource.class);
        classes.add(OpenApiResource.class);
        classes.add(IllegalArgumentExceptionMapper.class);
        classes.add(NoSuchMetadataExceptionMapper.class);
        classes.add(AuthorizationExceptionMapper.class);
        return classes;
    }

    protected void fillCaches(DephyGraphDatasource ds) {
        cacheMetadata(ds);
        cacheFilters(ds);
        cacheIrFilters(ds);
        try (Handle handle = ds.getJdbi().open()) {
            MiscDao miscDao = new MiscDao(handle);
            dataProductionTimestamp = miscDao.getDataProductionTimestamp();
        }
    }

    protected void cacheMetadata(DephyGraphDatasource ds) {
        metadataCache.invalidateAll();
        try (Handle handle = ds.getJdbi().open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            List<Metadata> allMetadata = datamartDao.getAllMetadata();
            for (Metadata m: allMetadata) {
                metadataCache.put(m.getColumnname(), m);
            }
            LOGGER.info("Metadata cache size: {}", metadataCache.size());
        }
    }

    protected void cacheFilters(DephyGraphDatasource ds) {
        filterCache.invalidateAll();
        try (Handle handle = ds.getJdbi().open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            List<Metadata> filtersMetadata = datamartDao.getAllFilterMetadata();
            for (Metadata filterMetadata : filtersMetadata) {
                List<String> filterValues = datamartDao.getFilterValues(filterMetadata);
                FilterValues filter = new FilterValues();
                filter.setColumnname(filterMetadata.getColumnname());
                filter.setDisplayname(filterMetadata.getDisplayname());
                filter.setCategory(filterMetadata.getCategory());
                filter.setValues(filterValues);
                filter.setMultiple(StringUtils.isNotBlank(filterMetadata.getSeparator()));
                filter.setRecommendedFilter(filterMetadata.isRecommendedfilter());
                filterCache.put(filterMetadata.getColumnname(), filter);
            }
        }
        LOGGER.info("Filters cache size: {}", filterCache.size());
    }

    protected void cacheIrFilters(DephyGraphDatasource ds) {
        irFilterCache.invalidateAll();
        try (Handle handle = ds.getJdbi().open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            List<Metadata> irFiltersMetadata = datamartDao.getAllIrFilterMetadata();
            for (Metadata irFilterMetadata : irFiltersMetadata) {
                List<String> filterValues = datamartDao.getFilterValues(irFilterMetadata);
                FilterValues filter = new FilterValues();
                filter.setColumnname(irFilterMetadata.getColumnname());
                filter.setDisplayname(irFilterMetadata.getDisplayname());
                filter.setCategory(irFilterMetadata.getCategory());
                filter.setValues(filterValues);
                filter.setMultiple(StringUtils.isNotBlank(irFilterMetadata.getSeparator()));
                irFilterCache.put(irFilterMetadata.getColumnname(), filter);
            }
        }
        LOGGER.info("IR Filters cache size: {}", irFilterCache.size());
    }

    protected class CacheRefreshTask extends TimerTask {
        @Override
        public void run() {
            String timestamp;
            try (Handle handle = ds.getJdbi().open()) {
                MiscDao miscDao = new MiscDao(handle);
                timestamp = miscDao.getDataProductionTimestamp();
            }
            if (!StringUtils.equals(timestamp, dataProductionTimestamp)) {
                LOGGER.info("New data production timestamp: {}", timestamp);
                LOGGER.info("Updating caches...");
                fillCaches(ds);
            }
        }
    }
}
