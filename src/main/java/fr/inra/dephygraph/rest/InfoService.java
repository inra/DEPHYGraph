package fr.inra.dephygraph.rest;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.inra.dephygraph.DephyGraphConfig;
import fr.inra.dephygraph.DephyGraphDatasource;
import fr.inra.dephygraph.dao.MiscDao;
import fr.inra.dephygraph.model.Metadata;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.jdbi.v3.core.Handle;

import java.io.Serializable;

/**
 * @author ymartel (martel@codelutin.com)
 */
@Path("/v1/info")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class InfoService {

    protected final DephyGraphDatasource ds;

    public InfoService(DephyGraphDatasource ds) {
        this.ds = ds;
    }

    @GET
    @Path("")
    @Operation(summary = "Get information about system",
        responses = {
            @ApiResponse(description = "Some useful information about application, like config",
                content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Metadata.class)))})
    public ApplicationStatus getAllMetadata() {

        String lastAgrosystExtractionDate;
        String dataProductionTimestamp;
        try (Handle handle = ds.getJdbi().open()) {
            MiscDao dao = new MiscDao(handle);
            lastAgrosystExtractionDate = dao.getLastAgrosystExtractionDate();
            dataProductionTimestamp = dao.getDataProductionTimestamp();
        }

        DephyGraphConfig dephyGraphConfig = DephyGraphConfig.get();

        ApplicationStatus status = new ApplicationStatus();
        status.setDbUp(lastAgrosystExtractionDate != null);
        status.setMinDataRequired(dephyGraphConfig.getMinDataRequired());
        status.setHelpLink(dephyGraphConfig.getHelpLink());
        status.setVersion(dephyGraphConfig.getVersion());
        status.setLastAgrosystExtractionDate(lastAgrosystExtractionDate);
        status.setDataProductionTimestamp(dataProductionTimestamp);

        return status;
    }

    protected static class ApplicationStatus implements Serializable {
        protected boolean dbUp;
        protected String helpLink;
        protected String version;
        protected String lastAgrosystExtractionDate;
        protected String dataProductionTimestamp;
        protected int minDataRequired;

        public boolean isDbUp() {
            return dbUp;
        }

        public void setDbUp(boolean dbUp) {
            this.dbUp = dbUp;
        }

        public String getHelpLink() {
            return helpLink;
        }

        public void setHelpLink(String helpLink) {
            this.helpLink = helpLink;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public int getMinDataRequired() {
            return minDataRequired;
        }

        public void setMinDataRequired(int minDataRequired) {
            this.minDataRequired = minDataRequired;
        }

        public String getLastAgrosystExtractionDate() {
            return lastAgrosystExtractionDate;
        }

        public void setLastAgrosystExtractionDate(String lastAgrosystExtractionDate) {
            this.lastAgrosystExtractionDate = lastAgrosystExtractionDate;
        }

        public String getDataProductionTimestamp() {
            return dataProductionTimestamp;
        }

        public void setDataProductionTimestamp(String dataProductionTimestamp) {
            this.dataProductionTimestamp = dataProductionTimestamp;
        }
    }
}
