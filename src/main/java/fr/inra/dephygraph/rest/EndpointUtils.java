package fr.inra.dephygraph.rest;

import jakarta.servlet.http.HttpServletRequest;

final class EndpointUtils {

    public static String getClientIp(HttpServletRequest request) {
        String clientIp = "";
        if (request != null) {
            clientIp = request.getHeader("X-FORWARDED-FOR");
            if (clientIp == null || "".equals(clientIp)) {
                clientIp = request.getRemoteAddr();
            }
        }
        return clientIp;
    }

}
