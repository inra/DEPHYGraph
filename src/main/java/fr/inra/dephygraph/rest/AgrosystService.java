package fr.inra.dephygraph.rest;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import fr.inra.dephygraph.DephyGraphConfig;
import fr.inra.dephygraph.DephyGraphDatasource;
import fr.inra.dephygraph.dao.AgrosystInfoDao;
import fr.inra.dephygraph.exceptions.AuthorizationException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.MediaType;
import org.apache.commons.lang3.StringUtils;
import org.jdbi.v3.core.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author ymartel (martel@codelutin.com)
 */
@Path("/v1/agrosyst")
public class AgrosystService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AgrosystService.class);

    protected static final String USER_ID_CLAIM = "userId";
    protected static final String USER_NAME_CLAIM = "userName";

    protected static Cache<String, AgrosystInfo> dataCache = CacheBuilder.newBuilder().expireAfterWrite(30, TimeUnit.MINUTES).build();

    protected final DephyGraphDatasource ds;

    public AgrosystService(DephyGraphDatasource ds) {
        this.ds = ds;
        // Schedule a database session refresh !
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(new AgrosystInfoCleanerTask(this.ds), 0, 1,  TimeUnit.DAYS);
    }

    @POST
    @Path("/prepare")
    @Operation(summary = "Prepare connexion between Agrosyst and DEPHYGraph :"
        + "from agrosyst, post needed data, such as User name, authorized dephyNb, ...",
        responses = {
            @ApiResponse(description = "A randomized token corresponding to data key",
                content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = String.class)))})
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String prepareConnexion(@FormParam("agrosystInfo") AgrosystInfo agrosystInfo) {

        String key = UUID.randomUUID().toString();
        LOGGER.info("Prepare connexion for {}, with {} dephyNb", agrosystInfo.getUserName(), agrosystInfo.getAllowedDephyNb().size());
        agrosystInfo.setId(UUID.randomUUID());
        dataCache.put(key, agrosystInfo);
        return key;
    }

    @POST
    @Path("/login/{token}")
    @Operation(summary = "Prepare connexion between Agrosyst and DEPHYGraph :"
        + "from agrosyst, post needed data, such as User name, authorized dephyNb, ...",
        responses = {
            @ApiResponse(description = "A randomized token corresponding to data key",
                content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = String.class)))})
    public String login(@PathParam("token") String token) throws Exception {

        LOGGER.info("Login from token {}", token);

        AgrosystInfo agrosystInfo = dataCache.getIfPresent(token);

        if (agrosystInfo == null) {
            //TODO ymartel 20180730 : use better exception
            throw new IllegalArgumentException("Cannot retrieve token");
        }
        LOGGER.info("Login is {}", agrosystInfo.getUserName());

        try (Handle handle = ds.getJdbi().open()) {

            // Two steps : save date in DB and return a Json Web token

            // Save in database
            fr.inra.dephygraph.dao.AgrosystInfoDao  agrosystInfoDao = new fr.inra.dephygraph.dao.AgrosystInfoDao(handle);
            OffsetDateTime expirationDate = OffsetDateTime.now().plus(24, ChronoUnit.HOURS);
            agrosystInfo.setExpirationDate(expirationDate);
            agrosystInfoDao.saveInfo(agrosystInfo);

            // Make JWT
            Algorithm algorithm = Algorithm.HMAC256(DephyGraphConfig.get().getWebSecurityKey());
            String jwtoken = JWT.create()
                .withIssuer("DEPHYGraph")
                .withClaim(USER_ID_CLAIM, agrosystInfo.getId().toString())
                .withClaim(USER_NAME_CLAIM, agrosystInfo.getUserName())
                .withExpiresAt(new Date(expirationDate.toInstant().toEpochMilli()))
                .sign(algorithm);

            dataCache.invalidate(token);

            return jwtoken;
        }

    }

    protected static AgrosystInfo checkAuthorization(DephyGraphDatasource ds, String authorizationToken) throws AuthorizationException {
        LOGGER.info("Checking authorization");

        String token = StringUtils.replace(authorizationToken, "Bearer ", "");
        Algorithm algorithm = Algorithm.HMAC256(DephyGraphConfig.get().getWebSecurityKey());

        JWTVerifier verifier = JWT.require(algorithm).build();
        DecodedJWT decodedToken;

        try {
            decodedToken = verifier.verify(token);
        } catch (JWTVerificationException e) {
            throw new AuthorizationException();
        }

        String userId = decodedToken.getClaim(USER_ID_CLAIM).asString();
        String userName = decodedToken.getClaim(USER_NAME_CLAIM).asString();
        try (Handle handle = ds.getJdbi().open()) {
            // Retrieve session info from database
            fr.inra.dephygraph.dao.AgrosystInfoDao  agrosystInfoDao = new fr.inra.dephygraph.dao.AgrosystInfoDao(handle);
            AgrosystInfo agrosystInfo = agrosystInfoDao.retrieveInfo(UUID.fromString(userId));
            agrosystInfo.setUserName(userName);// If NPE: no session, go in catch clause !
            return agrosystInfo;
        } catch (Exception e) {
            LOGGER.error("cannot retrieve Agrosyst Info session for user '{} (id : {})' from database : maybe expired ?", userName, userId);
            throw new AuthorizationException();
        }
    }


    protected static class AgrosystInfoCleanerTask extends TimerTask {

        private final DephyGraphDatasource ds;

        public AgrosystInfoCleanerTask(DephyGraphDatasource ds) {
            this.ds = ds;
        }

        @Override
        public void run() {
            try (Handle handle = ds.getJdbi().open()) {
                // Save in database
                fr.inra.dephygraph.dao.AgrosystInfoDao  agrosystInfoDao = new AgrosystInfoDao(handle);
                agrosystInfoDao.removeExpiredEntries();
            } catch (Exception e) {
                LOGGER.error("Error during AgrosystInfo purge in database", e);
            }
        }
    }

}
