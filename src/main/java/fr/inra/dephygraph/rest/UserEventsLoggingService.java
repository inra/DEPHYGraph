package fr.inra.dephygraph.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.inra.dephygraph.model.UserEventTrace;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

@Path("/v1/uevent")
public class UserEventsLoggingService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserEventsLoggingService.class);

    private static final Marker USER_EVENT_LOG_MARKER = MarkerFactory.getMarker("USER_EVENT");

    protected final ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);

    @POST
    @Path("/log")
    public boolean logEvent(@Context HttpServletRequest request, UserEventTrace userEventTrace) {
        try {
            String clientIp = EndpointUtils.getClientIp(request);
            LOGGER.info(USER_EVENT_LOG_MARKER, "{} ({})", mapper.writeValueAsString(userEventTrace), clientIp);
            return true;
        } catch (JsonProcessingException e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        }
    }
}
