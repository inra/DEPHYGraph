package fr.inra.dephygraph.rest;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.inra.dephygraph.DephyGraphDatasource;
import fr.inra.dephygraph.dao.DatamartDao;
import fr.inra.dephygraph.model.Metadata;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.jdbi.v3.core.Handle;

import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */
@Path("/v1/metadata")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MetadataService {

    protected final DephyGraphDatasource ds;

    public MetadataService(DephyGraphDatasource ds) {
        this.ds = ds;
    }

    @GET
    @Path("/")
    @Operation(summary = "Get all metadata. Use case is to list interest choices mainly.",
        responses = {
            @ApiResponse(description = "All the available metadata",
                content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Metadata.class)))})
    public List<Metadata> getAllMetadata() throws Exception {
        try (Handle h = ds.getJdbi().open()) {
            DatamartDao dao = new DatamartDao(h);
            return dao.getAllVisibleMetadata();
        }
    }

    @GET
    @Path("/qualitatives")
    @Operation(summary = "Get only the qualitative metadata. Use case is to list variation choices mainly.",
        responses = {
            @ApiResponse(description = "All the available qualitative metadata",
                content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Metadata.class)))})
    public List<Metadata> getAllQualitativeMetadata() throws Exception {
        try (Handle h = ds.getJdbi().open()) {
            DatamartDao dao = new DatamartDao(h);
            return dao.getAllQualitativeMetadata();
        }
    }

    @GET
    @Path("/name/{columnName}")
    @Operation(summary = "Get metadata description from is column name.",
        responses = {
            @ApiResponse(description = "The metadata corresponding",
                content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Metadata.class)))})
    public Metadata getMetadataByName(@PathParam("columnName") String columnName) throws Exception {
        try (Handle h = ds.getJdbi().open()) {
            DatamartDao dao = new DatamartDao(h);
            return dao.getMetadataByColumnName(columnName);
        }
    }

    @GET
    @Path("/name/{columnName}/values")
    public List<String> getQualitativeMetadataValues(@PathParam("columnName") String columnName) throws Exception {
        try (Handle h = ds.getJdbi().open()) {
            DatamartDao dao = new DatamartDao(h);
            return dao.getQualitativeMetadataValues(columnName);
        }
    }
}
