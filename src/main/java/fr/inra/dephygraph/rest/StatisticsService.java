package fr.inra.dephygraph.rest;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.cache.Cache;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.inra.dephygraph.DephyGraphConfig;
import fr.inra.dephygraph.DephyGraphDatasource;
import fr.inra.dephygraph.dao.DatamartDao;
import fr.inra.dephygraph.dao.MiscDao;
import fr.inra.dephygraph.dao.RecommendedGraphicDao;
import fr.inra.dephygraph.model.Coordinates;
import fr.inra.dephygraph.model.CoupleValuesData;
import fr.inra.dephygraph.model.DistributionEffectif;
import fr.inra.dephygraph.model.DoubleRepartitionData;
import fr.inra.dephygraph.model.FilterChoice;
import fr.inra.dephygraph.model.FilterCohort;
import fr.inra.dephygraph.model.Intervalle;
import fr.inra.dephygraph.model.LightStatisticData;
import fr.inra.dephygraph.model.Metadata;
import fr.inra.dephygraph.model.MetadataConstants;
import fr.inra.dephygraph.model.QualiResult;
import fr.inra.dephygraph.model.QualiQualiResult;
import fr.inra.dephygraph.model.QuantiQualiResult;
import fr.inra.dephygraph.model.QuantiQuantiResult;
import fr.inra.dephygraph.model.QuantiResult;
import fr.inra.dephygraph.model.RecommendedGraphic;
import fr.inra.dephygraph.model.ResultWithCoordinatesAndSectors;
import fr.inra.dephygraph.model.Sector;
import fr.inra.dephygraph.model.SimpleRepartitionData;
import fr.inra.dephygraph.model.SortType;
import fr.inra.dephygraph.model.SpecificValue;
import fr.inra.dephygraph.model.StatisticData;
import fr.inra.dephygraph.exceptions.InvalidFilterException;
import fr.inra.dephygraph.model.Systeme;
import fr.inra.dephygraph.model.SystemeWithYXValues;
import fr.inra.dephygraph.model.TypeDecoupageGeo;
import fr.inra.dephygraph.model.VariableGeoDistribution;
import fr.inra.dephygraph.model.param.AbstractDistributionParam;
import fr.inra.dephygraph.model.param.FilterParam;
import fr.inra.dephygraph.model.param.DistributionWithOneDimensionParam;
import fr.inra.dephygraph.model.param.DistributionWithTwoDimensionsParam;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;
import org.jdbi.v3.core.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author ymartel (martel@codelutin.com)
 */
@Path("/v1/statistics")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class StatisticsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(StatisticsService.class);

    private static final Marker DISTRIBUTION_PARAM_LOG_MARKER = MarkerFactory.getMarker("DISTRIBUTION_PARAM");

    private static final Integer[] PERCENTILE_FRACTIONS = {5, 20, 35, 50, 65, 80, 95};

    private static final int MIN_INTERVALLES = 9;

    protected final DephyGraphDatasource ds;

    protected final Cache<String, Metadata> metadataCache;

    protected final ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);

    public StatisticsService(DephyGraphDatasource ds, Cache<String, Metadata> metadataCache) {
        this.ds = ds;
        this.metadataCache = metadataCache;
    }

    protected List<FilterChoice> getFilterChoicesFromDistributionParams(AbstractDistributionParam params) throws Exception {
        List<FilterChoice> filterChoices = new ArrayList<>();
        for (FilterParam f : params.getFilterParams()) {
            Metadata metadata = metadataCache.getIfPresent(f.getColumnName());
            if (metadata == null) {
                throw new InvalidFilterException(f.getColumnName());
            }
            if ((metadata.isIrfilter() || FiltersService.DEPHY_NB_COLUMN_NAME.equals(metadata.getColumnname()))
                    && !params.isAuthenticated()) {
                throw new InvalidFilterException(f.getColumnName());
            }
            if (!metadata.isFilter()) {
                throw new InvalidFilterException(f.getColumnName());
            }
            FilterChoice filterChoice = FilterChoice.fromMetadata(metadata);
            filterChoice.setValues(f.getValues());
            filterChoices.add(filterChoice);
        }
        filterChoices.add(getDefaultValidationFilter());
        return filterChoices;
    }

    @POST
    @Path("/qualitativeDistribution/")
    @Consumes(MediaType.APPLICATION_JSON)
    public QualiResult getQualitativeDistribution(
            @Context HttpServletRequest request,
            DistributionWithOneDimensionParam param,
            @HeaderParam("Authorization") String authorization) throws Exception {

        if (StringUtils.isNotBlank(authorization)) {
            AgrosystService.checkAuthorization(ds, authorization);
            param.setAuthenticated(true);
        } else {
            param.setAuthenticated(false);
        }

        logParam(request, param);

        QualiResult qualiResult = new QualiResult();

        try (Handle h = ds.getJdbi().open()) {
            MiscDao dao = new MiscDao(h);
            String lastAgrosystExtractionDate = dao.getLastAgrosystExtractionDate();
            qualiResult.setLastAgrosystExtractionDate(lastAgrosystExtractionDate);
        }

        Metadata interest = metadataCache.getIfPresent(param.getColumnName());
        List<FilterChoice> filterChoices = getFilterChoicesFromDistributionParams(param);
        List<SimpleRepartitionData> repartition = getQualitativeDistribution(
                interest, filterChoices, param.getFilterCohort(), param.getMinDataRequired(), param.getMinDataRequiredForCohort());
        qualiResult.setRepartition(repartition);

        Set<Coordinates> coordinates = Sets.newHashSet();
        Set<Systeme> systemes = Sets.newHashSet();
        repartition.forEach(r -> {
            if (CollectionUtils.isNotEmpty(r.getCoordinates())) {
                coordinates.addAll(r.getCoordinates());
            }
            if (CollectionUtils.isNotEmpty(r.getSystemes())) {
                systemes.addAll(r.getSystemes());
            }
        });
        qualiResult.setCoordinates(coordinates);

        fillEffectifs(systemes, param.getMinDataRequired(), qualiResult);

        Set<Metadata> metadata = new HashSet<>();
        metadata.add(interest);
        metadata.addAll(getMetadataForFilterChoices(filterChoices));
        Set<Sector> metadataSectors = getSectorsFromMetadata(metadata);
        Set<Sector> filterChoicesSectors = getSectorsForFilterChoices(filterChoices);
        qualiResult.setSectors(Sets.intersection(metadataSectors, filterChoicesSectors));
        qualiResult.setEffectifTotal(systemes.size());

        return qualiResult;
    }

    @POST
    @Path("/qualitativeQualitativeDistribution/")
    @Consumes(MediaType.APPLICATION_JSON)
    public QualiQualiResult getQualitativeQualitativeDistribution(
            @Context HttpServletRequest request,
            DistributionWithTwoDimensionsParam param,
            @HeaderParam("Authorization") String authorization) throws Exception {

        if (StringUtils.isNotBlank(authorization)) {
            AgrosystService.checkAuthorization(ds, authorization);
            param.setAuthenticated(true);
        } else {
            param.setAuthenticated(false);
        }

        logParam(request, param);

        QualiQualiResult qualiQualiResult = new QualiQualiResult();

        try (Handle h = ds.getJdbi().open()) {
            MiscDao dao = new MiscDao(h);
            String lastAgrosystExtractionDate = dao.getLastAgrosystExtractionDate();
            qualiQualiResult.setLastAgrosystExtractionDate(lastAgrosystExtractionDate);
        }

        Metadata interest = metadataCache.getIfPresent(param.getyColumnName());
        Metadata variation = metadataCache.getIfPresent(param.getxColumnName());
        List<FilterChoice> filterChoices = getFilterChoicesFromDistributionParams(param);
        List<DoubleRepartitionData> repartition = getQualitativeQualitativeDistribution(
                interest, variation, filterChoices,
                param.getFilterCohort(), param.getMinDataRequired(), param.getMinDataRequiredForCohort());
        qualiQualiResult.setRepartition(repartition);

        Set<Coordinates> coordinates = Sets.newHashSet();
        Set<Systeme> systemes = Sets.newHashSet();
        repartition.forEach(r -> {
            if (CollectionUtils.isNotEmpty(r.getCoordinates())) {
                coordinates.addAll(r.getCoordinates());
            }
            if (CollectionUtils.isNotEmpty(r.getSystemes())) {
                systemes.addAll(r.getSystemes());
            }
        });
        qualiQualiResult.setCoordinates(coordinates);

        fillEffectifs(systemes, param.getMinDataRequired(), qualiQualiResult);

        Set<Metadata> metadata = new HashSet<>();
        metadata.add(interest);
        metadata.add(variation);
        metadata.addAll(getMetadataForFilterChoices(filterChoices));
        Set<Sector> metadataSectors = getSectorsFromMetadata(metadata);
        Set<Sector> filterChoicesSectors = getSectorsForFilterChoices(filterChoices);
        qualiQualiResult.setSectors(Sets.intersection(metadataSectors, filterChoicesSectors));
        qualiQualiResult.setEffectifTotal(systemes.size());

        return qualiQualiResult;
    }

    @POST
    @Path("/quantitativeDistribution/")
    @Consumes(MediaType.APPLICATION_JSON)
    public QuantiResult getQuantitativeDistribution(
            @Context HttpServletRequest request,
            DistributionWithOneDimensionParam param,
            @HeaderParam("Authorization") String authorization) throws Exception {

        if (StringUtils.isNotBlank(authorization)) {
            AgrosystService.checkAuthorization(ds, authorization);
            param.setAuthenticated(true);
        } else {
            param.setAuthenticated(false);
        }

        logParam(request, param);

        QuantiResult quantiResult = new QuantiResult();

        try (Handle h = ds.getJdbi().open()) {
            MiscDao dao = new MiscDao(h);
            String lastAgrosystExtractionDate = dao.getLastAgrosystExtractionDate();
            quantiResult.setLastAgrosystExtractionDate(lastAgrosystExtractionDate);
        }

        Metadata interest = metadataCache.getIfPresent(param.getColumnName());
        List<FilterChoice> filterChoices = getFilterChoicesFromDistributionParams(param);
        StatisticData repartition = getQuantitativeDistribution(
                interest, filterChoices, param.getFilterCohort(), param.getMinDataRequired());
        quantiResult.setRepartition(LightStatisticData.from(repartition));

        Set<SystemeWithYXValues> systemes = Set.copyOf(repartition.getSystemes());
        fillEffectifs(systemes, param.getMinDataRequired(), quantiResult);
        fillMoyennesYValue(interest, systemes, quantiResult);
        quantiResult.getVariableYGeoDistribution().setUnit(interest.getUnity());

        Set<Metadata> metadata = new HashSet<>();
        metadata.add(interest);
        metadata.addAll(getMetadataForFilterChoices(filterChoices));
        Set<Sector> metadataSectors = getSectorsFromMetadata(metadata);
        Set<Sector> filterChoicesSectors = getSectorsForFilterChoices(filterChoices);
        quantiResult.setSectors(Sets.intersection(metadataSectors, filterChoicesSectors));
        quantiResult.setCoordinates(Sets.newHashSet(repartition.getCoordinates()));
        quantiResult.setEffectifTotal(systemes.size());

        return quantiResult;
    }

    @POST
    @Path("/quantitativeQualitativeDistribution/")
    @Consumes(MediaType.APPLICATION_JSON)
    public QuantiQualiResult getQuantitativeQualitativeDistribution(
            @Context HttpServletRequest request,
            DistributionWithTwoDimensionsParam param,
            @HeaderParam("Authorization") String authorization) throws Exception {

        if (StringUtils.isNotBlank(authorization)) {
            AgrosystService.checkAuthorization(ds, authorization);
            param.setAuthenticated(true);
        } else {
            param.setAuthenticated(false);
        }

        logParam(request, param);

        QuantiQualiResult quantiQualiResult = new QuantiQualiResult();

        try (Handle h = ds.getJdbi().open()) {
            MiscDao dao = new MiscDao(h);
            String lastAgrosystExtractionDate = dao.getLastAgrosystExtractionDate();
            quantiQualiResult.setLastAgrosystExtractionDate(lastAgrosystExtractionDate);
        }

        Metadata interest = metadataCache.getIfPresent(param.getyColumnName());
        Metadata variation = metadataCache.getIfPresent(param.getxColumnName());
        List<FilterChoice> filterChoices = getFilterChoicesFromDistributionParams(param);
        List<StatisticData> repartition = getQuantitativeQualitativeDistribution(
                interest, variation, filterChoices,
                param.getFilterCohort(), param.getMinDataRequired(), param.getMinDataRequiredForCohort());
        quantiQualiResult.setRepartition(
                repartition.stream().map(LightStatisticData::from).collect(Collectors.toList()));

        Set<Coordinates> coordinates = Sets.newHashSet();
        Set<SystemeWithYXValues> systemes = Sets.newHashSet();
        repartition.forEach(r -> {
            if (CollectionUtils.isNotEmpty(r.getCoordinates())) {
                coordinates.addAll(r.getCoordinates());
            }
            if (CollectionUtils.isNotEmpty(r.getSystemes())) {
                systemes.addAll(r.getSystemes());
            }
        });
        quantiQualiResult.setCoordinates(coordinates);

        fillEffectifs(systemes, param.getMinDataRequired(), quantiQualiResult);
        fillMoyennesYValue(interest, systemes, quantiQualiResult);
        quantiQualiResult.getVariableYGeoDistribution().setUnit(interest.getUnity());

        Set<Metadata> metadata = new HashSet<>();
        metadata.add(interest);
        metadata.add(variation);
        metadata.addAll(getMetadataForFilterChoices(filterChoices));
        Set<Sector> metadataSectors = getSectorsFromMetadata(metadata);
        Set<Sector> filterChoicesSectors = getSectorsForFilterChoices(filterChoices);
        quantiQualiResult.setSectors(Sets.intersection(metadataSectors, filterChoicesSectors));
        quantiQualiResult.setEffectifTotal(systemes.size());

        return quantiQualiResult;
    }

    @POST
    @Path("/quantitativeQuantitativeDistribution/")
    @Consumes(MediaType.APPLICATION_JSON)
    public QuantiQuantiResult getQuantitativeQuantitativeDistribution(
            @Context HttpServletRequest request,
            DistributionWithTwoDimensionsParam param,
            @HeaderParam("Authorization") String authorization) throws Exception {

        if (StringUtils.isNotBlank(authorization)) {
            AgrosystService.checkAuthorization(ds, authorization);
            param.setAuthenticated(true);
        } else {
            param.setAuthenticated(false);
        }

        logParam(request, param);

        QuantiQuantiResult quantiQuantiResult = new QuantiQuantiResult();

        try (Handle h = ds.getJdbi().open()) {
            MiscDao dao = new MiscDao(h);
            String lastAgrosystExtractionDate = dao.getLastAgrosystExtractionDate();
            quantiQuantiResult.setLastAgrosystExtractionDate(lastAgrosystExtractionDate);
        }

        Metadata yQuantitative = metadataCache.getIfPresent(param.getyColumnName());
        Metadata xQuantitative = metadataCache.getIfPresent(param.getxColumnName());
        List<FilterChoice> filterChoices = getFilterChoicesFromDistributionParams(param);
        List<CoupleValuesData> repartition = getQuantitativeQuantitativeDistribution(
                yQuantitative, xQuantitative, filterChoices, param.getFilterCohort(), param.getMinDataRequired());
        quantiQuantiResult.setRepartition(repartition);

        Set<Coordinates> coordinates = Sets.newHashSet();
        Set<SystemeWithYXValues> systemes = Sets.newHashSet();
        repartition.forEach(r -> {
            if (CollectionUtils.isNotEmpty(r.getCoordinates())) {
                coordinates.addAll(r.getCoordinates());
                r.getCoordinates().clear();
            }
            if (CollectionUtils.isNotEmpty(r.getSystemes())) {
                systemes.addAll(r.getSystemes());
                r.getSystemes().clear();
            }
        });
        quantiQuantiResult.setCoordinates(coordinates);

        fillEffectifs(systemes, param.getMinDataRequired(), quantiQuantiResult);
        fillMoyennesYValue(yQuantitative, systemes, quantiQuantiResult);
        fillMoyennesXValue(xQuantitative, systemes, quantiQuantiResult);
        quantiQuantiResult.getVariableYGeoDistribution().setUnit(yQuantitative.getUnity());
        quantiQuantiResult.getVariableXGeoDistribution().setUnit(xQuantitative.getUnity());

        Set<Metadata> metadata = new HashSet<>();
        metadata.add(yQuantitative);
        metadata.add(xQuantitative);
        metadata.addAll(getMetadataForFilterChoices(filterChoices));
        Set<Sector> metadataSectors = getSectorsFromMetadata(metadata);
        Set<Sector> filterChoicesSectors = getSectorsForFilterChoices(filterChoices);
        quantiQuantiResult.setSectors(Sets.intersection(metadataSectors, filterChoicesSectors));
        quantiQuantiResult.setEffectifTotal(systemes.size());

        return quantiQuantiResult;
    }

    @GET
    @Path("/recommendedGraphics")
    public List<RecommendedGraphic> getRecommendedGraphics() {
        List<RecommendedGraphic> result;
        try (Handle h = ds.getJdbi().open()) {
            RecommendedGraphicDao dao = new RecommendedGraphicDao(h);
            result = dao.getRecommendedGraphics();
        }
        return result;
    }

    @POST
    @Path("/dephyNb/values")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public List<SpecificValue> getSpecificValues(
            @FormParam("dephyNbs") List<String> dephyNbs,
            @FormParam("xMetadata") Metadata xMetadata,
            @FormParam("yMetadata") Metadata yMetadata,
            @FormParam("filterChoices") List<FilterChoice> filterChoices,
            @HeaderParam("Authorization") String authorization) throws Exception {

        AgrosystInfo agrosystInfo = null;
        if (StringUtils.isNotBlank(authorization)) {
            LOGGER.info("Check authorization");
            agrosystInfo = AgrosystService.checkAuthorization(ds, authorization);
        }

        try (Handle h = ds.getJdbi().open()) {
            if (xMetadata != null && yMetadata != null) {
                return getDephyNbValues(h, dephyNbs, xMetadata, yMetadata, filterChoices, agrosystInfo);
            } else if (xMetadata != null) {
                return getDephyNbValues(h, dephyNbs, xMetadata, filterChoices, agrosystInfo);
            } else if (yMetadata != null) {
                return getDephyNbValues(h, dephyNbs, yMetadata, filterChoices, agrosystInfo);
            } else {
                return Collections.emptyList();
            }
        }
    }

    @Deprecated
    protected List<FilterChoice> getFilterChoices(List<FilterChoice> paramFilterChoices, AgrosystInfo agrosystInfo) throws Exception {
        List<FilterChoice> filterChoices;
        if (CollectionUtils.isEmpty(paramFilterChoices)) {
            filterChoices = getDefaultFilters();
        } else {
            filterChoices = checkFilters(paramFilterChoices, agrosystInfo);
            filterChoices.add(getDefaultValidationFilter());
        }
        return filterChoices;
    }

    protected Set<Metadata> getMetadataForFilterChoices(List<FilterChoice> filterChoices) {
        Set<Metadata> result = new HashSet<>();
        for (FilterChoice filterChoice : filterChoices) {
            Metadata metadata = metadataCache.getIfPresent(filterChoice.getColumnname());
            result.add(metadata);
        }
        return result;
    }

    protected Set<Sector> getSectorsForFilterChoices(List<FilterChoice> filterChoices) {
        Set<Sector> workSet = Sector.ALL_SECTORS;
        for (FilterChoice filterChoice : filterChoices) {
            if (Sector.METADATA_COLUMN_NAME.equals(filterChoice.getColumnname())) {
                Set<Sector> sectors = filterChoice.getValues().stream()
                        .map(Sector::fromLabel)
                        .collect(Collectors.toSet());
                workSet = Sets.intersection(workSet, sectors);
            }
        }
        Set<Sector> result = new HashSet<>(workSet);
        return result;
    }

    protected Set<Sector> getSectorsFromMetadata(Set<Metadata> metadata) {
        Set<Sector> workSet = Sector.ALL_SECTORS;
        for (Metadata m : metadata) {
            if (StringUtils.isNotBlank(m.getAvailabilitybysector()) && !Sector.ALL_MARKER.equals(m.getAvailabilitybysector())) {
                Set<Sector> sectors = Arrays.stream(m.getAvailabilitybysector().split(Sector.LIST_SEPARATOR))
                        .map(String::trim)
                        .map(Sector::fromLabel)
                        .collect(Collectors.toSet());
                workSet = Sets.intersection(workSet, sectors);
            }
        }
        Set<Sector> result = new HashSet<>(workSet);
        return result;
    }

    protected List<SimpleRepartitionData> getQualitativeDistribution(Metadata interestMetadata,
                                                                     List<FilterChoice> filterChoices,
                                                                     FilterCohort filterCohort,
                                                                     Integer minDataRequiredParam,
                                                                     Integer minDataRequiredForCohortParam) throws Exception {
        List<SimpleRepartitionData> result;
        Integer minDataRequired = Optional.ofNullable(minDataRequiredParam)
                .orElse(DephyGraphConfig.get().getMinDataRequired());
        Integer minDataRequiredForCohort = Optional.ofNullable(minDataRequiredForCohortParam)
                .orElse(DephyGraphConfig.get().getMinDataRequiredForCohort());
        try (Handle h = ds.getJdbi().open()) {
            DatamartDao dao = new DatamartDao(h);
            result = dao.getQualiDistribution(interestMetadata, filterChoices, filterCohort, minDataRequired, minDataRequiredForCohort);
        }
        return result;
    }

    protected List<DoubleRepartitionData> getQualitativeQualitativeDistribution(Metadata interestMetadata, Metadata variationMetadata,
                                                                                List<FilterChoice> filterChoices,
                                                                                FilterCohort filterCohort,
                                                                                Integer minDataRequiredParam,
                                                                                Integer minDataRequiredForCohortParam) throws Exception {
        List<DoubleRepartitionData> result;
        Integer minDataRequired = Optional.ofNullable(minDataRequiredParam)
                .orElse(DephyGraphConfig.get().getMinDataRequired());
        Integer minDataRequiredForCohort = Optional.ofNullable(minDataRequiredForCohortParam)
                .orElse(DephyGraphConfig.get().getMinDataRequiredForCohort());
        try (Handle h = ds.getJdbi().open()) {
            DatamartDao dao = new DatamartDao(h);
            result = dao.getQualiQualiDistribution(interestMetadata, variationMetadata, filterChoices,
                    filterCohort, minDataRequired, minDataRequiredForCohort);
        }
        return result;
    }

    protected StatisticData getQuantitativeDistribution(Metadata interestMetadata,
                                                        List<FilterChoice> filterChoices,
                                                        FilterCohort filterCohort,
                                                        Integer minDataRequiredParam) throws Exception {
        StatisticData result;
        Integer minDataRequired = Optional.ofNullable(minDataRequiredParam)
                .orElse(DephyGraphConfig.get().getMinDataRequired());
        try (Handle h = ds.getJdbi().open()) {
            DatamartDao dao = new DatamartDao(h);
            result = dao.getQuantiDistribution(interestMetadata, filterChoices, filterCohort, minDataRequired);
        }
        return result;
    }

    protected List<StatisticData> getQuantitativeQualitativeDistribution(Metadata interestMetadata, Metadata variationMetadata,
                                                                         List<FilterChoice> filterChoices,
                                                                         FilterCohort filterCohort,
                                                                         Integer minDataRequiredParam,
                                                                         Integer minDataRequiredForCohortParam) throws Exception {
        List<StatisticData> result;
        Integer minDataRequired = Optional.ofNullable(minDataRequiredParam)
                .orElse(DephyGraphConfig.get().getMinDataRequired());
        Integer minDataRequiredForCohort = Optional.ofNullable(minDataRequiredForCohortParam)
                .orElse(DephyGraphConfig.get().getMinDataRequiredForCohort());
        try (Handle h = ds.getJdbi().open()) {
            DatamartDao dao = new DatamartDao(h);
            result = dao.getQuantiQualiDistribution(interestMetadata, variationMetadata,
                    filterChoices, filterCohort, minDataRequired, minDataRequiredForCohort);
        }
        return result;
    }

    protected List<CoupleValuesData> getQuantitativeQuantitativeDistribution(Metadata yQuantitativeMetadata, Metadata xQuantitativeMetadata,
                                                                             List<FilterChoice> filterChoices,
                                                                             FilterCohort filterCohort,
                                                                             Integer minDataRequiredParam) throws Exception {
        List<CoupleValuesData> result;
        Integer minDataRequired = Optional.ofNullable(minDataRequiredParam)
                .orElse(DephyGraphConfig.get().getMinDataRequired());
        try (Handle h = ds.getJdbi().open()) {
            DatamartDao dao = new DatamartDao(h);
            List<CoupleValuesData> coupleValuesData = dao.getQuantiQuantiDistribution(yQuantitativeMetadata, xQuantitativeMetadata, filterChoices, filterCohort);
            // Do not return too less data
            if (coupleValuesData.size() >= minDataRequired) {
                result =  coupleValuesData;
            } else {
                result = Collections.emptyList();
            }
        }
        return result;
    }

    protected void fillEffectifs(Set<? extends Systeme> systemes, Integer minDataRequiredParamParam, ResultWithCoordinatesAndSectors result) {
        Integer minDataRequired = Optional.ofNullable(minDataRequiredParamParam)
                .orElse(DephyGraphConfig.get().getMinDataRequired());

        DistributionEffectif distributionEffectif = new DistributionEffectif();

        List.of(
                TypeDecoupageGeo.REGION,
                TypeDecoupageGeo.ANCIENNE_REGION,
                TypeDecoupageGeo.DEPARTEMENT,
                TypeDecoupageGeo.ARRONDISSEMENT,
                TypeDecoupageGeo.BASSIN_VITICOLE
        ).forEach(decoupage -> {
            Map<String, Long> effectifParDecoupage = systemes.stream()
                    .collect(Collectors.groupingBy(
                            s -> Optional.ofNullable(s.getZoneGeo(decoupage)).orElse("N/A"),
                            Collectors.counting()));
            effectifParDecoupage.remove("N/A");
            // Anonymisation du découpage
            effectifParDecoupage.replaceAll((key, value) -> value < minDataRequired ? 0L : value);
            distributionEffectif.setEffectifParDecoupage(decoupage, effectifParDecoupage);

            double[] effectifs = effectifParDecoupage.values().stream()
                    .mapToDouble(Long::doubleValue)
                    .toArray();
            if (effectifs.length >= MIN_INTERVALLES) {
                List<Intervalle<Long>> intervalles = getIntervallesEffectif(effectifs);
                distributionEffectif.setIntervallesEffectif(decoupage, intervalles);
            }
        });

        result.setDistributionEffectif(distributionEffectif);
    }

    protected double roundValue(double value) {
        double absoluteValue = Math.abs(value);
        if (absoluteValue < 10) {
            return BigDecimal.valueOf(value).setScale(2, RoundingMode.HALF_UP).doubleValue();
        } else if (absoluteValue >= 10 && absoluteValue < 100) {
            return BigDecimal.valueOf(value).setScale(1, RoundingMode.HALF_UP).doubleValue();
        } else {
            return BigDecimal.valueOf(value).setScale(0, RoundingMode.HALF_UP).doubleValue();
        }
    }

    protected void fillMoyennesYValue(Metadata yVar, Set<SystemeWithYXValues> systemes, ResultWithCoordinatesAndSectors result) {
        VariableGeoDistribution variableGeoDistribution = new VariableGeoDistribution();

        DistributionEffectif distributionEffectif = result.getDistributionEffectif();

        List.of(
                TypeDecoupageGeo.REGION,
                TypeDecoupageGeo.ANCIENNE_REGION,
                TypeDecoupageGeo.DEPARTEMENT,
                TypeDecoupageGeo.ARRONDISSEMENT,
                TypeDecoupageGeo.BASSIN_VITICOLE
        ).forEach(decoupage -> {
            Map<String, Long> effectifParDecoupage = distributionEffectif.getEffectifParDecoupage(decoupage);
            Map<String, Double> moyenneParDecoupage = systemes.stream()
                    .filter(s ->
                            (s.getZoneGeo(decoupage) != null)
                                    && (effectifParDecoupage.getOrDefault(s.getZoneGeo(decoupage), 0L) > 0))
                    .collect(Collectors.groupingBy(
                            s -> Optional.ofNullable(s.getZoneGeo(decoupage)).orElse("N/A"),
                            Collectors.averagingDouble(SystemeWithYXValues::getyVal)));
            moyenneParDecoupage.remove("N/A");
            moyenneParDecoupage.replaceAll((d, v) -> roundValue(v));
            variableGeoDistribution.setMoyenneParDecoupage(decoupage, moyenneParDecoupage);

            double[] values = moyenneParDecoupage.values().stream().mapToDouble(v -> v).toArray();
            if (values.length >= MIN_INTERVALLES) {
                List<Intervalle<Double>> intervalles = getIntervalles(values);
                variableGeoDistribution.setIntervalles(decoupage, intervalles);
            }
        });

        if (yVar.getCategory().contains(MetadataConstants.EVOLUTION_IFT_SIMPLIFIED_CATEGORY_NAME)) {
            variableGeoDistribution.setSortType(SortType.DESCENDING);
        }

        result.setVariableYGeoDistribution(variableGeoDistribution);
    }

    protected void fillMoyennesXValue(Metadata xVar, Set<SystemeWithYXValues> systemes, ResultWithCoordinatesAndSectors result) {
        VariableGeoDistribution variableGeoDistribution = new VariableGeoDistribution();

        DistributionEffectif distributionEffectif = result.getDistributionEffectif();

        List.of(
                TypeDecoupageGeo.REGION,
                TypeDecoupageGeo.ANCIENNE_REGION,
                TypeDecoupageGeo.DEPARTEMENT,
                TypeDecoupageGeo.ARRONDISSEMENT,
                TypeDecoupageGeo.BASSIN_VITICOLE
        ).forEach(decoupage -> {
            Map<String, Long> effectifParDecoupage = distributionEffectif.getEffectifParDecoupage(decoupage);
            Map<String, Double> moyenneParDecoupage = systemes.stream()
                    .filter(s ->
                            (s.getZoneGeo(decoupage) != null)
                                    && (effectifParDecoupage.getOrDefault(s.getZoneGeo(decoupage), 0L) > 0))
                    .collect(Collectors.groupingBy(
                            s -> Optional.ofNullable(s.getZoneGeo(decoupage)).orElse("N/A"),
                            Collectors.averagingDouble(SystemeWithYXValues::getxVal)));
            moyenneParDecoupage.remove("N/A");
            moyenneParDecoupage.replaceAll((d, v) -> roundValue(v));
            variableGeoDistribution.setMoyenneParDecoupage(decoupage, moyenneParDecoupage);

            double[] values = moyenneParDecoupage.values().stream().mapToDouble(v -> v).toArray();
            if (values.length >= MIN_INTERVALLES) {
                List<Intervalle<Double>> intervalles = getIntervalles(values);
                variableGeoDistribution.setIntervalles(decoupage, intervalles);
            }
        });

        if (xVar.getCategory().contains(MetadataConstants.EVOLUTION_IFT_SIMPLIFIED_CATEGORY_NAME)) {
            variableGeoDistribution.setSortType(SortType.DESCENDING);
        }

        result.setVariableXGeoDistribution(variableGeoDistribution);
    }

    protected List<Intervalle<Long>> getIntervallesEffectif(double[] effectifs) {
        Percentile p = new Percentile().withEstimationType(Percentile.EstimationType.R_7);
        long minDataRequired = DephyGraphConfig.get().getMinDataRequired();

        List<Intervalle<Long>> intervalles = new ArrayList<>();

        long low = 0;
        long high = minDataRequired;
        Intervalle<Long> intervalle = Intervalle.from(low, high - 1);
        intervalles.add(intervalle);
        for (Integer fraction : PERCENTILE_FRACTIONS) {
            low = high;
            long nextHigh = Math.round(p.evaluate(effectifs, fraction));
            if (nextHigh > low) {
                high = nextHigh;
                intervalle = Intervalle.from(low, high - 1);
                intervalles.add(intervalle);
            }
        }
        intervalle = Intervalle.from(high, null);
        intervalles.add(intervalle);

        return intervalles;
    }

    protected List<Intervalle<Double>> getIntervalles(double[] values) {
        Percentile p = new Percentile().withEstimationType(Percentile.EstimationType.R_7);

        List<Intervalle<Double>> intervalles = new ArrayList<>();

        Double low = null;
        Double high = null;
        for (Integer fraction : PERCENTILE_FRACTIONS) {
            double value = p.evaluate(values, fraction);
            double nextHigh = roundValue(value);
            if (low == null || nextHigh > low) {
                high = nextHigh;
                intervalles.add(Intervalle.from(low, high));
                low = high;
            }
        }
        intervalles.add(Intervalle.from(high, null));

        return intervalles;
    }

    @Deprecated
    protected List<FilterChoice> checkFilters(Collection<FilterChoice> filterChoices, AgrosystInfo agrosystInfo) throws Exception {
        List<FilterChoice> checkedFilters = new ArrayList<>();
        for (FilterChoice filterChoice : filterChoices) {
            Metadata metadata = getMetadata(filterChoice.getColumnname());

            // Condition for a filter :
            // - Should be an existing Metadata
            // - Should be a filter OR if an user is connected an IR Filter
            // - Think about particular case of "DephyNb" : could be used as filter, with from session values
            if (metadata == null || ((metadata.isIrfilter() || FiltersService.DEPHY_NB_COLUMN_NAME.equals(metadata.getColumnname())) && agrosystInfo == null) || !metadata.isFilter()) {
                throw new InvalidFilterException(filterChoice.getColumnname());
            }

            FilterChoice validationFilter = new FilterChoice();
            validationFilter.copyMetadata(metadata);
            validationFilter.setValues(Lists.newCopyOnWriteArrayList(filterChoice.getValues()));
            checkedFilters.add(validationFilter);
        }
        return checkedFilters;
    }

    protected List<FilterChoice> getDefaultFilters() throws Exception {
        List<FilterChoice> defaultFilters = new ArrayList<>();
        FilterChoice validationFilter = getDefaultValidationFilter();
        if (validationFilter != null) {
            defaultFilters.add(validationFilter);
        }
        return defaultFilters;
    }

    protected FilterChoice getDefaultValidationFilter() throws Exception {
        Metadata validationMetadata = getMetadata("validation");
        FilterChoice validationFilter = null;
        if (validationMetadata != null) {
            validationFilter = new FilterChoice();
            validationFilter.copyMetadata(validationMetadata);
            validationFilter.setValues(List.of("true"));
        }
        return validationFilter;
    }

    protected List<SpecificValue> getDephyNbValues(Handle h, List<String> dephyNbs, Metadata xMetadata, Metadata yMetadata, List<FilterChoice> paramFilterChoices, AgrosystInfo agrosystInfo) throws Exception {
        List<FilterChoice> filterChoices = getFilterChoices(paramFilterChoices, agrosystInfo);

        FilterChoice dephyNbsFilter = new FilterChoice();
        dephyNbsFilter.setColumnname("dephyNb");
        dephyNbsFilter.setValues(dephyNbs);

        DatamartDao datamartDao = new DatamartDao(h);
        return datamartDao.getSpecificValues(dephyNbsFilter, xMetadata, yMetadata, filterChoices);

    }

    protected List<SpecificValue> getDephyNbValues(Handle h, List<String> dephyNbs, Metadata searchMetadata, List<FilterChoice> paramFilterChoices, AgrosystInfo agrosystInfo) throws Exception {
        List<FilterChoice> filterChoices = getFilterChoices(paramFilterChoices, agrosystInfo);

        FilterChoice dephyNbsFilter = new FilterChoice();
        dephyNbsFilter.setColumnname("dephyNb");
        dephyNbsFilter.setValues(dephyNbs);

        DatamartDao datamartDao = new DatamartDao(h);
        return datamartDao.getSpecificValues(dephyNbsFilter, searchMetadata, filterChoices);

    }

    protected Metadata getMetadata(String colName) {
        return metadataCache.getIfPresent(colName);
    }

    protected void logParam(HttpServletRequest request, AbstractDistributionParam param) {
        try {
            String clientIp = EndpointUtils.getClientIp(request);
            LOGGER.info(DISTRIBUTION_PARAM_LOG_MARKER, "{} ({})", mapper.writeValueAsString(param), clientIp);
        } catch (JsonProcessingException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
