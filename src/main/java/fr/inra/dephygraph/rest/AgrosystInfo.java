package fr.inra.dephygraph.rest;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class AgrosystInfo {

    protected UUID id;
    protected String userName;
    protected List<String> allowedDephyNb;
    protected List<String> allowedIrCode;
    protected OffsetDateTime expirationDate;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    //FIXME: trick cause Spgeed does not support camelcase in attribute
    public void setUsername(String userName) {
        this.userName = userName;
    }

    public List<String> getAllowedDephyNb() {
        return allowedDephyNb;
    }

    public void setAllowedDephyNb(List<String> allowedDephyNb) {
        if (allowedDephyNb == null) {
            this.allowedDephyNb = null;
        } else {
            this.allowedDephyNb = new ArrayList<>();
            this.allowedDephyNb.addAll(allowedDephyNb);
        }
    }

    //FIXME: trick cause Spgeed does not support camelcase in attribute
    public void setAlloweddephynb(List<String> allowedDephyNb) {
        this.setAllowedDephyNb(allowedDephyNb);
    }

    public List<String> getAllowedIrCode() {
        return allowedIrCode;
    }

    public void setAllowedIrCode(List<String> allowedIrCode) {
        if (allowedIrCode == null) {
            this.allowedIrCode = null;
        } else {
            this.allowedIrCode = new ArrayList<>();
            this.allowedIrCode.addAll(allowedIrCode);
        }
    }

    public OffsetDateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(OffsetDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    //FIXME: trick cause Spgeed does not support camelcase in attribute
    public void setExpirationdate(OffsetDateTime expirationDate) {
        this.setExpirationDate(expirationDate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AgrosystInfo that = (AgrosystInfo) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(userName, that.userName) &&
            Objects.equals(allowedDephyNb, that.allowedDephyNb) &&
            Objects.equals(allowedIrCode, that.allowedIrCode);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, userName, allowedDephyNb, allowedIrCode);
    }
}
