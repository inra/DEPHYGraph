package fr.inra.dephygraph.rest;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.cache.Cache;
import com.google.common.collect.Lists;
import fr.inra.dephygraph.DephyGraphDatasource;
import fr.inra.dephygraph.dao.AgrosystInfoDao;
import fr.inra.dephygraph.dao.DatamartDao;
import fr.inra.dephygraph.model.FilterCohort;
import fr.inra.dephygraph.model.FilterValues;
import fr.inra.dephygraph.model.Metadata;
import fr.inra.dephygraph.exceptions.AuthorizationException;
import fr.inra.dephygraph.exceptions.NoSuchMetadataException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.apache.commons.lang3.StringUtils;
import org.jdbi.v3.core.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ymartel (martel@codelutin.com)
 */
@Path("/v1/filters")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class FiltersService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FiltersService.class);

    protected final DephyGraphDatasource ds;

    protected final Cache<String, FilterValues> filterCache;
    protected final Cache<String, FilterValues> irFilterCache;

    protected static final String DEPHY_NB_COLUMN_NAME = "dephyNb";
    protected static final String CAMPAIGN_COLUMN_NAME = "c104_campaign_dis";
    protected static final String NETWORK_AGE_COLUMN_NAME = "c103_networkYears";

    public FiltersService(DephyGraphDatasource ds,
                          Cache<String, FilterValues> filterCache,
                          Cache<String, FilterValues> irFilterCache) {
        this.ds = ds;
        this.filterCache = filterCache;
        this.irFilterCache = irFilterCache;
    }

    @GET
    @Path("/")
    @Operation(summary = "Get only the metadata usable as filters",
        responses = {
            @ApiResponse(description = "All the available filter metadata",
                content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Metadata.class)))})
    public List<Metadata> getAllFilters() throws Exception {
        try (Handle h = ds.getJdbi().open()) {
            DatamartDao dao = new DatamartDao(h);

            return dao.getAllFilterMetadata();
        }
    }

    @GET
    @Path("/{filterName}/values")
    @Operation(summary = "Get all existing values for a given filter metadata",
        responses = {
            @ApiResponse(description = "All the values, as string",
                content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = FilterValues.class)))})
    public FilterValues getFilterValues(@PathParam("filterName") String filterName) throws Exception {

        FilterValues values = getFilterValues(ds, filterName, null, false);
        if (values == null) {
            throw new NoSuchMetadataException(filterName);
        }

        return values;
    }

    @GET
    @Path("/dephyNb/values")
    @Operation(summary = "Get all authorized dephyNbs values for current user",
        responses = {
            @ApiResponse(description = "All the values, as string",
                content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = FilterValues.class)))})
    public FilterValues getDephyNbValues(@PathParam("filterName") String filterName,
                                         @HeaderParam("Authorization") String authorization) throws Exception {

        if (StringUtils.isBlank(authorization)) {
            throw new AuthorizationException("No authorization token");
        }

        LOGGER.info("Check authorization");
        AgrosystInfo agrosystInfo = AgrosystService.checkAuthorization(ds, authorization);

        try (Handle handle = ds.getJdbi().open()) {
            AgrosystInfoDao agrosystInfoDao = new AgrosystInfoDao(handle);
            List<String> allowedDephyNb = agrosystInfoDao.retrieveDephyNb(agrosystInfo.getId());

            if (allowedDephyNb.size() == 0) {
                DatamartDao  datamartDao = new DatamartDao(handle);
                Metadata filterMetadata = datamartDao.getMetadataByColumnName(DEPHY_NB_COLUMN_NAME);
                FilterValues emptyFilterValues = new FilterValues();
                emptyFilterValues.setColumnname(filterName);
                emptyFilterValues.setDisplayname(filterMetadata.getDisplayname());
                emptyFilterValues.setValues(Collections.emptyList());
                return emptyFilterValues;
            } else {
                return getFilterValues(ds, DEPHY_NB_COLUMN_NAME, allowedDephyNb, true);
            }
        }
    }

    @GET
    @Path("/values")
    @Operation(summary = "Get all filters with their existing values",
        responses = {
            @ApiResponse(description = "All the filters, with their values",
                content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = FilterValues.class)))})
    public List<FilterValues> getAllValues(@HeaderParam("Authorization") String authorization) throws Exception {

        AgrosystInfo agrosystInfo = null;
        if (StringUtils.isNotBlank(authorization)) {
            LOGGER.info("Check authorization");
            agrosystInfo = AgrosystService.checkAuthorization(ds, authorization);
        }

        ArrayList<FilterValues> filterValues = Lists.newArrayList(filterCache.asMap().values());
        if (agrosystInfo != null) {
            // An IR is connected : get allowed dephyNbs for this user from session
            try (Handle h = ds.getJdbi().open()) {
                AgrosystInfoDao  agrosystInfoDao = new AgrosystInfoDao(h);
                List<String> retrieveDephyNb = agrosystInfoDao.retrieveDephyNb(agrosystInfo.getId());

                FilterValues dephyNbValues = getFilterValues(ds, DEPHY_NB_COLUMN_NAME, retrieveDephyNb, true);
                if (dephyNbValues != null) {
                    filterValues.add(dephyNbValues);
                }
                // Add specific IR Filters too
                filterValues.addAll(irFilterCache.asMap().values());
            }
        }

        return filterValues.stream().sorted(Comparator.comparing(FilterValues::getDisplayname)).collect(Collectors.toList());
    }

    @GET
    @Path("/filterCohortValues")
    public FilterCohort getFilterCohortValues() {
        FilterCohort result = new FilterCohort();
        try (Handle h = ds.getJdbi().open()) {
            DatamartDao datamartDao = new DatamartDao(h);
            List<String> years = datamartDao.getQualitativeMetadataValues(CAMPAIGN_COLUMN_NAME);
            List<String> ages = datamartDao.getQualitativeMetadataValues(NETWORK_AGE_COLUMN_NAME);
            result.setYearsFromStrings(years);
            result.setNetworkAges(new HashSet<>(ages));
        }
        return result;
    }

    protected FilterValues getFilterValues(DephyGraphDatasource ds, String filterName, List<String> purposedValues, boolean isAuthorized) throws Exception {

        FilterValues filter = null;

        try (Handle handle = ds.getJdbi().open()) {
            DatamartDao dao = new DatamartDao(handle);
            Metadata filterMetadata = dao.getMetadataByColumnName(filterName);

            if (filterMetadata != null && (filterMetadata.isFilter() || (isAuthorized && filterMetadata.isFilter()) )) {
                List<String> filterValues = dao.getEffectiveFilterValues(filterMetadata, purposedValues);
                filter = new FilterValues();
                filter.setColumnname(filterName);
                filter.setDisplayname(filterMetadata.getDisplayname());
                filter.setValues(filterValues);
            }
        }

        return filter;
    }

}
