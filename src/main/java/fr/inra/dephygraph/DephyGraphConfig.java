package fr.inra.dephygraph;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class DephyGraphConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(DephyGraphConfig.class);

    protected static DephyGraphConfig instance = new DephyGraphConfig();

    public static DephyGraphConfig get() {
        return instance;
    }

    protected ApplicationConfig configDefault;
    protected ApplicationConfig config;

    protected FileTime systemConfigModificationTime;
    protected FileTime userConfigModificationTime;

    protected DephyGraphConfig() {
        loadConfiguration();
    }

    protected void loadConfiguration() {
        configDefault = new ApplicationConfig("dephygraph.properties");
        try {
            configDefault.parse();
            setConfigurationModificationTimes(configDefault);
        } catch (ArgumentsParserException e) {
            LOGGER.error("Parse config error", e);
        }

        this.config = new ApplicationConfig(configDefault.getFlatOptions(), "dephygraph-test.properties");
        try {
            this.config.parse();
        } catch (ArgumentsParserException e) {
            LOGGER.error("Parse config error", e);
        }
    }

    protected void setConfigurationModificationTimes(ApplicationConfig conf) {
        if (conf == null) {
            return;
        }
        systemConfigModificationTime = getConfigFileLastModifiedTime(conf.getSystemConfigFile());
        userConfigModificationTime = getConfigFileLastModifiedTime(conf.getUserConfigFile());
    }

    protected FileTime getConfigFileLastModifiedTime(File configFile) {
        if (configFile == null) {
            return null;
        }
        try {
            BasicFileAttributes attr = Files.readAttributes(configFile.toPath(), BasicFileAttributes.class);
            return attr.lastModifiedTime();
        } catch(Exception e) {
            LOGGER.warn("Failed to read attributes of configuration file {}", configFile);
            return null;
        }
    }

    protected boolean configurationChanged(ApplicationConfig conf) {
        if (conf == null) {
            return false;
        }

        if (systemConfigModificationTime != null) {
            FileTime currentSystemConfigModificationTime = getConfigFileLastModifiedTime(conf.getSystemConfigFile());
            if (systemConfigModificationTime.compareTo(currentSystemConfigModificationTime) != 0) {
                return true;
            }
        }
        if (userConfigModificationTime != null) {
            FileTime currentUserConfigFileLastModifiedTime = getConfigFileLastModifiedTime(conf.getUserConfigFile());
            if (userConfigModificationTime.compareTo(currentUserConfigFileLastModifiedTime) != 0) {
                return true;
            }
        }

        return false;
    }

    protected void reloadConfigurationIfChanged() {
        if (configurationChanged(configDefault)) {
            loadConfiguration();
            LOGGER.info("Reloaded configuration");
        }
    }

    protected String getMandatoryOption(String key) {
        reloadConfigurationIfChanged();
        String result = this.config.getOption(key);
        Preconditions.checkState(StringUtils.isNotEmpty(result), key + " is missing");
        return result;
    }

    protected String getOption(String key) {
        reloadConfigurationIfChanged();
        return this.config.getOption(key);
    }

    protected int getOptionAsInt(String key) {
        reloadConfigurationIfChanged();
        return this.config.getOptionAsInt(key);
    }

    public String getDataSourceUrl() {
        return getMandatoryOption("dephygraph.datasource.url");
    }

    public String getDataSourceUser() {
        return getMandatoryOption("dephygraph.datasource.user");
    }

    public String getDataSourcePassword() {
        return getMandatoryOption("dephygraph.datasource.password");
    }

    public int getMinDataRequired() {
        return getOptionAsInt("dephygraph.minDataRequired");
    }

    public int getMinDataRequiredForCohort() {
        return getOptionAsInt("dephygraph.minDataRequiredForCohort");
    }

    public String getVersion() {
        return getOption("dephygraph.version");
    }

    public String getHelpLink() {
        return getOption("dephygraph.helpLink");
    }

    public String getWebSecurityKey() {
        return getOption("dephygraph.webSecurityKey");
    }

    public String getLogConfigLocation() {
        return getOption("dephygraph.logConfigLocation");
    }
}
