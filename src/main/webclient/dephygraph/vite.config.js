import { fileURLToPath, URL } from 'url';
import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {

  const env = loadEnv('development', process.cwd(), '')

  return {
    plugins: [vue()],
    resolve: {
      alias: {
          '@': fileURLToPath(new URL('./src', import.meta.url))
      }
    },
    server: {
      proxy: {
        "/api": {
          ws: true,
          changeOrigin: true,
          target: env.VITE_APP_SERVER_URL
        },
        "/img": {
          ws: true,
          changeOrigin: true,
          target: env.VITE_APP_SERVER_URL
        },
        "/font": {
          ws: true,
          changeOrigin: true,
          target: env.VITE_APP_SERVER_URL
        },
        "/help": {
          ws: true,
          changeOrigin: true,
          target: env.VITE_APP_SERVER_URL
        },
        "/carto": {
          ws: true,
          changeOrigin: true,
          target: env.VITE_APP_SERVER_URL
        }
      }
    },
    base: './'
  }
})
