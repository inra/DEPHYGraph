import { fileURLToPath, URL } from 'url';
import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'

// Exemple de valeur pour VITE_APP_SERVER_URL renseigné dans .env.development.local avec Docker:
// VITE_APP_SERVER_URL=http://host.docker.internal:8084/dephy-graph/

export default defineConfig(({ mode }) => {

  const env = loadEnv('development', process.cwd(), '')

  return {
    plugins: [vue()],
    resolve: {
      alias: {
          '@': fileURLToPath(new URL('./src', import.meta.url))
      }
    },
    server: {
      host: true,
      port: 3000,
      hmr: {
        clientPort: 9091
      },
      proxy: {
        "/api": {
          ws: true,
          changeOrigin: true,
          target: env.VITE_APP_SERVER_URL
        },
        "/img": {
          ws: true,
          changeOrigin: true,
          target: env.VITE_APP_SERVER_URL
        },
        "/font": {
          ws: true,
          changeOrigin: true,
          target: env.VITE_APP_SERVER_URL
        },
        "/help": {
          ws: true,
          changeOrigin: true,
          target: env.VITE_APP_SERVER_URL
        },
        "/carto": {
          ws: true,
          changeOrigin: true,
          target: env.VITE_APP_SERVER_URL
        }
      }
    }
  }
})
