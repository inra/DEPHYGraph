const colorsPalette = [
  '#5578C2', '#5793F3', '#76D2C5', '#16A086', '#9A5071',
  '#7375A5', '#4B7C83', '#355C7D', '#9FD6D2', '#DAD8A7',
  '#556270', '#919999', '#E0E0E0', '#73A8AF', '#B3CC57',
  '#ECF081', '#D9CC8C', '#8C6673', '#948C75', '#D9CEB2',
  '#78412D', '#181623', '#40474F', '#2A135B', '#361B3C',
  '#974B69', '#B7728F', '#C98A45', '#035469', '#038079',
  '#2A054F'
]

const getColor = (index) => {
  return colorsPalette[index % colorsPalette.length]
}

const buildColorsPalette = (count) => {
  let palette = []

  for (let i = 0; i < count; i++) {
    palette.push(colorsPalette[i % colorsPalette.length])
  }

  return palette
}

export function useColorsUtils() {
  return {
    colorsPalette,
    getColor,
    buildColorsPalette
  }
}
