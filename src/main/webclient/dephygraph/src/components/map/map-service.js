let minDataRequired = null
let regionGeoVec = null
let ancienneRegionGeoVec = null
let departementGeoVec = null
let arrondissementGeoVec = null
let bassinViticoleGeoVec = null

const getMinDataRequired = async () => {
  if (minDataRequired == null) {
    const response = await fetch('api/v1/info/')
    const data = await response.json()
    minDataRequired = data.minDataRequired
    return minDataRequired
  } else {
    return Promise.resolve(minDataRequired)
  }
}

const getRegionGeoVec = async () => {
  if (regionGeoVec == null) {
    const response = await fetch('carto/geoVec_reg.json')
    const data = await response.json()
    regionGeoVec = data
    return regionGeoVec
  } else {
    return Promise.resolve(regionGeoVec)
  }
}

const getAncienneRegionGeoVec = async () => {
  if (ancienneRegionGeoVec == null) {
    const response = await fetch('carto/geoVec_oldreg.json')
    const data = await response.json()
    ancienneRegionGeoVec = data
    return ancienneRegionGeoVec
  } else {
    return Promise.resolve(ancienneRegionGeoVec)
  }
}

const getDepartementGeoVec = async () => {
  if (departementGeoVec == null) {
    const response = await fetch('carto/geoVec_dep.json')
    const data = await response.json()
    departementGeoVec = data
    return departementGeoVec
  } else {
    return Promise.resolve(departementGeoVec)
  }
}

const getArrondissementGeoVec = async () => {
  if (arrondissementGeoVec == null) {
    const response = await fetch('carto/geoVec_arrond.json')
    const data = await response.json()
    arrondissementGeoVec = data
    return arrondissementGeoVec
  } else {
    return Promise.resolve(arrondissementGeoVec)
  }
}

const getBassinViticoleGeoVec = async () => {
  if (bassinViticoleGeoVec == null) {
    const response = await fetch('carto/geoVec_viti.json')
    const data = await response.json()
    bassinViticoleGeoVec = data
    return bassinViticoleGeoVec
  } else {
    return Promise.resolve(bassinViticoleGeoVec)
  }
}

export function useMapService() {
  return {
    getMinDataRequired,
    getRegionGeoVec,
    getAncienneRegionGeoVec,
    getDepartementGeoVec,
    getArrondissementGeoVec,
    getBassinViticoleGeoVec
  }
}
