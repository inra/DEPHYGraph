import { ref } from 'vue'

// Relatives URL defined here and not directly in the directive to make Rollup happy

export function useWelcomeResources() {
  const imgHome1Url = ref('img/home-1.jpg')
  const imgHome2Url = ref('img/home-2.jpg')
  const imgHome3Url = ref('img/home-3.jpg')
  const imgHome4Url = ref('img/home-4.jpg')

  const logoEcophytoDephyUrl = ref('img/ecophyto_dephy_logo.png')
  const logoInraeUrl = ref('img/logo_inrae.png')
  const logoGouvernementUrl = ref('img/logo_gouvernement_512px.png')
  const logoOfbUrl = ref('img/logo_ofb.jpg')

  const logoFacebookUrl = ref('img/fb_alt_logo.png')
  const logoYouTubeUrl = ref('img/yt_alt_logo.png')
  const logoLinkedInUrl = ref('img/li_alt_logo.png')

  return {
    imgHome1Url, imgHome2Url, imgHome3Url, imgHome4Url,
    logoEcophytoDephyUrl, logoInraeUrl, logoGouvernementUrl, logoOfbUrl,
    logoFacebookUrl, logoYouTubeUrl, logoLinkedInUrl
  }
}
