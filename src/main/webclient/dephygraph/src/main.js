import { createApp } from 'vue'
import 'vue-multiselect/dist/vue-multiselect.css'
import 'leaflet/dist/leaflet.css'
import '@/style.css'
import router from '@/router'
import App from '@/App.vue'

import FiltersModal from '@/components/modals/FiltersModal.vue'
import UrlGeneratorModal from '@/components/modals/UrlGeneratorModal.vue'
import BoxPlotChartConfigModal from '@/components/modals/BoxPlotChartConfigModal.vue'

const app = createApp(App)
app.use(router)
app.component('FiltersModal', FiltersModal)
app.component('UrlGeneratorModal', UrlGeneratorModal)
app.component('BoxPlotChartConfigModal', BoxPlotChartConfigModal)
app.mount('#app')
