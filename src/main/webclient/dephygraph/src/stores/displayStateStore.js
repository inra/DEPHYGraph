import { reactive } from 'vue'

export const displayStateStore = reactive({
  dashboardSearchMinimized: false,
  dashboardDisplayConfig: {
    graph: true,
    map: true,
    table: true,
  },
  currentMapLayerName: null,

  minimizeDashboardSearch() {
    this.dashboardSearchMinimized = true
  },

  maximizeDashboardSearch() {
    this.dashboardSearchMinimized = false
  },

  displayAll() {
    this.dashboardDisplayConfig.graph = true
    this.dashboardDisplayConfig.map = true
    this.dashboardDisplayConfig.table = true
  }
})
