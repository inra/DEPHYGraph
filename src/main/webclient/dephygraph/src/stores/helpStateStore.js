import { reactive } from 'vue'

import { APP_CONSTANTS } from '@/constants'

import { useUserEventsLogger } from '@/user-events-logger'

const {
  logHelpVariableEvent
} = useUserEventsLogger()

export const helpStateStore = reactive({
  helpDisplayed: false,
  helpCategory: APP_CONSTANTS.HELP_CATEGORY_CONTEXT,
  selectedVariable: null,

  showHelp(variable) {
    // Use like:
    //   const v = metadataStore.variable(colname)
    //   helpStateStore.showHelp(v)
    this.helpDisplayed = true
    if (variable) {
      this.selectedVariable = variable
      logHelpVariableEvent({
        action: "Affichage du panneau d'aide",
        variable: variable && variable.columnname || ''
      })
    }
  },

  refreshHelp(variable) {
    if (this.helpDisplayed === true && variable) {
      this.selectedVariable = variable
    }
  },

  closeHelp() {
    this.helpDisplayed = false
  },

  toggleHelp() {
    this.helpDisplayed = !this.helpDisplayed
  },

  setHelpCategory(category) {
    this.helpCategory = category
    logHelpVariableEvent({
      action: `Affichage de l'aide (${category})`
    })
  }
})
