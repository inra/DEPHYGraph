import _ from 'lodash'
import { reactive } from 'vue'

import { APP_CONSTANTS } from '@/constants'

export const metadataStore = reactive({
  metadataList: [],
  metadataMap: {},
  cohortTypesList: [
    {
      displayname: 'Par campagne',
      columnname: APP_CONSTANTS.VARIABLE_NAME_CAMPAIGN,
      values: []
    },
    {
      displayname: 'Par ancienneté dans le réseau',
      columnname: APP_CONSTANTS.VARIABLE_NAME_NETWORK_YEARS,
      values: []
    }
  ],
  cohortTypesMap: {},
  loading: false,
  error: null,

  variable(name) {
    return this.metadataMap[name]
  },

  variableValues(name) {
    return this.metadataMap[name] && this.metadataMap[name].values || []
  },

  lightMetadataList() {
    return _(this.metadataList).map((item) => ({
      category: item.category,
      columnname: item.columnname,
      displayname: item.displayname,
      qualitative: item.qualitative,
      quantitative: item.quantitative,
      unity: item.unity || null,
      recommendedquery: item.recommendedquery,
      availabilitybysector: item.availabilitybysector,
      vardef: item.vardef
    }))
  },

  lightFiltersList() {
    return _(this.metadataList)
      .filter((item) => item.filter === true)
      .map((item) => ({
        category: item.category,
        columnname: item.columnname,
        displayname: item.displayname,
        recommendedfilter: item.recommendedfilter
      }))
  },

  async fetchMetadata() {
    if (this.metadataList.length == 0) {
      this.loading = true
      this.error = null
      try {
        this.metadataList = await fetch('api/v1/metadata/')
          .then((response) => response.json())
        for (let item of this.metadataList) {
          this.metadataMap[item.columnname] = item
          this.metadataMap[item.columnname].values = null
        }
        let valuesList = await fetch('api/v1/filters/values')
          .then((response) => response.json())
        for (let item of valuesList) {
          this.metadataMap[item.columnname].values = item.values
        }
        let cohortValuesList = await fetch('api/v1/filters/filterCohortValues')
          .then((response) => response.json())
        for (let item of this.cohortTypesList) {
          if (item.columnname == APP_CONSTANTS.VARIABLE_NAME_CAMPAIGN) {
            item.values = cohortValuesList.years.sort()
          } else if (item.columnname == APP_CONSTANTS.VARIABLE_NAME_NETWORK_YEARS) {
            item.values = cohortValuesList.networkAges.sort()
          }
          this.cohortTypesMap[item.columnname] = item
        }
      } catch (error) {
        this.error = error
      } finally {
        this.loading = false
      }
    }
  }
})
