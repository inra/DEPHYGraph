import { reactive } from 'vue'

export const infoStore = reactive({
  version: null,
  dataQualityWarningAcknowledgeDate: localStorage.getItem('dataQualityWarningAcknowledgeDate'),
  loading: false,
  error: null,

  getVersion() {
    return this.version
  },

  hasAckDataQualityWarning() {
    return !!this.dataQualityWarningAcknowledgeDate
  },

  async fetchVersion() {
    if (!this.version) {
      this.version = null
      this.loading = true
      try {
        let data = await fetch('api/v1/info/')
          .then((response) => response.json())
        this.version = data.version
      } catch (error) {
        this.error = error
      } finally {
        this.loading = false
      }
    }
  },

  ackDataQualityWarning() {
    localStorage.setItem('dataQualityWarningAcknowledgeDate', new Date())
    this.dataQualityWarningAcknowledgeDate = localStorage.getItem('dataQualityWarningAcknowledgeDate')
  }
})
