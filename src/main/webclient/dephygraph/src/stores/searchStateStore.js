import _ from 'lodash'
import { reactive, shallowRef } from 'vue'

import { metadataStore } from '@/stores/metadataStore'
import { useColorsUtils } from '@/colors-utils'

import { APP_CONSTANTS } from '@/constants'

const {
  getColor,
  buildColorsPalette
} = useColorsUtils()

const buildXLabelsDescription = (repartition) => {
  let xLabelsDescr = {
    values: [],
    colorMap: {}
  }

  let nextColorIndex = 0;
  let xLabels = [];

  _.forEachRight(repartition, (item) => {
    _.forEach(item.innerRepartition, (r) => {
      if (!xLabelsDescr.colorMap[r.valueName]) {
        let color = getColor(nextColorIndex);
        xLabelsDescr.colorMap[r.valueName] = color;
        xLabels.push({
          label: r.valueName,
          color: color
        });
        ++nextColorIndex;
      }
    });
  });
  xLabelsDescr.values = _.orderBy(xLabels, ['label']);

  return xLabelsDescr
}

const showMissingCohortMessage = (params) => {
  if (params.xColumnName == APP_CONSTANTS.VARIABLE_NAME_NETWORK_YEARS
      || params.xColumnName == APP_CONSTANTS.VARIABLE_NAME_CAMPAIGN
      || params.xColumnName == APP_CONSTANTS.VARIABLE_NAME_PZ0) {
    if (params.filterCohort.years.length == 0 && params.filterCohort.networkAges.length == 0) {
      return true
    }
  }
  return false
}

const noDataCausedByCohortYearAndPz0 = (params) => {
  if (params.xColumnName == APP_CONSTANTS.VARIABLE_NAME_PZ0) {
    if (params.filterCohort.years.length > 0 && params.filterCohort.years.indexOf('2011') >= 0) {
      return true
    }
  }
  return false
}

const noDataCausedByCohortAgeAndPz0 = (params) => {
  if (params.xColumnName == APP_CONSTANTS.VARIABLE_NAME_PZ0) {
    if (params.filterCohort.networkAges.length > 0 && params.filterCohort.networkAges.indexOf('État initial') >= 0) {
      return true
    }
  }
  return false
}

export const searchStateStore = reactive({
  searchMode: APP_CONSTANTS.QUICK_SEARCH,
  quickSearches: [],
  selectedQuickSearch: null,
  query: {
    xVariable: null,
    yVariable: null,
    filtersMap: {},
    cohortsMap: {}
  },
  distribution: {},
  searchResult: shallowRef({}),
  loadingQuickSearches: false,
  loadingSearchResult: false,
  error: null,

  canSwapVariables() {
    if (!this.query.yVariable || !this.query.xVariable) {
      return false
    } else if (this.query.yVariable.qualitative === false && this.query.xVariable.qualitative === true) {
      return false
    } else {
      return true
    }
  },

  hasSelectedFilters() {
    return _.keys(this.query.filtersMap).length > 0
  },

  hasSelectedCohorts() {
    return _.keys(this.query.cohortsMap).length > 0
  },

  selectedFiltersCount() {
    return _.keys(this.query.filtersMap).length + _.keys(this.query.cohortsMap).length
  },

  getRouteQuery() {
    let routeQuery = {}
    if (!this.query.yVariable) {
      return routeQuery
    }
    routeQuery.y = this.query.yVariable.columnname
    if (this.query.xVariable) {
      routeQuery.x = this.query.xVariable.columnname
    }

    _.forIn(this.query.filtersMap, (entry, key) => {
      if (entry.selection && entry.selection.length > 0) {
        routeQuery['f'+key] = entry.selection.toString()
      }
    })
    _.forIn(this.query.cohortsMap, (entry, key) => {
      if (entry.selection && entry.selection.length > 0) {
        if (key == APP_CONSTANTS.VARIABLE_NAME_CAMPAIGN) {
          routeQuery['cyears'] = entry.selection.toString()
        } else if (key == APP_CONSTANTS.VARIABLE_NAME_NETWORK_YEARS) {
          routeQuery['cnetworkages'] = entry.selection.toString()
        }
      }
    })

    return routeQuery
  },

  async fetchQuickSearches() {
    if (this.quickSearches.length == 0) {
      this.loadingQuickSearches = true
      this.error = null
      try {
        this.quickSearches = await fetch('api/v1/statistics/recommendedGraphics')
          .then((response) => response.json())
      } catch (error) {
        this.error = error
      } finally {
        this.loadingQuickSearches = false;
      }
    }
  },

  async fetchSearchResult() {
    if (this.loadingSearchResult === true) {
      console.error('Search query already in progress')
      return
    }

    let options = {
      method: 'POST',
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/json'
      }
    }

    let params = {
      filterParams: [],
      filterCohort: {
        years: [],
        networkAges: []
      }
    }

    _.forIn(this.query.filtersMap, (entry, key) => {
      params.filterParams.push({
        columnName: key,
        values: entry.selection
      })
    })
    _.forIn(this.query.cohortsMap, (entry, key) => {
      if (entry.selection && entry.selection.length > 0) {
        if (key == APP_CONSTANTS.VARIABLE_NAME_CAMPAIGN) {
          params.filterCohort.years = entry.selection
        } else if (key == APP_CONSTANTS.VARIABLE_NAME_NETWORK_YEARS) {
          params.filterCohort.networkAges = entry.selection
        }
      }
    })

    let newDistribution = {}

    try {
      this.loadingSearchResult = true
      this.error = null
      this.searchResult.value = null

      if (this.query.yVariable && !this.query.xVariable) {
        params.columnName = this.query.yVariable.columnname
        options.body = JSON.stringify(params)
        if (this.query.yVariable.qualitative === true) {
          let result = await fetch('api/v1/statistics/qualitativeDistribution/', options)
            .then((response) => response.json())
          result.type = APP_CONSTANTS.DISTRIBUTION_TYPE_QUALI
          newDistribution.lastExtractionDate = result.lastAgrosystExtractionDate
          newDistribution.systemsCount = result.effectifTotal
          newDistribution.hasData = result.repartition.length > 0
          newDistribution.hoveredItem = null
          newDistribution.colors = buildColorsPalette(result.repartition.length)
          this.searchResult.value = result
        } else if (this.query.yVariable.quantitative === true) {
          let result = await fetch('api/v1/statistics/quantitativeDistribution/', options)
            .then((response) => response.json())
          result.type = APP_CONSTANTS.DISTRIBUTION_TYPE_QUANTI
          newDistribution.lastExtractionDate = result.lastAgrosystExtractionDate
          newDistribution.systemsCount = result.effectifTotal
          newDistribution.hasData = result.repartition.values && result.repartition.values.length > 0
          this.searchResult.value = result
        }
      } else if (this.query.yVariable && this.query.xVariable) {
        params.xColumnName = this.query.xVariable.columnname
        params.yColumnName = this.query.yVariable.columnname
        options.body = JSON.stringify(params)
        if (this.query.yVariable.qualitative === true && this.query.xVariable.qualitative === true) {
          let result = await fetch('api/v1/statistics/qualitativeQualitativeDistribution/', options)
            .then((response) => response.json())
          result.type = APP_CONSTANTS.DISTRIBUTION_TYPE_QUALI_QUALI
          newDistribution.lastExtractionDate = result.lastAgrosystExtractionDate
          newDistribution.systemsCount = result.effectifTotal
          newDistribution.xLabelsDescription = buildXLabelsDescription(result.repartition)
          newDistribution.showMissingCohortMessage = showMissingCohortMessage(params)
          newDistribution.hasData = result.repartition.length > 0
          newDistribution.noDataCausedByCohortYearAndPz0 = noDataCausedByCohortYearAndPz0(params)
          newDistribution.noDataCausedByCohortAgeAndPz0 = noDataCausedByCohortAgeAndPz0(params)
          this.searchResult.value = result
        } else if (this.query.yVariable.quantitative === true && this.query.xVariable.qualitative === true) {
          let result = await fetch('api/v1/statistics/quantitativeQualitativeDistribution/', options)
            .then((response) => response.json())
          result.type = APP_CONSTANTS.DISTRIBUTION_TYPE_QUANTI_QUALI
          newDistribution.lastExtractionDate = result.lastAgrosystExtractionDate
          newDistribution.systemsCount = result.effectifTotal
          newDistribution.showMissingCohortMessage = showMissingCohortMessage(params)
          newDistribution.hasData = result.repartition.length > 0
          newDistribution.noDataCausedByCohortYearAndPz0 = noDataCausedByCohortYearAndPz0(params)
          newDistribution.noDataCausedByCohortAgeAndPz0 = noDataCausedByCohortAgeAndPz0(params)
          this.searchResult.value = result
        } else if (this.query.yVariable.quantitative === true && this.query.xVariable.quantitative === true) {
          let result = await fetch('api/v1/statistics/quantitativeQuantitativeDistribution/', options)
            .then((response) => response.json())
          result.type = APP_CONSTANTS.DISTRIBUTION_TYPE_QUANTI_QUANTI
          newDistribution.lastExtractionDate = result.lastAgrosystExtractionDate
          newDistribution.systemsCount = result.effectifTotal
          newDistribution.hasData = result.repartition.length > 0
          this.searchResult.value = result
        }
      }
      if (this.searchResult.value) {
        newDistribution.type = this.searchResult.value.type
        newDistribution.dataAvailability = this.searchResult.value.sectorsAvailabilityDescription
      }
    } catch (error) {
      this.error = error
    } finally {
      this.selectedQuickSearch = null
      this.distribution = newDistribution
      this.loadingSearchResult = false
    }
  },

  syncFromQuickSearch() {
    if (!this.selectedQuickSearch) {
      return
    }

    let newQuery = {
      xVariable: null,
      yVariable: null,
      filtersMap: {},
      cohortsMap: {}
    }

    const params = JSON.parse(this.selectedQuickSearch.parameters)
    newQuery.yVariable = metadataStore.variable(params.yColumnName)
    if (params.xColumnName) {
      newQuery.xVariable = metadataStore.variable(params.xColumnName)
    }

    if (params.filterParams) {
      for (let f of params.filterParams) {
        const filterVariable = metadataStore.variable(f.columnName)
        newQuery.filtersMap[f.columnName] = {
          variable: f.columnName,
          columnname: f.columnName,
          label: filterVariable.displayname,
          displayname: filterVariable.displayname,
          selection: _.clone(f.values)
        }
      }
    }
    if (params.filterCohort) {
      if (params.filterCohort.years && params.filterCohort.years.length > 0) {
        const cohortType = metadataStore.cohortTypesMap[APP_CONSTANTS.VARIABLE_NAME_CAMPAIGN]
        newQuery.cohortsMap[cohortType.columnname] = {
          variable: cohortType.columnname,
          columnname: cohortType.columnname,
          label: cohortType.displayname,
          displayname: cohortType.displayname,
          selection: _.clone(params.filterCohort.years)
        }
      }
      if (params.filterCohort.networkAges && params.filterCohort.networkAges.length > 0) {
        const cohortType = metadataStore.cohortTypesMap[APP_CONSTANTS.VARIABLE_NAME_NETWORK_YEARS]
        newQuery.cohortsMap[cohortType.columnname] = {
          variable: cohortType.columnname,
          columnname: cohortType.columnname,
          label: cohortType.displayname,
          displayname: cohortType.displayname,
          selection: _.clone(params.filterCohort.networkAges)
        }
      }
    }

    this.query = newQuery
  },

  syncFromRouteQuery(routeQuery) {
    if (!routeQuery) {
      return
    }

    let newQuery = {
      xVariable: null,
      yVariable: null,
      filtersMap: {},
      cohortsMap: {}
    }

    if (routeQuery.y) {
      newQuery.yVariable = metadataStore.variable(routeQuery.y)
    }

    if (routeQuery.x) {
      newQuery.xVariable = metadataStore.variable(routeQuery.x)
    }

    if (routeQuery.cyears) {
      const cohortType = metadataStore.cohortTypesMap[APP_CONSTANTS.VARIABLE_NAME_CAMPAIGN]
      newQuery.cohortsMap[cohortType.columnname] = {
        variable: cohortType.columnname,
        columnname: cohortType.columnname,
        label: cohortType.displayname,
        displayname: cohortType.displayname,
        selection: routeQuery.cyears.split(',')
      }
    }

    if (routeQuery.cnetworkages) {
      const cohortType = metadataStore.cohortTypesMap[APP_CONSTANTS.VARIABLE_NAME_NETWORK_YEARS]
      newQuery.cohortsMap[cohortType.columnname] = {
        variable: cohortType.columnname,
        columnname: cohortType.columnname,
        label: cohortType.displayname,
        displayname: cohortType.displayname,
        selection: routeQuery.cnetworkages.split(',')
      }
    }

    for (let key of _.keys(routeQuery)) {
      if (key[0] == 'f') {
        var columnName = key.slice(1);
        const filterVariable = metadataStore.variable(columnName)
        if (filterVariable) {
          newQuery.filtersMap[columnName] = {
            variable: columnName,
            columnname: columnName,
            label: filterVariable.displayname,
            displayname: filterVariable.displayname,
            selection: routeQuery[key].split(',')
          }
        } else {
          console.error('Unknown variable:', columnName)
        }
      }
    }

    this.query = newQuery
  },

  swapVariables() {
    if (!this.canSwapVariables) {
      return
    }
    let tmp = this.query.yVariable
    this.query.yVariable = this.query.xVariable
    this.query.xVariable = tmp
  },

  clearYVariable() {
    this.query.yVariable = null
  },

  clearXVariable() {
    this.query.xVariable = null
  },

  setSelectedFiltersMap(selection) {
    this.query.filtersMap = {}
    _.forIn(selection, (entry, key) => {
      if (entry.selection && entry.selection.length > 0) {
        this.query.filtersMap[key] = _.clone(entry)
      }
    })
  },

  setSelectedCohortsMap(selection, cohortType) {
    this.query.cohortsMap = {}
    _.forIn(selection, (entry, key) => {
      if (entry.selection && entry.selection.length > 0) {
        if (key == cohortType) {
          this.query.cohortsMap[key] = _.clone(entry)
        }
      }
    })
  },

  removeSelectedFilter(key) {
    let newMap = _.cloneDeep(this.query.filtersMap)
    delete newMap[key]
    this.query.filtersMap = newMap;
  },

  removeSelectedCohort(key) {
    let newMap = _.cloneDeep(this.query.cohortsMap)
    delete newMap[key]
    this.query.cohortsMap = newMap
  },

  hasData() {
    return this.distribution && this.distribution.hasData === true
  },

  isQualiDistribution() {
    return this.hasData()
      && this.distribution.type == APP_CONSTANTS.DISTRIBUTION_TYPE_QUALI
  },

  isQualiQualiDistribution() {
    return this.hasData()
      && this.distribution.type == APP_CONSTANTS.DISTRIBUTION_TYPE_QUALI_QUALI
  },

  isQuantiDistribution() {
    return this.hasData()
      && this.distribution.type == APP_CONSTANTS.DISTRIBUTION_TYPE_QUANTI
  },

  isQuantiQualiDistribution() {
    return this.hasData()
      && this.distribution.type == APP_CONSTANTS.DISTRIBUTION_TYPE_QUANTI_QUALI
  },

  isQuantiQuantiDistribution() {
    return this.hasData()
      && this.distribution.type == APP_CONSTANTS.DISTRIBUTION_TYPE_QUANTI_QUANTI
  }
})
