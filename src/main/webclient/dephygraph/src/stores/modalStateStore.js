import { reactive } from 'vue'

export const modalStateStore = reactive({
  modalDisplayed: false,
  modalName: null,
  modalContext: null,

  showModal(name, context) {
    this.modalName = name
    this.modalDisplayed = true
    this.modalContext = context
  },

  closeModal() {
    this.modalDisplayed = false
    this.modalName = null
    this.modalContext = null
  }
})
