import _ from 'lodash'
import pdfMake from 'pdfmake'

import { usePdfExportFonts } from '@/pdf-export-fonts'

import { APP_CONSTANTS } from '@/constants'

const {
  setFonts
} = usePdfExportFonts()

const styleDefinitions = {
  subTitle: {
    color: '#555555',
    fontSize: 9
  },
  list : {
    color: '#555555',
    fontSize: 9,
    margin: [-10, 0, 0, 0]
  },
  listElementTitle: {
    margin: [0, 20, 0, 5],
    fontSize: 10,
    bold: true,
    color: '#555555'
  },
  listElementValues: {
    fontSize: 10,
    color: '#555555'
  },
  footer: {
    fontSize: 10,
    color: '#CCCCCC'
  },
  graphicalElement: {
    alignment: 'center'
  },
  sectionTitle: {
    alignment: 'center',
    fontSize: 11,
    color: '#555555'
  },
  sectionsubTitle: {
    alignment: 'center',
    fontSize: 10,
    color: '#777777'
  },
  dataTable: {
    fontSize: 8,
    margin: [0, 10, 0, 10]
  },
  legend: {
    fontSize: 8
  }
}

const getVariableNameWithUnit = (variable) => {
  var result = '';
  if (variable) {
    result += variable.displayname;
    if (variable.unity) {
      result += ` (${variable.unity})`
    }
  }
  return result;
}

const buildFormattedDate = () => {
  const today = new Date()
  const month = ('0' + (today.getMonth() + 1)).slice(-2)
  const result =
    ('0' + today.getDate()).slice(-2) + '/' +
    month + '/' +
    today.getFullYear()
  return result
}

const computeTableColumnsWidth = (length) => {
  let columnsWidth= []
  let optimalWidth = 100/(length)
  for (let i=0; i<length; i++) {
    columnsWidth.push(optimalWidth + '%')
  }
  return columnsWidth
}

const buildPdfFileName = (interestVariableName, variationVariableName) => {
  let fileName = 'DEPHYGraph-';

  fileName += (interestVariableName.length > 20 ?
    interestVariableName.substring(0, 19) + '...' :
    interestVariableName);
  if (variationVariableName) {
    fileName += '-' + (variationVariableName.length > 20 ?
      variationVariableName.substring(0, 19) + '...' :
      variationVariableName);
  }

  fileName += '.pdf'

  return fileName
}

const buildFiltersDescription = (params) => {
  const filtersMap = params.query.filtersMap
  const description = _.values(filtersMap)
    .map(filter => {
      return filter.label + ' (' + filter.selection.join(', ') + ') '
    })
    .join(', ')
  return description
}

const isPz0Variation = (params) => {
  return params.query.xVariable && params.query.xVariable.columnname == APP_CONSTANTS.VARIABLE_NAME_PZ0 || false
}

const computePz0Evolution = (params, property) => {
  if (params.repartition.length > 1 && params.repartition[0][property] != 0) {
    const col1 = params.repartition[0][property];
    const col2 = params.repartition[1][property];
    const value = ((col2 - col1) / col1) * 100;
    return value.toFixed(2) + ' %';
  } else {
    return 'ND';
  }
}

const getDocumentSearchQuery = (params, limitFilters) => {
  let result = [
    'Variable d\'intérêt (Y) : ' + getVariableNameWithUnit(params.query.yVariable)
  ]
  if (params.query.xVariable) {
    result.push('Variable (X) : ' + getVariableNameWithUnit(params.query.xVariable))
  }

  const filtersDescription = buildFiltersDescription(params)
  if (filtersDescription && (!limitFilters || filtersDescription.length < 192)) {
    result.push(filtersDescription)
  }

  if (params.distribution.dataAvailability) {
    result.push(params.distribution.dataAvailability)
  }

  return result
}

const getDocumentCohortQuery = (params) => {
  const cohortsMap = params.query.cohortsMap
  const result = _.values(cohortsMap).map(cohort => {
    const label = 'Cohorte ' + cohort.label.replace('Par', 'par')
    return label + ' : ' + cohort.selection.join(', ')

  })

  return result
}

const getDocumentFooter = (text) => {
  return [
    {
      table: {
        widths: ['*'],
        body:  [
          [
            {
              margin: [40, 0],
              style: 'footer',
              table: {
                widths: ['*'],
                heights: [60],
                body: [
                  ['Notes'],
                ]
              },
              layout: {
                hLineColor: function (i, node) { return '#AAA';},
                vLineColor: function (i, node) { return '#AAA';}
              }
            }
          ],
          [
            {
              margin: [60, 0],
              columns: [
                {
                  image: 'INRAE',
                  fit: [75, 45],
                  margin: [0, 10, 0, 0],
                  width: '*',
                  alignment: 'center'
                },
                {
                  image: 'MINISTERES',
                  fit: [75, 45],
                  width: 65,
                  alignment: 'center'
                },
                {
                  image: 'OFB',
                  fit: [75, 45],
                  margin: [0, 10, 0, 0],
                  width: '*',
                  alignment: 'center'
                }
              ]
            }
          ],
          [
            {
              margin: [0, 10],
              text: text,
              alignment: 'center',
              style: 'footer'
            }
          ]
        ]
      },
      layout: {
        hLineColor: (i, node) => { return '#FFF'},
        vLineColor: (i, node) => { return '#FFF'}
      }
    }
  ]
}

const getDocumentChart = (params) => {
  let sections = [
    {
      text: "Représentation graphique",
      style: 'sectionTitle',
      margin: [0, 10, 0, 0]
    },
    {
      type: 'none',
      style: 'sectionsubTitle',
      ul: getDocumentSearchQuery(params)
    },
    {
      type: 'none',
      style: 'sectionsubTitle',
      ul: getDocumentCohortQuery(params)
    }
  ]

  params.graphPngList.forEach(png => {
    const width = params.graphWidth || 360
    let section = {
      image: png,
      width: width,
      margin: [0, 5, 0, 5],
      style: 'graphicalElement'
    }
    if (params.graphHeight) {
      section.height = params.graphHeight
    }
    sections.push(section)
  })

  return sections
}

const getDocumentChartLegend = (params) => {
  let legend = {}
  if (params.distribution.xLabelsDescription) {
    var legendTable = []
    _.forEach(params.distribution.xLabelsDescription.values, (v) => {
      legendTable.push([
        {
          text : '',
          fillColor : v.color
        },
        {
          text: v.label
        }
      ])
    })

    legend = {
      style: 'legend',
      table: {
        body: legendTable
      },
      layout: {
        hLineColor: (i, node) => { return '#FFF'},
        vLineColor: (i, node) => { return '#FFF'},
      }
    }
  }
  return legend
}

const getDocumentQualiTable = (params) => {
  let tableBody = []
  params.repartition.forEach((item, index) => {
    tableBody.push([
      item.valueName,
      item.valueOccurrence,
      {text : '', fillColor : params.distribution.colors[index]}
    ])
  })

  const table = {
    style: 'dataTable',
    table: {
      widths: ['auto', '*', 'auto'],
      body: tableBody
    },
    layout: {
      hLineColor: (i, node) => { return '#AAA' },
      vLineColor:  (i, node) => { return '#AAA'},
      paddingLeft: (i, node) => { return 10 },
      paddingRight: (i, node) => { return 10 },
      paddingTop: (i, node) => { return 5 },
      paddingBottom: (i, node) => { return 5 }
    }
  }

  return table
}

const getDocumentQualiQualiTable = (params) => {
  const labels = _.map(params.repartition, (r) => r.valueName)
  const variationsLabels = _.map(params.distribution.xLabelsDescription.values, (v) => v.label)
  let values = []

  _.forEach(params.repartition, (r) => {
    let innerValues = []
    _.forEach(params.distribution.xLabelsDescription.values, (xLabel) => {
      const data = _.keyBy(r.innerRepartition, (o) => o.valueName)
      const v = data[xLabel.label] && data[xLabel.label].valueOccurrence || 0
      innerValues.push(v)
    })
    values.push(innerValues);
  })

  let table = null

  const hasTooManyVariations = variationsLabels.length > 10;
  if (hasTooManyVariations) {
    table = []

    values.forEach((value, i) => {
      const title = labels[i]
      table.push({
        text: labels[i],
        style: 'listElementTitle'
      })

      variationsLabels.forEach((label, j) => {
        table.push({
          text: label + ' : ' + value[j],
          style: 'listElementValues'
        })
      })
    })
  } else {
    let tableBody = []

    tableBody.push([''].concat(variationsLabels))
    values.forEach((value, i) => {
      tableBody.push([labels[i]].concat(value))
    })

    table = {
      style: 'dataTable',
      table: {
        widths: computeTableColumnsWidth(variationsLabels.length + 1),
        body: tableBody
      },
      layout: {
        hLineColor: (i, node) => { return '#AAA' },
        vLineColor:  (i, node) => { return '#AAA'},
        paddingLeft: (i, node) => { return 10 },
        paddingRight: (i, node) => { return 10 },
        paddingTop: (i, node) => { return 5 },
        paddingBottom: (i, node) => { return 5 }
      }
    }
  }

  return table
}

const getDocumentQuantiTable = (params) => {
  const labels = [
    '', 'Maximum', '9ème décile', '3ème quartile','Moyenne',
    'Médiane', '1er quartile', '1er décile', 'Minimum'
  ]

  let tableBody = _.chunk(labels)
  tableBody[0].push(params.interestVariableName)
  tableBody[1].push(params.repartition.max)
  tableBody[2].push(params.repartition.ninthDecile)
  tableBody[3].push(params.repartition.thirdQuartile)
  tableBody[4].push(params.repartition.average)
  tableBody[5].push(params.repartition.median)
  tableBody[6].push(params.repartition.firstQuartile)
  tableBody[7].push(params.repartition.firstDecile)
  tableBody[8].push(params.repartition.min)

  const table = {
    style: 'dataTable',
    table: {
      widths: ['50%', '50%'],
      body: tableBody
    },
    layout: {
      hLineColor: (i, node) => { return '#AAA' },
      vLineColor:  (i, node) => { return '#AAA'},
      paddingLeft: (i, node) => { return 10 },
      paddingRight: (i, node) => { return 10 },
      paddingTop: (i, node) => { return 5 },
      paddingBottom: (i, node) => { return 5 }
    }
  }

  return table
}

const getDocumentQuantiQualiTable = (params) => {
  const labels = [
    '', 'Maximum', '9ème décile', '3ème quartile','Moyenne',
    'Médiane', '1er quartile', '1er décile', 'Minimum',
    'Effectif'
  ]

  let table = null

  const hasTooManyVariations = params.repartition.length > 10;

  if (hasTooManyVariations) {
    table = []

    params.repartition.forEach((item) => {
      table.push({
        text: item.valueName,
        style: 'listElementTitle'
      })

      table.push({
        text: [
          labels[1] + ' : ' + item.max + '\n',
          labels[2] + ' : ' + item.ninthDecile + '\n',
          labels[3] + ' : ' + item.thirdQuartile + '\n',
          labels[4] + ' : ' + item.average + '\n',
          labels[5] + ' : ' + item.median + '\n',
          labels[6] + ' : ' + item.firstQuartile + '\n',
          labels[7] + ' : ' + item.firstDecile + '\n',
          labels[8] + ' : ' + item.min + '\n',
          labels[9] + ' : ' + item.values.length + '\n'
        ],
        style: 'listElementValues'
      })
    })
  } else {
    let columnsCount = params.repartition.length + 1

    let tableBody = _.chunk(labels)
    params.repartition.forEach((item) => {
      tableBody[0].push(item.valueName)
      tableBody[1].push(item.max)
      tableBody[2].push(item.ninthDecile)
      tableBody[3].push(item.thirdQuartile)
      tableBody[4].push(item.average)
      tableBody[5].push(item.median)
      tableBody[6].push(item.firstQuartile)
      tableBody[7].push(item.firstDecile)
      tableBody[8].push(item.min)
      tableBody[9].push(item.values.length)
    })

    if (isPz0Variation(params)) {
      tableBody[0].push('Évolution')
      tableBody[1].push(computePz0Evolution(params, 'max'))
      tableBody[2].push(computePz0Evolution(params, 'ninthDecile'))
      tableBody[3].push(computePz0Evolution(params, 'thirdQuartile'))
      tableBody[4].push(computePz0Evolution(params, 'average'))
      tableBody[5].push(computePz0Evolution(params, 'median'))
      tableBody[6].push(computePz0Evolution(params, 'firstQuartile'))
      tableBody[7].push(computePz0Evolution(params, 'firstDecile'))
      tableBody[8].push(computePz0Evolution(params, 'min'))
      tableBody[9].push('')
      ++columnsCount
    }
  
    table = {
      style: 'dataTable',
      table: {
        widths: computeTableColumnsWidth(columnsCount),
        body: tableBody
      },
      layout: {
        hLineColor: (i, node) => { return '#AAA' },
        vLineColor:  (i, node) => { return '#AAA'},
        paddingLeft: (i, node) => { return 10 },
        paddingRight: (i, node) => { return 10 },
        paddingTop: (i, node) => { return 5 },
        paddingBottom: (i, node) => { return 5 }
      }
    }
  }

  return table
}

const initDocumentConfig = (params) => {
  const baseUrl = window.location.origin + window.location.pathname
  let config = {}

  config.images = {
    DEPHY: baseUrl + 'img/ecophyto_dephy_logo.png',
    INRAE: baseUrl + 'img/logo_inrae.png',
    MINISTERES: baseUrl + 'img/logo_gouvernement_512px.png',
    OFB: baseUrl + 'img/logo_ofb.jpg',
  },

  config.styles = styleDefinitions

  config.pageMargins = [40, 100, 40, 170]

  const infoList = [
    'Document généré sur DEPHYGraph le ' + buildFormattedDate(),
    'Date d\'actualisation des données : ' + params.distribution.lastExtractionDate,
    'Effectif total : ' + params.distribution.systemsCount
  ]

  config.header = {
    margin: [20, 10],
    table: {
      widths: ['auto', '*'],
      body: [[
        {
          image: 'DEPHY',
          border: [false, false, false, true],
          fit: [150, 50],
          alignment: 'center',
          fillColor: '#FFFFFF',
          margin: [0, 0, 20, 5]
        },
        {
          border: [false, false, false, true],
          margin: [0, 0, 0, 5],
          stack : [
            {
              type: 'none',
              style: 'list',
              ul: getDocumentSearchQuery(params, true)
            },
            {
              text: infoList.join(' | '),
              style: 'subTitle'
            },
          ]
        }
      ]]
    },
  }

  config.footer = (currentPage, pageCount) => {
    let text = 'DEPHYGraph'
    if (pageCount > 1) {
      text += '     ' + currentPage.toString() + ' / ' + pageCount;
    }
    return getDocumentFooter(text)
  }

  config.pageBreakBefore = (currentNode, followingNodesOnPage, nodesOnNextPage, previousNodesOnPage) => {
    return (currentNode.text == 'Valeurs numériques')
  }

  config.content = [
    getDocumentChart(params),
    getDocumentChartLegend(params)
  ]

  let table = null
  if (params.distribution.type == APP_CONSTANTS.DISTRIBUTION_TYPE_QUALI) {
    table = getDocumentQualiTable(params)
  } else if (params.distribution.type == APP_CONSTANTS.DISTRIBUTION_TYPE_QUALI_QUALI) {
    table = getDocumentQualiQualiTable(params)
  } else if (params.distribution.type == APP_CONSTANTS.DISTRIBUTION_TYPE_QUANTI) {
    table = getDocumentQuantiTable(params)
  } else if (params.distribution.type == APP_CONSTANTS.DISTRIBUTION_TYPE_QUANTI_QUALI) {
    table = getDocumentQuantiQualiTable(params)
  }

  if (table) {
    config.content.push([
      {
        text: "Valeurs numériques",
        style: 'sectionTitle',
        margin: [0, 10, 0, 0]
      },
      {
        type: 'none',
        style: 'sectionsubTitle',
        ul: getDocumentSearchQuery(params)
      },
      {
        type: 'none',
        style: 'sectionsubTitle',
        ul: getDocumentCohortQuery(params)
      },
      table
    ])
  }

  return config
}

const generatePdf = (params) => {
  setFonts(pdfMake)

  const pdfFileName = buildPdfFileName(params.interestVariableName, params.variationVariableName)

  const documentConfig = initDocumentConfig(params)

  pdfMake.createPdf(documentConfig).download(pdfFileName)
}

export function usePdfExport() {
  return {
    generatePdf
  }
}
