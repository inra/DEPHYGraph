const logEvent = (event) => {
  event.screen = {
    w: window.screen.width * window.devicePixelRatio,
    h: window.screen.height * window.devicePixelRatio
  }

  let options = {
    method: 'POST',
    cache: 'no-cache',
    headers: {
      'Content-Type': 'application/json'
    }
  }
  options.body = JSON.stringify(event)

  fetch('api/v1/uevent/log/', options)
}

const logQuickSearchUserEvent = (data) => {
  logEvent({
    topic: 'Graphique recommandé',
    data,
  })
}

const logVariableSelectionEvent = (data) => {
  logEvent({
    topic: 'Selection de variable',
    data,
  })
}

const logHelpVariableEvent = (data) => {
  logEvent({
    topic: 'Aide',
    data,
  })
}

const logUrlGenerationEvent = (data) => {
  logEvent({
    topic: 'Sauvegarde URL',
    data: data,
  })
}

const logPdfExportEvent = (data) => {
  logEvent({
    topic: 'Export PDF',
    data: data,
  })
}

const logDisplayConfigurationEvent = (data) => {
  logEvent({
    topic: "Configuration de l'affichage",
    data: data,
  })
}

export function useUserEventsLogger() {
  return {
    logEvent,
    logQuickSearchUserEvent,
    logVariableSelectionEvent,
    logHelpVariableEvent,
    logUrlGenerationEvent,
    logPdfExportEvent,
    logDisplayConfigurationEvent
  }
}
