import { 
  Canvg,
  presets
} from 'canvg'

const preset = presets.offscreen()

const splitSvgIntoPngList = async (element) => {
  let imageSections = [];

  let svg = null
  let containers = element.getElementsByClassName('svg-container');
  if (containers && containers[0]) {
    let content = containers[0].innerHTML;
    svg = content.match(/<svg.*?<\/svg>/g)[0];
  }
  if (svg) {
    svg = svg.replaceAll('<br>', '');
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d')
    const v = await Canvg.fromString(ctx, svg, preset)
    await v.render()

    let originalHeight = canvas.height;
    let originalWidth = canvas.width;

    let sectionHeight = 800;

    let wholeSectionsCount = Math.floor(originalHeight / sectionHeight);
    let remainder = originalHeight % sectionHeight;

    let y = 0;
    for (let i = 0; i < wholeSectionsCount; i++) {
      const subCanvas = document.createElement('canvas');
      const subCtxt = subCanvas.getContext('2d');
      subCanvas.width = originalWidth;
      subCanvas.height = sectionHeight;
      subCtxt.drawImage(canvas, 0, y, originalWidth, sectionHeight,  0, 0, originalWidth, sectionHeight);
      imageSections.push(subCanvas.toDataURL("image/png"));
      y += sectionHeight;
    }

    if (remainder) {
      const subCanvas = document.createElement('canvas');
      const subCtxt = subCanvas.getContext('2d');
      subCanvas.width = originalWidth;
      subCanvas.height = sectionHeight;
      subCtxt.drawImage(canvas, 0, y, originalWidth, remainder,  0, 0, originalWidth, remainder);
      imageSections.push(subCanvas.toDataURL("image/png"));
    }
  }

  return imageSections
}

export function useImageUtils() {
  return {
    splitSvgIntoPngList
  }
}
