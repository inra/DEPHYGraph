import _ from 'lodash'

const hideCategoryPrefix = (name) => {
  if (name && name.match('[0-9][0-9]\\.[0-9][0-9]-.*')) {
    return name.slice(6)
  }

  if (name && name.match('[0-9][0-9]-.*')) {
    return name.slice(3)
  }
  return name
}

const orderAndGroupVariables = (variables, filterPredicate) => {
  if (!filterPredicate) {
    filterPredicate = () => true
  }

  let recommendedVariables = _(variables)
      .filter(filterPredicate)
      .filter((v) => v.recommendedquery === true)
      .sortBy('columnname')
      .value()

  let result = _(variables)
      .filter(filterPredicate)
      .sortBy('category', 'columnname')
      .groupBy('category')
      .map( (group, category) => ({
        categoryName: hideCategoryPrefix(category),
        categoryOptions: _.orderBy(group, 'columnname')
      }))
      .value()

  result.unshift({
    categoryName: "Variables recommandées",
    categoryOptions: recommendedVariables
  })

  return result
}

const groupFilters = (filters) => {
  let recommendedFilters = _(filters)
    .filter((f) => f.recommendedfilter === true)
    .sortBy('columnname')
    .value()

  var result = _(filters)
    .sortBy('category', 'columnname')
    .groupBy('category')
    .map((group, category) => ({
      categoryName: hideCategoryPrefix(category),
      categoryOptions: _.orderBy(group, 'displayname'),
    }))
    .value()

  result.unshift({
    categoryName: 'Filtres recommandés',
    categoryOptions: recommendedFilters,
  });

  return result
}

const matchesDisplayNameIfPresent = (v, partialName) => {
  if (!partialName) {
    return true
  }

  const basicPartialName = _.deburr(partialName).toLowerCase()
  const basicDisplayName = _.deburr(v.displayname).toLowerCase()
  return basicDisplayName.includes(basicPartialName)
}

const getXVariableInclusionPredicate = (ySelection, partialName) => {
  return (v) => {
    const matchesName = matchesDisplayNameIfPresent(v, partialName)
    if (ySelection) {
      if (v.columnname == ySelection.columnname) {
        return false
      } else if (ySelection.qualitative === true && v.qualitative === false) {
        return false
      } else {
        return matchesName
      }
    } else {
      return matchesName
    }
  }
}

const getYVariableInclusionPredicate = (xSelection, partialName) => {
  return (v) => {
    const matchesName = matchesDisplayNameIfPresent(v, partialName)
    if (xSelection) {
      if (v.columnname == xSelection.columnname) {
        return false
      } else if (xSelection.quantitative === true && v.qualitative === true) {
        return false
      } else {
        return matchesName
      }
    } else {
      return matchesName
    }
  }
}

const getVariableAvailabilityText = (variable) =>{
  if (variable) {
    if (variable.availabilitybysector == 'ALL') {
      return 'toutes les filières'
    } else {
      let msg = ''
      let sectors = variable.availabilitybysector.split(',')
      if (sectors.length > 1) {
        msg += 'les filières '
      } else {
        msg += 'la filière '
      }
      msg += _.join(sectors, ', ')
      return msg
    }
  } else {
    return ''
  }
}

const getVariableNameWithUnit = (variable) => {
  var result = '';
  if (variable) {
    result += variable.displayname;
    if (variable.unity) {
      result += ` (${variable.unity})`
    }
  }
  return result;
}

export function useVariablesUtils() {
  return { 
    hideCategoryPrefix, 
    orderAndGroupVariables,
    groupFilters,
    getXVariableInclusionPredicate,
    getYVariableInclusionPredicate,
    getVariableAvailabilityText,
    getVariableNameWithUnit,
  }
}
