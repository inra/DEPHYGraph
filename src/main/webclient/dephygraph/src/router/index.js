import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'

import { infoStore } from '@/stores/infoStore'
import { metadataStore } from '@/stores/metadataStore'

import WelcomePage from '@/components/welcome/WelcomePage.vue'
import AboutPage from '@/components/AboutPage.vue'
import DashboardPage from '@/components/dashboard/DashboardPage.vue'
import DataQualityWarningPage from '@/components/DataQualityWarningPage.vue'

const routes = [
  {
    path: '/',
    name: 'Welcome',
    component: WelcomePage,
    meta: { requiresMetadata: true }
  },
  {
    path: '/about',
    name: 'About',
    component: AboutPage
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: DashboardPage,
    meta: { requiresMetadata: true }
  },
  {
    path: '/data-warning',
    name: 'DataWarning',
    component: DataQualityWarningPage
  }
]

const router = createRouter({
  //history: createWebHistory(),
  history: createWebHashHistory(),
  routes,
})

router.beforeEach(async (to, from) => {
  if (!infoStore.hasAckDataQualityWarning() && to.name !== 'DataWarning') {
    return { name: 'DataWarning' }
  }

  if (to.meta.requiresMetadata) {
    await metadataStore.fetchMetadata()
  }
})

export default router;
