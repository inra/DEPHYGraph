# Front-End DEPHYGraph

## Mise à jour des icones

La génération de la police d'icône se fait à l'aide de [svgtofont](https://github.com/jaywcjlove/svgtofont).

* Ajouter une icône au format svg (ex: help.svg) dans le dossier `src/main/resources/design/icones`
* Exécuter la commande `npm run update-icons-font`
* Incorporer l'icône avec `<i class="icon icon-help"/>`

