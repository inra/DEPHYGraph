package fr.inra.dephygraph;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.SingleInstancePostgresRule;
import fr.inra.dephygraph.model.FilterChoice;
import fr.inra.dephygraph.model.Metadata;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.junit.Rule;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class AbstractTest {

    @Rule
    public SingleInstancePostgresRule database = EmbeddedPostgresRules.singleInstance();

    protected void fillDatabase(Jdbi jdbi) throws Exception {
        String initTestDBScript = new String(Files.readAllBytes(Paths.get(getClass().getResource("/initTestDB.sql").toURI())));
        String insertDataScript = new String(Files.readAllBytes(Paths.get(getClass().getResource("/insertDataTest.sql").toURI())));

        try (Handle h = jdbi.open()) {
            h.createScript(initTestDBScript).execute();
            h.createScript(insertDataScript).execute();
        }
    }

    protected void addAgrosystInfoTable(Jdbi jdbi) throws Exception {
        String dbScript = new String(Files.readAllBytes(Paths.get(getClass().getResource("/add_agrosyst_info.sql").toURI())));
        try (Handle h = jdbi.open()) {
            h.createScript(dbScript).execute();
        }
    }

    protected Metadata getTotalIftMetadata() {
        Metadata totalIftMetadata = new Metadata();
        totalIftMetadata.setColumnname("totalIFT");
        totalIftMetadata.setDisplayname("IFT total (hors biocontrôle, avec TS)");
        totalIftMetadata.setQualitative(false);
        totalIftMetadata.setQuantitative(true);
        totalIftMetadata.setFilter(false);
        totalIftMetadata.setHidden(false);
        return totalIftMetadata;
    }

    protected Metadata getExpensesMetadata() {
        Metadata expensesMetadata = new Metadata();
        expensesMetadata.setColumnname("expenses");
        expensesMetadata.setDisplayname("Charges");
        expensesMetadata.setQualitative(false);
        expensesMetadata.setQuantitative(true);
        expensesMetadata.setFilter(false);
        expensesMetadata.setHidden(false);
        return expensesMetadata;
    }

    protected Metadata getCampaignMetadata() {
        Metadata campaignMetadata = new Metadata();
        campaignMetadata.setColumnname("campaign");
        campaignMetadata.setDisplayname("Campagne");
        campaignMetadata.setQualitative(false);
        campaignMetadata.setQuantitative(true);
        campaignMetadata.setFilter(true);
        campaignMetadata.setHidden(false);
        campaignMetadata.setSeparator(",");
        return campaignMetadata;
    }

    protected Metadata getGroundTypeMetadata() {
        Metadata groundTypeMetadata = new Metadata();
        groundTypeMetadata.setColumnname("groundType");
        groundTypeMetadata.setDisplayname("Type de sol");
        groundTypeMetadata.setQualitative(true);
        groundTypeMetadata.setQuantitative(false);
        groundTypeMetadata.setFilter(true);
        groundTypeMetadata.setHidden(false);
        return groundTypeMetadata;
    }

    protected Metadata getGroundTextureMetadata() {
        Metadata groundTextureMetadata = new Metadata();
        groundTextureMetadata.setColumnname("groundTexture");
        groundTextureMetadata.setDisplayname("Texture de sol");
        groundTextureMetadata.setQualitative(true);
        groundTextureMetadata.setQuantitative(false);
        groundTextureMetadata.setFilter(true);
        groundTextureMetadata.setHidden(false);
        return groundTextureMetadata;
    }

    protected Metadata getQualiCampaignMetadata() {
        Metadata qualiCampaignMetadata = new Metadata();
        qualiCampaignMetadata.setColumnname("qualiCampaign");
        qualiCampaignMetadata.setDisplayname("Campagne mode qualitatif");
        qualiCampaignMetadata.setQualitative(true);
        qualiCampaignMetadata.setQuantitative(false);
        qualiCampaignMetadata.setFilter(false);
        qualiCampaignMetadata.setHidden(false);
        qualiCampaignMetadata.setSeparator(",");
        return qualiCampaignMetadata;
    }

    protected Metadata getRegionMetadata() {
        Metadata regionMetadata = new Metadata();
        regionMetadata.setColumnname("region");
        regionMetadata.setDisplayname("Région");
        regionMetadata.setQualitative(true);
        regionMetadata.setQuantitative(false);
        regionMetadata.setFilter(true);
        regionMetadata.setHidden(false);
        return regionMetadata;
    }

    protected Metadata getRootstockMetadata() {
        Metadata rootstockMetadata = new Metadata();
        rootstockMetadata.setColumnname("rootstock");
        rootstockMetadata.setDisplayname("Porte-Greffe");
        rootstockMetadata.setQualitative(true);
        rootstockMetadata.setQuantitative(false);
        rootstockMetadata.setFilter(false);
        rootstockMetadata.setHidden(false);
        rootstockMetadata.setSeparator(",");
        return rootstockMetadata;
    }

    protected Metadata getPz0Metadata() {
        Metadata pz0Metadata = new Metadata();
        pz0Metadata.setColumnname("pz0");
        pz0Metadata.setDisplayname("PZ0");
        pz0Metadata.setQualitative(true);
        pz0Metadata.setQuantitative(false);
        pz0Metadata.setFilter(true);
        pz0Metadata.setHidden(false);
        return pz0Metadata;
    }

    protected Metadata getIndustrialFarmingMetadata() {
        Metadata groundTypeMetadata = new Metadata();
        groundTypeMetadata.setColumnname("industrialFarming");
        groundTypeMetadata.setDisplayname("Culture industrielle");
        groundTypeMetadata.setQualitative(true);
        groundTypeMetadata.setQuantitative(false);
        groundTypeMetadata.setFilter(true);
        groundTypeMetadata.setHidden(false);
        return groundTypeMetadata;
    }

    protected FilterChoice getCampaignFilter(String... campaignValues) {
        FilterChoice campaignFilter = new FilterChoice();
        campaignFilter.setColumnname("campaign");
        for (String campaignValue : campaignValues) {
            campaignFilter.addValue(campaignValue);
        }
        campaignFilter.setSeparator(",");
        return campaignFilter;
    }

    protected FilterChoice getValidationFilter(boolean value) {
        FilterChoice validationFilter = new FilterChoice();
        validationFilter.setColumnname("validation");
        validationFilter.addValue(Boolean.toString(value));
        return validationFilter;
    }

    protected FilterChoice getDephyNbFilter(String... dephyNbValues) {
        FilterChoice dephyNbFilter = new FilterChoice();
        dephyNbFilter.setColumnname("dephyNb");
        for (String dephyNbValue : dephyNbValues) {
            dephyNbFilter.addValue(dephyNbValue);
        }
        return dephyNbFilter;
    }
}
