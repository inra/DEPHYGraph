package fr.inra.dephygraph;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import fr.inra.dephygraph.rest.AgrosystInfo;
import fr.inra.dephygraph.rest.AgrosystService;
import org.jdbi.v3.core.Jdbi;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class AgrosystServiceTest extends AbstractTest {

    @Before
    public void initDb() throws Exception {
        DataSource ds = database.getEmbeddedPostgres().getPostgresDatabase();
        Jdbi jdbi = Jdbi.create(ds);
        fillDatabase(jdbi);
    }

    @Test
    public void testConnexionFlow() throws Exception {

        String userName = "Agro";
        List<String> allowedDephyNb = Arrays.asList("dephyOne", "dephyTwo", "deThree");
        List<String> allowedIrCode = Arrays.asList("DC132");

        AgrosystInfo agrosystInfo = new AgrosystInfo();
        agrosystInfo.setUserName(userName);
        agrosystInfo.setAllowedDephyNb(allowedDephyNb);
        agrosystInfo.setAllowedIrCode(allowedIrCode);

        DephyGraphDatasource datasource = new DephyGraphDatasource(database.getEmbeddedPostgres().getPostgresDatabase());

        AgrosystService service = new AgrosystService(datasource);
        String dephyToken = service.prepareConnexion(agrosystInfo);
        Assert.assertNotNull(dephyToken);

        String jwToken = service.login(dephyToken);
        Assert.assertNotNull(jwToken);

        DecodedJWT decodedJWT = JWT.decode(jwToken);
        Map<String, Claim> claims = decodedJWT.getClaims();
        Assert.assertNotNull(claims);

        Assert.assertEquals(userName, claims.get("userName").asString());

    }

    @Test
    public void testConnexionWithTwoServiceInstances() throws Exception {

        String userName = "Agro2";
        List<String> allowedDephyNb = Arrays.asList("dephyOne", "dephyTwo", "deThree", "dephyFour");
        List<String> allowedIrCode = Arrays.asList("DC132", "AF1");

        AgrosystInfo agrosystInfo = new AgrosystInfo();
        agrosystInfo.setUserName(userName);
        agrosystInfo.setAllowedDephyNb(allowedDephyNb);
        agrosystInfo.setAllowedIrCode(allowedIrCode);

        DephyGraphDatasource datasource = new DephyGraphDatasource(database.getEmbeddedPostgres().getPostgresDatabase());

        AgrosystService service = new AgrosystService(datasource);

        String dephyToken = service.prepareConnexion(agrosystInfo);
        Assert.assertNotNull(dephyToken);

        AgrosystService otherService = new AgrosystService(datasource);
        String jwToken = otherService.login(dephyToken);
        Assert.assertNotNull(jwToken);

        DecodedJWT decodedJWT = JWT.decode(jwToken);
        Map<String, Claim> claims = decodedJWT.getClaims();
        Assert.assertNotNull(claims);

        Assert.assertEquals(userName, claims.get("userName").asString());

    }

    @Test(expected = IllegalArgumentException.class)
    public void testReUseConnexionToken() throws Exception {

        String userName = "Agro3";
        List<String> allowedDephyNb = Arrays.asList("dephyOne", "dephyTwo");
        List<String> allowedIrCode = Arrays.asList("C3P0","R2D2");

        AgrosystInfo agrosystInfo = new AgrosystInfo();
        agrosystInfo.setUserName(userName);
        agrosystInfo.setAllowedDephyNb(allowedDephyNb);
        agrosystInfo.setAllowedIrCode(allowedIrCode);

        DephyGraphDatasource datasource = new DephyGraphDatasource(database.getEmbeddedPostgres().getPostgresDatabase());
        AgrosystService service = new AgrosystService(datasource);

        String dephyToken = service.prepareConnexion(agrosystInfo);
        Assert.assertNotNull(dephyToken);

        // First login : ok
        String jwToken = service.login(dephyToken);
        Assert.assertNotNull(jwToken);

        DecodedJWT decodedJWT = JWT.decode(jwToken);
        Map<String, Claim> claims = decodedJWT.getClaims();
        Assert.assertNotNull(claims);
        Assert.assertEquals(userName, claims.get("userName").asString());

        // Second try with same token : wrong !
        service.login(dephyToken);

    }

    @Test
    public void testConnexionWithNoDephyNb() throws Exception {

        String userName = "Agro";
        List<String> allowedDephyNb = Collections.emptyList();
        List<String> allowedIrCode = Arrays.asList("DC132");

        AgrosystInfo agrosystInfo = new AgrosystInfo();
        agrosystInfo.setUserName(userName);
        agrosystInfo.setAllowedDephyNb(allowedDephyNb);
        agrosystInfo.setAllowedIrCode(allowedIrCode);

        DephyGraphDatasource datasource = new DephyGraphDatasource(database.getEmbeddedPostgres().getPostgresDatabase());

        AgrosystService service = new AgrosystService(datasource);
        String dephyToken = service.prepareConnexion(agrosystInfo);
        Assert.assertNotNull(dephyToken);

        String jwToken = service.login(dephyToken);
        Assert.assertNotNull(jwToken);

        DecodedJWT decodedJWT = JWT.decode(jwToken);
        Map<String, Claim> claims = decodedJWT.getClaims();
        Assert.assertNotNull(claims);

        Assert.assertEquals(userName, claims.get("userName").asString());

    }

}
