package fr.inra.dephygraph.model;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.SingleInstancePostgresRule;
import fr.inra.dephygraph.AbstractTest;
import org.jdbi.v3.core.Jdbi;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class DatamartDaoLegacyTest extends AbstractTest {
    @Rule
    public SingleInstancePostgresRule database = EmbeddedPostgresRules.singleInstance();

    @Test
    public void testGetMetadata() throws Exception {

        DataSource ds = database.getEmbeddedPostgres().getPostgresDatabase();

        Jdbi jdbi = Jdbi.create(ds);

        fillDatabase(jdbi);

        jdbi.useHandle(h -> {
            fr.inra.dephygraph.dao.DatamartDao datamartDao = new fr.inra.dephygraph.dao.DatamartDao(h);

            List<Metadata> metadata = datamartDao.getMetadata(false, true, true);
            Assert.assertNotNull(metadata);
            Assert.assertEquals(6, metadata.size());

            metadata = datamartDao.getMetadata(true, false, true);
            Assert.assertNotNull(metadata);
            Assert.assertEquals(1, metadata.size());

            metadata = datamartDao.getMetadata(true, false, false);
            Assert.assertNotNull(metadata);
            Assert.assertEquals(2, metadata.size());

            metadata = datamartDao.getMetadata(false, true, false);
            Assert.assertNotNull(metadata);
            Assert.assertEquals(1, metadata.size());
            Assert.assertEquals("qualiCampaign", metadata.get(0).getColumnname());

            // Simply on qualitative/quantitative distinction
            metadata = datamartDao.getAllVisibleMetadata();
            Assert.assertNotNull(metadata);
            Assert.assertEquals(10, metadata.size());
            Assert.assertEquals("region", metadata.get(0).getColumnname());
            Assert.assertEquals("Région", metadata.get(0).getDisplayname());
            Assert.assertEquals("context", metadata.get(0).getCategory());
            Assert.assertTrue(metadata.get(0).isQualitative());
            Assert.assertFalse(metadata.get(0).isQuantitative());
            Assert.assertTrue(metadata.get(0).isFilter());
            Assert.assertNull(metadata.get(0).getSeparator());
            Assert.assertFalse(metadata.get(0).isHidden());

            metadata = datamartDao.getAllQualitativeMetadata();
            Assert.assertNotNull(metadata);
            Assert.assertEquals(7, metadata.size());
            for (Metadata metadatum : metadata) {
                Assert.assertTrue(metadatum.isQualitative());
                Assert.assertFalse(metadatum.isQuantitative());
            }
        });
    }

    @Test
    public void testGetMetadataByColumnName() throws Exception {

        DataSource ds = database.getEmbeddedPostgres().getPostgresDatabase();

        Jdbi jdbi = Jdbi.create(ds);

        fillDatabase(jdbi);

        jdbi.useHandle(h -> {
            fr.inra.dephygraph.dao.DatamartDao datamartDao = new fr.inra.dephygraph.dao.DatamartDao(h);
            {
                Metadata metadata = datamartDao.getMetadataByColumnName("groundType");
                Assert.assertNotNull(metadata);
                Assert.assertEquals("groundType", metadata.getColumnname());
                Assert.assertEquals("Type de sol", metadata.getDisplayname());
                Assert.assertTrue(metadata.isQualitative());
                Assert.assertFalse(metadata.isQuantitative());
                Assert.assertTrue(metadata.isFilter());
                Assert.assertFalse(metadata.isHidden());
            }
            {
                Metadata metadata = datamartDao.getMetadataByColumnName("totalIFT");
                Assert.assertNotNull(metadata);
                Assert.assertEquals("totalIFT", metadata.getColumnname());
                Assert.assertEquals("IFT total (hors biocontrôle, avec TS)", metadata.getDisplayname());
                Assert.assertFalse(metadata.isQualitative());
                Assert.assertTrue(metadata.isQuantitative());
                Assert.assertFalse(metadata.isFilter());
                Assert.assertFalse(metadata.isHidden());
            }
        });
    }

    @Test
    public void testGetFilterValues() throws Exception {

        DataSource ds = database.getEmbeddedPostgres().getPostgresDatabase();

        Jdbi jdbi = Jdbi.create(ds);

        fillDatabase(jdbi);

        jdbi.useHandle(h -> {
            fr.inra.dephygraph.dao.DatamartDao datamartDao = new fr.inra.dephygraph.dao.DatamartDao(h);

            // Retrieve DephyNb
            Metadata dephyNbMetadata = new Metadata();
            dephyNbMetadata.setColumnname("dephyNb");
            dephyNbMetadata.setDisplayname("N° Dephy");
            dephyNbMetadata.setQualitative(true);
            dephyNbMetadata.setQuantitative(false);
            dephyNbMetadata.setFilter(true);
            dephyNbMetadata.setHidden(false);
            List<String> dephyNbList = datamartDao.getFilterValues(dephyNbMetadata);
            Assert.assertTrue(dephyNbList.contains("GCF31915"));
            Assert.assertTrue(dephyNbList.contains("PYF10215"));
            Assert.assertTrue(dephyNbList.contains("PYF10202"));
            Assert.assertTrue(dephyNbList.contains("PYF10467"));
            Assert.assertTrue(dephyNbList.contains("PYF27458"));

            // Retrieve Campaigns
            List<Metadata> metadata = datamartDao.getMetadata(true, false, true);
            List<String> filterValues = datamartDao.getFilterValues(metadata.get(0));
            Assert.assertNotNull(filterValues);
            Assert.assertEquals(8, filterValues.size());
            List<String> filterValueList = Lists.newArrayList(filterValues);
            Assert.assertTrue(filterValueList.contains("2009"));
            Assert.assertTrue(filterValueList.contains("2010"));
            Assert.assertTrue(filterValueList.contains("2011"));
            Assert.assertTrue(filterValueList.contains("2012"));
            Assert.assertTrue(filterValueList.contains("2013"));
            Assert.assertTrue(filterValueList.contains("2014"));
            Assert.assertTrue(filterValueList.contains("2015"));
            Assert.assertTrue(filterValueList.contains("2016"));

            // Retrieve Region values : only empty : no filter values
            Metadata regionMetadata = getRegionMetadata();
            List<String> regionValues = datamartDao.getFilterValues(regionMetadata);
            Assert.assertEquals(0, regionValues.size());

            // Retrieve Region
            Metadata pz0Metadata = getPz0Metadata();
            List<String> pz0Values = datamartDao.getFilterValues(pz0Metadata);
            Assert.assertEquals(2, pz0Values.size());
            List<String> pz0ValueList = Lists.newArrayList(pz0Values);
            Assert.assertTrue(pz0ValueList.contains("Post-PZ0"));
            Assert.assertTrue(pz0ValueList.contains("PZ0"));
        });
    }

    @Test
    public void testGetEffectiveFilterValues() throws Exception {
        DataSource ds = database.getEmbeddedPostgres().getPostgresDatabase();

        Jdbi jdbi = Jdbi.create(ds);

        fillDatabase(jdbi);

        jdbi.useHandle(h -> {
            fr.inra.dephygraph.dao.DatamartDao datamartDao = new fr.inra.dephygraph.dao.DatamartDao(h);

            Metadata dephyNbMetadata = new Metadata();
            dephyNbMetadata.setColumnname("dephyNb");
            dephyNbMetadata.setDisplayname("N° Dephy");
            dephyNbMetadata.setQualitative(true);
            dephyNbMetadata.setQuantitative(false);
            dephyNbMetadata.setFilter(true);
            dephyNbMetadata.setHidden(false);

            List<String> effectiveFilterValues = datamartDao.getEffectiveFilterValues(dephyNbMetadata, Collections.emptyList());
            Assert.assertTrue(effectiveFilterValues.size() > 0);
        });
        jdbi.useHandle(h -> {
            fr.inra.dephygraph.dao.DatamartDao datamartDao = new fr.inra.dephygraph.dao.DatamartDao(h);

            Metadata dephyNbMetadata = new Metadata();
            dephyNbMetadata.setColumnname("dephyNb");
            dephyNbMetadata.setDisplayname("N° Dephy");
            dephyNbMetadata.setQualitative(true);
            dephyNbMetadata.setQuantitative(false);
            dephyNbMetadata.setFilter(true);
            dephyNbMetadata.setHidden(false);

            List<String> purposedValues = Lists.newArrayList("PYF10215", "PYF10202");

            List<String> effectiveFilterValues = datamartDao.getEffectiveFilterValues(dephyNbMetadata, purposedValues);
            Assert.assertEquals(2,effectiveFilterValues.size());
            Assert.assertTrue(effectiveFilterValues.contains("PYF10215"));
            Assert.assertTrue(effectiveFilterValues.contains("PYF10202"));
        });
        jdbi.useHandle(h -> {
            fr.inra.dephygraph.dao.DatamartDao datamartDao = new fr.inra.dephygraph.dao.DatamartDao(h);

            Metadata dephyNbMetadata = new Metadata();
            dephyNbMetadata.setColumnname("dephyNb");
            dephyNbMetadata.setDisplayname("N° Dephy");
            dephyNbMetadata.setQualitative(true);
            dephyNbMetadata.setQuantitative(false);
            dephyNbMetadata.setFilter(true);
            dephyNbMetadata.setHidden(false);

            List<String> purposedValues = Lists.newArrayList("NA");

            List<String> effectiveFilterValues = datamartDao.getEffectiveFilterValues(dephyNbMetadata, purposedValues);
            Assert.assertEquals(0, effectiveFilterValues.size());
        });
    }

    @Test
    public void testGetFilterMetadata() throws Exception {

        DataSource ds = database.getEmbeddedPostgres().getPostgresDatabase();

        Jdbi jdbi = Jdbi.create(ds);

        fillDatabase(jdbi);

        jdbi.useHandle(h -> {
            fr.inra.dephygraph.dao.DatamartDao datamartDao = new fr.inra.dephygraph.dao.DatamartDao(h);

            List<Metadata> filters = datamartDao.getAllFilterMetadata();
            Assert.assertEquals(7, filters.size());
            Assert.assertEquals("campaign", filters.get(0).columnname);
            Assert.assertEquals("sector", filters.get(1).columnname);
            Assert.assertEquals("rootstock", filters.get(2).columnname);
            Assert.assertEquals("pz0", filters.get(3).columnname);
            Assert.assertEquals("region", filters.get(4).columnname);
            Assert.assertEquals("groundTexture", filters.get(5).columnname);
            Assert.assertEquals("groundType", filters.get(6).columnname);
        });
    }

    protected double roundWithTwoDecimals(double input) {
        BigDecimal value = new BigDecimal(input);
        BigDecimal roundedValue = value.setScale(2, RoundingMode.HALF_UP);
        return roundedValue.doubleValue();
    }

    @Test
    public void testGetSpecificValuesForOneMetadata() throws Exception {

        DataSource ds = database.getEmbeddedPostgres().getPostgresDatabase();
        Jdbi jdbi = Jdbi.create(ds);

        fillDatabase(jdbi);

        jdbi.useHandle(h -> {
            fr.inra.dephygraph.dao.DatamartDao datamartDao = new fr.inra.dephygraph.dao.DatamartDao(h);

            Metadata groundTypeMetadata = getGroundTypeMetadata();

            List<FilterChoice> filterChoices = new ArrayList<>();
            FilterChoice dephyNbFilter = getDephyNbFilter("GCF31915", "PYF10215", "PYF10202", "PYF10467", "PYF27458", "FAK27458");
            List<SpecificValue> specificValues = datamartDao.getSpecificValues(dephyNbFilter, groundTypeMetadata, filterChoices);
            Assert.assertEquals(6, specificValues.size());

            Assert.assertEquals("FAK27458", specificValues.get(0).getSpecific());
            Assert.assertEquals(1, specificValues.get(0).getxValue().size());
            Assert.assertEquals("Limon argileux/craie", specificValues.get(0).getxValue().get(0));
            Assert.assertNull(specificValues.get(0).getyValue());
            Assert.assertEquals(0d, specificValues.get(0).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(0d, specificValues.get(0).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("PYF10202", specificValues.get(2).getSpecific());
            Assert.assertEquals(1, specificValues.get(2).getxValue().size());
            Assert.assertEquals("Tourbe", specificValues.get(2).getxValue().get(0));
            Assert.assertNull(specificValues.get(2).getyValue());
            Assert.assertEquals(46.1179379d, specificValues.get(2).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(6.7349079d, specificValues.get(2).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("PYF10215", specificValues.get(3).getSpecific());
            Assert.assertEquals(1, specificValues.get(3).getxValue().size());
            Assert.assertEquals("Tourbe", specificValues.get(3).getxValue().get(0));
            Assert.assertNull(specificValues.get(3).getyValue());
            Assert.assertEquals(48.4921056d, specificValues.get(3).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(5.3860503d, specificValues.get(3).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("PYF27458", specificValues.get(5).getSpecific());
            Assert.assertEquals(1, specificValues.get(5).getxValue().size());
            Assert.assertEquals("Limon argileux/craie", specificValues.get(5).getxValue().get(0));
            Assert.assertNull(specificValues.get(5).getyValue());
            Assert.assertEquals(0d, specificValues.get(5).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(0d, specificValues.get(5).getCoordinates().getLongitude(), 0.0001d);

            filterChoices = new ArrayList<>();
            FilterChoice campaign20142016Filter = getCampaignFilter("2014", "2016");
            filterChoices.add(campaign20142016Filter);
            specificValues = datamartDao.getSpecificValues(dephyNbFilter, groundTypeMetadata, filterChoices);
            Assert.assertEquals(4, specificValues.size());

            Assert.assertEquals("FAK27458", specificValues.get(0).getSpecific());
            Assert.assertEquals(1, specificValues.get(0).getxValue().size());
            Assert.assertEquals("Limon argileux/craie", specificValues.get(0).getxValue().get(0));
            Assert.assertNull(specificValues.get(0).getyValue());
            Assert.assertEquals(0d, specificValues.get(0).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(0d, specificValues.get(0).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("GCF31915", specificValues.get(1).getSpecific());
            Assert.assertEquals(1, specificValues.get(1).getxValue().size());
            Assert.assertEquals("Tourbe", specificValues.get(1).getxValue().get(0));
            Assert.assertNull(specificValues.get(1).getyValue());
            Assert.assertEquals(45.72854d, specificValues.get(1).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(4.9911999d, specificValues.get(1).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("PYF10467", specificValues.get(2).getSpecific());
            Assert.assertEquals(1, specificValues.get(2).getxValue().size());
            Assert.assertEquals("Limon battant sain", specificValues.get(2).getxValue().get(0));
            Assert.assertNull(specificValues.get(2).getyValue());
            Assert.assertEquals(53.8324973d, specificValues.get(2).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(3.9234691d, specificValues.get(2).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("PYF27458", specificValues.get(3).getSpecific());
            Assert.assertEquals(1, specificValues.get(3).getxValue().size());
            Assert.assertEquals("Limon argileux/craie", specificValues.get(3).getxValue().get(0));
            Assert.assertNull(specificValues.get(3).getyValue());
            Assert.assertEquals(0d, specificValues.get(3).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(0d, specificValues.get(3).getCoordinates().getLongitude(), 0.0001d);


            // Try now with a multiple values Metadata
            Metadata qualiCampaignMetadata = getQualiCampaignMetadata();

            filterChoices = new ArrayList<>();
            specificValues = datamartDao.getSpecificValues(dephyNbFilter, qualiCampaignMetadata, filterChoices);
            Assert.assertEquals(6, specificValues.size());

            Assert.assertEquals("FAK27458", specificValues.get(0).getSpecific());
            Assert.assertEquals(1, specificValues.get(0).getxValue().size());
            Assert.assertEquals("2014", specificValues.get(0).getxValue().get(0));
            Assert.assertNull(specificValues.get(0).getyValue());
            Assert.assertEquals(0d, specificValues.get(0).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(0d, specificValues.get(0).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("GCF31915", specificValues.get(1).getSpecific());
            Assert.assertEquals(3, specificValues.get(1).getxValue().size());
            Assert.assertEquals("2014", specificValues.get(1).getxValue().get(0));
            Assert.assertEquals("2015", specificValues.get(1).getxValue().get(1));
            Assert.assertEquals("2016", specificValues.get(1).getxValue().get(2));
            Assert.assertNull(specificValues.get(1).getyValue());
            Assert.assertEquals(45.72854d, specificValues.get(1).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(4.9911999d, specificValues.get(1).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("PYF10202", specificValues.get(2).getSpecific());
            Assert.assertEquals(3, specificValues.get(2).getxValue().size());
            Assert.assertEquals("2009", specificValues.get(2).getxValue().get(0));
            Assert.assertEquals("2010", specificValues.get(2).getxValue().get(1));
            Assert.assertEquals("2011", specificValues.get(2).getxValue().get(2));
            Assert.assertEquals(46.1179379d, specificValues.get(2).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(6.7349079d, specificValues.get(2).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("PYF10215", specificValues.get(3).getSpecific());
            Assert.assertEquals(1, specificValues.get(3).getxValue().size());
            Assert.assertEquals("2011", specificValues.get(3).getxValue().get(0));
            Assert.assertEquals(48.4921056d, specificValues.get(3).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(5.3860503d, specificValues.get(3).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("PYF10467", specificValues.get(4).getSpecific());
            Assert.assertEquals(2, specificValues.get(4).getxValue().size());
            Assert.assertEquals("2013", specificValues.get(4).getxValue().get(0));
            Assert.assertEquals("2014", specificValues.get(4).getxValue().get(1));
            Assert.assertEquals(53.8324973d, specificValues.get(4).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(3.9234691d, specificValues.get(4).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("PYF27458", specificValues.get(5).getSpecific());
            Assert.assertEquals(1, specificValues.get(5).getxValue().size());
            Assert.assertEquals("2014", specificValues.get(5).getxValue().get(0));
            Assert.assertEquals(0d, specificValues.get(5).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(0d, specificValues.get(5).getCoordinates().getLongitude(), 0.0001d);
        });

    }

    @Test
    public void testGetSpecificValuesForTwoMetadata() throws Exception {

        DataSource ds = database.getEmbeddedPostgres().getPostgresDatabase();
        Jdbi jdbi = Jdbi.create(ds);

        fillDatabase(jdbi);

        jdbi.useHandle(h -> {
            fr.inra.dephygraph.dao.DatamartDao datamartDao = new fr.inra.dephygraph.dao.DatamartDao(h);

            Metadata groundTypeMetadata = getGroundTypeMetadata();
            Metadata expensesMetadata = getExpensesMetadata();

            List<FilterChoice> filterChoices = new ArrayList<>();
            FilterChoice dephyNbFilter = getDephyNbFilter("GCF31915", "PYF10215", "PYF10202", "PYF10467", "PYF27458", "FAK27458");
            List<SpecificValue> specificValues = datamartDao.getSpecificValues(dephyNbFilter, groundTypeMetadata, expensesMetadata, filterChoices);
            Assert.assertEquals(6, specificValues.size());

            Assert.assertEquals("FAK27458", specificValues.get(0).getSpecific());
            Assert.assertEquals("Limon argileux/craie", specificValues.get(0).getxValue().get(0));
            Assert.assertEquals("3.1", specificValues.get(0).getyValue().get(0));
            Assert.assertEquals(0d, specificValues.get(0).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(0d, specificValues.get(0).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("PYF10202", specificValues.get(2).getSpecific());
            Assert.assertEquals("Tourbe", specificValues.get(2).getxValue().get(0));
            Assert.assertEquals("1.8", specificValues.get(2).getyValue().get(0));
            Assert.assertEquals(46.1179379d, specificValues.get(2).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(6.7349079d, specificValues.get(2).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("PYF10215", specificValues.get(3).getSpecific());
            Assert.assertEquals("Tourbe", specificValues.get(3).getxValue().get(0));
            Assert.assertEquals("10.2", specificValues.get(3).getyValue().get(0));
            Assert.assertEquals(48.4921056d, specificValues.get(3).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(5.3860503d, specificValues.get(3).getCoordinates().getLongitude(), 0.0001d);


            Assert.assertEquals("PYF27458", specificValues.get(5).getSpecific());
            Assert.assertEquals("Limon argileux/craie", specificValues.get(5).getxValue().get(0));
            Assert.assertEquals("0.7", specificValues.get(5).getyValue().get(0));
            Assert.assertEquals(0d, specificValues.get(5).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(0d, specificValues.get(5).getCoordinates().getLongitude(), 0.0001d);

            filterChoices = new ArrayList<>();
            FilterChoice campaign20142016Filter = getCampaignFilter("2014", "2016");
            filterChoices.add(campaign20142016Filter);
            specificValues = datamartDao.getSpecificValues(dephyNbFilter, groundTypeMetadata, expensesMetadata, filterChoices);
            Assert.assertEquals(4, specificValues.size());

            Assert.assertEquals("FAK27458", specificValues.get(0).getSpecific());
            Assert.assertEquals("Limon argileux/craie", specificValues.get(0).getxValue().get(0));
            Assert.assertEquals("3.1", specificValues.get(0).getyValue().get(0));
            Assert.assertEquals(0d, specificValues.get(0).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(0d, specificValues.get(0).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("GCF31915", specificValues.get(1).getSpecific());
            Assert.assertEquals("Tourbe", specificValues.get(1).getxValue().get(0));
            Assert.assertEquals("1.6", specificValues.get(1).getyValue().get(0));
            Assert.assertEquals(45.72854d, specificValues.get(1).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(4.9911999d, specificValues.get(1).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("PYF10467", specificValues.get(2).getSpecific());
            Assert.assertEquals("Limon battant sain", specificValues.get(2).getxValue().get(0));
            Assert.assertEquals("2", specificValues.get(2).getyValue().get(0));
            Assert.assertEquals(53.8324973d, specificValues.get(2).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(3.9234691d, specificValues.get(2).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("PYF27458", specificValues.get(3).getSpecific());
            Assert.assertEquals("Limon argileux/craie", specificValues.get(3).getxValue().get(0));
            Assert.assertEquals("0.7", specificValues.get(3).getyValue().get(0));
            Assert.assertEquals(0d, specificValues.get(3).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(0d, specificValues.get(3).getCoordinates().getLongitude(), 0.0001d);


            // Try now with a multiple values Metadata
            Metadata campaignMetadata = getCampaignMetadata();

            filterChoices = new ArrayList<>();
            specificValues = datamartDao.getSpecificValues(dephyNbFilter, groundTypeMetadata, campaignMetadata, filterChoices);
            Assert.assertEquals(6, specificValues.size());

            Assert.assertEquals("FAK27458", specificValues.get(0).getSpecific());
            Assert.assertEquals("Limon argileux/craie", specificValues.get(0).getxValue().get(0));
            Assert.assertEquals("2014", specificValues.get(0).getyValue().get(0));
            Assert.assertEquals(0d, specificValues.get(0).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(0d, specificValues.get(0).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("GCF31915", specificValues.get(1).getSpecific());
            Assert.assertEquals(3, specificValues.get(1).getxValue().size());
            Assert.assertEquals("Tourbe", specificValues.get(1).getxValue().get(0));
            Assert.assertEquals(3, specificValues.get(1).getyValue().size());
            Assert.assertEquals("2014", specificValues.get(1).getyValue().get(0));
            Assert.assertEquals("2015", specificValues.get(1).getyValue().get(1));
            Assert.assertEquals("2016", specificValues.get(1).getyValue().get(2));
            Assert.assertEquals(45.72854d, specificValues.get(1).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(4.9911999d, specificValues.get(1).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("PYF10202", specificValues.get(2).getSpecific());
            Assert.assertEquals(3, specificValues.get(2).getxValue().size());
            Assert.assertEquals("Tourbe", specificValues.get(2).getxValue().get(0));
            Assert.assertEquals(3, specificValues.get(2).getyValue().size());
            Assert.assertEquals("2009", specificValues.get(2).getyValue().get(0));
            Assert.assertEquals("2010", specificValues.get(2).getyValue().get(1));
            Assert.assertEquals("2011", specificValues.get(2).getyValue().get(2));
            Assert.assertEquals(46.1179379d, specificValues.get(2).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(6.7349079d, specificValues.get(2).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("PYF10215", specificValues.get(3).getSpecific());
            Assert.assertEquals(1, specificValues.get(3).getxValue().size());
            Assert.assertEquals("Tourbe", specificValues.get(3).getxValue().get(0));
            Assert.assertEquals(1, specificValues.get(3).getyValue().size());
            Assert.assertEquals("2011", specificValues.get(3).getyValue().get(0));
            Assert.assertEquals(48.4921056d, specificValues.get(3).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(5.3860503d, specificValues.get(3).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("PYF10467", specificValues.get(4).getSpecific());
            Assert.assertEquals(2, specificValues.get(4).getxValue().size());
            Assert.assertEquals("Limon battant sain", specificValues.get(4).getxValue().get(0));
            Assert.assertEquals(2, specificValues.get(4).getyValue().size());
            Assert.assertEquals("2013", specificValues.get(4).getyValue().get(0));
            Assert.assertEquals("2014", specificValues.get(4).getyValue().get(1));
            Assert.assertEquals(53.8324973d, specificValues.get(4).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(3.9234691d, specificValues.get(4).getCoordinates().getLongitude(), 0.0001d);

            Assert.assertEquals("PYF27458", specificValues.get(5).getSpecific());
            Assert.assertEquals(1, specificValues.get(5).getxValue().size());
            Assert.assertEquals("Limon argileux/craie", specificValues.get(5).getxValue().get(0));
            Assert.assertEquals(1, specificValues.get(5).getyValue().size());
            Assert.assertEquals("2014", specificValues.get(5).getyValue().get(0));
            Assert.assertEquals(0d, specificValues.get(5).getCoordinates().getLatitude(), 0.0001d);
            Assert.assertEquals(0d, specificValues.get(5).getCoordinates().getLongitude(), 0.0001d);
        });
    }

}
