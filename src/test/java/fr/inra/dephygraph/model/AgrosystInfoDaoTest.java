package fr.inra.dephygraph.model;

/*-
 * #%L
 * DEPHY-Graph
 * %%
 * Copyright (C) 2018 - 2019 Inra
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import fr.inra.dephygraph.AbstractTest;
import fr.inra.dephygraph.rest.AgrosystInfo;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.junit.Assert;
import org.junit.Test;

import javax.sql.DataSource;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class AgrosystInfoDaoTest extends AbstractTest {

    @Test
    public void testCreateInfo() throws Exception {

        DataSource ds = database.getEmbeddedPostgres().getPostgresDatabase();

        Jdbi jdbi = Jdbi.create(ds);

        addAgrosystInfoTable(jdbi);

        try (Handle handle = jdbi.open()) {
            fr.inra.dephygraph.dao.AgrosystInfoDao agrosystInfoDao = new fr.inra.dephygraph.dao.AgrosystInfoDao (handle);

            AgrosystInfo agrosystInfo = new AgrosystInfo();
            agrosystInfo.setId(UUID.randomUUID());
            agrosystInfo.setUserName("Defi Graff");
            agrosystInfo.setAllowedDephyNb(Arrays.asList("dephyOne", "Deuxphy", "DeThree"));
            agrosystInfo.setExpirationDate(OffsetDateTime.now().plus(24, ChronoUnit.HOURS));

            agrosystInfoDao.saveInfo(agrosystInfo);
        }

        try (Handle handle = jdbi.open()) {
            fr.inra.dephygraph.dao.AgrosystInfoDao agrosystInfoDao = new fr.inra.dephygraph.dao.AgrosystInfoDao (handle);

            AgrosystInfo agrosystInfo = new AgrosystInfo();
            agrosystInfo.setId(UUID.randomUUID());
            agrosystInfo.setUserName("Defi Graff");
            agrosystInfo.setAllowedDephyNb(Arrays.asList("dephyOne", "Deuxphy", "DeThree"));
            agrosystInfo.setExpirationDate(OffsetDateTime.now().plus(24, ChronoUnit.HOURS));

            agrosystInfoDao.saveInfo(agrosystInfo);
        }
    }

    @Test
    public void testRetrieveDephyNb() throws Exception {

        DataSource ds = database.getEmbeddedPostgres().getPostgresDatabase();

        Jdbi jdbi = Jdbi.create(ds);

        addAgrosystInfoTable(jdbi);

        try (Handle handle = jdbi.open()) {
            fr.inra.dephygraph.dao.AgrosystInfoDao agrosystInfoDao = new fr.inra.dephygraph.dao.AgrosystInfoDao (handle);

            // Create an info
            AgrosystInfo agrosystInfo = new AgrosystInfo();
            agrosystInfo.setId(UUID.randomUUID());
            agrosystInfo.setUserName("Defi Graff");
            agrosystInfo.setAllowedDephyNb(Arrays.asList("dephyOne", "Deuxphy", "DeThree"));
            agrosystInfo.setExpirationDate(OffsetDateTime.now().plus(24, ChronoUnit.HOURS));
            agrosystInfoDao.saveInfo(agrosystInfo);

            // Try to find it !
            List<String> dephyNbs = agrosystInfoDao.retrieveDephyNb(agrosystInfo.getId());
            Assert.assertEquals(3, dephyNbs.size());

            // Try to find from non existent id !
            dephyNbs = agrosystInfoDao.retrieveDephyNb(UUID.randomUUID());
            Assert.assertEquals(0, dephyNbs.size());

            // Create an expired info
            AgrosystInfo expiredAgrosystInfo = new AgrosystInfo();
            expiredAgrosystInfo.setId(UUID.randomUUID());
            expiredAgrosystInfo.setUserName("Defi Groff");
            expiredAgrosystInfo.setAllowedDephyNb(Arrays.asList("dephyOne", "Deuxphy"));
            expiredAgrosystInfo.setExpirationDate(OffsetDateTime.now());
            agrosystInfoDao.saveInfo(expiredAgrosystInfo);

            // Try to find its dephyNbs : no !
            dephyNbs = agrosystInfoDao.retrieveDephyNb(expiredAgrosystInfo.getId());
            Assert.assertEquals(0, dephyNbs.size());

        }
    }

    @Test
    public void testRetrieveInfo() throws Exception {

        DataSource ds = database.getEmbeddedPostgres().getPostgresDatabase();

        Jdbi jdbi = Jdbi.create(ds);

        addAgrosystInfoTable(jdbi);

        try (Handle handle = jdbi.open()) {
            fr.inra.dephygraph.dao.AgrosystInfoDao agrosystInfoDao = new fr.inra.dephygraph.dao.AgrosystInfoDao (handle);

            // Create an info
            AgrosystInfo agrosystInfo = new AgrosystInfo();
            agrosystInfo.setId(UUID.randomUUID());
            agrosystInfo.setUserName("Defi Graff");
            agrosystInfo.setAllowedDephyNb(Arrays.asList("dephyOne", "Deuxphy", "DeThree"));
            agrosystInfo.setExpirationDate(OffsetDateTime.now().plus(24, ChronoUnit.HOURS));
            agrosystInfoDao.saveInfo(agrosystInfo);


            // Try to find it !
            AgrosystInfo retrieveInfo = agrosystInfoDao.retrieveInfo(agrosystInfo.getId());
            Assert.assertNotNull(retrieveInfo);
            Assert.assertEquals(agrosystInfo.getId(), retrieveInfo.getId());
            Assert.assertEquals(3, retrieveInfo.getAllowedDephyNb().size());
            Assert.assertNull(retrieveInfo.getUserName()); // Do not keep this information

            // Try to find from non existent id !
            retrieveInfo = agrosystInfoDao.retrieveInfo(UUID.randomUUID());
            Assert.assertNull(retrieveInfo);

            // Create an expired info
            AgrosystInfo expiredAgrosystInfo = new AgrosystInfo();
            expiredAgrosystInfo.setId(UUID.randomUUID());
            expiredAgrosystInfo.setUserName("Defi Groff");
            expiredAgrosystInfo.setAllowedDephyNb(Arrays.asList("dephyOne", "Deuxphy"));
            expiredAgrosystInfo.setExpirationDate(OffsetDateTime.now());
            agrosystInfoDao.saveInfo(expiredAgrosystInfo);

            // Try to find its dephyNbs : no !
            retrieveInfo = agrosystInfoDao.retrieveInfo(expiredAgrosystInfo.getId());
            Assert.assertNull(retrieveInfo);

        }
    }

    @Test
    public void testRemoveExpiredEntries() throws Exception {
        DataSource ds = database.getEmbeddedPostgres().getPostgresDatabase();

        Jdbi jdbi = Jdbi.create(ds);

        addAgrosystInfoTable(jdbi);

        try (Handle handle = jdbi.open()) {
            fr.inra.dephygraph.dao.AgrosystInfoDao agrosystInfoDao = new fr.inra.dephygraph.dao.AgrosystInfoDao(handle);

            // Create an info
            AgrosystInfo agrosystInfo = new AgrosystInfo();
            agrosystInfo.setId(UUID.randomUUID());
            agrosystInfo.setUserName("Defi Graff");
            agrosystInfo.setAllowedDephyNb(Arrays.asList("dephyOne", "Deuxphy", "DeThree"));
            agrosystInfo.setExpirationDate(OffsetDateTime.now().minus(24, ChronoUnit.HOURS));
            agrosystInfoDao.saveInfo(agrosystInfo);

            int result = agrosystInfoDao.removeExpiredEntries();
            Assert.assertEquals(1, result);
        }
        try (Handle handle = jdbi.open()) {
            fr.inra.dephygraph.dao.AgrosystInfoDao agrosystInfoDao = new fr.inra.dephygraph.dao.AgrosystInfoDao(handle);

            // Create an info
            AgrosystInfo agrosystInfo = new AgrosystInfo();
            agrosystInfo.setId(UUID.randomUUID());
            agrosystInfo.setUserName("Defi Graff");
            agrosystInfo.setAllowedDephyNb(Arrays.asList("dephyOne", "Deuxphy", "DeThree"));
            agrosystInfo.setExpirationDate(OffsetDateTime.now().plus(24, ChronoUnit.HOURS));
            agrosystInfoDao.saveInfo(agrosystInfo);

            int result = agrosystInfoDao.removeExpiredEntries();
            Assert.assertEquals(0, result);
        }
    }
}
