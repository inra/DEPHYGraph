package fr.inra.dephygraph.dao;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.SingleInstancePostgresRule;
import fr.inra.dephygraph.DephyGraphConfig;
import fr.inra.dephygraph.DephyGraphDatasource;
import fr.inra.dephygraph.model.CoupleValuesData;
import fr.inra.dephygraph.model.DoubleRepartitionData;
import fr.inra.dephygraph.model.FilterChoice;
import fr.inra.dephygraph.model.FilterCohort;
import fr.inra.dephygraph.model.Metadata;
import fr.inra.dephygraph.model.SimpleRepartitionData;
import fr.inra.dephygraph.model.StatisticData;
import fr.inra.dephygraph.model.Systeme;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DatamartDaoTest {
    @Rule
    public SingleInstancePostgresRule database = EmbeddedPostgresRules.singleInstance();

    protected DephyGraphDatasource getDataSource() {
        DataSource ds = database.getEmbeddedPostgres().getPostgresDatabase();
        return new DephyGraphDatasource(ds);
    }

    protected void fillDatabase(Jdbi jdbi) throws Exception {
        String metadataInitScript = new String(Files.readAllBytes(Paths.get(getClass().getResource("/metadata.sql").toURI())));
        Assert.assertTrue(metadataInitScript.length() > 0);
        String cohortDataInitScript = new String(Files.readAllBytes(Paths.get(getClass().getResource("/cohort_data_test.sql").toURI())));
        Assert.assertTrue(cohortDataInitScript.length() > 0);

        try (Handle h = jdbi.open()) {
            h.createScript(metadataInitScript).execute();
            h.createScript(cohortDataInitScript).execute();
        }
    }

    @Test
    public void testGetQualitativeMetadataValues() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            List<String> campaigns = datamartDao.getQualitativeMetadataValues("c104_campaign_dis");
            Assert.assertTrue(campaigns.size() > 0);
            Set<String> years = new HashSet<>(campaigns);
            Assert.assertTrue(years.contains("2011"));
            Assert.assertTrue(years.contains("2021"));
        }
    }

    public static class WrappedQualiDistribution {
        public Map<String, Integer> distribution = Maps.newHashMap();
        public Set<String> dephyNbs = Sets.newHashSet();
        public Set<Systeme> systems = Sets.newHashSet();

        public WrappedQualiDistribution(List<SimpleRepartitionData> qualiDistribution) {
            for (SimpleRepartitionData data : qualiDistribution) {
                distribution.put(data.getValueName(), data.getValueOccurrence());
                systems.addAll(data.getSystemes());
                dephyNbs.addAll(data.getSystemes().stream().map(Systeme::getDephyNb).collect(Collectors.toSet()));
            }
        }

        public int getValueOccurrence(String value) {
            return distribution.get(value);
        }
    }

    @Test
    public void testGetSectorDistributionWithoutCohort() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata sectorMetadata = datamartDao.getMetadataByColumnName("c101_sector");
            Assert.assertNotNull(sectorMetadata);

            List<SimpleRepartitionData> result = datamartDao.getQualiDistribution(sectorMetadata, List.of(), null,
                    DephyGraphConfig.get().getMinDataRequired(), DephyGraphConfig.get().getMinDataRequiredForCohort());
            Assert.assertEquals(5, result.size());

            WrappedQualiDistribution qualiDistribution = new WrappedQualiDistribution(result);
            Assert.assertEquals(26, qualiDistribution.getValueOccurrence("Arboriculture"));
        }
    }

    @Test
    public void testGetQualiDistributionWithCohortFromYears() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata sectorMetadata = datamartDao.getMetadataByColumnName("c101_sector");
            Assert.assertNotNull(sectorMetadata);

            FilterCohort cohort = FilterCohort.ofYears(2012, 2013, 2014);
            List<SimpleRepartitionData> result = datamartDao.getQualiDistribution(sectorMetadata, List.of(), cohort,
                    DephyGraphConfig.get().getMinDataRequired(), DephyGraphConfig.get().getMinDataRequiredForCohort());
            Assert.assertTrue(result.size() > 0);

            WrappedQualiDistribution qualiDistribution = new WrappedQualiDistribution(result);

            Set<String> expectedDephyNbs = Sets.newHashSet(
                    "GCF10756", "GCF10756", "GCF11067", "GCF25166",
                    "LGF27356", "PYF11052", "PYF11061", "VIF11103", "VIF25191", "VIF26441");
            Assert.assertEquals(expectedDephyNbs, qualiDistribution.dephyNbs);
            Assert.assertEquals(27, qualiDistribution.systems.size());

            Assert.assertEquals(9, qualiDistribution.getValueOccurrence("Grandes cultures"));
            Assert.assertEquals(3, qualiDistribution.getValueOccurrence("Maraîchage"));
            Assert.assertEquals(6, qualiDistribution.getValueOccurrence("Polyculture-élevage"));
            Assert.assertEquals(9, qualiDistribution.getValueOccurrence("Viticulture"));
        }

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata sectorMetadata = datamartDao.getMetadataByColumnName("c101_sector");
            Assert.assertNotNull(sectorMetadata);

            Metadata regionMetadata = datamartDao.getMetadataByColumnName("c105_administrativeRegion");
            Assert.assertNotNull(regionMetadata);
            FilterChoice filterChoice = FilterChoice.fromMetadata(regionMetadata);
            filterChoice.setValues(List.of("Bretagne"));

            FilterCohort cohort = FilterCohort.ofYears(2012, 2013, 2014);
            List<SimpleRepartitionData> result = datamartDao.getQualiDistribution(sectorMetadata, List.of(filterChoice), cohort,
                    DephyGraphConfig.get().getMinDataRequired(), DephyGraphConfig.get().getMinDataRequiredForCohort());
            Assert.assertTrue(result.size() > 0);

            WrappedQualiDistribution qualiDistribution = new WrappedQualiDistribution(result);
            Set<String> expectedDephyNbs = Sets.newHashSet("PYF11052");
            Assert.assertEquals(expectedDephyNbs, qualiDistribution.dephyNbs);
            Assert.assertEquals(3, qualiDistribution.systems.size());

            Assert.assertEquals(3, qualiDistribution.getValueOccurrence("Polyculture-élevage"));
        }

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata sectorMetadata = datamartDao.getMetadataByColumnName("c101_sector");
            Assert.assertNotNull(sectorMetadata);

            Metadata managementTypeMetadata = datamartDao.getMetadataByColumnName("c110_managementType");
            Assert.assertNotNull(managementTypeMetadata);
            FilterChoice filterChoice = FilterChoice.fromMetadata(managementTypeMetadata);
            filterChoice.setValues(List.of("Agriculture biologique"));

            FilterCohort cohort = FilterCohort.ofYears(2016, 2017);
            List<SimpleRepartitionData> result = datamartDao.getQualiDistribution(sectorMetadata, List.of(filterChoice), cohort,
                    DephyGraphConfig.get().getMinDataRequired(), DephyGraphConfig.get().getMinDataRequiredForCohort());
            Assert.assertTrue(result.size() > 0);

            WrappedQualiDistribution qualiDistribution = new WrappedQualiDistribution(result);
            Set<String> expectedDephyNbs = Sets.newHashSet("GCF37082", "LGF35330");
            Assert.assertEquals(expectedDephyNbs, qualiDistribution.dephyNbs);
            Assert.assertEquals(4, qualiDistribution.systems.size());

            Assert.assertEquals(2, qualiDistribution.getValueOccurrence("Grandes cultures"));
            Assert.assertEquals(2, qualiDistribution.getValueOccurrence("Maraîchage"));
        }

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata groundWorkTypeMetadata = datamartDao.getMetadataByColumnName("c201_groundWorkType");
            Assert.assertNotNull(groundWorkTypeMetadata);

            Metadata managementTypeMetadata = datamartDao.getMetadataByColumnName("c110_managementType");
            Assert.assertNotNull(managementTypeMetadata);
            FilterChoice filterChoice = FilterChoice.fromMetadata(managementTypeMetadata);
            filterChoice.setValues(List.of("Agriculture conventionnelle"));

            FilterCohort cohort = FilterCohort.ofYears(2017, 2018, 2019, 2020);
            List<SimpleRepartitionData> result = datamartDao.getQualiDistribution(groundWorkTypeMetadata, List.of(filterChoice), cohort,
                    DephyGraphConfig.get().getMinDataRequired(), DephyGraphConfig.get().getMinDataRequiredForCohort());
            Assert.assertTrue(result.size() > 0);

            WrappedQualiDistribution qualiDistribution = new WrappedQualiDistribution(result);
            Set<String> expectedDephyNbs = Sets.newHashSet(
                    "GCF10756", "GCF25166", "GCF31293", "GCF32453", "GCF35859", "GCF36933", "GCF37303", "PYF11052");
            Assert.assertEquals(expectedDephyNbs, qualiDistribution.dephyNbs);
            Assert.assertEquals(32, qualiDistribution.systems.size());

            Assert.assertEquals(12, qualiDistribution.getValueOccurrence("4 - Techniques culturales simplifiées"));
            Assert.assertEquals(4, qualiDistribution.getValueOccurrence("5 - Semis direct"));
            Assert.assertEquals(5, qualiDistribution.getValueOccurrence("2 - Labour fréquent"));
            Assert.assertEquals(10, qualiDistribution.getValueOccurrence("3 - Labour occasionnel"));
            Assert.assertEquals(1, qualiDistribution.getValueOccurrence("1 - Labour systématique"));
        }
    }

    @Test
    public void testGetPz0DistributionWithCohortFromYears() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata pz0Metadata = datamartDao.getMetadataByColumnName("c102_pz0");
            Assert.assertNotNull(pz0Metadata);

            Metadata groundWorkTypeMetadata = datamartDao.getMetadataByColumnName("c201_groundWorkType");
            Assert.assertNotNull(groundWorkTypeMetadata);

            FilterChoice groundWorkTypeFilterChoice = FilterChoice.fromMetadata(groundWorkTypeMetadata);
            groundWorkTypeFilterChoice.setValues(List.of("1 - Labour systématique", "2 - Labour fréquent", "3 - Labour occasionnel"));

            FilterCohort cohort = FilterCohort.ofYears(2016, 2017);
            {
                List<SimpleRepartitionData> result = datamartDao.getQualiDistribution(pz0Metadata,
                        List.of(groundWorkTypeFilterChoice), cohort,
                        DephyGraphConfig.get().getMinDataRequired(), DephyGraphConfig.get().getMinDataRequiredForCohort());
                Assert.assertTrue(result.size() > 0);

                WrappedQualiDistribution qualiDistribution = new WrappedQualiDistribution(result);
                Set<String> expectedDephyNbs = Sets.newHashSet(
                        "GCF36933", "PYF11052");
                Assert.assertEquals(expectedDephyNbs, qualiDistribution.dephyNbs);
                Assert.assertEquals(6, qualiDistribution.systems.size());
            }
            {
                List<SimpleRepartitionData> result = datamartDao.getQualiDistribution(pz0Metadata,
                        List.of(groundWorkTypeFilterChoice), cohort,
                        3, 3);
                Assert.assertEquals(0, result.size());
            }
            {
                List<SimpleRepartitionData> result = datamartDao.getQualiDistribution(pz0Metadata,
                        List.of(groundWorkTypeFilterChoice), cohort,
                        0, 3);
                Assert.assertEquals(0, result.size());
            }
            {
                List<SimpleRepartitionData> result = datamartDao.getQualiDistribution(pz0Metadata,
                        List.of(groundWorkTypeFilterChoice), cohort,
                        5, 0);
                Assert.assertEquals(0, result.size());
            }
        }
    }

    @Test
    public void testGetQualiDistributionWithCohortFromNetworkAges() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata sectorMetadata = datamartDao.getMetadataByColumnName("c101_sector");
            Assert.assertNotNull(sectorMetadata);
            Metadata regionMetadata = datamartDao.getMetadataByColumnName("c105_administrativeRegion");
            Assert.assertNotNull(regionMetadata);

            FilterChoice regionFilterChoice = FilterChoice.fromMetadata(regionMetadata);
            regionFilterChoice.setValues(List.of("Bretagne"));

            FilterCohort cohort = FilterCohort.ofNetworkAges(
                    "État initial", "État initial + 01 an", "État initial + 02 ans", "État initial + 03 ans");
            List<SimpleRepartitionData> result = datamartDao.getQualiDistribution(sectorMetadata, List.of(regionFilterChoice), cohort,
                    DephyGraphConfig.get().getMinDataRequired(), DephyGraphConfig.get().getMinDataRequiredForCohort());
            Assert.assertTrue(result.size() > 0);

            WrappedQualiDistribution qualiDistribution = new WrappedQualiDistribution(result);

            Set<String> expectedDephyNbs = Sets.newHashSet(
                    "GCF36180", "LGF35330", "PYF11052");
            Assert.assertEquals(expectedDephyNbs, qualiDistribution.dephyNbs);
            Assert.assertEquals(12, qualiDistribution.systems.size());

            Assert.assertEquals(4, qualiDistribution.getValueOccurrence("Maraîchage"));
            Assert.assertEquals(8, qualiDistribution.getValueOccurrence("Polyculture-élevage"));
        }
    }

    public static class WrappedQualiQualiDistribution {
        public Map<String, Integer> distribution = Maps.newHashMap();
        public int occurrence;
        public Set<String> dephyNbs = Sets.newHashSet();

        public WrappedQualiQualiDistribution(List<DoubleRepartitionData> result, String yValueName) {
            DoubleRepartitionData data = result.stream().filter(d -> Objects.equals(d.getValueName(), yValueName))
                    .findFirst()
                    .get();
            occurrence = data.getValueOccurrence();
            dephyNbs.addAll(data.getSystemes().stream().map(Systeme::getDephyNb).collect(Collectors.toSet()));
            for (SimpleRepartitionData d : data.getInnerRepartition()) {
                distribution.put(d.getValueName(), d.getValueOccurrence());
            }
        }

        public int getOccurrence() {
            return occurrence;
        }

        public int getValueOccurrenceAt(String value) {
            return distribution.get(value);
        }
    }

    @Test
    public void testGetSectorPerCampaignDistributionWithoutCohort() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata sectorMetadata = datamartDao.getMetadataByColumnName("c101_sector");
            Assert.assertNotNull(sectorMetadata);
            Metadata campaignMetadata = datamartDao.getMetadataByColumnName("c104_campaign_dis");
            Assert.assertNotNull(campaignMetadata);

            {
                List<DoubleRepartitionData> result = datamartDao.getQualiQualiDistribution(sectorMetadata,
                        campaignMetadata, List.of(), null,
                        DephyGraphConfig.get().getMinDataRequired(), DephyGraphConfig.get().getMinDataRequiredForCohort());
                Assert.assertTrue(result.size() > 0);
                {
                    WrappedQualiQualiDistribution qualiQualiDistribution = new WrappedQualiQualiDistribution(result, "Arboriculture");
                    Assert.assertEquals(26, qualiQualiDistribution.getOccurrence());
                    Assert.assertEquals(2, qualiQualiDistribution.getValueOccurrenceAt("2014"));
                    Assert.assertEquals(2, qualiQualiDistribution.getValueOccurrenceAt("2015"));
                    Assert.assertEquals(4, qualiQualiDistribution.getValueOccurrenceAt("2016"));
                    Assert.assertEquals(4, qualiQualiDistribution.getValueOccurrenceAt("2017"));
                    Assert.assertEquals(4, qualiQualiDistribution.getValueOccurrenceAt("2018"));
                    Assert.assertEquals(4, qualiQualiDistribution.getValueOccurrenceAt("2019"));
                    Assert.assertEquals(4, qualiQualiDistribution.getValueOccurrenceAt("2020"));
                    Assert.assertEquals(2, qualiQualiDistribution.getValueOccurrenceAt("2021"));
                }
                {
                    WrappedQualiQualiDistribution qualiQualiDistribution = new WrappedQualiQualiDistribution(result, "Maraîchage");
                    Assert.assertEquals(20, qualiQualiDistribution.getOccurrence());
                    Assert.assertEquals(1, qualiQualiDistribution.getValueOccurrenceAt("2012"));
                    Assert.assertEquals(1, qualiQualiDistribution.getValueOccurrenceAt("2013"));
                    Assert.assertEquals(1, qualiQualiDistribution.getValueOccurrenceAt("2014"));
                    Assert.assertEquals(1, qualiQualiDistribution.getValueOccurrenceAt("2015"));
                    Assert.assertEquals(3, qualiQualiDistribution.getValueOccurrenceAt("2016"));
                    Assert.assertEquals(3, qualiQualiDistribution.getValueOccurrenceAt("2017"));
                    Assert.assertEquals(2, qualiQualiDistribution.getValueOccurrenceAt("2018"));
                    Assert.assertEquals(3, qualiQualiDistribution.getValueOccurrenceAt("2019"));
                    Assert.assertEquals(3, qualiQualiDistribution.getValueOccurrenceAt("2020"));
                    Assert.assertEquals(2, qualiQualiDistribution.getValueOccurrenceAt("2021"));

                }
            }
            {
                List<DoubleRepartitionData> result = datamartDao.getQualiQualiDistribution(sectorMetadata,
                        campaignMetadata, List.of(), null,
                        3, 0);
                Assert.assertTrue(result.size() > 0);
                {
                    WrappedQualiQualiDistribution qualiQualiDistribution = new WrappedQualiQualiDistribution(result, "Arboriculture");
                    Assert.assertEquals(20, qualiQualiDistribution.getOccurrence());
                    Assert.assertEquals(4, qualiQualiDistribution.getValueOccurrenceAt("2016"));
                    Assert.assertEquals(4, qualiQualiDistribution.getValueOccurrenceAt("2017"));
                    Assert.assertEquals(4, qualiQualiDistribution.getValueOccurrenceAt("2018"));
                    Assert.assertEquals(4, qualiQualiDistribution.getValueOccurrenceAt("2019"));
                    Assert.assertEquals(4, qualiQualiDistribution.getValueOccurrenceAt("2020"));
                }
                {
                    WrappedQualiQualiDistribution qualiQualiDistribution = new WrappedQualiQualiDistribution(result, "Maraîchage");
                    Assert.assertEquals(12, qualiQualiDistribution.getOccurrence());
                    Assert.assertEquals(3, qualiQualiDistribution.getValueOccurrenceAt("2016"));
                    Assert.assertEquals(3, qualiQualiDistribution.getValueOccurrenceAt("2017"));
                    Assert.assertEquals(3, qualiQualiDistribution.getValueOccurrenceAt("2019"));
                    Assert.assertEquals(3, qualiQualiDistribution.getValueOccurrenceAt("2020"));
                }
            }
        }
    }

    @Test
    public void testGetSectorPerCampaignDistributionWithCohortFromYears() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata sectorMetadata = datamartDao.getMetadataByColumnName("c101_sector");
            Assert.assertNotNull(sectorMetadata);
            Metadata campaignMetadata = datamartDao.getMetadataByColumnName("c104_campaign_dis");
            Assert.assertNotNull(campaignMetadata);
            Metadata regionMetadata = datamartDao.getMetadataByColumnName("c105_administrativeRegion");
            Assert.assertNotNull(regionMetadata);

            FilterChoice regionFilterChoice = FilterChoice.fromMetadata(regionMetadata);
            regionFilterChoice.setValues(List.of("Auvergne-Rhône-Alpes", "Bourgogne-Franche-Comté", "Bretagne", "Centre-Val de Loire",
                    "Hauts-de-France", "Nouvelle-Aquitaine", "Occitanie", "Pays de la Loire"));

            FilterChoice sectorFilterChoice = FilterChoice.fromMetadata(sectorMetadata);
            sectorFilterChoice.setValues(List.of("Viticulture", "Arboriculture"));

            FilterCohort cohort = FilterCohort.ofYears(2012, 2013, 2014, 2015);
            List<DoubleRepartitionData> result = datamartDao.getQualiQualiDistribution(sectorMetadata, campaignMetadata,
                    List.of(regionFilterChoice, sectorFilterChoice), cohort,
                    DephyGraphConfig.get().getMinDataRequired(), DephyGraphConfig.get().getMinDataRequiredForCohort());
            Assert.assertTrue(result.size() > 0);

            {
                WrappedQualiQualiDistribution qualiQualiDistribution = new WrappedQualiQualiDistribution(result, "Viticulture");
                Assert.assertEquals(8, qualiQualiDistribution.getOccurrence());
                Assert.assertEquals(2, qualiQualiDistribution.getValueOccurrenceAt("2012"));
                Assert.assertEquals(2, qualiQualiDistribution.getValueOccurrenceAt("2013"));
                Assert.assertEquals(2, qualiQualiDistribution.getValueOccurrenceAt("2014"));
                Assert.assertEquals(2, qualiQualiDistribution.getValueOccurrenceAt("2015"));

                Assert.assertEquals(2, qualiQualiDistribution.dephyNbs.size());
                Assert.assertTrue(qualiQualiDistribution.dephyNbs.contains("VIF11103"));
                Assert.assertTrue(qualiQualiDistribution.dephyNbs.contains("VIF26441"));
            }
        }
    }

    @Test
    public void testGetSectorPerCampaignDistributionWithCohortFromNetworkAges() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata sectorMetadata = datamartDao.getMetadataByColumnName("c101_sector");
            Assert.assertNotNull(sectorMetadata);
            Metadata campaignMetadata = datamartDao.getMetadataByColumnName("c104_campaign_dis");
            Assert.assertNotNull(campaignMetadata);
            Metadata regionMetadata = datamartDao.getMetadataByColumnName("c105_administrativeRegion");
            Assert.assertNotNull(regionMetadata);

            FilterChoice regionFilterChoice = FilterChoice.fromMetadata(regionMetadata);
            regionFilterChoice.setValues(List.of("Auvergne-Rhône-Alpes", "Bourgogne-Franche-Comté", "Bretagne", "Centre-Val de Loire",
                    "Hauts-de-France", "Nouvelle-Aquitaine", "Occitanie", "Pays de la Loire"));

            FilterChoice sectorFilterChoice = FilterChoice.fromMetadata(sectorMetadata);
            sectorFilterChoice.setValues(List.of("Viticulture", "Arboriculture"));

            FilterCohort cohort = FilterCohort.ofNetworkAges(
                    "État initial", "État initial + 01 an", "État initial + 02 ans", "État initial + 03 ans");
            List<DoubleRepartitionData> result = datamartDao.getQualiQualiDistribution(sectorMetadata, campaignMetadata,
                    List.of(regionFilterChoice, sectorFilterChoice), cohort,
                    DephyGraphConfig.get().getMinDataRequired(), DephyGraphConfig.get().getMinDataRequiredForCohort());
            Assert.assertTrue(result.size() > 0);

            {
                WrappedQualiQualiDistribution qualiQualiDistribution = new WrappedQualiQualiDistribution(result, "Viticulture");
                Assert.assertEquals(20, qualiQualiDistribution.getOccurrence());
                Assert.assertEquals(2, qualiQualiDistribution.getValueOccurrenceAt("2012"));
                Assert.assertEquals(2, qualiQualiDistribution.getValueOccurrenceAt("2013"));
                Assert.assertEquals(2, qualiQualiDistribution.getValueOccurrenceAt("2014"));
                Assert.assertEquals(2, qualiQualiDistribution.getValueOccurrenceAt("2015"));
                Assert.assertEquals(3, qualiQualiDistribution.getValueOccurrenceAt("2016"));
                Assert.assertEquals(3, qualiQualiDistribution.getValueOccurrenceAt("2017"));
                Assert.assertEquals(3, qualiQualiDistribution.getValueOccurrenceAt("2018"));
                Assert.assertEquals(3, qualiQualiDistribution.getValueOccurrenceAt("2019"));

                Assert.assertEquals(5, qualiQualiDistribution.dephyNbs.size());
                Assert.assertTrue(qualiQualiDistribution.dephyNbs.contains("VIF11103"));
                Assert.assertTrue(qualiQualiDistribution.dephyNbs.contains("VIF26441"));
                Assert.assertTrue(qualiQualiDistribution.dephyNbs.contains("VIF35228"));
            }
        }
    }

    @Test
    public void testGetGroundWorkTypePerPz0DistributionWithCohortFromYears() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata totalIFTMetadata = datamartDao.getMetadataByColumnName("c601_totalIFT");
            Assert.assertNotNull(totalIFTMetadata);

            Metadata pz0Metadata = datamartDao.getMetadataByColumnName("c102_pz0");
            Assert.assertNotNull(pz0Metadata);

            Metadata groundWorkTypeMetadata = datamartDao.getMetadataByColumnName("c201_groundWorkType");
            Assert.assertNotNull(groundWorkTypeMetadata);

            FilterChoice groundWorkTypeFilterChoice = FilterChoice.fromMetadata(groundWorkTypeMetadata);
            groundWorkTypeFilterChoice.setValues(List.of("1 - Labour systématique", "2 - Labour fréquent", "3 - Labour occasionnel"));

            FilterCohort cohort = FilterCohort.ofYears(2016, 2017);

            {
                List<DoubleRepartitionData> result = datamartDao.getQualiQualiDistribution(
                        groundWorkTypeMetadata, pz0Metadata, List.of(groundWorkTypeFilterChoice), cohort,
                        2, 1);
                Assert.assertTrue(result.size() > 0);
                Set<String> dephyNbs = result.stream().flatMap(r -> r.getSystemes().stream())
                        .map(Systeme::getDephyNb).collect(Collectors.toSet());
                Assert.assertEquals(2, dephyNbs.size());
                Assert.assertTrue(dephyNbs.contains("GCF36933"));
                Assert.assertTrue(dephyNbs.contains("PYF11052"));

                {
                    WrappedQualiQualiDistribution qualiQualiDistribution = new WrappedQualiQualiDistribution(result, "3 - Labour occasionnel");
                    Assert.assertEquals(1, qualiQualiDistribution.dephyNbs.size());
                    Assert.assertEquals(3, qualiQualiDistribution.getOccurrence());
                }
                {
                    WrappedQualiQualiDistribution qualiQualiDistribution = new WrappedQualiQualiDistribution(result, "1 - Labour systématique");
                    Assert.assertEquals(1, qualiQualiDistribution.dephyNbs.size());
                    Assert.assertEquals(3, qualiQualiDistribution.getOccurrence());
                }
            }
            {
                List<DoubleRepartitionData> result = datamartDao.getQualiQualiDistribution(
                        groundWorkTypeMetadata, pz0Metadata, List.of(groundWorkTypeFilterChoice),
                        cohort, 2, 2);
                Assert.assertEquals(0, result.size());
            }
            {
                List<DoubleRepartitionData> result = datamartDao.getQualiQualiDistribution(
                        groundWorkTypeMetadata, pz0Metadata, List.of(groundWorkTypeFilterChoice),
                        cohort, 0, 3);
                Assert.assertEquals(0, result.size());
            }
        }
    }

    @Test
    public void testGetIFTTotalDistributionWithoutCohort() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata totalIFTMetadata = datamartDao.getMetadataByColumnName("c601_totalIFT");
            Assert.assertNotNull(totalIFTMetadata);

            StatisticData result = datamartDao.getQuantiDistribution(totalIFTMetadata, List.of(), null,
                    DephyGraphConfig.get().getMinDataRequired());
            Assert.assertTrue(result.getValues().length > 0);
            Assert.assertEquals(184, result.getSystemes().size());
            Assert.assertEquals(0.0, result.getMin(), 0.001);
            Assert.assertEquals(28.04, result.getMax(), 0.001);
        }
    }

    @Test
    public void testGetIFTTotalDistributionWithCohortFromYears() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata totalIFTMetadata = datamartDao.getMetadataByColumnName("c601_totalIFT");
            Assert.assertNotNull(totalIFTMetadata);

            Metadata regionMetadata = datamartDao.getMetadataByColumnName("c105_administrativeRegion");
            Assert.assertNotNull(regionMetadata);

            FilterChoice regionFilterChoice = FilterChoice.fromMetadata(regionMetadata);
            regionFilterChoice.setValues(List.of("Bretagne"));

            FilterCohort cohort = FilterCohort.ofYears(2016, 2017);

            StatisticData result = datamartDao.getQuantiDistribution(totalIFTMetadata, List.of(regionFilterChoice), cohort,
                    DephyGraphConfig.get().getMinDataRequired());
            Assert.assertTrue(result.getValues().length > 0);
            Assert.assertEquals(6, result.getSystemes().size());
            Assert.assertEquals(0.0, result.getMin(), 0.001);
            Assert.assertEquals(2.95, result.getMax(), 0.001);
            Assert.assertEquals(1.38, result.getAverage(), 0.001);
        }
    }

    @Test
    public void testGetIFTTotalDistributionWithCohortFromNetworkAges() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata totalIFTMetadata = datamartDao.getMetadataByColumnName("c601_totalIFT");
            Assert.assertNotNull(totalIFTMetadata);

            Metadata regionMetadata = datamartDao.getMetadataByColumnName("c105_administrativeRegion");
            Assert.assertNotNull(regionMetadata);

            FilterChoice regionFilterChoice = FilterChoice.fromMetadata(regionMetadata);
            regionFilterChoice.setValues(List.of("Bretagne"));

            FilterCohort cohort = FilterCohort.ofNetworkAges("État initial", "État initial + 01 an");

            StatisticData result = datamartDao.getQuantiDistribution(totalIFTMetadata, List.of(regionFilterChoice), cohort,
                    DephyGraphConfig.get().getMinDataRequired());
            Assert.assertTrue(result.getValues().length > 0);
            Assert.assertEquals(8, result.getSystemes().size());
            Assert.assertEquals(0.0, result.getMin(), 0.001);
            Assert.assertEquals(3.98, result.getMax(), 0.001);
            Assert.assertEquals(1.98, result.getAverage(), 0.001);
        }
    }

    public static class WrappedQuantiQualiDistribution {
        public Map<String, StatisticData> distribution = Maps.newHashMap();
        public Set<String> dephyNbs = Sets.newHashSet();

        public WrappedQuantiQualiDistribution(List<StatisticData> result) {
            for (StatisticData data : result) {
                distribution.put(data.getValueName(), data);
                dephyNbs.addAll(data.getSystemes().stream().map(Systeme::getDephyNb).collect(Collectors.toSet()));
            }
        }

        public StatisticData get(String xValue) {
            return distribution.get(xValue);
        }
    }

    @Test
    public void testGetIFTTotalPerManagementTypeDistributionWithoutCohort() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata totalIFTMetadata = datamartDao.getMetadataByColumnName("c601_totalIFT");
            Assert.assertNotNull(totalIFTMetadata);

            Metadata managementTypeMetadata = datamartDao.getMetadataByColumnName("c110_managementType");
            Assert.assertNotNull(managementTypeMetadata);

            Metadata regionMetadata = datamartDao.getMetadataByColumnName("c105_administrativeRegion");
            Assert.assertNotNull(regionMetadata);

            FilterChoice regionFilterChoice = FilterChoice.fromMetadata(regionMetadata);
            regionFilterChoice.setValues(List.of("Bretagne"));

            List<StatisticData> result = datamartDao.getQuantiQualiDistribution(totalIFTMetadata, managementTypeMetadata,
                    List.of(regionFilterChoice), null,
                    DephyGraphConfig.get().getMinDataRequired(), DephyGraphConfig.get().getMinDataRequiredForCohort());
            Assert.assertTrue(result.size() > 0);

            WrappedQuantiQualiDistribution quantiQualiDistribution = new WrappedQuantiQualiDistribution(result);
            Set<String> expectedDephyNbs = Sets.newHashSet(
                    "GCF36180", "LGF34209", "LGF35330", "LGF37537", "PYF11052");
            Assert.assertEquals(expectedDephyNbs, quantiQualiDistribution.dephyNbs);

            {
                StatisticData data = quantiQualiDistribution.get("Agriculture biologique");
                Assert.assertEquals(0.0, data.getMin(), 0.001);
                Assert.assertEquals(0.0, data.getMax(), 0.001);
            }
            {
                StatisticData data = quantiQualiDistribution.get("Agriculture conventionnelle");
                Assert.assertEquals(0.51, data.getMin(), 0.001);
                Assert.assertEquals(3.98, data.getMax(), 0.001);
            }
        }
    }

    @Test
    public void testGetIFTTotalPerManagementTypeDistributionWithCohortFromYears() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata totalIFTMetadata = datamartDao.getMetadataByColumnName("c601_totalIFT");
            Assert.assertNotNull(totalIFTMetadata);

            Metadata managementTypeMetadata = datamartDao.getMetadataByColumnName("c110_managementType");
            Assert.assertNotNull(managementTypeMetadata);

            Metadata regionMetadata = datamartDao.getMetadataByColumnName("c105_administrativeRegion");
            Assert.assertNotNull(regionMetadata);

            FilterChoice regionFilterChoice = FilterChoice.fromMetadata(regionMetadata);
            regionFilterChoice.setValues(List.of("Bretagne"));

            FilterCohort cohort = FilterCohort.ofYears(2016, 2017);

            List<StatisticData> result = datamartDao.getQuantiQualiDistribution(totalIFTMetadata, managementTypeMetadata,
                    List.of(regionFilterChoice), cohort,
                    DephyGraphConfig.get().getMinDataRequired(), DephyGraphConfig.get().getMinDataRequiredForCohort());
            Assert.assertTrue(result.size() > 0);

            WrappedQuantiQualiDistribution quantiQualiDistribution = new WrappedQuantiQualiDistribution(result);
            Set<String> expectedDephyNbs = Sets.newHashSet(
                    "GCF36180", "LGF35330", "PYF11052");
            Assert.assertEquals(expectedDephyNbs, quantiQualiDistribution.dephyNbs);

            {
                StatisticData data = quantiQualiDistribution.get("Agriculture biologique");
                Assert.assertEquals(0.0, data.getMin(), 0.001);
                Assert.assertEquals(0.0, data.getMax(), 0.001);
            }
            {
                StatisticData data = quantiQualiDistribution.get("Agriculture conventionnelle");
                Assert.assertEquals(0.73, data.getMin(), 0.001);
                Assert.assertEquals(2.95, data.getMax(), 0.001);
            }
        }
    }

    @Test
    public void testGetIFTTotalPerManagementTypeDistributionWithCohortFromNetworkAges() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata totalIFTMetadata = datamartDao.getMetadataByColumnName("c601_totalIFT");
            Assert.assertNotNull(totalIFTMetadata);

            Metadata managementTypeMetadata = datamartDao.getMetadataByColumnName("c110_managementType");
            Assert.assertNotNull(managementTypeMetadata);

            Metadata regionMetadata = datamartDao.getMetadataByColumnName("c105_administrativeRegion");
            Assert.assertNotNull(regionMetadata);

            FilterChoice regionFilterChoice = FilterChoice.fromMetadata(regionMetadata);
            regionFilterChoice.setValues(List.of("Bretagne"));

            FilterCohort cohort = FilterCohort.ofNetworkAges("État initial", "État initial + 01 an");

            List<StatisticData> result = datamartDao.getQuantiQualiDistribution(totalIFTMetadata, managementTypeMetadata,
                    List.of(regionFilterChoice), cohort,
                    DephyGraphConfig.get().getMinDataRequired(), DephyGraphConfig.get().getMinDataRequiredForCohort());
            Assert.assertTrue(result.size() > 0);

            WrappedQuantiQualiDistribution quantiQualiDistribution = new WrappedQuantiQualiDistribution(result);
            Set<String> expectedDephyNbs = Sets.newHashSet(
                    "GCF36180", "LGF34209", "LGF35330", "PYF11052");
            Assert.assertEquals(expectedDephyNbs, quantiQualiDistribution.dephyNbs);

            {
                StatisticData data = quantiQualiDistribution.get("Agriculture biologique");
                Assert.assertEquals(0.0, data.getMin(), 0.001);
                Assert.assertEquals(0.0, data.getMax(), 0.001);
            }
            {
                StatisticData data = quantiQualiDistribution.get("Agriculture conventionnelle");
                Assert.assertEquals(0.73, data.getMin(), 0.001);
                Assert.assertEquals(3.98, data.getMax(), 0.001);
            }
        }
    }

    @Test
    public void testGetIFTTotalPerPz0DistributionWithCohortFromYears() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata totalIFTMetadata = datamartDao.getMetadataByColumnName("c601_totalIFT");
            Assert.assertNotNull(totalIFTMetadata);

            Metadata pz0Metadata = datamartDao.getMetadataByColumnName("c102_pz0");
            Assert.assertNotNull(pz0Metadata);

            Metadata groundWorkTypeMetadata = datamartDao.getMetadataByColumnName("c201_groundWorkType");
            Assert.assertNotNull(groundWorkTypeMetadata);

            FilterChoice groundWorkTypeFilterChoice = FilterChoice.fromMetadata(groundWorkTypeMetadata);
            groundWorkTypeFilterChoice.setValues(List.of("1 - Labour systématique", "2 - Labour fréquent", "3 - Labour occasionnel"));

            FilterCohort cohort = FilterCohort.ofYears(2016, 2017);

            {
                List<StatisticData> result = datamartDao.getQuantiQualiDistribution(totalIFTMetadata, pz0Metadata,
                        List.of(groundWorkTypeFilterChoice), cohort,
                        DephyGraphConfig.get().getMinDataRequired(), DephyGraphConfig.get().getMinDataRequiredForCohort());
                Assert.assertTrue(result.size() > 0);

                WrappedQuantiQualiDistribution quantiQualiDistribution = new WrappedQuantiQualiDistribution(result);
                Set<String> expectedDephyNbs = Sets.newHashSet(
                        "GCF36933", "PYF11052");
                Assert.assertEquals(expectedDephyNbs, quantiQualiDistribution.dephyNbs);

                {
                    StatisticData data = quantiQualiDistribution.get("Systèmes avant l'entrée dans le réseau DEPHY");
                    Assert.assertEquals(2.52, data.getMin(), 0.001);
                    Assert.assertEquals(3.05, data.getMax(), 0.001);
                    Assert.assertEquals(2, data.getSystemes().size());
                }
                {
                    StatisticData data = quantiQualiDistribution.get("Systèmes évoluant au sein du réseau DEPHY");
                    Assert.assertEquals(2.17, data.getMin(), 0.001);
                    Assert.assertEquals(4.31, data.getMax(), 0.001);
                    Assert.assertEquals(4, data.getSystemes().size());
                }
            }
            {
                List<StatisticData> result = datamartDao.getQuantiQualiDistribution(totalIFTMetadata, pz0Metadata,
                        List.of(groundWorkTypeFilterChoice), cohort, 3, 3);
                Assert.assertEquals(0, result.size());
            }
            {
                List<StatisticData> result = datamartDao.getQuantiQualiDistribution(totalIFTMetadata, pz0Metadata,
                        List.of(groundWorkTypeFilterChoice), cohort, 0, 3);
                Assert.assertEquals(0, result.size());
            }
        }
    }

    @Test
    public void testGetFilteredTypeConduiteOnIFTTotalPerPz0DistributionWithCohortFromYears() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata totalIFTMetadata = datamartDao.getMetadataByColumnName("c601_totalIFT");
            Assert.assertNotNull(totalIFTMetadata);

            Metadata pz0Metadata = datamartDao.getMetadataByColumnName("c102_pz0");
            Assert.assertNotNull(pz0Metadata);

            Metadata managementTypeMetadata = datamartDao.getMetadataByColumnName("c110_managementType");
            Assert.assertNotNull(managementTypeMetadata);

            FilterChoice managementTypeFilterChoice = FilterChoice.fromMetadata(managementTypeMetadata);
            managementTypeFilterChoice.setValues(List.of("Agriculture biologique"));

            FilterCohort cohort = FilterCohort.ofYears(2017, 2018, 2019, 2020);

            {
                List<StatisticData> result = datamartDao.getQuantiQualiDistribution(totalIFTMetadata, pz0Metadata,
                        List.of(managementTypeFilterChoice), cohort,
                        8, 2);
                Assert.assertTrue(result.size() > 0);

                WrappedQuantiQualiDistribution quantiQualiDistribution = new WrappedQuantiQualiDistribution(result);
                Set<String> expectedDephyNbs = Sets.newHashSet(
                        "GCF37082", "LGF35330");
                Assert.assertEquals(expectedDephyNbs, quantiQualiDistribution.dephyNbs);
                {
                    StatisticData data = quantiQualiDistribution.get("Systèmes évoluant au sein du réseau DEPHY");
                    Assert.assertEquals(8, data.getSystemes().size());
                }
                {
                    StatisticData data = quantiQualiDistribution.get("Systèmes avant l'entrée dans le réseau DEPHY");
                    Assert.assertEquals(2, data.getSystemes().size());
                }
            }
            {
                List<StatisticData> result = datamartDao.getQuantiQualiDistribution(totalIFTMetadata, pz0Metadata,
                        List.of(managementTypeFilterChoice), cohort,
                        8, 3);
                Assert.assertEquals(0, result.size());
            }
            {
                List<StatisticData> result = datamartDao.getQuantiQualiDistribution(totalIFTMetadata, pz0Metadata,
                        List.of(managementTypeFilterChoice), cohort,
                        9, 0);
                Assert.assertEquals(0, result.size());
            }
        }
    }

    @Test
    public void testGetIFTTotalPerPz0DistributionWithCohortFromNetworkAges() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata totalIFTMetadata = datamartDao.getMetadataByColumnName("c601_totalIFT");
            Assert.assertNotNull(totalIFTMetadata);

            Metadata pz0Metadata = datamartDao.getMetadataByColumnName("c102_pz0");
            Assert.assertNotNull(pz0Metadata);

            Metadata groundWorkTypeMetadata = datamartDao.getMetadataByColumnName("c201_groundWorkType");
            Assert.assertNotNull(groundWorkTypeMetadata);

            FilterChoice groundWorkTypeFilterChoice = FilterChoice.fromMetadata(groundWorkTypeMetadata);
            groundWorkTypeFilterChoice.setValues(List.of("1 - Labour systématique", "2 - Labour fréquent", "3 - Labour occasionnel"));

            FilterCohort cohort = FilterCohort.ofNetworkAges("État initial + 01 an", "État initial + 02 ans", "État initial + 03 ans");

            List<StatisticData> result = datamartDao.getQuantiQualiDistribution(totalIFTMetadata, pz0Metadata,
                    List.of(groundWorkTypeFilterChoice), cohort,
                    DephyGraphConfig.get().getMinDataRequired(), DephyGraphConfig.get().getMinDataRequiredForCohort());
            Assert.assertTrue(result.size() > 0);

            WrappedQuantiQualiDistribution quantiQualiDistribution = new WrappedQuantiQualiDistribution(result);
            Set<String> expectedDephyNbs = Sets.newHashSet(
                    "GCF36933", "PYF11052", "GCF37303", "GCF31293", "PYF11061", "GCF36180");
            Assert.assertEquals(expectedDephyNbs, quantiQualiDistribution.dephyNbs);

            {
                StatisticData data = quantiQualiDistribution.get("Systèmes avant l'entrée dans le réseau DEPHY");
                Assert.assertEquals(0.42, data.getMin(), 0.001);
                Assert.assertEquals(3.05, data.getMax(), 0.001);
            }
            {
                StatisticData data = quantiQualiDistribution.get("Systèmes évoluant au sein du réseau DEPHY");
                Assert.assertEquals(0.0, data.getMin(), 0.001);
                Assert.assertEquals(4.31, data.getMax(), 0.001);
            }
        }
    }

    public double roundToFourDecimals(double value) {
        return BigDecimal.valueOf(value).setScale(4, RoundingMode.HALF_UP).doubleValue();
    }

    public boolean equalsWithFourDecimalsRounding(CoupleValuesData data, double x, double y) {
        return Double.compare(roundToFourDecimals(data.getAbscissa()), roundToFourDecimals(x)) == 0
                && Double.compare(roundToFourDecimals(data.getOrdinate()), roundToFourDecimals(y)) == 0;
    }

    @Test
    public void testGetQuantiQuantiDistributionWithoutCohort() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata totalIFTMetadata = datamartDao.getMetadataByColumnName("c601_totalIFT");
            Assert.assertNotNull(totalIFTMetadata);
            Metadata sectorMetadata = datamartDao.getMetadataByColumnName("c101_sector");
            Assert.assertNotNull(sectorMetadata);
            Metadata regionMetadata = datamartDao.getMetadataByColumnName("c105_administrativeRegion");
            Assert.assertNotNull(regionMetadata);

            FilterChoice sectorFilterChoice = FilterChoice.fromMetadata(sectorMetadata);
            sectorFilterChoice.setValues(List.of("Maraîchage"));
            FilterChoice regionFilterChoice = FilterChoice.fromMetadata(regionMetadata);
            regionFilterChoice.setValues(List.of("Bretagne"));

            List<CoupleValuesData> result = datamartDao.getQuantiQuantiDistribution(totalIFTMetadata, totalIFTMetadata,
                    List.of(sectorFilterChoice, regionFilterChoice), null);
            Assert.assertEquals(5, result.size());
            {
                boolean present = result.stream().anyMatch(d -> equalsWithFourDecimalsRounding(d, 0.0, 0.0) && d.getWeight() == 6);
                Assert.assertTrue(present);
            }
            {
                double expected = roundToFourDecimals(3.98201098901099);
                boolean present = result.stream().anyMatch(d -> equalsWithFourDecimalsRounding(d, expected, expected) && d.getWeight() == 1);
                Assert.assertTrue(present);
            }
        }
    }

    @Test
    public void testGetQuantiQuantiDistributionWithCohortFromYears() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata totalIFTMetadata = datamartDao.getMetadataByColumnName("c601_totalIFT");
            Assert.assertNotNull(totalIFTMetadata);
            Metadata regionMetadata = datamartDao.getMetadataByColumnName("c105_administrativeRegion");
            Assert.assertNotNull(regionMetadata);

            FilterChoice regionFilterChoice = FilterChoice.fromMetadata(regionMetadata);
            regionFilterChoice.setValues(List.of("Bretagne"));

            FilterCohort cohort = FilterCohort.ofYears(2016, 2017);

            List<CoupleValuesData> result = datamartDao.getQuantiQuantiDistribution(totalIFTMetadata, totalIFTMetadata,
                    List.of(regionFilterChoice), cohort);
            Assert.assertEquals(5, result.size());
            {
                boolean present = result.stream().anyMatch(d -> equalsWithFourDecimalsRounding(d, 0.0, 0.0) && d.getWeight() == 2);
                Assert.assertTrue(present);
            }
            {
                double expected = roundToFourDecimals(2.95211904761905);
                boolean present = result.stream().anyMatch(d -> equalsWithFourDecimalsRounding(d, expected, expected) && d.getWeight() == 1);
                Assert.assertTrue(present);
            }
        }
    }

    @Test
    public void testGetQuantiQuantiDistributionWithCohortFromNetworkAges() throws Exception {
        Jdbi jdbi = getDataSource().getJdbi();
        fillDatabase(jdbi);

        try (Handle handle = jdbi.open()) {
            DatamartDao datamartDao = new DatamartDao(handle);
            Metadata totalIFTMetadata = datamartDao.getMetadataByColumnName("c601_totalIFT");
            Assert.assertNotNull(totalIFTMetadata);
            Metadata regionMetadata = datamartDao.getMetadataByColumnName("c105_administrativeRegion");
            Assert.assertNotNull(regionMetadata);

            FilterChoice regionFilterChoice = FilterChoice.fromMetadata(regionMetadata);
            regionFilterChoice.setValues(List.of("Bretagne"));

            FilterCohort cohort = FilterCohort.ofNetworkAges("État initial", "État initial + 01 an");

            List<CoupleValuesData> result = datamartDao.getQuantiQuantiDistribution(totalIFTMetadata, totalIFTMetadata,
                    List.of(regionFilterChoice), cohort);
            Assert.assertEquals(7, result.size());
            Set<Systeme> systemes = result.stream().flatMap(r -> r.getSystemes().stream()).collect(Collectors.toSet());
            Assert.assertEquals(8, systemes.size());
            {
                boolean present = result.stream().anyMatch(d -> equalsWithFourDecimalsRounding(d, 0.0, 0.0) && d.getWeight() == 2);
                Assert.assertTrue(present);
            }
            {
                double expected = roundToFourDecimals(2.95211904761905);
                boolean present = result.stream().anyMatch(d -> equalsWithFourDecimalsRounding(d, expected, expected) && d.getWeight() == 1);
                Assert.assertTrue(present);
            }
        }
    }

    @Test
    public void testQuantiles() {
        double[] samples = {
                17, 31, 65, 73, 102, 151, 197, 312, 709, 848, 938, 1000, 1070, 1391, 1981, 2001, 2297, 2852
        };
        Set<Long> expectedValues = Sets.newHashSet(
                29L, 85L, 195L, 779L, 1004L, 1745L, 2380L);

        Integer[] fractions = {95, 80, 65, 50, 35, 20, 5};
        Percentile p = new Percentile().withEstimationType(Percentile.EstimationType.R_7);
        Map<Integer, Long> percentiles = Arrays.stream(fractions)
                .collect(Collectors.toMap(
                        Function.identity(),
                        (f) -> Math.round(p.evaluate(samples, f))));
        Assert.assertEquals(expectedValues, Sets.newHashSet(percentiles.values()));
    }
}
