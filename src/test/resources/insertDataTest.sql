---
-- #%L
-- DEPHY-Graph
-- %%
-- Copyright (C) 2018 - 2019 Inra
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Lesser Public License for more details.
-- 
-- You should have received a copy of the GNU General Lesser Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/lgpl-3.0.html>.
-- #L%
---
-- For test, add a new column in DB
ALTER TABLE data ADD COLUMN qualiCampaign TEXT;


INSERT INTO metadata VALUES (
    md5(random()::text || clock_timestamp()::text)::uuid,
    'qualiCampaign',
    'Campagne mode Qualitatif',
    null,
    true,
    false,
    false,
    false,
    ',',
    'general',
    false
    );

-- INSERT INTO data VALUES ('realized', 'dephyNb', validation, 'sector',      'pz0',      'campaign',         'region', 'latitude', 'longitude', 'groundType',                 'groundTexture',  totalIFT,         rootsotck,   industrialFarming, expenses,  qualiCampaign);
INSERT INTO data VALUES ('Synthétisé', 'GCF31915', true,  'Grandes cultures', 'PZ0',      '2014, 2015, 2016', null,     45.72854,   4.9911999,  'Tourbe',                      'Limon-argileux', 1.45308960974216,  null,       false,             1.6     , '2014, 2015, 2016');
INSERT INTO data VALUES ('Synthétisé', 'PYF10215', true,  'Grandes cultures', 'PZ0',      '2011',             null,     48.4921056, 5.3860503,  'Tourbe',                      'Argileux',       8.55783081054688,  null,       false,             10.2    , '2011');
INSERT INTO data VALUES ('Synthétisé', 'PYF10202', true,  'Grandes cultures', 'PZ0',      '2009, 2010, 2011', null,     46.1179379, 6.7349079,  'Tourbe',                      'Limoneux',       2.64801011118107,  null,       false,             1.8     , '2009, 2010, 2011');
INSERT INTO data VALUES ('Synthétisé', 'PYF10467', true,  'Grandes cultures', 'Post-PZ0', '2013, 2014',       null,     53.8324973, 3.9234691,  'Limon battant sain',          'Limon-argileux', null,              null,       false,             2       , '2013, 2014');
INSERT INTO data VALUES ('Synthétisé', 'PYF27458', true,  'Grandes cultures', 'Post-PZ0', '2014',             null,     null,       null,       'Limon argileux/craie',        'Argileux',       0.272543721366674, null,       false,             0.7     , '2014');
INSERT INTO data VALUES ('Réalisé',    'FAK27458', true,  'Grandes cultures', null,       '2014',             null,     null,       null,       'Limon argileux/craie',        'Limoneux',       5.72884214948863,  null,       false,             3.1     , '2014');

INSERT INTO data VALUES ('Synthétisé', 'GCF11163', false, 'Grandes cultures', 'PZ0',      '2010',             null,     89.237575,  10.4353468, 'Champagne et aubue profonde', 'Limon-argileux', 3.09143781638704,  null,       true,              2.1     , '2010');
INSERT INTO data VALUES ('Synthétisé', 'GCF24632', true,  'Grandes cultures', 'PZ0',      '2010, 2011, 2012', null,     73.345865,  3.7571233,  'Limon argileux/craie',        'Argileux',       6.69947671890259,  null,       true,              4.9     , '2010, 2011, 2012');
INSERT INTO data VALUES ('Synthétisé', 'PYF10866', false, 'Grandes cultures', 'Post-PZ0', '2016',             null,     67.45867,   15.458934,  'Limon argileux/craie',        'Limoneux',       null,              null,       true,              null    , '2016');
INSERT INTO data VALUES ('Synthétisé', 'PYF10281', true,  'Grandes cultures', 'PZ0',      '2009, 2010, 2011', null,     56.267963,  2.4679734,  'Champagne et aubue profonde', 'Limon-argileux', 5.24928286112845,  null,       true,              3.2     , '2009, 2010, 2011');
INSERT INTO data VALUES ('Synthétisé', 'GCF39532', false, 'Grandes cultures', 'Post-PZ0', '2013, 2014, 2016', null,     43.67658,   1.6586343,  'Marais non calcaire',         'Argileux',       null,              null,       true,              2.7     , '2013, 2014, 2016');
INSERT INTO data VALUES ('Synthétisé', 'PYF10374', true,  'Grandes cultures', 'PZ0',      '2011',             null,     82.436862,  2.543586,   'Limon argileux/craie',        'Limoneux',       5.67272910103202,  null,       true,              3.0     , '2011');
INSERT INTO data VALUES ('Synthétisé', 'PYF10234', false, 'Grandes cultures', 'PZ0',      '2009, 2010, 2011', null,     41.4568790, 5.8673323,  'Champagne et aubue profonde', 'Limon-argileux', 2.59181213378906,  null,       true,              1.9     , '2009, 2010, 2011');
INSERT INTO data VALUES ('Synthétisé', 'PYF10297', true,  'Grandes cultures', 'PZ0',      '2009, 2010, 2011', null,     58.6564232, 6.3235852,  'Limon battant sain',          'Argileux',       0.9528256226331,   null,       null,              0.1     , '2009, 2010, 2011');
INSERT INTO data VALUES ('Synthétisé', 'PYF10574', true,  'Grandes cultures', 'Post-PZ0', '2015',             null,     null,       null,       'tourbe',                      'Limoneux',       null,              null,       null,              null    , '2015');
INSERT INTO data VALUES ('Synthétisé', 'GCF26982', false, 'Grandes cultures', 'Post-PZ0', '2011',             null,     null,       null,       'Limon battant sain',          'Limon-argileux', null,              null,       null,              null    , '2011');
INSERT INTO data VALUES ('Synthétisé', 'PYF10953', true,  'Grandes cultures', 'PZ0',      '2009, 2010, 2011', null,     null,       null,       'tourbe',                      'Argileux',       1.15542670711875,  null,       null,              0       , '2009, 2010, 2011');
