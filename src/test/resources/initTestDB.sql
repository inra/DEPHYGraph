---
-- #%L
-- DEPHY-Graph
-- %%
-- Copyright (C) 2018 - 2019 Inra
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Lesser Public License for more details.
-- 
-- You should have received a copy of the GNU General Lesser Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/lgpl-3.0.html>.
-- #L%
---
-- Database definition

-- Tables
CREATE TABLE IF NOT EXISTS metadata(
    id              UUID        PRIMARY KEY,
    columnName      TEXT        UNIQUE NOT NULL,
    displayName     TEXT        NOT NULL,
    unity           TEXT,
    qualitative     BOOLEAN,
    quantitative    BOOLEAN,
    filter          BOOLEAN,
    irfilter        BOOLEAN,
    separator       TEXT,
    category        TEXT,
    hidden          BOOLEAN,
    recommendedQuery BOOLEAN DEFAULT FALSE
);

-- TOTHINK
-- GRANT SELECT, INSERT, UPDATE, DELETE ON metadata TO dephygraph;

CREATE TABLE IF NOT EXISTS data(
    realized            TEXT,
    dephyNb             TEXT,
    validation          boolean,
    sector              TEXT,
    pz0                 TEXT,
    campaign            TEXT,
    region              TEXT,
    latitude            decimal,
    longitude           decimal,
    groundType          TEXT,
    groundTexture       TEXT,
    totalIFT            decimal,
    rootstock           TEXT,
    industrialFarming   boolean,
    expenses            decimal

);

-- insert metadata corresponding to data column ...
--
-- realized     | Réalisé/Synthétisé    | no unity  | qualitative   | no quantitative   | filter      | not filter | no separator | general | hidden
-- dephyNb      | N° Dephy              | no unity  | qualitative   | no quantitative   | filter      | not filter | no separator | general | hidden
-- validation   | Validation            | no unity  | qualitative   | no quantitative   | filter      | not filter | no separator | general | hidden
-- sector       | Filière               | no unity  | qualitative   | no quantitative   | filter      | not filter | no separator | general | not hidden
-- pz0          | PZ0                   | no unity  | qualitative   | no quantitative   | filter      | not filter | no separator | general | not hidden
-- campaign     | Campagne              | no unity  | no qualitative| quantitative      | filter      | not filter | ','          | general | not hidden
-- region       | Région                | no unity  | qualitative   | no quantitative   | filter      | not filter |              | context | not hidden
-- latitude     | Latitute              | no unity  | no qualitative| quantitative      | filter      | not filter |              | context | hidden
-- longitude    | Longitude             | no unity  | no qualitative| quantitative      | filter      | not filter |              | context | hidden
-- groundType   | Type de sol           | no unity  | qualitative   | no quantitative   | filter      | not filter |              | context | not hidden
-- groundTexture| Texture de sol        | no unity  | qualitative   | no quantitative   | filter      | not filter |              | context | not hidden
-- totalIFT     | IFT total (hors...)   | no unity  | no qualitative| quantitative      | not filter  | not filter |              | ift     | not hidden
-- rootstock    | Porte-Greffe          | no unity  | qualitative   | no quantitative   | filter      | not filter | ','          | strategy| not hidden

INSERT INTO metadata VALUES (md5(random()::text || clock_timestamp()::text)::uuid,   'realized',      'Réalisé/Synthétisé',                    null, true,   false,  true,  false,  null, 'general',  true  );

INSERT INTO metadata VALUES (md5(random()::text || clock_timestamp()::text)::uuid,   'dephyNb',       'N° Dephy',                              null, true,   false,  true,  false,  null, 'general',  true  );

INSERT INTO metadata VALUES (md5(random()::text || clock_timestamp()::text)::uuid,   'validation',    'Validation',                            null, true,   false,  true,  false,  null, 'general',  true  );

INSERT INTO metadata VALUES (md5(random()::text || clock_timestamp()::text)::uuid,   'sector',        'Filière',                               null, true,   false,  true,  false,  null, 'general',  false, true );

INSERT INTO metadata VALUES (md5(random()::text || clock_timestamp()::text)::uuid,   'pz0',           'PZ0',                                   null, true,   false,  true,  false,  null, 'general',  false );

INSERT INTO metadata VALUES (md5(random()::text || clock_timestamp()::text)::uuid,   'campaign',      'Campagne',                              null, false,  true,   true,  false,  ',',  'general',  false, true );

INSERT INTO metadata VALUES (md5(random()::text || clock_timestamp()::text)::uuid,   'region',        'Région',                                null, true,   false,  true,  false,  null, 'context',  false );

INSERT INTO metadata VALUES (md5(random()::text || clock_timestamp()::text)::uuid,   'latitude',      'Latitude',                              null, false,  true,   true,  false,  null, 'context',  true  );

INSERT INTO metadata VALUES (md5(random()::text || clock_timestamp()::text)::uuid,   'longitude',     'Longitude',                             null, false,  true,   true,  false,  null, 'context',  true  );

INSERT INTO metadata VALUES (md5(random()::text || clock_timestamp()::text)::uuid,   'groundType',    'Type de sol',                           null, true,   false,  true,  false,  null, 'context',  false );

INSERT INTO metadata VALUES (md5(random()::text || clock_timestamp()::text)::uuid,   'groundTexture', 'Texture de sol',                        null, true,   false,  true,  false,  null, 'context',  false );

INSERT INTO metadata VALUES (md5(random()::text || clock_timestamp()::text)::uuid,   'totalIFT',      'IFT total (hors biocontrôle, avec TS)', null, false,  true,   false, false,  null, 'ift',      false );

INSERT INTO metadata VALUES (md5(random()::text || clock_timestamp()::text)::uuid,   'rootstock',     'Porte-Greffe',                          null, true,   false,  true,  false,  ',',  'strategy', false );

INSERT INTO metadata VALUES (md5(random()::text || clock_timestamp()::text)::uuid,   'expenses',      'Charges',                               null, false,  true,   false, false,  null, 'expenses', false );

-- Miscellaneous table, used to contain other data, like last extraction date to make datastore.
CREATE TABLE IF NOT EXISTS miscellaneous(
    id      UUID        PRIMARY KEY,
    key     TEXT        UNIQUE NOT NULL,
    data    TEXT
);

INSERT INTO miscellaneous(id, key, data) VALUES (md5(random()::text || clock_timestamp()::text)::uuid, 'lastAgrosystExtractionDate', 'Mars 2018') ON CONFLICT (key) DO UPDATE SET data = EXCLUDED.data;

-- AgrosystInfo structure
CREATE TABLE IF NOT EXISTS agrosystInfo(
    id              UUID        PRIMARY KEY,
    dephyNb         TEXT[],
    expirationDate  DATE        NOT NULL
);
