CREATE TABLE metadata (
	id uuid NOT NULL,
	columnname text NOT NULL,
	displayname text NOT NULL,
	unity text NULL,
	qualitative bool NULL,
	quantitative bool NULL,
	separator text NULL,
	"filter" bool NULL,
	irfilter bool NULL,
	hidden bool NULL,
	category text NULL,
	recommendedquery bool NULL,
	availabilitybysector text NULL,
	recommendedfilter bool NOT NULL DEFAULT false,
	CONSTRAINT metadata_columnname_key UNIQUE (columnname),
	CONSTRAINT metadata_pkey PRIMARY KEY (id)
);

INSERT INTO metadata (id,columnname,displayname,unity,qualitative,quantitative,separator,"filter",irfilter,hidden,category,recommendedquery,availabilitybysector,recommendedfilter) VALUES
	 ('b55a5a19-9020-3de9-cd46-4ba389f5a09f','realized','Réalisé ou Synthétisé',NULL,true,false,NULL,true,true,true,'01-Contexte',false,'ALL',false),
	 ('5aaa7ffc-9725-3c63-8481-6378feee1e7e','dephyNb','N° Dephy',NULL,true,false,NULL,true,false,true,'01-Contexte',false,'ALL',false),
	 ('2507949c-0194-dd70-b9d7-6c21589e8473','latitude','Latitude',NULL,false,true,NULL,true,false,true,'01-Contexte',false,'ALL',false),
	 ('483af5a7-3f37-1f50-54a8-45b3925c0d37','longitude','Longitude',NULL,false,true,NULL,true,false,true,'01-Contexte',false,'ALL',false),
	 ('7ea3f9b5-5f5e-c850-c5d7-56e491deaec0','validation','Validation',NULL,true,false,NULL,true,false,true,'01-Contexte',false,'ALL',false),
	 ('c935dbbd-8ef0-8885-8fc0-39d8abc636a6','c102_pz0','Ancienneté dans le réseau',NULL,true,false,NULL,true,false,false,'01-Contexte',false,'ALL',false),
	 ('8ef582b7-36a6-1ef0-c10a-663e6f6be56e','c103_networkYears','Nombre d''années dans le réseau',NULL,true,false,NULL,true,false,false,'01-Contexte',false,'ALL',false),
	 ('2a38b6dd-2cb0-678d-8152-61aa5a0eabc5','c106_regionPre2015','Région pré-2015',NULL,true,false,NULL,true,false,false,'01-Contexte',false,'ALL',false),
	 ('75d1e40b-4be4-7a32-a572-4e6d88d35f1d','c107_departement','Département',NULL,true,false,NULL,true,false,false,'01-Contexte',false,'ALL',false),
	 ('e89f0582-5228-5515-1b7a-2c1b01bd532a','c111_species','Espèces pérennes',NULL,true,false,',',true,false,false,'01-Contexte',false,'Arboriculture',false);
INSERT INTO metadata (id,columnname,displayname,unity,qualitative,quantitative,separator,"filter",irfilter,hidden,category,recommendedquery,availabilitybysector,recommendedfilter) VALUES
	 ('d0255650-07f8-585a-ad0e-bd44893f6509','c112_variety','Variété et Cépage',NULL,true,false,',',true,false,false,'01-Contexte',false,'Arboriculture, Viticulture',false),
	 ('a1cd02c6-073c-766c-2328-62710e80f759','c120_arboriculture_typo_sdc','Typologie de sytème Arboriculture',NULL,true,false,NULL,true,false,false,'01-Contexte',false,'Arboriculture',false),
	 ('8d5429a8-2e6c-c2c0-3d79-ea84bf9cfe62','c121_maraichage_typo_sdc','Typologie de sytème Maraîchage',NULL,true,false,NULL,true,false,false,'01-Contexte',false,'Maraîchage',false),
	 ('171dd929-45d2-27b2-b820-96caf05610f0','c122_horticulture_typo_sdc','Typologie de sytème Horticulture',NULL,true,false,NULL,true,false,false,'01-Contexte',false,'Horticulture',false),
	 ('8baca313-ee7a-da03-999b-14c73cb0bc91','c123_cult_tropicales_typo_sdc','Typologie de sytème Cultures tropicales',NULL,true,false,NULL,true,false,false,'01-Contexte',false,'Cultures tropicales',false),
	 ('0404ddbf-4c81-10b1-de69-94d8f16737ae','c201_groundWorkType','Type de travail du sol',NULL,true,false,NULL,true,false,false,'02-Stratégie Agronomique',false,'Grandes cultures, Polyculture-élevage',false),
	 ('80f1d690-5f05-cff7-ac0d-fcd9694ab44d','c203_mechanicalWeeding','Désherbage mécanique',NULL,true,false,NULL,true,false,false,'02-Stratégie Agronomique',false,'Grandes cultures, Polyculture-élevage',false),
	 ('bda967d7-c153-4491-151b-bacda4d08e73','c204_mechanicalWeedingInterventionFrequency','Fréquence d''intervention de désherbage mécanique','nombre de passages/ha/an',false,true,NULL,false,false,false,'02-Stratégie Agronomique',false,'Grandes cultures, Polyculture-élevage',false),
	 ('51741967-313b-abe9-6a86-cfae1ed8848a','c301_workingTime','Temps de travail','h/ha/an',false,true,NULL,false,false,false,'03-Consommation',false,'ALL',false),
	 ('22edaa84-0c30-f18b-7d11-5a95f02914fe','c302_mechanizationTime','Temps d''utilisation du matériel','h/ha/an',false,true,NULL,false,false,false,'03-Consommation',false,'ALL',false);
INSERT INTO metadata (id,columnname,displayname,unity,qualitative,quantitative,separator,"filter",irfilter,hidden,category,recommendedquery,availabilitybysector,recommendedfilter) VALUES
	 ('20e18353-dc8a-b108-af2f-a038d71cc837','c303_labourTime','Temps d''interventions manuelles','h/ha/an',false,true,NULL,false,false,false,'03-Consommation',false,'Arboriculture, Viticulture, Cultures tropicales, Maraîchage',false),
	 ('65e094a3-1bfc-5a90-f126-454da85e2ada','c304_fuelConsumption','Consommation de carburant','L/ha/an',false,true,NULL,false,false,false,'03-Consommation',false,'ALL',false),
	 ('aa7ee6d6-3cf7-8846-02c2-f2b07a31eb0c','c401_fertilizationUnityN','Unités N','unités/ha/an',false,true,NULL,false,false,false,'04-Fertilisation',false,'ALL',false),
	 ('a1fdb895-a2dd-8d9b-086a-ba16b7e07473','c402_fertilizationMineralUnityN','Unités N minéral','unités/ha/an',false,true,NULL,false,false,false,'04-Fertilisation',false,'ALL',false),
	 ('768ae853-762a-b5be-2849-ba0da4bdeba4','c403_fertilizationOrganicUnityN','Unités N organique','unités/ha/an',false,true,NULL,false,false,false,'04-Fertilisation',false,'ALL',false),
	 ('e91f55a7-9b72-5ad7-f3b0-33d992cf1cc2','c404_fertilizationUnityP','Unités P','unités/ha/an',false,true,NULL,false,false,false,'04-Fertilisation',false,'ALL',false),
	 ('143fe119-808d-1cc0-2553-4bb52b3c05af','c405_fertilizationMineralUnityP','Unités P minéral','unités/ha/an',false,true,NULL,false,false,false,'04-Fertilisation',false,'ALL',false),
	 ('a2c64dd3-7c2f-54a2-b5bf-c1f8d816dd14','c406_fertilizationOrganicUnityP','Unités P organique','unités/ha/an',false,true,NULL,false,false,false,'04-Fertilisation',false,'ALL',false),
	 ('4ae86773-c9cc-285f-36c5-541762d41dd5','c407_fertilizationUnityK','Unités K','unités/ha/an',false,true,NULL,false,false,false,'04-Fertilisation',false,'ALL',false),
	 ('b46c42db-f6f3-6735-cb5e-67fa667af19a','c408_fertilizationMineralUnityK','Unités K minéral','unités/ha/an',false,true,NULL,false,false,false,'04-Fertilisation',false,'ALL',false);
INSERT INTO metadata (id,columnname,displayname,unity,qualitative,quantitative,separator,"filter",irfilter,hidden,category,recommendedquery,availabilitybysector,recommendedfilter) VALUES
	 ('083ea866-2919-1c16-1bf6-f2e6ac97709a','c409_fertilizationOrganicUnityK','Unités K organique','unités/ha/an',false,true,NULL,false,false,false,'04-Fertilisation',false,'ALL',false),
	 ('df348fa1-c383-341f-6128-316bf7252766','c501_grossProceeds','Produit brut','€/ha/an',false,true,NULL,false,false,false,'05-Performances Économiques',false,'Grandes cultures, Polyculture-élevage',false),
	 ('ad2b8af6-c38c-c5f5-19f9-4a31b79b27e3','c502_grossProfit','Marge brute','€/ha/an',false,true,NULL,false,false,false,'05-Performances Économiques',false,'Grandes cultures, Polyculture-élevage',false),
	 ('c770f14b-555d-0203-0087-a4ba89c4c31a','c503_semiNetMargin','Marge semi-nette','€/ha/an',false,true,NULL,false,false,false,'05-Performances Économiques',false,'Grandes cultures, Polyculture-élevage',false),
	 ('c29c96f2-fe4f-4ec4-5b13-fe49a802b19e','c504_outLabourTotalExpenses','Charges Totales (hors main d''oeuvre)','€/ha/an',false,true,NULL,false,false,false,'05-Performances Économiques',false,'ALL',false),
	 ('f0f7cc59-2c17-e31f-92a1-8c5ab513112e','c505_operatingExpenses','Charges Opérationelles','€/ha/an',false,true,NULL,false,false,false,'05-Performances Économiques',false,'ALL',false),
	 ('db504f56-72db-f40d-7ee8-d50b24648e01','c506_labourExpenses','Charges Main d''oeuvre','€/ha/an',false,true,NULL,false,false,false,'05-Performances Économiques',false,'ALL',false),
	 ('c9edf581-35a5-e080-9d34-77fbd8ff917c','c507_mechanizationExpenses','Charges Mécanisation','€/ha/an',false,true,NULL,false,false,false,'05-Performances Économiques',false,'ALL',false),
	 ('33568b63-154e-90a9-dcce-9f3ee1bf76ba','c601_totalIFT','IFT Total (hors biocontrôle, hors TS)',NULL,false,true,NULL,false,false,false,'06-Performances d''IFT',true,'ALL',false),
	 ('583c7880-be15-84b9-c7d0-0f5ca9b5e787','c602_IFT_hh_hts','IFT (hors herbicides, hors TS)',NULL,false,true,NULL,false,false,false,'06-Performances d''IFT',false,'ALL',false);
INSERT INTO metadata (id,columnname,displayname,unity,qualitative,quantitative,separator,"filter",irfilter,hidden,category,recommendedquery,availabilitybysector,recommendedfilter) VALUES
	 ('c41fcb88-00a5-28bd-ff63-e0f08d71bc0a','c603_biocontrolIFT','IFT Biocontrôle',NULL,false,true,NULL,false,false,false,'06-Performances d''IFT',false,'ALL',false),
	 ('cdd0fe8c-4618-1e50-f17d-7de4ea7f2aae','c605_herbicideIFT','IFT Herbicides',NULL,false,true,NULL,false,false,false,'06-Performances d''IFT',false,'ALL',false),
	 ('49daac9c-4a9f-98e7-cc4f-8449a731c632','c607_insecticideIFT','IFT Insecticides',NULL,false,true,NULL,false,false,false,'06-Performances d''IFT',false,'ALL',false),
	 ('1872b2f0-cda0-4643-1eae-e4f12fd411ba','c608_fungicideIFT','IFT Fongicides',NULL,false,true,NULL,false,false,false,'06-Performances d''IFT',false,'ALL',false),
	 ('3a7d637f-7545-f8f5-8f49-0459579ced55','c609_otherIFT','IFT Autres',NULL,false,true,NULL,false,false,false,'06-Performances d''IFT',false,'ALL',false),
	 ('c8bd9415-344e-f922-e253-595af0d9be50','c610_biologicalWaysSolution','Recours aux moyens biologiques',NULL,false,true,NULL,false,false,false,'06-Performances d''IFT',false,'ALL',false),
	 ('da4b8370-6316-fae5-9504-cc26b00f8970','c101_sector','Filière',NULL,true,false,NULL,true,false,false,'01-Contexte',true,'ALL',true),
	 ('27f09cea-a909-dec1-f71a-a6ea7444465c','c104_campaign_dis','Campagne(s)',NULL,true,false,',',true,false,false,'01-Contexte',true,'ALL',true),
	 ('69a76497-2773-b98a-59eb-a51b275447ca','c105_administrativeRegion','Région Administrative',NULL,true,false,NULL,true,false,false,'01-Contexte',false,'ALL',true),
	 ('0be26db2-be5f-ad11-49e7-e63ef1d18b25','c110_managementType','Type de conduite',NULL,true,false,NULL,true,false,false,'01-Contexte',false,'ALL',true);
