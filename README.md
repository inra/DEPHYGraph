# Description

Le réseau **DEPHY** utilise **Agrosyst** pour décrire les pratiques des agriculteurs du réseau.

L'application *DEPHYGraph* vise à fournir une interface graphique interactive pour les données du réseau **DEPHY**.
Les objectifs principaux sont de :
- permettre aux utilisateurs de **Agrosyst** d'avoir un retour sur les données saisies ;
- offrir une vision plus globale aux acteurs extérieurs, tel que le Ministère de l'agriculture.

## Fonctionnement

Le fonctionnement se veut simple : l'utilisateur choisit une variable (qualitative ou quantitative) dont il souhaite obtenir des statistiques (respectivement répartition ou caculs statistiques sur les valeurs comme le median ou les ntiles).
Il peut mettre en avant les variances de ses données en fonction d'une variable qualitative.
Il est également possible de filtrer les données en fonction de différents critères.

# Installation

## Préparation de la base de données

L'application est conçue pour fonctionner en s'appuyant sur une base de données postgresql en exploitant au maximum les possibilités offertes par PostgreSQL.

* créer la base de données `PostgreSQL` (nommage par défaut : `dephygraph`)
* créer les tables `data`, `metadata`, `miscellaneous`, `agrosystInfo` avec la dernière structure en date indiquée dans le fichier `src/main/resources/initDb.sql`
  * __ATTENTION__ à penser à bien accorder les droits à l'utilisateur (ici `dephygraph`) sur les tables :

  ```
  GRANT SELECT, INSERT, UPDATE, DELETE ON data TO dephygraph;
  GRANT SELECT, INSERT, UPDATE, DELETE ON metadata TO dephygraph;
  GRANT SELECT, INSERT, UPDATE, DELETE ON miscellaneous TO dephygraph;
  GRANT SELECT, INSERT, UPDATE, DELETE ON agrosystInfo TO dephygraph;
  ```

**Mise à jour de la base de données** : 

Pré-requis installer le logiciel DBeaver https://dbeaver.io/

Se connecter à la base de données de DEPHY_GRAPH en utilisant DBeaver
- Supprimer les tables 'metadata' et 'data' existantes
- Importer le script de création des structures de tables pour 'metadata' et 'data'
  - Aller sur le nom de la BD → Editeur SQL → Script SQL → Copier/coller le contenu de metadata.sql dans la fenêtre de script et cliquer sur le bouton d'exécution du script (3ème bouton ![img.png](dbeaver-run-script-button.png))
- Importer les données de la table 'data':
  - clic droit sur 'Schema → public → Tables → data' → importer des données → CSV → Sélectionner le fichier CSV → Dans les propriétés, pour lapropriété ``NULL value mark`` mettre: NA → déplier la ligne principale pour voir le détail et vérifier le mapping, chaque colonne source doit être indiquée comme 'existing' → Suivant → Suivant → Commencer
- Mettre à jour la table miscellaneous avec la période des données
  - ``UPDATE miscellaneous SET data = 'Juillet 2022' WHERE key = 'lastAgrosystExtractionDate';``

## Installation de l'application

L'application consiste en un WAR à déployer via un conteneur web tel que __Tomcat__.
Elle s'accompagne d'un fichier de configuration à déposer dans `/etc/dephygraph.properties`.

Le livrable de la dernière version est téléchargeable [ici](https://gitlab.nuiton.org/inra/DEPHYGraph/-/jobs/artifacts/dephy-graph-1.0.2/download?job=build%3Atag)

### Configuration de l'application

Le fichier `/etc/dephygraph.properties` doit contenir au minimum la configuration d'accès à la base de données, par l'intermédiaire de trois variables pour la localisation de la base de données, le nom d'utilisateur et son mot de passe.
  ```
dephygraph.datasource.url=jdbc:postgresql://localhost:5432/dephygraph
dephygraph.datasource.user=dephygraph
dephygraph.datasource.password=your_P@ssw0rd
  ```

Dans le but de renforcer l'anonymisation, il est également possible de configurer le nombre minimal de résultat à afficher (en dessous de ce nombre, aucun résultat ne sera retourné par l'application) :
  ```
dephygraph.minDataRequired=10
  ```

La configuration des logs est externalisable, dans le format de [LOGBack](https://logback.qos.ch/manual/index.html), en précisant le fichier de configuration des logs :
```
dephygraph.logConfigLocation=/var/local/dephygraph/logback.xml
```

Afin de garantir la communication avec Agrosyst, un token ([JWT](https://jwt.io/)) est utilisé dans le frontend, et nécessite la configuration d'une passphrase :
```
dephygraph.webSecurityKey=WelcomeAgrosystUserAndLookYourData
```

Personnalisation du numéro de version de DEPHYgraph. Si l'on souhaite personnaliser le numéro de version affiché dans l'interface il est possible de surcharger le paramètre :
```
dephygraph.version
```

# Prérequis Développeur

- Maven 3
- Java 17
- npm

## Prérequis Base de données

* base de données `PostgreSQL` (nommage par défaut : `dephygraph`)
* renseigner les informations de connexion dans le fichier `~/.config/dephygraph.properties` (exemple de configuration dans `src/main/resources`)
* créer les tables `data`, `metadata`, `miscellaneous` et `agrosystInfo` avec la structure indiquée dans le fichier `src/main/resources/initDB.sql` (ou la dernière structure en date indiquée dans le fichier `src/main/resources/migration/20181212-database.sql`)
  * __ATTENTION__ à penser à bien accorder les droits à l'utilisateur sur les tables :
  ```
  GRANT SELECT, INSERT, UPDATE, DELETE ON data TO dephygraph;
  GRANT SELECT, INSERT, UPDATE, DELETE ON metadata TO dephygraph;
  GRANT SELECT, INSERT, UPDATE, DELETE ON miscellaneous TO dephygraph;
  GRANT SELECT, INSERT, UPDATE, DELETE ON agrosystInfo TO dephygraph;
  ```

## Démarrer l'application

### Démarrer le backend

* `mvn jetty:run`

Le backend tourne par défaut sur http://localhost:8084/dephy-graph/

### Démarrer le frontend

* `cd src/main/webclient/dephygraph`
* `cp .env.production .env`, ouvrir le fichier nouvellement créé et mettre l'URL du backend dans la clé `VITE_APP_SERVER_URL`
* `npm i`
* `npm run dev`

Le frontend est accessible à l'adresse indiquée dans la console.

## Schema de la Base de Données et fonctionnement

### Data
Les données brutes sont stockées dans une seule table de la base de données nommée `data`.
Certaines colonnes sont des données internes privées, alors que d'autres correspondent aux variables de requêtage de l'application.
Chaque ligne/entrée dans cette table correspond à un système de culture.

### Metadata
Afin de pouvoir traiter au mieux ces données au sein de l'application, une seconde table permet de spécifier le comportement de chaque colonne de la table `data` en tant que variable, au travers des colonnes suivantes :

|   Colonne    |  Type   |  nullable  | description de la colonne                                                                                           |
|:------------:|:-------:|:----------:|---------------------------------------------------------------------------------------------------------------------|
| id           | uuid    |  not null  | un simple identifiant de ligne                                                                                      |
| columnname   | text    |  not null  | nom de la colonne dans la table `data` correspondant à la variable                                                  |
| displayname  | text    |  not null  | nom à afficher pour la variable dans l'application                                                                  |
| unity        | text    |            | unité de la variable pour affichage dans l'application (ex : `L/ha`)                                                |
| qualitative  | boolean |            | permet de définir si la variable est une données qualitative                                                        |
| quantitative | boolean |            | permet de définir si la variable est une données quantitative                                                       |
| filter       | boolean |            | permet de définir si la variable peut être utilisée en tant que filtre                                              |
| separator    | text    |            | Si la variable est à données multiples, définit le caractère de séparation (null si la variable n'est pas multiple) |
| hidden       | boolean |            | Permet de savoir si la variable nécessite une authentification pour être vue                                        |


C'est donc à partir de cette table `metadata` que l'on récupère la liste des variables pouvant être proposées à l'utilisateur sur l'application.
Elle permet également d'effectuer les recherches et croisements de données de façon convenable (en gérant les cas de multiples valeurs notamment).

### Miscellaneous

Afin de contenir des informations périphériques à l'application, une table `miscellaneous` a été ajoutée, de simple format `clef/valeur`. Elle sert actuellement à conserver la date d'extraction des données depuis Agrosyst.

  ```
  INSERT INTO miscellaneous(id, key, data) VALUES (md5(random()::text || clock_timestamp()::text)::uuid, 'lastAgrosystExtractionDate', 'Mars 2018') ON CONFLICT (key) DO UPDATE SET data = EXCLUDED.data;
  ```

### AgrosystInfo

Avec la connexion depuis Agrosyst, il est nécessaire de conserver les informations de l'utilisateur connecté.

La connexion depuis Agrosyst va créer un identifiant d'utilisateur propre à DEPHYGraph, permettant de ne pas conserver une information personnelle provenant d'Agrosyst,
et lui affecter la liste des numéros DEPHY auxquels il a accès sur Agrosyst.

La durée de cette session est limitée à 24h, après lesquelles elle pourra alors être supprimée automatiquement par le système.

|   Colonne      |  Type   |  nullable  | description de la colonne                                                                                           |
|:--------------:|:-------:|:----------:|---------------------------------------------------------------------------------------------------------------------|
| id             | uuid    |  not null  | un identifiant d'utilisateur (qui **n'est pas** l'identifiant Agrosyst), en lien avec la session utilisateur        |
| dephynb        | text[]  |            | liste des dephyNb auxquels l'utilisateur a accès sur Agrosyst et qu'il pourra utiliser sur DEPHYGraph               |
| expirationdate | date    |  not null  | date d'expiration de la session utilisateur et à partir de laquelle cette entrée en base peut être effacée          |

### Exemple concernant le lien `data` et `metadata`

Considérons le tableau de données suivant ( correspondant à la table `data` donc):

|   realized   |   dephyNb     | validation |      sector        |     pz0      |      campaign      | region | latitude | longitude | groundType                    | groundTexture    | totalIFT,         |
|:------------:|:-------------:|:----------:|:------------------:|:------------:|--------------------|--------|----------|-----------|:-----------------------------:|:----------------:|-------------------|
| 'Synthétisé' | 'dephnyNb01'  |  true      | 'Grandes cultures' |   'PZ0'      | '2014, 2015, 2016' | null   | null     | null      | 'Tourbe'                      | 'Limon-argileux' | 1.45308960974216  |
| 'Synthétisé' | 'dephnyNb02'  |  true      | 'Grandes cultures' |   'PZ0'      | '2011'             | null   | null     | null      | 'Tourbe'                      | 'Argileux',      | 8.55783081054688  |
| 'Synthétisé' | 'dephnyNb03'  |  true      | 'Grandes cultures' |   'PZ0'      | '2009, 2010, 2011' | null   | null     | null      | 'Tourbe'                      | 'Limoneux',      | 2.64801011118107  |
| 'Synthétisé' | 'dephnyNb04'  |  true      | 'Grandes cultures' |   'Post-PZ0' | '2013, 2014'       | null   | null     | null      | 'Limon battant sain'          | 'Limon-argileux' | null              |
| 'Synthétisé' | 'dephnyNb05'  |  true      | 'Grandes cultures' |   'Post-PZ0' | '2014'             | null   | null     | null      | 'Limon argileux/craie'        | 'Argileux',      | 0.272543721366674 |
| 'Réalisé'    | 'dephnyNb06'  |  true      | 'Grandes cultures' |   'null'     | '2014'             | null   | null     | null      | 'Limon argileux/craie'        | 'Limoneux',      | 5.72884214948863  |
| 'Synthétisé' | 'dephnyNb07'  |  false     | 'Grandes cultures' |   'PZ0'      | '2010'             | null   | null     | null      | 'Champagne et aubue profonde' | 'Limon-argileux' | 3.09143781638704  |
| 'Synthétisé' | 'dephnyNb08'  |  true      | 'Grandes cultures' |   'PZ0'      | '2010, 2011, 2012' | null   | null     | null      | 'Limon argileux/craie'        | 'Argileux',      | 6.69947671890259  |
| 'Synthétisé' | 'dephnyNb09'  |  false     | 'Grandes cultures' |   'Post-PZ0' | '2016'             | null   | null     | null      | 'Limon argileux/craie'        | 'Limoneux',      | null              |
| 'Synthétisé' | 'dephnyNb10'  |  true      | 'Grandes cultures' |   'PZ0'      | '2009, 2010, 2011' | null   | null     | null      | 'Champagne et aubue profonde' | 'Limon-argileux' | 5.24928286112845  |
| 'Synthétisé' | 'dephnyNb11'  |  false     | 'Grandes cultures' |   'Post-PZ0' | '2013, 2014, 2016' | null   | null     | null      | 'Marais non calcaire'         | 'Argileux',      | null              |
| 'Synthétisé' | 'dephnyNb12'  |  true      | 'Grandes cultures' |   'PZ0'      | '2011'             | null   | null     | null      | 'Limon argileux/craie'        | 'Limoneux',      | 5.67272910103202  |
| 'Synthétisé' | 'dephnyNb13'  |  false     | 'Grandes cultures' |   'PZ0'      | '2009, 2010, 2011' | null   | null     | null      | 'Champagne et aubue profonde' | 'Limon-argileux' | 2.59181213378906  |
| 'Synthétisé' | 'dephnyNb14'  |  true      | 'Grandes cultures' |   'PZ0'      | '2009, 2010, 2011' | null   | null     | null      | 'Limon battant sain'          | 'Argileux',      | 0.9528256226331   |
| 'Synthétisé' | 'dephnyNb15'  |  true      | 'Grandes cultures' |   'Post-PZ0' | '2015'             | null   | null     | null      | 'tourbe'                      | 'Limoneux',      | null              |
| 'Synthétisé' | 'dephnyNb16'  |  false     | 'Grandes cultures' |   'Post-PZ0' | '2011'             | null   | null     | null      | 'Limon battant sain'          | 'Limon-argileux' | null              |
| 'Synthétisé' | 'dephnyNb17'  |  true      | 'Grandes cultures' |   'PZ0'      | '2009, 2010, 2011' | null   | null     | null      | 'tourbe'                      | 'Argileux',      | 1.15542670711875  |

Avec la table metadata suivante :

|   columnname    |      displayname       | unity | qualitative | quantitative | filter | separator | hidden |
|:---------------:|:----------------------:|:-----:|:-----------:|:------------:|:------:|:---------:|:------:|
| 'realized'      | 'Réalisé/Synthétisé'   | null  | true        | false        | true   | null      | true   |
| 'dephyNb'       | 'N° Dephy'             | null  | true        | false        | true   | null      | true   |
| 'validation'    | 'Validation'           | null  | true        | false        | true   | null      | true   |
| 'sector'        | 'Filière'              | null  | true        | false        | true   | null      | false  |
| 'pz0'           | 'PZ0'                  | null  | true        | false        | true   | null      | false  |
| 'campaign'      | 'Campagne'             | null  | true        | false        | true   | ','       | false  |
| 'region'        | 'Région'               | null  | true        | false        | true   | null      | false  |
| 'latitude'      | 'Latitute'             | null  | false       | true         | true   | null      | true   |
| 'longitude'     | 'Longitude'            | null  | false       | true         | true   | null      | true   |
| 'groundType'    | 'Type de sol'          | null  | true        | false        | true   | null      | false  |
| 'groundTexture' | 'Texture de sol'       | null  | true        | false        | true   | null      | false  |
| 'totalIFT'      | 'IFT total (hors...)'  | null  | false       | true         | false  | null      | false  |


Dans l'interface d'application, seront donc affichées les variables qualitatives suivantes :
* Filière,
* PZ0,
* Région,
* Type de sol,
* Texture de sol,

ainsi que les variables quantitatives suivantes :
* Campagne,
* totalIFT.

Les filtres proposés seront les différentes valeurs pour les variables suivantes :
* Filière,
* PZ0,
* Campagne,
* Région,
* Type de sol,
* Texture de sol.

À noter que pour les campagnes, les valeurs utilisées seront les valeurs "individuelles" et non groupées (par exemple, nous n'aurons pas '2014, 2015, 2016' mais les trois valeurs séparément)

# Release and deploy in production

* Change version in ```pom.xml``` and ```package.json``` with :
  - ```$ mvn versions:set -DnewVersion=x.x.x```
  - ```$ cd src/main/webclient/dephygraph && npm version x.x.x```

* Commit modification with a nice commit message like ```Version x.x.x```
* Create a tag on last commit with name ```dephy-graph-x.x.x```
* Push commit and tag on GitLab
* Go on GitLab wait for ```dephy-graph-x.x.x``` tag pipeline to finish and then launch manual release job to create a docker release (not necessary but recommended)
* From build job of ```dephy-graph-x.x.x``` tag pipeline, go to the artifact generated page and download ```dephy-graph-x.x.x.war```
* Connect your machine to the INRAE VPN
* Copy the release in your personal folder on agrosyst-prod instance with ```$ scp dephy-graph-x.x.x.war agrosyst-prod:/home/dcosse/dephy-graph-x.x.x.war```
* Move the release file in ```/opt/docker/dephygraph-prod/receipe/``` with sudo
* Edit the file ```/opt/docker/dephygraph-prod/docker-compose.yml``` with sudo to update the version number ```x.x.x```
* Edit the file ```/opt/docker/dephygraph-prod/config/dephygraph-prod.properties``` with sudo to update the version number ```x.x.x```
* Build, update the docker instance and prune everything by running ```$ docker-compose down && docker-compose build && docker-compose up -d && docker system prune -a --volumes```

N.B.: to update DEPHYGraph Test, do the same on agrosyst-test instance

# Connect to the production and test instance

Connect to the vpn:
```$ sudo openconnect --protocol=gp acces.intranet.inra.fr -u dcosse```

```
2 gateway servers available:
gateway_pad-dc-idf (pad.dcidf.inrae.fr)
gateway_pad-dc-tls (pad.dctlse.inrae.fr)
```
Choose one of them

Then:

URL: http://dephygraph.fr
```$ ssh agrosyst-prod```

```.ssh/config
## Agryz (prod) : nécessite le VPN
Host=agrosyst-prod
Hostname=147.100.179.64
User=dcosse
```

URL: https://agrosyst.fr/dephygraph-tests/
```$ ssh agrosyst-test```

```.ssh/config
## Test (agrosyst-test) : nécessite le VPN
Host=agrosyst-test
Hostname=147.100.179.66
User=dcosse
```
